% Relationship Distribution Histogram
classdef IBSDescriptorSet < RegionrelDescriptorSet
    
properties
    % values are ordered: distance histogram, normal histogram, normal difference histogram
    val = [];
    
    verts = cell(1,0);
    
    % number of bins
    ndist = 10; % region distance histogram
    nnormalangle = 10; % normal angle histogram
    nnormaldiff = 5; % normal difference histogram (called point feature histogram in IBS paper)
end

methods(Static)
function s = defaultSettings
    s = RegionrelDescriptorSet.defaultSettings;
    % values gotten empirically from the COCO 10k dataset and the riding
    % relationship
    s.defaultdistvar = 0.0318;
    s.defaultnanglevar = 0.0333;
    s.defaultndiffvar = 0.0293;
    
    s.separationinset = 0.003;
end
end

methods

% obj = IBSDescriptorSet()
% obj = IBSDescriptorSet(obj2)
% obj = IBSDescriptorSet(ndist,nnormal,npfh)
% obj = IBSDescriptorSet(ndist,nnormal,npfh,verts,val)
% obj = IBSDescriptorSet(ndist,nnormal,npfh,sourceregions,targetregions,imgmin,imgmax,imgres)
% obj = IBSDescriptorSet(...,nvpairs)
function obj = IBSDescriptorSet(varargin)
    
    obj = obj@RegionrelDescriptorSet;
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'IBSDescriptorSet')
        obj.copyFrom(varargin{1});
    elseif numel(varargin) == 3 || (numel(varargin) >= 4 && ischar(varargin{4})) || ...
           numel(varargin) == 5 || (numel(varargin) >= 6 && ischar(varargin{6})) || ...
           numel(varargin) == 8 || (numel(varargin) >= 9 && ischar(varargin{9}))
        
        if numel(varargin) >= 8 && not(ischar(varargin{4})) && not(ischar(varargin{6}))
            obj.settings = nvpairs2struct(varargin(9:end),obj.settings);
        elseif numel(varargin) >= 5 && not(ischar(varargin{3}))
            obj.settings = nvpairs2struct(varargin(6:end),obj.settings);
        else
            obj.settings = nvpairs2struct(varargin(4:end),obj.settings);
        end
        
        obj.ndist = varargin{1};
        obj.nnormalangle = varargin{2};
        obj.nnormaldiff = varargin{3};
        
        obj.val = zeros(obj.ndist+obj.nnormalangle+obj.nnormaldiff,0);
        
        if numel(varargin) >= 8 && not(ischar(varargin{4})) && not(ischar(varargin{6}))
            % compute from image regions
            obj.compute(varargin{4:8})
        elseif numel(varargin) >= 5 && not(ischar(varargin{4}))
            % set manually
            if size(varargin{5},2) ~= numel(varargin{4}) || ...
               size(varargin{5},1) ~= obj.ndist+obj.nnormalangle+obj.nnormaldiff
                error('Invalid size for descriptor values.');
            end
            
            obj.verts = varargin{4};
            obj.val = varargin{5};
        end
    else
        error('Invalid input arguments.');
    end
end

function delete(obj)
    % do nothing
    obj.clear;
end

function clear(obj)
    obj.clear@RegionrelDescriptorSet;
    
    obj.verts = cell(1,0);
    obj.val = zeros(obj.ndist+obj.nnormalangle+obj.nnormaldiff,0);
end

function copyFrom(obj,obj2)
    obj.copyFrom@RegionrelDescriptorSet(obj2);
    
    obj.val = obj2.val;
    
    obj.verts = obj2.verts;
    
    obj.ndist = obj2.ndist;
    obj.nnormalangle = obj2.nnormalangle;
    obj.nnormaldiff = obj2.nnormaldiff;
end

function obj2 = clone(obj)
    obj2 = IBSDescriptorSet(obj);
end

function obj2 = cloneSubset(obj,ind)
    obj2 = IBSDescriptorSet;
    
    obj2.ndist = obj.ndist;
    obj2.nnormalangle = obj.nnormalangle;
    obj2.nnormaldiff = obj.nnormaldiff;
    
    obj2.settings = obj.settings;
    
    obj2.append(obj,ind);
end


function compute(obj,sourceregions,targetregions,imgmin,imgmax,imgres)
    obj.clear;
    
    if isempty(sourceregions)
        return;
    end
    
    obj.val = zeros(obj.ndist+obj.nnormalangle+obj.nnormaldiff,numel(sourceregions));
    obj.verts = cell(1,numel(sourceregions));
    for i=1:numel(sourceregions)

        if isempty(sourceregions{i})
            error('Empty source region.');
        end
        
        sourcepolys = ImageAnnotation.an2imcoords([sourceregions{i}.polygon],imgmin,imgmax);
        targetpolys = ImageAnnotation.an2imcoords([targetregions{i}.polygon],imgmin,imgmax);
        
        if isempty(sourcepolys) || isempty(targetpolys)
            obj.verts{i} = cell(1,0);
            continue; % leave at 0 value (since the values are histograms bins)
        end
        
        % compute ibs
        [ibs,segtype] = polygonIBS(sourcepolys,targetpolys);
        degeneratemask = false(1,numel(ibs));
        for j=1:numel(ibs)
            if all(ibs{j}(:,1) == ibs{j}(:,end))
                ibs{j}(:,end) = []; % remove duplicate vertex at the end
            end
            degeneratemask(j) = size(ibs{j},2) <= 1;
        end
        ibs(degeneratemask) = [];
        if isempty(ibs)
            obj.verts{i} = cell(1,0);
            continue; % leave at 0 value (since the values are histograms bins)
        end
        segclosed = segtype == 5;
        obj.verts{i} = ibs;
        
        % sample ibs
        [~,~,bbdiag] = pointsetBoundingbox([sourcepolys,targetpolys]);
        samplespacing = bbdiag .* 0.03; % sample spacing: 1% of the region bounding box diagonal
        ibssamples = cell(1,numel(ibs));
        ibssampleedgeinds = cell(1,numel(ibs));
        for j=1:numel(ibs)
            [ibssamples{j},~,~,ibssampleedgeinds{j}] = polylineResample(ibs{j},samplespacing,'spacing',segclosed(j),false);
        end
        
        
        % compute ibs features
        
        valoffset = 0;
        
        % distance from sample to source region (should be the same as
        % distance to target region)
        allibssamples = [ibssamples{:}];
 
        dists = zeros(size(allibssamples,2),numel(sourcepolys));
        samplesrcedgeind = zeros(size(allibssamples,2),numel(sourcepolys));
        for j=1:numel(sourcepolys)
            [dists(:,j),samplesrcedgeind(:,j)] = pointPolylineDistance_mex(...
                allibssamples(1,:),...
                allibssamples(2,:),...
                sourcepolys{j}(1,[1:end,1]),sourcepolys{j}(2,[1:end,1]));
        end
        [dists,samplesrcpolyind] = min(dists,[],2);
        samplesrcedgeind = samplesrcedgeind(sub2ind(size(samplesrcedgeind),(1:numel(samplesrcpolyind))',samplesrcpolyind));
        dists = dists';
        samplesrcpolyind = samplesrcpolyind';
        samplesrcedgeind = samplesrcedgeind';

        insourceregionmask = pointInPolygon_mex(allibssamples(1,:),allibssamples(2,:),sourcepolys,'winding');
        insourceregionmask = any(insourceregionmask,2);
        dists(insourceregionmask) = 0;
        
        obj.val(valoffset+1 : valoffset+obj.ndist,i) = ...
            histcounts(dists,linspace(0,0.5*bbdiag,obj.ndist+1)) ./ numel(dists);
        
        valoffset = valoffset+obj.ndist;
        
        % angle between normals on the side of the source region and up
        % ([0;1]) direction
        sourcepolynormals = cell(1,numel(sourcepolys));
        for j=1:numel(sourcepolys)
            sourcepolynormals{j} = polylineNormals(sourcepolys{j},true,false);
            if not(ispolycw(sourcepolys{j}(1,:),sourcepolys{j}(2,:)))
                sourcepolynormals{j} = sourcepolynormals{j} .* -1;
            end
        end
        srcpolyoffset = [0,cumsum(cellfun(@numel,sourcepolynormals)/2)];
        sourcepolynormals = [sourcepolynormals{:}];
        samplesrcnormalind = samplesrcedgeind+srcpolyoffset(samplesrcpolyind);
        
        sampleindoffset = [0,cumsum(cellfun(@numel,ibssampleedgeinds))];
        
        samplenormals = cell(1,numel(ibssamples));
        for j=1:numel(ibssamples)
            vertexnormals = polylineNormals(ibs{j},segclosed(j),true);
            
            sampleinds = sampleindoffset(j) + (1:size(ibssamples{j},2));
            
            v1ind = ibssampleedgeinds{j};
            
            if segclosed(j)
                v2ind = smod(ibssampleedgeinds{j}+1,1,size(ibs{j},2)+1);
            else
                v2ind = ibssampleedgeinds{j}+1;
            end
            s = ibssamples{j};
            
            f2 = sqrt(sum((s-ibs{j}(:,v1ind)).^2,1));
            f1 = sqrt(sum((s-ibs{j}(:,v2ind)).^2,1));
            fsum = (f1+f2);
            f1 = f1 ./ fsum;
            f2 = f2 ./ fsum;
            
            samplenormals{j} = bsxfun(@times,f1,vertexnormals(:,v1ind)) + bsxfun(@times,f2,vertexnormals(:,v2ind));
            
            samplesrcnormals = sourcepolynormals(:,samplesrcnormalind(sampleinds));
            wrongsidemask = dot(samplenormals{j},samplesrcnormals) > 0;
        
            if sum(wrongsidemask) > sum(not(wrongsidemask))
                samplenormals{j} = samplenormals{j}.*-1;
            end
            
            samplenormallen = sqrt(sum(samplenormals{j}.^2,1));
            mask = samplenormallen == 0; % should practically never happen
            samplenormals{j} = bsxfun(@rdivide,samplenormals{j}(:,not(mask)),samplenormallen);
        end
        
        allsamplenormals = [samplenormals{:}];
        normalangle = acos(max(-1,min(1,dot(allsamplenormals,repmat([0;1],1,size(allsamplenormals,2)) ))));
        obj.val(valoffset+1 : valoffset+obj.nnormalangle,i) = ...
            histcounts(normalangle,linspace(0,pi,obj.nnormalangle+1)) ./ numel(normalangle);

        
        valoffset = valoffset+obj.nnormalangle;
        
        % difference between all normal pairs
        normaldiff = acos(max(-1,min(1,1-pdist([samplenormals{:}]','cosine'))));
        obj.val(valoffset+1 : valoffset+obj.nnormaldiff,i) = ...
            histcounts(normaldiff,linspace(0,pi,obj.nnormaldiff+1)) ./ numel(normaldiff);
    end

end

function [d,dbin] = distance(obj,template,val,method)
    
    % inverse variance weighting
    vars = mean(bsxfun(@minus,val,mean(val,2)).^2,2);
    
    distinds = 1:obj.ndist;
    nangleinds = obj.ndist+1 : obj.ndist+obj.nnormalangle;
    ndiffinds = obj.ndist+obj.nnormalangle+1 : obj.ndist+obj.nnormalangle+obj.nnormaldiff;
    
    zerovarmask = vars == 0;
    if any(zerovarmask)
        
        distvars = vars(distinds);
        mask = distvars == 0;
        if all(mask)
            distvars(:) = obj.settings.defaultdistvar;
        elseif any(mask)
            distvars(mask) = mean(distvars(not(mask)));
        end
        vars(distinds) = distvars;
        
        nanglevars = vars(nangleinds);
        mask = nanglevars == 0;
        if all(mask)
            nanglevars(:) = obj.settings.defaultnanglevar;
        elseif any(mask)
            nanglevars(mask) = mean(nanglevars(not(mask)));
        end
        vars(nangleinds) = nanglevars;
        
        ndiffvars = vars(ndiffinds);
        mask = ndiffvars == 0;
        if all(mask)
            ndiffvars(:) = obj.settings.defaultndiffvar;
        elseif any(mask)
            ndiffvars(mask) = mean(ndiffvars(not(mask)));
        end
        vars(ndiffinds) = ndiffvars;
    end
    
    % weighting factors a,b,c as in paper
    binweights = [...
        0.5.* (1./vars(distinds) ./ sum(1./vars(distinds)));...
        0.4.* (1./vars(nangleinds) ./ sum(1./vars(nangleinds)));...
        0.1.* (1./vars(ndiffinds) ./ sum(1./vars(ndiffinds)))];

    params = cell(1,0);
    
    if not(strcmp(method,'IBSweightedL1dist'))
        error('IBS descriptor currently only supports the distance metric ''IBSweightedL1dist''.');
    end    

    d = zeros(size(template,2),size(val,2));
    for i=1:size(template,2)
        if nargout >= 2
            [d(i,:),dbin] = weighteddist(template(:,i),val,binweights,'L1dist',params{:});
        else
            d(i,:) = weighteddist(template(:,i),val,binweights,'L1dist',params{:});
        end
    end
end

function [s,sbin] = similarity(obj,template,val,method)
    
    % inverse variance weighting
    vars = mean(bsxfun(@minus,val,mean(val,2)).^2,2);
    
    distinds = 1:obj.ndist;
    nangleinds = obj.ndist+1 : obj.ndist+obj.nnormalangle;
    ndiffinds = obj.ndist+obj.nnormalangle+1 : obj.ndist+obj.nnormalangle+obj.nnormaldiff;
    
    zerovarmask = vars == 0;
    if any(zerovarmask)
        
        distvars = vars(distinds);
        mask = distvars == 0;
        if all(mask)
            distvars(:) = obj.settings.defaultdistvar;
        elseif any(mask)
            distvars(mask) = mean(distvars(not(mask)));
        end
        vars(distinds) = distvars;
        
        nanglevars = vars(nangleinds);
        mask = nanglevars == 0;
        if all(mask)
            nanglevars(:) = obj.settings.defaultnanglevar;
        elseif any(mask)
            nanglevars(mask) = mean(nanglevars(not(mask)));
        end
        vars(nangleinds) = nanglevars;
        
        ndiffvars = vars(ndiffinds);
        mask = ndiffvars == 0;
        if all(mask)
            ndiffvars(:) = obj.settings.defaultndiffvar;
        elseif any(mask)
            ndiffvars(mask) = mean(ndiffvars(not(mask)));
        end
        vars(ndiffinds) = ndiffvars;
    end
    
    % weighting factors a,b,c as in paper
    binweights = [...
        0.5.* (1./vars(distinds) ./ sum(1./vars(distinds)));...
        0.4.* (1./vars(nangleinds) ./ sum(1./vars(nangleinds)));...
        0.1.* (1./vars(ndiffinds) ./ sum(1./vars(ndiffinds)))];

    params = cell(1,0);
    
    if not(strcmp(method,'negativeIBSweightedL1dist'))
        error('IBS descriptor currently only supports the similarity metric ''negativeIBSweightedL1dist''.');
    end
    
    s = zeros(size(template,2),size(val,2));
    for i=1:size(template,2)
        if nargout >= 2
            [s(i,:),sbin] = weightedsim(template(:,i),val,binweights,'negativeL1dist',params{:});
        else
            s(i,:) = weightedsim(template(:,i),val,binweights,'negativeL1dist',params{:});
        end
    end
end

function b = iscompatible(obj,obj2)
    b = obj.ndist == obj2.ndist && ...
        obj.nnormalangle == obj2.nnormalangle && ...
        obj.nnormaldiff == obj2.nnormaldiff && ...
        isequaln(obj.settings,obj2.settings);
end

function append(obj,obj2,ind2)
    if not(iscompatible(obj,obj2))
        error('Descriptors are not compatible.');
    end
    
    if nargin < 3
        ind2 = 1:size(obj2.val,2);
    end
    
    obj.val = [obj.val,obj2.val(:,ind2)];
    obj.verts = [obj.verts,obj2.verts(:,ind2)];
end

function keepSubset(obj,ind)
    obj.val = obj.val(:,ind);
    obj.verts = obj.verts(:,ind);
end

function deleteSubset(obj,ind)
    obj.val(:,ind) = [];
    obj.verts(:,ind) = [];
end

function copySubset(obj,ind,obj2,ind2)
    if not(iscompatible(obj,obj2))
        error('Descriptors are not compatible.');
    end
    
    if nargin < 4 || isempty(ind2)
        ind2 = 1:size(obj2.val,2);
    end
    
    obj.val(:,ind) = obj2.val(:,ind2);
    obj.verts(:,ind) = obj2.verts(:,ind2);
end

end % methods

methods(Static)
    
function [angularpos,radialpos] = binaxes(nangular,nradial,rad,radlin)
    [angularpos,radialpos] = PointrelDescriptorSet.binaxes(nangular,nradial,rad,radlin);
end

function binarea = binareas(angularpos,radialpos)
    binarea = PointrelDescriptorSet.binareas(angularpos,radialpos);
    binarea = [binarea;binarea];
end

function distmethod = simmethod2distmethod(simmethod)
    if strcmp(simmethod,'negativeIBSweightedL1dist')
        distmethod = 'IBSweightedL1dist';
    else
        error('No distance method for given similarity method found.');
    end
end

function obj = saveobj(obj)
    % do nothing for now
end

function obj = loadobj(obj)
    if isstruct(obj)
        error('Could not load the object, need to implement this conversion.');
    end
end

end % methods(Static)

end % classdef
