classdef ImageDatabaseImporter < handle

properties
    imgdbinfoimporter = ImageDatabaseInfoImporter.empty;
end

methods

function obj = ImageDatabaseImporter
    if nargin == 0
        % do nothing
    else
        error('Invalid arguments.');
    end
end

function clear(obj)
    delete(obj.imgdbinfoimporter(isvalid(obj.imgdbinfoimporter)));
    obj.imgdbinfoimporter = ImageDatabaseInfoImporter.empty;
end
    
function [dbinfo,dbinfofilenames,imgpath,annopath,wsfilename,tsfilename,descfname] = import(obj,filename)
    
    if isempty(filename)
        error('No file name given.')
    end
    
    if not(ischar(filename))
        error('File name must be a string.');
    end
    
    obj.clear;
    
    [dbinfofilenames,imgpath,annopath,wsfilename,tsfilename,descfname] = obj.importDatabase(filename);
    
    obj.imgdbinfoimporter = ImageDatabaseInfoImporter;
    dbinfo = obj.imgdbinfoimporter.import(dbinfofilenames);
end

end

methods(Static)

function [dbinfo,dbinfofilenames,imgpath,annopath,wsfilename,tsfilename,descfname] = importImageDatabase(filename,varargin)
    importer = ImageDatabaseImporter;
    [dbinfo,dbinfofilenames,imgpath,annopath,wsfilename,tsfilename,descfname] = importer.import(filename,varargin{:});
    delete(importer(isvalid(importer)));
end

function [dbinfofilenames,imgpath,annopath,wsfilename,tsfilename,descfname] = importDatabase(filename)
    
    dbinfofilenames = struct;
    imgpath = [];
    annopath = [];
    wsfilename = [];
    tsfilename = [];
    descfname = [];
    
    fid = fopen(filename,'r');
    if fid < 0
        error(['Cannot open file ''', filename,'''.']);
    end
    
    try
        
        linestr = fgetl(fid);
        while ischar(linestr)
            
            tokens = strsplit(linestr,':');
            if numel(tokens) ~= 2
                error('Invalid line, must contain exactly one '':'' (file names may not contain this character).');
            end
            
            fieldname = strtrim(tokens{1});
            value = strtrim(tokens{2});
            
            if strcmp(fieldname,'dbinfo')
                dbinfofilenames.dbinfo = cleanfilename(value);
            elseif strcmp(fieldname,'categories')
                dbinfofilenames.categories = cleanfilename(value);
            elseif strcmp(fieldname,'relcategories')
                dbinfofilenames.relcategories = cleanfilename(value);
            elseif strcmp(fieldname,'categorycolors')
                dbinfofilenames.categorycolors = cleanfilename(value);
            elseif strcmp(fieldname,'imagepath')
                imgpath = cleanfilename(value);
            elseif strcmp(fieldname,'annotationpath')
                annopath = cleanfilename(value);
            elseif strcmp(fieldname,'workingset')
                wsfilename = cleanfilename(value);
            elseif strcmp(fieldname,'trainingset')
                wsfilename = cleanfilename(value);
            elseif strcmp(fieldname,'descriptors')
                descfname = cleanfilename(value);
            else
                error(['Unknown field name ''',fieldname,'''.']);
            end
            
            linestr = fgetl(fid);
        end
        
    catch err
        fclose(fid);
        rethrow(err);
    end
    
    fclose(fid);
    
    if not(ischar(wsfilename))
        wsfilename = '';
    end
    
    if not(ischar(tsfilename))
        tsfilename = '';
    end
    
    if not(ischar(descfname))
        descfname = '';
    end
    
    if not(isstruct(dbinfofilenames)) || ...
       not(isfield(dbinfofilenames,'dbinfo')) || ...
       not(isfield(dbinfofilenames,'categories')) || ...
       not(isfield(dbinfofilenames,'relcategories')) || ...
       not(isfield(dbinfofilenames,'categorycolors'))
    
        error('Some database info file names are missing.');
            
    end
    
    if not(ischar(imgpath)) || ...
       not(ischar(annopath)) || ...
       not(ischar(wsfilename)) || ...
       not(ischar(tsfilename)) || ...
       not(ischar(descfname))
   
        error('Some database file names or paths are missing.');
        
    end
end

end

end
