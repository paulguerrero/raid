function initImgstructure

    addpathsImgstructure;
    
    ok = true;
    rtimes = cell(1,0);

    [moduleok,modulertimes] = checkPrecompiledInfo('mlutil/precompiled.conf');
    ok = ok & moduleok;
    rtimes = [rtimes,modulertimes'];
    [moduleok,modulertimes] = checkPrecompiledInfo('mlboost/precompiled.conf');
    ok = ok & moduleok;
    rtimes = [rtimes,modulertimes'];
    [moduleok,modulertimes] = checkPrecompiledInfo('mlgeometry/precompiled.conf');
    ok = ok & moduleok;
    rtimes = [rtimes,modulertimes'];
    [moduleok,modulertimes] = checkPrecompiledInfo('precompiled.conf');
    ok = ok & moduleok;
    rtimes = [rtimes,modulertimes'];
    
    rtimes = unique(rtimes);
    
    envarch = computer('arch');
    cc = mex.getCompilerConfigurations('C++','Installed');
    envrtimes = {cc.ShortName};
    
    if not(ok)
        disp(['Need to compile for your architecture ',envarch,' ...']);
        prompt = 'Do you want recompile now? Y/N [Y]: ';
        str = input(prompt,'s');
        if isempty(str)
            str = 'Y';
        end
        if strcmp(str,'Y')
            compileImgstructure;
        else
            return;
        end
    end
    
    hasrtime = ismember(rtimes,envrtimes);
    if not(all(hasrtime))
        missingrtimes = rtimes(not(hasrtime));
        missingrtimes = missingrtimes(:);
        missingrtimes = [missingrtimes,repmat(' ',numel(missingrtimes),1)];
        warning([...
            'Precompiled libraries were compiled with compiler configurations that are not available on this machine. ',...
            'If you have not done so already, you may need to install the runtimes: ',[missingrtimes{:}],', ',...
            'or recompile with your installed runtime (run ''compileImgstructure'' to do that).']);
    end
    
    % check java heap memory
    if java.lang.Runtime.getRuntime.maxMemory < 1e09
        warning(['Java heap memory is set to under a GB. ',...
                 'User interface components use heap memory, ',...
                 'so it is recommended to increase to at least a GB ',...
                 'when displaying complex scenes in the editor. ',...
                 '(Preferences -> Matlab -> General -> Java Heap Memory)']);
    end

end
