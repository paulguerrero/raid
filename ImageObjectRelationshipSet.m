classdef ImageObjectRelationshipSet < handle
    
properties
    id = zeros(1,0);
    sourceobjs = cell(1,0);
    targetobjs = cell(1,0);
    labels = cell(1,0);
    labelweights = cell(1,0);
end

methods

% obj = ImageObjectRelationshipSet()
% obj = ImageObjectRelationshipSet(obj2)
% obj = ImageObjectRelationshipSet(id,sourceobjs,targetobjs,labels)
% obj = ImageObjectRelationshipSet(id,sourceobjs,targetobjs,labels,labelweights)
function obj = ImageObjectRelationshipSet(varargin)
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'ImageObjectRelationshipSet')
        obj.copyFrom(varargin{1});
    elseif numel(varargin) == 4 || numel(varargin) == 5
        obj.addRelationships(varargin{:});
    else
        error('Invalid arguments.');
    end
end

function copyFrom(obj,obj2)
    obj.id = obj2.id;
    obj.labels = obj2.labels;
    obj.sourceobjs = obj2.sourceobjs;
    obj.targetobjs = obj2.targetobjs;
    
    obj.labelweights = obj2.labelweights;
end

function obj2 = clone(obj)
    obj2 = ImageObjectRelationshipSet(obj);
end

function setRelationships(obj,id,label,sourceobjs,taregtobjs,varargin)
    obj.removeRelationships(1:numel(obj.id));
    obj.addRelationships(id,label,sourceobjs,taregtobjs,varargin{:});
end

function addRelationships(obj,id,sourceobjs,targetobjs,labels,labelweights)
    
    % validate relationships
    if numel(unique(id))~=numel(id) || any(ismember(id,obj.id))
        error('Some relationship ids are duplicate or already present in the set.');
    end
    if any(id <= 0)
        error('Relationship IDs must be >= 1.');
    end
    if not(iscell(sourceobjs))
        if numel(id) == 1
            sourceobjs = {sourceobjs};
        else
            error('Source objects must be cell arrays');
        end
    end
    if not(iscell(targetobjs))
        if numel(id) == 1
            targetobjs = {targetobjs};
        else
            error('Target objects must be cell arrays');
        end
    end
    if not(iscell(labels))
        if numel(id) == 1
            labels = {labels};
        else
            error('Labels must be cell arrays');
        end
    end
    if any(not(obj.findRelationships(sourceobjs,targetobjs,'exact')==0))
        error('Some relationships already exist.');
    end
    for i=1:numel(labels)
        if numel(unique(labels{i})) ~= numel(labels{i})
            error('Duplicate labels.');
        end
    end
    if nargin < 6 || isempty(labelweights)
        labelweights = cell(size(labels));
        for i=1:numel(labelweights)
            labelweights{i} = ones(size(labels{i}));
        end
    else
        if not(iscell(labelweights))
            if numel(id) == 1
                labelweights = {labelweights};
            else
                error('Label weights must be cell arrays');
            end
        end
        for i=1:numel(labelweights)
            if any(labelweights{i} < 0) || any(labelweights{i} > 1)
                error('Relationship weights must be in [0,1].');
            end
        end
    end
    
    % add relationships
    newinds = numel(obj.id)+1:numel(obj.id)+numel(id);
    obj.id(:,newinds) = id;
    obj.sourceobjs(:,newinds) = sourceobjs;
    obj.targetobjs(:,newinds) = targetobjs;
    obj.labels(:,newinds) = labels;
    obj.labelweights(:,newinds) = labelweights;
end

function removeRelationships(obj,inds)
    obj.id(:,inds) = [];
    obj.sourceobjs(:,inds) = [];
    obj.targetobjs(:,inds) = [];
    obj.labels(:,inds) = [];
    obj.labelweights(:,inds) = [];
end

function lblinds = addLabels(obj,relind,lbls,lblweights)
    if nargin < 4 || isempty(lblweights)
        lblweights = ones(size(lbls));
    end
    
    if numel(lbls) ~= numel(unique(lbls))
        error('Duplicate labels');
    end
    
    % update existing weights of existing labels
    [existmask,existind] = ismember(lbls,obj.labels{relind});
    obj.labelweights{relind}(existind(existmask)) = lblweights(existmask);
    
    lblinds = [existind(existmask),numel(obj.labels{relind})+1:sum(not(existmask))];
    
    % add new labels
    obj.labels{relind} = [obj.labels{relind},lbls(not(existmask))];
    obj.labelweights{relind} = [obj.labelweights{relind},lblweights(not(existmask))];
end

function removeLabels(obj,relind,lblinds)
    obj.labels{relind}(lblinds) = [];
    obj.labelweights{relind}(lblinds) = [];
end

function inds = findRelationships(obj,sourceobjs,targetobjs,findmode)
    
    if nargin < 4 || isempty(findmode)
        findmode = 'exact';
    end
    
    if strcmp(findmode,'exact')
        
        bothids = cell(1,numel(sourceobjs));
        for i=1:numel(sourceobjs)
            bothids{i} = [-[sourceobjs{i}.id]-10,[targetobjs{i}.id]+10]; % just to avoid cases where the id is -1 or 0
        end

        setbothids = cell(1,numel(obj.sourceobjs));
        for i=1:numel(obj.sourceobjs)
            setbothids{i} = [-[obj.sourceobjs{i}.id]-10,[obj.targetobjs{i}.id]+10];
        end

        [~,inds] = cellismember(bothids,setbothids,false);
    
    elseif strcmp(findmode,'contain_all')
        inds = zeros(1,0);
        for i=1:numel(obj.sourceobjs)
            if all(ismember(sourceobjs,obj.sourceobjs{i})) && ...
                    all(ismember(targetobjs,obj.targetobjs{i}))
                inds(:,end+1) = i; %#ok<AGROW>
            end
        end
        
    elseif strcmp(findmode,'contain_any')
        inds = zeros(1,0);
        for i=1:numel(obj.sourceobjs)
            if any(ismember(sourceobjs,obj.sourceobjs{i})) || ...
                    any(ismember(targetobjs,obj.targetobjs{i}))
                inds(:,end+1) = i; %#ok<AGROW>
            end
        end
    else
        error('Unknown find mode.');
    end
end

end

end
