classdef AnnotationImporter < handle

properties(SetAccess=protected)
    filename = ''; % filename that was last imported from
end

methods

function  obj = AnnotationImporter
    if nargin == 0
        % do nothing
    else
        error('Invalid arguments.');
    end
end

function clear(obj)
    obj.filename = '';
end

function annotation = import(obj, filename, format, varargin)
    obj.clear;
    
    obj.filename = filename;
    
    if nargin < 3 || isempty(format)
        % get format from file extension
        [~,~,ext] = fileparts(obj.filename);
        if strcmp(ext,'.xml')
            format = 'xml';
        else
            error('Unkown file extension for importing annotation.');
        end
    end
    
    if strcmp(format,'xml')
        annotation = obj.readxmlannotation;
    elseif strcmp(format,'svg')
        if numel(varargin) < 1
            error('Must pass category colors.');
        end
        annotation = obj.readsvgannotation(varargin{1});
    else
        error('Unrecognized format for importing annotation.');    
    end
end

end % methods

methods(Access=protected)

function annotation = readxmlannotation(obj)
    % replace dtd file on the web with local ones
%     fp = fopen(obj.filename,'rt');
%     filestr = fread(fp,inf,'*char')';
%     fclose(fp);
%     filestr = regexprep(filestr, '<!DOCTYPE [^>]*>','','once','ignorecase');

%     xmldoc = xmlreadstring(filestr);
    
    xmldoc = xmlread(obj.filename);
    
    annotationnode = getDOMChildElements(xmldoc.getDocumentElement,'annotation',1);
    
    annotation = obj.readImageAnnotationNode(annotationnode,[]);
end

function annotation = readImageAnnotationNode(obj,node,annotation)

    if isempty(annotation)
        annotation = ImageAnnotation;
    end
    
    objectnodelist = getDOMChildElements(node,'object');
    imgobjects = ImageObject.empty(1,0);
    for i=1:numel(objectnodelist)
        imgobjects(:,end+1) = obj.readImageObjectNode(objectnodelist(i),[]); %#ok<AGROW>
    end
    annotation.setImgobjects(imgobjects);
    
    imgobjids = [annotation.imgobjects.id];
    relationshipnodelist = getDOMChildElements(node,'relationship');
    relid = zeros(1,numel(relationshipnodelist));
    relsourceobjs = cell(1,numel(relationshipnodelist));
    reltargetobjs = cell(1,numel(relationshipnodelist));
    rellabels = cell(1,numel(relationshipnodelist));
    rellabelweights = cell(1,numel(relationshipnodelist));
    relok = false(1,numel(relationshipnodelist));
    for i=1:numel(relationshipnodelist)
        relnode = relationshipnodelist(i);
        relid(i) = sscanf(char(relnode.getAttribute('id')),'%d');
        
        relsrcids = reshape(sscanf(char(relnode.getAttribute('sourceids')),'%d,'),1,[]);
        [sm,sobjinds] = ismember(relsrcids,imgobjids);
        reltgtids = reshape(sscanf(char(relnode.getAttribute('targetids')),'%d,'),1,[]);
        [tm,tobjinds] = ismember(reltgtids,imgobjids);
        if not(all(sm)) || not(all(tm))
            warning('Skipping invalid relationship.');
            continue;
        end
        relsourceobjs{i} = annotation.imgobjects(sobjinds);
        reltargetobjs{i} = annotation.imgobjects(tobjinds);

        rellabels{i} = reshape(sscanf(char(relnode.getAttribute('labels')),'%d,'),1,[]);
        rellabelweights{i} = reshape(sscanf(char(relnode.getAttribute('labelweights')),'%f,'),1,[]);
        relok(i) = true;
    end
    annotation.relationships.addRelationships(...
        relid(relok),relsourceobjs(relok),reltargetobjs(relok),rellabels(relok),rellabelweights(relok));
    
    if node.hasAttribute('lblmask') ~= node.hasAttribute('objmask')
        error('Invalid annotation node, only one of label or object mask is provided.');
    end
    
    if node.hasAttribute('lblmask') && node.hasAttribute('objmask')
        lblmaskfilename = char(node.getAttribute('lblmask'));
        objmaskfilename = char(node.getAttribute('objmask'));
        
        if isempty(lblmaskfilename) || isempty(objmaskfilename)
            error('Invalid annotation node, empty label or object mask file names.');
        end
        
        lblmask = imread(lblmaskfilename);
        if size(lblmask,3) ~= 1
            error('Invalid label mask file, must contain one index channel.');
        end
        lblmask = flipud(double(lblmask));
        
        objmask = imread(objmaskfilename);
        if size(objmask,3) ~= 1
            error('Invalid object mask file, must contain one index channel.');
        end
        objmask = flipud(double(objmask));
        
        if any(size(lblmask) ~= size(objmask))
            error('Label and object mask do not have the same size.');
        end
        
        annotation.setMasks(lblmask,objmask);
    end
end

function imgobj = readImageObjectNode(obj,node,imgobj)
    
    if isempty(imgobj)
        imgobj = ImageObject;
    end
    
    imgobj.setId(sscanf(char(node.getAttribute('id')),'%d'));
    imgobj.setLabel(sscanf(char(node.getAttribute('label')),'%d'));
    
    polygonnodelist = getDOMChildElements(node,'polygon');
    polygon = cell(1,0);
    for i=1:numel(polygonnodelist)
        polygon{:,end+1} = obj.readPolygonNode(polygonnodelist(i),[]); %#ok<AGROW>
    end
    imgobj.setPolygon(polygon);
    
    imgobj.setIscrowd(logical(sscanf(char(node.getAttribute('iscrowd')),'%d')));
    
    imgobj.update;
end

function polygon = readPolygonNode(obj,node,polygon) %#ok<INUSL>
    if isempty(polygon)
        polygon = zeros(2,0);
    end
    
    polystr = char(node.getAttribute('verts'));
    polygon = [polygon,reshape(sscanf(polystr,'%f,'),2,[])];
end

function annotation = readsvgannotation(obj,catcolors)
    [type,coords,~,attr,canvasmin,canvasmax] = loadSvg(obj.filename);
    
    if isempty(canvasmax) || isempty(canvasmin)
        error('Cannot determine image size from svg file.');
    end
    
%     [imgmin,imgmax] = ImageAnnotation.imres2imsize(cavasmax-cavasmin);
    
    % close polylines where startpoint = endpoint
    for i=1:numel(type)
        if not(strcmp(type{i},'polygon'))
            if all(coords{i}(:,1) == coords{i}(:,end))
                coords{i}(:,end) = [];
                type{i} = 'polygon';
            end
        end
    end

    if not(all(strcmp(type,'polygon')))
        error('Some polygons are not closed in the annotation.');
    end
    
    % create image objects
    annotation = ImageAnnotation;
    for i=1:numel(coords)
        id = i;
        if not(attr{i}.isKey('fill'))
            error('Some polygons have no fill, cannot determine the label.');
        end
        fill_str = attr{i}('fill');
        
        rgbclr = rgbhex2rgbdec(fill_str,true);
        clrdist = sum(bsxfun(@minus,rgbclr,catcolors).^2,2);
        [~,lbl] = min(clrdist);
%         lbl = catcolors(clrind,1);
        
        % flip y
        coords{i}(2,:) = canvasmax(2) - (coords{i}(2,:) - canvasmin(2));
        coords{i} = ImageAnnotation.im2ancoords(coords{i},canvasmin,canvasmax);
        
        annotation.addImageobjects(ImageObject(id,lbl,coords(i),false));
    end
end

end % methods(Access=protected)

methods(Static)

function annotation = importAnnotation(filename,varargin)
    importer = AnnotationImporter;
    annotation = importer.import(filename,varargin{:});
    delete(importer(isvalid(importer)));
end

end % methods(Static)

end
