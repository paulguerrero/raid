classdef AnnotationExporter < handle
    
properties(SetAccess=protected)
    annotation = ImageAnnotation.empty(1,0);
    filename = ''; % last filename that was exported to
end

methods
    
function obj = AnnotationExporter(annotation)
    if nargin == 0
        % do nothing
    elseif nargin == 1
        obj.annotation = annotation;
        
%         if ischar(categories)
%             % from filename
%             obj.categories = SceneImporter.importCategories(categories);
%         elseif iscell(categories)
%             obj.categories = categories;
%         else
%             error('Invalid format for categories.');
%         end
    else
        error('Invalid arguments.');
    end
end

function clear(obj)
    obj.filename = '';
end

% export(obj,filename,format,categories)
% export(obj,filename,format,categoryfilename)
function export(obj,filename,format,varargin)
    
    if isempty(obj.annotation)
        error('No annotation given.');
    end
    
    if isempty(filename)
        error('Empty filename given.')
    end
    
    obj.clear;
    
    obj.filename = filename;
    
    if abs(nargin) < 3 || isempty(format)
        [~,~,ext] = fileparts(obj.filename);
        if strcmp(ext,'.xml')
            format = 'xml';
        else
            error('Unknown file extension for exporting annotation.');
        end
    end
    
    if strcmp(format,'xml')
        obj.writexmlannotation;
    else
        error('Unknown annotation export format.');
    end
end

end % methods

methods(Access=protected)

function writexmlannotation(obj)
    xmldoc = com.mathworks.xml.XMLUtils.createDocument('xml');
    xmldoc.setDocumentURI(['file:/',obj.filename]);
    
    obj.writeImageAnnotationNode(obj.annotation,[],xmldoc.getDocumentElement);
    
    xmlwrite(obj.filename,xmldoc);
end

function node = writeImageAnnotationNode(obj,annotation,node,parentnode)
    if isempty(node) && not(isempty(parentnode))
        node = parentnode.getOwnerDocument.createElement('annotation');
        parentnode.appendChild(node);
    end
    
    for i=1:numel(annotation.imgobjects)
        obj.writeImageObjectNode(annotation.imgobjects(i),[],node);
    end
    
    for i=1:size(annotation.relationships.id,2)
        relnode = node.getOwnerDocument.createElement('relationship');
        node.appendChild(relnode);
        
        relnode.setAttribute('id',sprintf('%d',annotation.relationships.id(i)));
        relnode.setAttribute('sourceids',sprintf('%d,',[annotation.relationships.sourceobjs{i}.id]));
        relnode.setAttribute('targetids',sprintf('%d,',[annotation.relationships.targetobjs{i}.id]));
        relnode.setAttribute('labels',sprintf('%d,',annotation.relationships.labels{i}));
        relnode.setAttribute('labelweights',sprintf('%.30g,',annotation.relationships.labelweights{i}));
    end
end

function node = writeImageObjectNode(obj,imgobj,node,parentnode)
    if isempty(node) && not(isempty(parentnode))
        node = parentnode.getOwnerDocument.createElement('object');
        parentnode.appendChild(node);
    end
    
    node.setAttribute('id',sprintf('%d',imgobj.id));
    node.setAttribute('label',sprintf('%d',imgobj.label));
    for i=1:numel(imgobj.polygon)
        obj.writePolygonNode(imgobj.polygon{i},[],node);
    end
    node.setAttribute('iscrowd',sprintf('%d',imgobj.iscrowd));
    node.setAttribute('saliency',sprintf('%d',imgobj.saliency));
end

function node = writePolygonNode(obj,polygon,node,parentnode) %#ok<INUSL>
    if isempty(node) && not(isempty(parentnode))
        node = parentnode.getOwnerDocument.createElement('polygon');
        parentnode.appendChild(node);
    end
    
    node.setAttribute('verts',sprintf('%.30g,',polygon));
end

end % methods(Access=protected)

methods(Static)

function exportAnnotation(annotation,filename,varargin)
    exporter = AnnotationExporter(annotation);
    exporter.export(filename,varargin{:});
    delete(exporter(isvalid(exporter)));
end

% % catconversions are nx2 cell arrays
% function categorycolors = importCategorycolors(filename)
%     
%     % open xml file
% %     fp = fopen(filename,'rt');
% %     filestr = fread(fp,inf,'*char')';
% %     fclose(fp);
% %     filestr = regexprep(filestr, '<!DOCTYPE [^>]*>','','once','ignorecase');
% %     
% %     xmldoc = xmlreadstring(filestr);
% 
%     xmldoc = xmlread(filename);
%     xmlnode = xmldoc.getDocumentElement;
%     
%     % read categories
%     catcolornodelist = getDOMChildElements(xmlnode,'categorycolor');
%     categorycolors = zeros(numel(catcolornodelist),4);
%     for i=1:numel(catcolornodelist)
%         categorycolors(i,1) = sscanf(char(catcolornodelist(i).getAttribute('id')),'%d');
%         categorycolors(i,2:4) = rgbhex2rgbdec(char(catcolornodelist(i).getAttribute('color')));
%     end
%     
%     % sort categories by id and make sure all ids are accounted for
%     [ids,perm] = sort(categoryids,'ascend');
%     if any(ids ~= (1:numel(ids))')
%         error('Some category colors are missing.');
%     end
%     categorycolors = categorycolors(perm,:);
% end

end % methods(Static)

end
