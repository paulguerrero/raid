classdef ImageDatabaseInfoExporter < handle

properties
   imgdbinfo = ImageDatabaseInfo.empty;
end

methods

function obj = ImageDatabaseInfoExporter(imgdbinfo)
    if nargin == 0
        % do nothing
    elseif nargin == 1
        obj.imgdbinfo = imgdbinfo;
    else
        error('Invalid arguments.');
    end
end

function clear(obj) %#ok<MANU>
    % do nothing
end
    
function export(obj,filenames)
    if isempty(obj.imgdbinfo)
        error('No database info given.');
    end
    
    if isempty(filenames)
        error('No filenames given.')
    end
    
    if ischar(filenames)
        dbpath = filenames;
        filenames = struct;
        filenames.dbinfo = fullfile(dbpath,'dbinfo.txt');
        filenames.categories = fullfile(dbpath,'categories.xml');
        filenames.relcategories = fullfile(dbpath,'relcategories.xml');
        filenames.categorycolors = fullfile(dbpath,'categorycolors.txt');
    end
    
    if not(all(isfield(filenames,{'dbinfo','categories','relcategories','categorycolors'})))
        error('Some filenames are missing.');
    end
    
    obj.clear;
    
    obj.exportDbinfo(...
        obj.imgdbinfo.imgfilenames,...
        obj.imgdbinfo.imghasrels,...
        filenames.dbinfo);
    
    obj.exportCategories(...
        obj.imgdbinfo.categories,...
        filenames.categories);
    
    obj.exportRelcategories(...
        obj.imgdbinfo.relcategories,...
        filenames.relcategories);
    
    obj.exportCategorycolors(...
        obj.imgdbinfo.categorycolors,...
        filenames.categorycolors);
end
    
end

methods(Static)

function exportImageDatabaseInfo(imgdbinfo,filenames,varargin)
    exporter = ImageDatabaseInfoExporter(imgdbinfo);
    exporter.export(filenames,varargin{:});
    delete(exporter(isvalid(exporter)));
end

function exportDbinfo(imgfilenames,hasrels,filename)
    hasrels = array2strings(hasrels,'%d');
   
    sep = repmat({','},1,numel(imgfilenames));
    nl = repmat({sprintf('\n')},1,numel(imgfilenames));
    str = [imgfilenames;sep;hasrels';nl];
    str = [str{:}];

    fid = fopen(filename,'W');
    fwrite(fid,str);
    fclose(fid);
end

% return database indices of the updated entries
function dbind = updateDbinfo(imgfilenames,hasrels,filename,allownew)
    
    if not(iscell(imgfilenames))
        if numel(hasrels) == 1
            imgfilenames = {imgfilenames};
        else
            error('Image file names must be given as cell array.');
        end
    end
    
    if nargin < 4 || isempty(allownew)
        allownew = false;
    end
    
    if exist(filename,'file')
        [dbimgfilenames,dbhasrels] = ImageDatabaseInfoImporter.importDbinfo(filename);
        [indb,dbind] = ismember(imgfilenames,dbimgfilenames);
    else
        dbimgfilenames = cell(1,0);
        dbhasrels = false(1,0);
        indb = false(1,numel(imgfilenames));
        dbind = zeros(1,0);
    end
    
    if not(allownew) && not(all(indb))
        error('Some image files were not found in the database.');
    end
    
    dbhasrels(dbind(indb)) = hasrels(indb);
    
    dbind = dbind(indb);
    
    if allownew
        dbind = [dbind,numel(dbimgfilenames)+1:numel(dbimgfilenames)+sum(not(indb))];
        dbimgfilenames = [dbimgfilenames,imgfilenames(not(indb))];
        dbhasrels = [dbhasrels,hasrels(not(indb))];
    end
    
    ImageDatabaseInfoExporter.exportDbinfo(dbimgfilenames,dbhasrels,filename);
end

function exportWorkingset(imgfilenames,filename)
    nl = repmat({sprintf('\n')},1,numel(imgfilenames));
    str = [imgfilenames;nl];
    str = [str{:}];
    
    fid = fopen(filename,'W');
    fwrite(fid,str);
    fclose(fid);
end

function exportCategories(categories,filename)
    
%     nl = repmat({sprintf('\n')},1,numel(imgfilenames));
%     str = [categories';nl];
%     str = [str{:}];
% 
%     fid = fopen(filename,'W');
%     fwrite(fid,str);
%     fclose(fid);
    
    xmldoc = com.mathworks.xml.XMLUtils.createDocument('xml');
    xmldoc.setDocumentURI(['file:/',filename]);
    xmlnode = xmldoc.getDocumentElement;

    for i=1:numel(categories)
        node = xmldoc.createElement('category');
        xmlnode.appendChild(node);
        
        node.setAttribute('name',categories{i});
        node.setAttribute('id',sprintf('%d',i));
    end
    
    xmlwrite(filename,xmldoc);
end

function exportRelcategories(categories,filename)
    
%     nl = repmat({sprintf('\n')},1,numel(imgfilenames));
%     str = [categories';nl];
%     str = [str{:}];
% 
%     fid = fopen(filename,'W');
%     fwrite(fid,str);
%     fclose(fid);
    
    xmldoc = com.mathworks.xml.XMLUtils.createDocument('xml');
    xmldoc.setDocumentURI(['file:/',filename]);
    xmlnode = xmldoc.getDocumentElement;

    for i=1:numel(categories)
        node = xmldoc.createElement('relcategory');
        xmlnode.appendChild(node);
        
        node.setAttribute('name',categories{i});
        node.setAttribute('id',sprintf('%d',i));
    end
    
    xmlwrite(filename,xmldoc);
end

% catcolors must be a nx3 array of [r,g,b]
function exportCategorycolors(catcolors,filename)
    hexcolors = rgbdec2rgbhex(catcolors,true);
    
    nl = repmat(sprintf('\n'),1,size(hexcolors,1));
    str = [hexcolors';nl];
    str = str(:)';

    fid = fopen(filename,'W');
    fwrite(fid,str);
    fclose(fid);
end

% catconversions must be nx2 cell arrays
function exportCategoryconversions(catconversions,filename)
    xmldoc = com.mathworks.xml.XMLUtils.createDocument('xml');
    xmldoc.setDocumentURI(['file:/',filename]);
    xmlnode = xmldoc.getDocumentElement;
    
    for i=1:size(catconversions,1)
        node = xmldoc.createElement('conversion');
        xmlnode.appendChild(node);
        
        node.setAttribute('from',catconversions{i,1});
        node.setAttribute('to',catconversions{i,2});
        node.setAttribute('id',sprintf('%d',i)); % to mantain the same order
    end
    
    xmlwrite(filename,xmldoc);
end

end

end
