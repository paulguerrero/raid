% one closed value can be specified per component one globally
function gobjs = showPolylines(polylines,gobjs,parent,varargin)
    
    % merge all components of a polyline into a single component,
    % seprated by nans (a single patch and its components can
    % only have faces with the same number of vertices)

    % all properties that can be given per component or per vertex must
    % be checked here
    
    if abs(nargin) < 2
        gobjs = gobjects(1,0);
    end
    if abs(nargin) < 3
        parent = gobjects(1,0);
    end
    
    if not(iscell(polylines))
        polylines = {polylines};
    end
    
    
    options = nvpairs2struct(varargin);
    optioninds = nvpairs2struct(varargin,[],true);
    
    if isfield(options,'vnormals')
        hasvnormals = true;
    else
        hasvnormals = false;
    end

    if isfield(options,'fvcolor')
        hasfvcolor = true;
    else
        hasfvcolor = false;
    end

    if isfield(options,'fvalpha')
        hasfvalpha = true;
    else
        hasfvalpha = false;
    end
    
    if isfield(options,'closed')
        closed = options.closed;
        varargin(optioninds.closed-1:optioninds.closed) = [];
    else
        closed = false;
    end
    
    if numel(closed) == 1
        closed = repmat(closed,1,numel(polylines));
    end


    for i=1:numel(polylines)
        if iscell(polylines{i}) && numel(polylines{i}) > 1
            % more than one component, needs to be merged

            if hasvnormals && iscell(options.vnormals)
                if iscell(options.vnormals{i}) % one normal per vertex
                    options.vnormals{i} = [options.vnormals{i};repmat({zeros(size(polylines{i}{1},1),1)},1,numel(polylines{i}))]; % last nan is necessary, otherwise nans don't work
                    options.vnormals{i} = [options.vnormals{i}{:}];
                elseif size(options.vnormals{i},2) > 1 % one normal per component => convert to normal per vertex
                    vn = zeros(size(polylines{i}{1},1),0);
                    for j=1:size(options.vnormals{i},2)
                        vn = [vn,repmat(options.vnormals{i}(:,j),1,size(polylines{i}{j},2)+1)]; %#ok<AGROW> % +1 for nan
                    end
                    options.vnormals{i} = vn;
                end
            end

            if hasfvcolor && iscell(options.fvcolor)
                if iscell(options.fvcolor{i}) % one color per vertex
                    options.fvcolor{i} = [options.fvcolor{i};repmat({zeros(3,1)},1,numel(polylines{i}))];
                    options.fvcolor{i} = [options.fvcolor{i}{:}];
                elseif size(options.fvcolor{i},2) > 1 % one color per component => convert to color per vertex
                    fvc = zeros(3,0);
                    for j=1:size(options.fvcolor{i},2)
                        fvc = [fvc,repmat(options.fvcolor{i}(:,j),1,size(polylines{i}{j},2)+1)]; %#ok<AGROW> % +1 for nan
                    end
                    options.fvcolor{i} = fvc;
                end
            end

            if hasfvalpha && iscell(options.fvalpha)
                if iscell(options.fvalpha{i}) % one alpha value per vertex
                    options.fvalpha{i} = [options.fvalpha{i};repmat({zeros(1,1)},1,numel(polylines{i}))]; % last nan is necessary, otherwise nans don't work
                    options.fvalpha{i} = [options.fvalpha{i}{:}];
                elseif size(options.fvalpha{i},2) > 1 % one alpha value per component => convert to alpha value per vertex
                    fva = zeros(1,0);
                    for j=1:size(options.fvalpha{i},2)
                        fva = [fva,repmat(options.fvalpha{i}(:,j),1,size(polylines{i}{j},2)+1)]; %#ok<AGROW> % +1 for nan
                    end
                    options.fvalpha{i} = fva;
                end
            end

            polylines{i} = [polylines{i};repmat({nan(size(polylines{i}{1},1),1)},1,numel(polylines{i}))]; % last nan is necessary, otherwise nans don't work
            polylines{i} = [polylines{i}{:}];

        end

        if not(closed(i))
            if hasvnormals
                options.vnormals{i}(:,end+1) = nan;
            end
            if hasfvcolor
                options.fvcolor{i}(:,end+1) = nan;
            end
            if hasfvalpha
                options.fvalpha{i}(:,end+1) = nan;
            end

            polylines{i}(:,end+1) = nan;
        end
    end
    
    if hasvnormals
        varargin(optioninds.vnormals) = options.vnormals;
    end
    if hasfvcolor
        varargin(optioninds.fvcolor) = options.fvcolor;
    end
    if hasfvalpha
        varargin(optioninds.fvalpha) = options.fvalpha;
    end
    
    faces = cell(1,numel(polylines));
    for i=1:numel(polylines)
        if iscell(polylines{i})
            faces{i} = cell(1,numel(polylines{i}));
            for j=1:numel(polylines{i})
                faces{i}{j} = (1:size(polylines{i}{j},2))';
            end
        else
            faces{i} = (1:size(polylines{i},2))';
        end
    end
    
    gobjs = showPatches(polylines,faces,gobjs,parent,varargin{:});
end
