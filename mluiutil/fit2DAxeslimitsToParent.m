function fit2DAxeslimitsToParent(ax)
    parent = ax.Parent;
    parentunits = parent.Units;
    parent.Units = points;
    parentpos = parent.Position;
    parent.Units = parentunits;
    
    parentaspect = parentpos(3) / parentpos(4);

    camtarget = ax.CameraTarget';
    campos = ax.CameraPosition';

    camtargetdist = sqrt(sum((camtarget-campos).^2,1));

    minextent = 2 * camtargetdist * (tan(ax.CameraViewAngle*0.5*(pi/180)));

    if parentaspect < 1
        xlim = [camtarget(1)-minextent*0.5,camtarget(1)+minextent*0.5];
        ylim = [camtarget(2)-(minextent*0.5)/parentaspect,camtarget(2)+(minextent*0.5)/parentaspect];
    else
        xlim = [camtarget(1)-(minextent*0.5)*parentaspect,camtarget(1)+(minextent*0.5)*parentaspect];
        ylim = [camtarget(2)-minextent*0.5,camtarget(2)+minextent*0.5];
    end
    set(ax,...
        'XLim',xlim,...
        'YLim',ylim);
    
    parent.Units = parentunits;
end
