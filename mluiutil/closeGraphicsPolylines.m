function [verts,nvpairs] = closeGraphicsPolylines(verts,nvpairs,skip)
    
    if nargin < 2
        nvpairs = cell(1,0);
    end
    
    if nargin < 3
        skip = [];
    end

    % close polygon boundary (last edge) when showing only edges
    if iscell(verts)
        for i=1:numel(verts)
            if iscell(verts{i})
                for j=1:numel(verts{i})
                    if isempty(skip) || not(skip{i}(j))
                        verts{i}{j} = verts{i}{j}(:,[1:end,1]);
                    end
                end
            else
                if isempty(skip) || not(skip(i))
                    verts{i} = verts{i}(:,[1:end,1]);
                end
            end
        end
    else
        if isempty(skip) || not(skip)
            verts = verts(:,[1:end,1]);
        end
    end
    
    
    % all properties that can be given per component or per vertex must
    % be checked here
    
    options = nvpairs2struct(nvpairs);
    optioninds = nvpairs2struct(nvpairs,[],true);

    if isfield(options,'vnormals')
        hasvnormals = true;
    else
        hasvnormals = false;
    end

    if isfield(options,'fvcolor')
        hasfvcolor = true;
    else
        hasfvcolor = false;
    end

    if isfield(options,'fvalpha')
        hasfvalpha = true;
    else
        hasfvalpha = false;
    end
    
    
    if hasvnormals
        if iscell(options.vnormals)
            for i=1:numel(options.vnormals)
                if iscell(options.vnormals{i})
                    for j=1:numel(options.vnormals{i})
                        if isempty(skip) || not(skip{i}(j))
                            options.vnormals{i}{j} = options.vnormals{i}{j}(:,[1:end,1]);
                        end
                    end
                else
                    if not(ischar(options.vnormals{i})) && (isempty(skip) || not(skip(i)))
                        options.vnormals{i} = options.vnormals{i}(:,[1:end,1]);
                    end
                end
            end
        else
            if not(ischar(vnormals)) && (isempty(skip) || not(skip))
                options.vnormals = options.vnormals(:,[1:end,1]);
            end
        end
        nvpairs{optioninds.vnormals} = options.vnormals;
    end
    
    if hasfvcolor
        if iscell(options.fvcolor)
            for i=1:numel(options.fvcolor)
                if iscell(options.fvcolor{i})
                    % one per vertex
                    for j=1:numel(options.fvcolor{i})
                        if isempty(skip) || not(skip{i}(j))
                            options.fvcolor{i}{j} = options.fvcolor{i}{j}(:,[1:end,1]);
                        end
                    end
                elseif iscell(verts{i})
                    % one per component, do nothing
                else
                    % one per vertex
                    if isempty(skip) || not(skip(i))
                        options.fvcolor{i} = options.fvcolor{i}(:,[1:end,1]);
                    end
                end
            end
        else
            % one per vertex
            if isempty(skip) || not(skip)
                options.fvcolor = options.fvcolor(:,[1:end,1]);
            end
        end
        nvpairs{optioninds.fvcolor} = options.fvcolor;
    end
    
    if hasfvalpha
        if iscell(options.vnormals)
            for i=1:numel(options.fvalpha)
                if iscell(options.fvalpha{i})
                    % one per vertex
                    for j=1:numel(options.fvalpha{i})
                        if isempty(skip) || not(skip{i}(j))
                            options.fvalpha{i}{j} = options.fvalpha{i}{j}(:,[1:end,1]);
                        end
                    end
                elseif iscell(verts{i})
                    % one per component, do nothing
                else
                    % one per vertex
                    if isempty(skip) || not(skip(i))
                        options.fvalpha{i} = options.fvalpha{i}(:,[1:end,1]);
                    end
                end
            end
        else
            % one per vertex
            if isempty(skip) || not(skip)
                options.fvalpha = options.fvalpha(:,[1:end,1]);
            end
        end
        
        nvpairs{optioninds.fvalpha} = options.fvalpha;
    end
end
