% gobjs = showPatches(verts,faces)
% gobjs = showPatches(verts,faces,gobjs)
% gobjs = showPatches(verts,faces,gobjs,parent)
% gobjs = showPatches(verts,faces,gobjs,parent,nvpairs)
% similar to patch(), but multiple patches can be given, each with multiple
% components and graphics objects are automatically created/updated as needed.
% -------------------------------------------------------------------------
% - verts: cell array with each cell containing a cell array of component
% vertices; or a cell array of patch vertices, or directly the vertics of
% a single patch. Vertices of a patch or component are 2xn or 3xn matrices.
% - faces: same as verts, but faces of a patch or component are kxm matrices,
% where k is the largest number of vertices in the patch (not component).
% Faces with fewer vertices have to be filled with NaNs.
% - gobjs: empty to create new patches, existing patches to update
% - parent: parent of the patches (e.g. axes)
% -------------------------------------------------------------------------
% available name-value parameters:
% - vnormals: normal per vertex or 'auto' for all patches or per patch
% - fnormals: normal per face or 'auto' for all patches or per patch
% - fvcolor: color per component, per vertex or per face
% - fvalpha: alpha per component, per vertex or per face
% - facecolor: single color, color per patch, 'none', 'flat', or 'interp'
% - edgecolor: single color, color per patch, 'none', 'flat', or 'interp'
% - facealpha: single alpha value, alpha per patch, 'none', 'flat' or 'interp'
% - edgealpha: single alpha value, alpha per patch, 'none', 'flat' or 'interp'
% - edgewidth: single value or value per patch
% - markersymbol: single marker symbol, marker per patch, 'none' or 'flat'
% - markeredgecolor: single color, color per patch, 'none' or 'flat'
% - markerfacecolor: single color, color per patch, 'none' or 'flat'
% - markersize: single value or value per patch
% - stackz: single value or value per patch
% - visible: 'on' or 'off', single value or per patch
% - pickableparts: 'visible', 'all' or 'none', single value or per patch
% - hittest: 'on' or 'off', single value or per patch
% -------------------------------------------------------------------------
% the following reflective properties are only set when creating a new
% patch (not when updating an existing patch):
% - facelighting: 'none', 'flat' or 'gouraud', single value or per patch
% - edgelighting: 'none', 'flat' or 'gouraud', single value or per patch
% - materialprops: (for details see patch properties) struct with
%       AmbientStrength
%       DiffuseStrength
%       SpecularStrength
%       SpecularColorReflectance
%       SpecularExponent
function gobjs = showPatches(verts,faces,gobjs,parent,varargin)

    if abs(nargin) < 3 || isempty(gobjs)
        gobjs = gobjects(1,0);
    end
    if abs(nargin) < 4 || isempty(parent)
        parent = gca;
    end
    
    if not(iscell(verts))
        verts = {verts};
    end
    if not(iscell(faces))
        faces = {faces};
    end
    
    if numel(parent) == 1 && numel(faces) > 1
        parent = parent(1,ones(1,numel(faces)));
    end
    
    options = struct(...
        'stackz',0,...
        'fvcolor',{repmat({[]},1,numel(faces))},...
        'fvalpha',{repmat({[]},1,numel(faces))},...
        'facecolor',[1;1;0.75],...
        'edgecolor',[0.5;0.5;0.5],...
        'facealpha',1.0,...
        'edgealpha',1.0,...
        'vnormals','auto',...
        'fnormals','auto',...
        'facelighting','flat',...
        'edgelighting','flat',...
        'edgewidth',1,...
        'edgestyle','-',...
        'markersymbol','none',...
        'markeredgecolor','none',...
        'markerfacecolor','none',...
        'markersize',6,...
        'pickableparts','none',...
        'hittest','off',...
        'visible','on',...
        'lightingbugWorkaround','off',...
        'materialprops',struct(...
            'AmbientStrength',0.3,...        
            'DiffuseStrength',0.6,...
            'SpecularStrength',0,...
            'SpecularColorReflectance',1,...
            'SpecularExponent',10));
        
    options = nvpairs2struct(varargin,options);
    
    if not(iscell(options.vnormals))
        options.vnormals = {options.vnormals};        
        if ischar(options.vnormals{1}) && numel(faces) ~= 1
            options.vnormals = options.vnormals(:,ones(1,numel(faces)));
        end
    end
    if not(iscell(options.fnormals))
        options.fnormals = {options.fnormals};
        if ischar(options.fnormals{1}) && numel(faces) ~= 1
            options.fnormals = options.fnormals(:,ones(1,numel(faces)));
        end
    end
    if not(iscell(options.fvcolor)) 
        options.fvcolor = {options.fvcolor};
    end
    if not(iscell(options.fvalpha)) 
        options.fvalpha = {options.fvalpha};
    end
    if not(iscell(options.facecolor)) 
        if ischar(options.facecolor) || size(options.facecolor,2) == 1
            % one color for all patches
            options.facecolor = {options.facecolor};
            if numel(faces) ~= 1
                options.facecolor = options.facecolor(:,ones(1,numel(faces)));
            end
        else
            % one color per patch given as array
            options.facecolor = mat2cell(options.facecolor,...
                size(options.facecolor,1),ones(1,size(options.facecolor,2)));
        end
    end
    if not(iscell(options.edgecolor)) 
        if ischar(options.edgecolor) || size(options.edgecolor,2) == 1
            % one color for all patches
            options.edgecolor = {options.edgecolor};
            if numel(faces) ~= 1
                options.edgecolor = options.edgecolor(:,ones(1,numel(faces)));
            end
        else
            % one color per patch given as array
            options.edgecolor = mat2cell(options.edgecolor,...
                size(options.edgecolor,1),ones(1,size(options.edgecolor,2)));
        end
    end
    if not(iscell(options.facealpha)) 
        if ischar(options.facealpha) || size(options.facealpha,2) == 1
            % one alpha value for all patches
            options.facealpha = {options.facealpha};
            if numel(faces) ~= 1
                options.facealpha = options.facealpha(:,ones(1,numel(faces)));
            end
        else
            % one alpha value per patch given as array
            options.facealpha = mat2cell(options.facealpha,...
                size(options.facealpha,1),ones(1,size(options.facealpha,2)));
        end
    end
    if not(iscell(options.edgealpha)) 
        if ischar(options.edgealpha) || size(options.edgealpha,2) == 1
            % one alpha value for all patches
            options.edgealpha = {options.edgealpha};
            if numel(faces) ~= 1
                options.edgealpha = options.edgealpha(:,ones(1,numel(faces)));
            end
        else
            % one alpha value per patch given as array
            options.edgealpha = mat2cell(options.edgealpha,...
                size(options.edgealpha,1),ones(1,size(options.edgealpha,2)));
        end
    end
    if not(iscell(options.edgewidth)) 
        if size(options.edgewidth,2) == 1
            % one value for all patches
            options.edgewidth = {options.edgewidth};
            if numel(faces) ~= 1
                options.edgewidth = options.edgewidth(:,ones(1,numel(faces)));
            end
        else
            % one value per patch given as array
            options.edgewidth = mat2cell(options.edgewidth,...
                size(options.edgewidth,1),ones(1,size(options.edgewidth,2)));
        end
    end
    if not(iscell(options.edgestyle)) 
        % one value for all patches
        options.edgestyle = {options.edgestyle};
        if numel(faces) ~= 1
            options.edgestyle = options.edgestyle(:,ones(1,numel(faces)));
        end
    end
    if not(iscell(options.markersymbol)) 
        % one value for all patches
        options.markersymbol = {options.markersymbol};
        if numel(faces) ~= 1
            options.markersymbol = options.markersymbol(:,ones(1,numel(faces)));
        end
    end
    if not(iscell(options.markeredgecolor)) 
        if ischar(options.markeredgecolor) || size(options.markeredgecolor,2) == 1
            % one value for all patches
            options.markeredgecolor = {options.markeredgecolor};
            if numel(faces) ~= 1
                options.markeredgecolor = options.markeredgecolor(:,ones(1,numel(faces)));
            end
        else
            % one value per patch given as array
            options.markeredgecolor = mat2cell(options.markeredgecolor,...
                size(options.markeredgecolor,1),ones(1,size(options.markeredgecolor,2)));
        end
    end
    if not(iscell(options.markerfacecolor)) 
        if ischar(options.markerfacecolor) || size(options.markerfacecolor,2) == 1
            % one value for all patches
            options.markerfacecolor = {options.markerfacecolor};
            if numel(faces) ~= 1
                options.markerfacecolor = options.markerfacecolor(:,ones(1,numel(faces)));
            end
        else
            % one value per patch given as array
            options.markerfacecolor = mat2cell(options.markerfacecolor,...
                size(options.markerfacecolor,1),ones(1,size(options.markerfacecolor,2)));
        end
    end
    if not(iscell(options.markersize)) 
        if size(options.markersize,2) == 1
            % one value for all patches
            options.markersize = {options.markersize};
            if numel(faces) ~= 1
                options.markersize = options.markersize(:,ones(1,numel(faces)));
            end
        else
            % one value per patch given as array
            options.markersize = mat2cell(options.markersize,...
                size(options.markersize,1),ones(1,size(options.markersize,2)));
        end
    end
    if not(iscell(options.stackz)) 
        if size(options.stackz,2) == 1
            % one value for all patches
            options.stackz = {options.stackz};
            if numel(faces) ~= 1
                options.stackz = options.stackz(:,ones(1,numel(faces)));
            end
        else
            % one value per patch given as array
            options.stackz = mat2cell(options.stackz,...
                size(options.stackz,1),ones(1,size(options.stackz,2)));
        end
    end
    if not(iscell(options.visible)) 
        if ischar(options.visible) || size(options.visible,2) == 1
            % one value for all patches
            options.visible = {options.visible};
            if numel(faces) ~= 1
                options.visible = options.visible(:,ones(1,numel(faces)));
            end
        else
            % one value per patch given as array
            options.visible = mat2cell(options.visible,...
                size(options.visible,1),ones(1,size(options.visible,2)));
        end
    end
    if not(iscell(options.pickableparts)) 
        if ischar(options.pickableparts) || size(options.pickableparts,2) == 1
            % one value for all patches
            options.pickableparts = {options.pickableparts};
            if numel(faces) ~= 1
                options.pickableparts = options.pickableparts(:,ones(1,numel(faces)));
            end
        else
            % one value per patch given as array
            options.pickableparts = mat2cell(options.pickableparts,...
                size(options.pickableparts,1),ones(1,size(options.pickableparts,2)));
        end
    end
    if not(iscell(options.hittest)) 
        if ischar(options.hittest) || size(options.hittest,2) == 1
            % one value for all patches
            options.hittest = {options.hittest};
            if numel(faces) ~= 1
                options.hittest = options.hittest(:,ones(1,numel(faces)));
            end
        else
            % one value per patch given as array
            options.hittest = mat2cell(options.hittest,...
                size(options.hittest,1),ones(1,size(options.hittest,2)));
        end
    end
    
    if numel(verts) ~= numel(faces) || ...
       numel(options.vnormals) ~= numel(faces) || ...
       numel(options.fnormals) ~= numel(faces) || ...
       numel(options.fvcolor) ~= numel(faces) || ...
       numel(options.fvalpha) ~= numel(faces) || ...
       numel(options.facecolor) ~= numel(faces) || ...
       numel(options.edgecolor) ~= numel(faces) || ...
       numel(options.facealpha) ~= numel(faces) || ...
       numel(options.edgealpha) ~= numel(faces) || ...
       numel(options.edgewidth) ~= numel(faces) || ...
       numel(options.edgestyle) ~= numel(faces) || ...
       numel(options.markersymbol) ~= numel(faces) || ...
       numel(options.markeredgecolor) ~= numel(faces) || ...
       numel(options.markerfacecolor) ~= numel(faces) || ...
       numel(options.markersize) ~= numel(faces) || ...
       numel(options.stackz) ~= numel(faces)
        error('Input sizes do not match.');
    end
    
    % create new graphic placeholders if necessary
    if numel(gobjs) ~= numel(faces) % || not(all(isgraphics(gobjs(:))))
        delete(gobjs(isvalid(gobjs)));
        gobjs = gobjects(1,numel(faces));
    end
    
    newgobjinds = find(not(isgraphics(gobjs(:)')));
    if not(isempty(newgobjinds))
        
        materialprops = struct2nvpairs(options.materialprops);
        
        % sort by stack z-value
        for i=newgobjinds
            if isempty(options.stackz{i})
                options.stackz{i} = 0;
            end
        end
        [~,inds] = sort([options.stackz{newgobjinds}],'ascend');
        newgobjinds = newgobjinds(inds);
        
%         if strcmp(options.selectable,'off')
%             selectableprops = {'HitTest','off','PickableParts','none'};
%         elseif strcmp(options.selectable,'visible')
%             selectableprops = {'HitTest','on','PickableParts','visible'};
%         elseif strcmp(options.selectable,'always')
%             selectableprops = {'HitTest','on','PickableParts','all'};
%         end

%                         selectableprops{:},...

        % create graphics objects
        for i=newgobjinds
            gobjs(i) = patch([0,0],[0,0],0,...
                'Parent',parent(i),...
                'FaceLighting',options.facelighting,...
                materialprops{:},...
                'BackfaceLighting','unlit',...
                'Visible','off');
            gobjs(i).addprop('Stackz');
            gobjs(i).Stackz = options.stackz{i};
        end
        
        % update z-stack of all parents
        updateZStack(unique(parent(newgobjinds)));
        
%         % add SetMethod (not earlier so the zstack does not updated
%         % repeatedly when creating the objects)
%         for i=newgobjinds
%             gobjs(i).findprop('Stackz').SetMethod = @changeStackz;
%         end
    end
    
    [parent,~,gparentind] = unique(parent);
    zstackdirty = false(1,numel(parent));
    
    for j=1:numel(faces) % over all patches
        
        if iscell(faces{j})
            
            if not(isempty(options.fvcolor{j})) && not(iscell(options.fvcolor{j}))
                % one color per component, convert to one color per face
                if size(options.fvcolor{j},2) ~= numel(faces{j})
                    error('fvcolor does not have one color per component.');
                end
                options.fvcolor{j} = mat2cell(options.fvcolor{j},...
                    size(options.fvcolor{j},1),ones(1,size(options.fvcolor{j},2)));
                for i=1:numel(faces{j})
                    options.fvcolor{j}{i} = options.fvcolor{j}{i}(:,ones(1,size(faces{j}{i},2)));
                end
            end
            
            if not(isempty(options.fvalpha{j})) && not(iscell(options.fvalpha{j}))
                % one alpha value per component, convert to one alpha value per face
                if size(options.fvalpha{j},2) ~= numel(faces{j})
                    error('fvalpha does not have one alpha value per component.');
                end
                options.fvalpha{j} = mat2cell(options.fvalpha{j},...
                    size(options.fvalpha{j},1),ones(1,size(options.fvalpha{j},2)));
                for i=1:numel(faces{j})
                    options.fvalpha{j}{i} = options.fvalpha{j}{i}(:,ones(1,size(faces{j}{i},2)));
                end
            end
            
            vindoffset = 0;
            for i=1:numel(faces{j})
                faces{j}{i} = faces{j}{i} + vindoffset;
                vindoffset = vindoffset + size(verts{j}{i},2);
            end
            verts{j} = [verts{j}{:}];
            faces{j} = [faces{j}{:}];
            if iscell(options.vnormals{j})
                options.vnormals{j} = [options.vnormals{j}{:}];
            end
            if iscell(options.fnormals{j})
                options.fnormals{j} = [options.fnormals{j}{:}];
            end
            if iscell(options.fvcolor{j})
                options.fvcolor{j} = [options.fvcolor{j}{:}];
            end
            if iscell(options.fvalpha{j})
                options.fvalpha{j} = [options.fvalpha{j}{:}];
            end
        end
        
        % give all parameters the correct format
        if not(ischar(options.edgecolor{j}))
            options.edgecolor{j} = options.edgecolor{j}';
        end
        if not(ischar(options.facecolor{j}))
            options.facecolor{j} = options.facecolor{j}';
        end
        if ischar(options.vnormals{j})
            vnormalpropname = 'VertexNormalsMode';
        else
            vnormalpropname = 'VertexNormals';
            options.vnormals{j} = options.vnormals{j}';
        end
        if ischar(options.fnormals{j})
            fnormalpropname = 'FaceNormalsMode';
        else
            fnormalpropname = 'FaceNormals';
            options.fnormals{j} = options.fnormals{j}';
        end
        if not(ischar(options.markeredgecolor{j}))
            options.markeredgecolor{j} = options.markeredgecolor{j}';
        end
        if not(ischar(options.markerfacecolor{j}))
            options.markerfacecolor{j} = options.markerfacecolor{j}';
        end
        
        % only pass parameters that have changed
        additionalargs = cell(1,0);
        if parent(gparentind(j)) ~= gobjs(j).Parent
            additionalargs(:,end+1:end+2) = {'Parent',parent(gparentind(j))};
        end
        if numel(options.edgecolor{j}) ~= numel(gobjs(j).EdgeColor) || ...
           any(options.edgecolor{j} ~= gobjs(j).EdgeColor)
            additionalargs(:,[end+1,end+2]) = {'EdgeColor',options.edgecolor{j}};
        end
        if numel(options.facecolor{j}) ~= numel(gobjs(j).FaceColor) || ...
           any(options.facecolor{j} ~= gobjs(j).FaceColor)
            additionalargs(:,[end+1,end+2]) = {'FaceColor',options.facecolor{j}};
        end
        if numel(options.edgealpha{j}) ~= numel(gobjs(j).EdgeAlpha) || ...
           any(options.edgealpha{j} ~= gobjs(j).EdgeAlpha)
            additionalargs(:,[end+1,end+2]) = {'EdgeAlpha',options.edgealpha{j}};
        end
        if numel(options.facealpha{j}) ~= numel(gobjs(j).FaceAlpha) || ...
           any(options.facealpha{j} ~= gobjs(j).FaceAlpha)
            additionalargs(:,[end+1,end+2]) = {'FaceAlpha',options.facealpha{j}};
        end
        if options.edgewidth{j} ~= gobjs(j).LineWidth
            additionalargs(:,[end+1,end+2]) = {'LineWidth',options.edgewidth{j}};
        end
        if not(strcmp(options.edgestyle{j},gobjs(j).LineStyle))
            additionalargs(:,[end+1,end+2]) = {'LineStyle',options.edgestyle{j}};
        end
        if numel(options.vnormals{j}) ~= numel(gobjs(j).(vnormalpropname)) || ...
           any(options.vnormals{j}(:) ~= gobjs(j).(vnormalpropname)(:))
            additionalargs(:,[end+1,end+2]) = {vnormalpropname,options.vnormals{j}};
        end
        if numel(options.fnormals{j}) ~= numel(gobjs(j).(fnormalpropname)) || ...
           any(options.fnormals{j}(:) ~= gobjs(j).(fnormalpropname)(:))
            additionalargs(:,[end+1,end+2]) = {fnormalpropname,options.fnormals{j}};
        end
        if not(strcmp(options.markersymbol{j},gobjs(j).Marker))
            additionalargs(:,[end+1,end+2]) = {'Marker',options.markersymbol{j}};
        end
        if numel(options.markeredgecolor{j}) ~= numel(gobjs(j).MarkerEdgeColor) || ...
           any(options.markeredgecolor{j} ~= gobjs(j).MarkerEdgeColor)
            additionalargs(:,[end+1,end+2]) = {'MarkerEdgeColor',options.markeredgecolor{j}};
        end
        if numel(options.markerfacecolor{j}) ~= numel(gobjs(j).MarkerFaceColor) || ...
           any(options.markerfacecolor{j} ~= gobjs(j).MarkerFaceColor)
            additionalargs(:,[end+1,end+2]) = {'MarkerFaceColor',options.markerfacecolor{j}};
        end
        if numel(options.markersize{j}) ~= numel(gobjs(j).MarkerSize) || ...
           any(options.markersize{j} ~= gobjs(j).MarkerSize)
            additionalargs(:,[end+1,end+2]) = {'MarkerSize',options.markersize{j}};
        end
        if numel(options.stackz{j}) ~= numel(gobjs(j).Stackz) || ...
           any(options.stackz{j} ~= gobjs(j).Stackz)
            additionalargs(:,[end+1,end+2]) = {'Stackz',options.stackz{j}};
            zstackdirty(gparentind(j)) = true;
        end
        if numel(options.visible{j}) ~= numel(gobjs(j).Visible) || ...
           any(options.visible{j} ~= gobjs(j).Visible)
            additionalargs(:,[end+1,end+2]) = {'Visible',options.visible{j}};
        end
        if numel(options.pickableparts{j}) ~= numel(gobjs(j).PickableParts) || ...
           any(options.pickableparts{j} ~= gobjs(j).PickableParts)
            additionalargs(:,[end+1,end+2]) = {'PickableParts',options.pickableparts{j}};
        end
        if numel(options.hittest{j}) ~= numel(gobjs(j).HitTest) || ...
           any(options.hittest{j} ~= gobjs(j).HitTest)
            additionalargs(:,[end+1,end+2]) = {'HitTest',options.hittest{j}};
        end
        
        if strcmp(options.lightingbugWorkaround,'on')
            shadedvcolors = computeDiffuseVertexColors_bugworkaround(...
                verts{j},faces{j},options.vnormals{j}',parent(gparentind(j)),options.fvcolor{j},options.materialprops);
            set(gobjs(j),...
                'Vertices', verts{j}',...
                'Faces', faces{j}',...
                'FaceVertexCData',shadedvcolors',...
                'FaceVertexAlphaData',options.fvalpha{j}',...
                'FaceLighting','none',...
                additionalargs{:});    
        else
            set(gobjs(j),...
                'Vertices', verts{j}',...
                'Faces', faces{j}',...
                'FaceVertexCData',options.fvcolor{j}',...
                'FaceVertexAlphaData',options.fvalpha{j}',...
                additionalargs{:});
            
        end
    end
    
    for i=1:numel(parent)
        if zstackdirty(i)
            updateZStack(parent(i));
        end        
    end
end

function shadedvcolors = computeDiffuseVertexColors_bugworkaround(verts,faces,vnormals,parent,vcolors,materialprops)
    
    if materialprops.SpecularStrength ~= 0
        error('Lighting bug workaround only implemented for non-specular materials (set SpecularStrength to 0).');
    end
    if size(vnormals,2) ~= size(verts,2)
        error('Need vertex normals for lighting bug workaround.');
    end
    if size(vcolors,2) == size(faces,2)
        vcolors_temp = reshape([vcolors;vcolors;vcolors],3,[]);
        vcolors = zeros(3,size(verts,2));
        vcolors(:,faces(:)) = vcolors_temp;
        clear vcolors_temp;
    end
    if size(vcolors,2) ~= 1 && ...
        size(vcolors,2) ~= size(verts,2)
        error('Need colors per vertex, per face or per patch for lighting bug workaround.');
    end

    % get transformation matrix into global 3D scene coordinates
    tmat = eye(4);
    prnt = parent;
    while not(isempty(prnt)) && strcmp(prnt.Type,'hgtransform')
        tmat = prnt.Matrix \ tmat;
        prnt = prnt.Parent;
    end
    
    % transform vertices and vertex normals
    verts = tmat * [verts;ones(1,size(verts,2))];
    vnormals = inv(tmat(1:3,1:3))' * vnormals;
    
    % re-homogenize
    verts = bsxfun(@rdivide,verts(1:3,:),verts(4,:));
%     vnormals = bsxfun(@rdivide,vnormals(1:3,:),vnormals(4,:));
    
    ax = prnt;
    
    lights = findall(ax,'Type','light');

    if not(isempty(lights))
        lightpos = [lights.Position];
        
        shadedvcolors = zeros(size(verts));
        for i=1:size(lightpos,2)
            lightdir = bsxfun(@minus,lightpos(:,i).*3,verts);
%             lightdir = lightpos(:,i.*ones(1,size(verts,2))); % simulate infinitely distant lighting
            lightdir = bsxfun(@rdivide,lightdir,sqrt(sum(lightdir.^2,1))); % normalize

            shadedvcolors = shadedvcolors + materialprops.DiffuseStrength*bsxfun(@times,vcolors,max(0,dot(lightdir,vnormals))); % lights are assumed to have color [1,1,1];
        end

        shadedvcolors = bsxfun(@plus, shadedvcolors, materialprops.AmbientStrength*vcolors);
    else
        shadedvcolors = vcolors;
    end
end
