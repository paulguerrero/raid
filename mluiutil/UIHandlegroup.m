% group ui elements with possibly different parents to hide/show/delete
% them together with one command
classdef UIHandlegroup < handle
    
properties(SetAccess=protected)
    isVisible = true;
    childs = gobjects(1,0);
    childsVisible = false(0,1);
end
    
methods
    
function obj = UIHandlegroup(childs,visible)
    if nargin == 0
        % do nothing
    elseif nargin == 1
        obj.addChilds(childs)
    elseif nargin == 2
        obj.addChilds(childs,visible)
    else
        error('Invalid arguments.');
    end
end

function delete(obj)
    for i=1:numel(obj.childs)
        if isgraphics(obj.childs(i))
            delete(obj.childs(i))
        end
    end
end

function hide(obj)
    if obj.isVisible
        obj.childsVisible = obj.handlesVisible(obj.childs);
    end

    mask = isgraphics(obj.childs);
    set(obj.childs(mask),'Visible','off');
    
    obj.isVisible = false;
end

function show(obj)
    mask = isgraphics(obj.childs);
    set(obj.childs(mask),{'Visible'},...
        arrayfun(@(x) iif(x,'on','off'),obj.childsVisible(mask),'UniformOutput',false)');
    
    obj.isVisible = true;
end

function addChilds(obj,childs)
    childs = unique(childs);

    if any(ismember(childs,obj.childs))
        error('Already has child.');
    end
    
    obj.childs = [obj.childs,childs];
    obj.childsVisible = [obj.childsVisible,obj.handlesVisible(childs)];
end

function removeChilds(obj,childs)
    mask = ismember(obj.childs,childs);
    obj.childs(mask) = [];
    obj.childsVisible(mask) = [];
end
    
end

methods(Static)
    
function b = handlesVisible(handles)
    mask = isgraphics(handles);
    b = cell(size(handles));

    if any(mask)
        if sum(mask) > 1
            b(mask) = get(handles(mask),'Visible');
        else
            b{mask} = get(handles(mask),'Visible');
        end
    end
    b = cellfun(@(x) strcmp(x,'on'),b);
end

end

end
