% always returns a line segment on a line through the mouse pointer, with
% the first point on the camera plane and the second point on the camera
% target plane (both planes are perpendicular to the camera direction)
function m = mouseray(ax)
    model2world = [];
    if strcmp(ax.Type,'hgtransform')
        [model2world,ax] = gmodel2world(ax);
    end
        
    mpos = get(ax,'CurrentPoint')';
    campos = get(ax,'CameraPosition')';
    camtarget = get(ax,'CameraTarget')';
    camdir = camtarget-campos;
    camdir = camdir ./ sqrt(sum(camdir.^2,1));
    m = linePlaneIntersection(camdir,campos,mpos(:,1),mpos(:,2));
    m = [m,m+(camtarget-campos)];
    
    if not(isempty(model2world))
        % inv(model2world) * [m;[1,1]]; 
        m = model2world \ [m;[1,1]];
        m = m(1:3,:);
    end
end
