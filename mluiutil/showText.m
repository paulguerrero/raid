function gobjs = showText(txt,txtpos,gobjs,parent,varargin)
    
    if abs(nargin) < 4 || isempty(gobjs)
        gobjs = gobjects(1,0);
    end
    if abs(nargin) < 5 || isempty(parent)
        parent = gca;
    end    

    if not(iscell(txt))
        txt = {txt};
    end
    if not(iscell(txtpos))
        txtpos = {txtpos};
    end
    
    if numel(txtpos) ~= numel(txt)
        error('Input sizes do not match.');
    end
    
    if numel(parent) == 1 && numel(txt) > 1
        parent = parent(1,ones(1,numel(txt)));
    end

    options = struct(...
        'stackz',0,...
        'Color',[0;0;0],...    
        'FontSize',12,...
        'FontWeight','normal',...
        'FontAngle','normal',...
        'FontName','Helvetica',...
        'FontSmoothing','on',...
        'Interpreter','tex',...
        'EdgeColor','none',...
        'BackgroundColor','none',...    
        'Margin',5,...    
        'LineStyle','-',...
        'LineWidth',0.5,...
        'HorizontalAlignment','left',...
        'VerticalAlignment','top');
    options = nvpairs2struct(varargin,options);
    
    % check and expand options
    % only for options that are either a string or one vector per graphics object
    optionnames = fieldnames(options);
    optioninds = 1:numel(optionnames); % all options that can be checked this way
    for i=optioninds
        if ischar(options.(optionnames{i})) || size(options.(optionnames{i}),2) == 1
            % one value for all textboxes
            options.(optionnames{i}) = {options.(optionnames{i})};
            if numel(txt) ~= 1
                options.(optionnames{i}) = options.(optionnames{i})(:,ones(1,numel(txt)));
            end
        else
            % one value per textbox given as array
            options.(optionnames{i}) = mat2cell(...
                options.(optionnames{i}),...
                size(options.(optionnames{i}),1),...
                ones(1,size(options.(optionnames{i}),2)));    
        end
        
        if numel(options.(optionnames{i})) ~= numel(txt)
            error('Input sizes do not match.');
        end
    end
    
    if numel(gobjs) ~= numel(txt) || not(all(isgraphics(gobjs(:))))
        delete(gobjs(isgraphics(gobjs)));
        gobjs = gobjects(1,numel(txt));
        
        for i=1:numel(options.stackz)
            if isempty(options.stackz{i})
                options.stackz{i} = 0;
            end
        end
        [~,gobjinds] = sort([options.stackz{:}],'ascend');
        
        for i=gobjinds
            gobjs(i) = text(...
                'Position',txtpos{i}',...
                'Parent',parent(i),...
                'HitTest','off',...
                'PickableParts','none',...
                'Visible','off');
            gobjs(i).addprop('Stackz');
            gobjs(i).Stackz = options.stackz{i};
        end
        
        % update z-stack of all parents
        updateZStack(unique(parent));
        
        % add SetMethod (not earlier so the zstack does not updated
        % repeatedly when creating the objects)
        for i=1:numel(gobjs)
            gobjs(i).findprop('Stackz').SetMethod = @changeStackz;
        end
    end
        
    for j=1:numel(txt)
        gobjparams = cell(1,0);
        
        if parent(j) ~= gobjs(j).Parent
            gobjparams(:,[end+1,end+2]) = {'Parent',parent(j)};
        end
        
        if not(strcmp(txt{j},gobjs(j).String))
            gobjparams(:,[end+1,end+2]) = {'String',txt{j}};
        end
        
        position = txtpos{j}';
        if numel(position) ~= numel(gobjs(j).Position) || ...
           any(position ~= gobjs(j).Position)
            gobjparams(:,[end+1,end+2]) = {'Position',position};
        end
        
        if strcmp(gobjs(j).Visible,'off')
            gobjparams(:,[end+1,end+2]) = {'Visible','on'};
        end
        
        % fill in all option parameters if they have changed
        optioninds = 2:numel(optionnames); % all options that can be handled this way
        for i=optioninds
            if not(ischar(options.(optionnames{i}){j})) && not(isscalar(options.(optionnames{i}){j}))
                options.(optionnames{i}){j} = options.(optionnames{i}){j}';
            end
            if numel(options.(optionnames{i}){j}) ~= numel(gobjs(j).(optionnames{i})) || ...
               any(options.(optionnames{i}){j} ~= gobjs(j).(optionnames{i}))
                gobjparams(:,[end+1,end+2]) = {optionnames{i},options.(optionnames{i}){j}};
            end
        end
        
        if not(isempty(gobjparams))
            set(gobjs(j),gobjparams{:});
        end
    end
end
