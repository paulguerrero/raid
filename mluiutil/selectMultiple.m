function selection = selectMultiple(selection,s,type)
    if strcmp(type,'extend')
        if not(isempty(selection))
            if not(isempty(s))
                mask = true(1,size(selection,2));
                for i=1:size(selection,1)
                    mask = mask & selection(i,:) == s(i,:);
                end
                if any(mask)
                    selection(:,mask) = [];
                else
                    selection(:,end+1) = s;
                end
            end
        else
            selection = s;
        end
    elseif strcmp(type,'normal')
        selection = s;
    end
end
