// output = polygonPolygonBoolean_mex(operation,poly1,poly2)
//
// operation: 'union', 'intersection' or 'difference'
//
// poly1 and poly2: input polygon sets as cell arrays. Each polygon in the
// set is a cell array of polygon components, first cell is the
// exterior boundary, the following cells are interior boundaries
// each boundary is 2xn matrix of vertices.
// Polygon boundaries must not self-intersect.
//
// output: n x m cell array of polygon sets, where n is the number of
// polygons in poly1 and m the number of polygons in poly2. Each cell
// (i,j) contains the polygon set resulting from the operation applied to
// poly1{i} and poly2{j}. Individual polygons in the set have the same
// format as the input.

#include "mex.h"

#include <boost/geometry.hpp>

#include "mexutils.h"
#include "mlboostutils.h"

/* the gateway function */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    if(nrhs != 3) {
        mexErrMsgTxt("Three inputs required.");
    }
    
    if(nlhs != 1) {
        mexErrMsgTxt("One output required.");
    }
    
    mwIndex inputind = 0;    
    
    // read operation string
    std::string operation;
    readString(prhs[inputind],operation);
    ++inputind;

    // read polygon set 1
    std::vector<polygon2D> p1;
    readBoostPolygon2DCellArray(prhs[inputind],p1);
    ++inputind;
    
    // read polygon set 2
    std::vector<polygon2D> p2;
    readBoostPolygon2DCellArray(prhs[inputind],p2);
    ++inputind;
    
    // perform the operations
    std::vector<std::vector<polygon2D> > output(p1.size()*p2.size());
    if (operation.compare("union") == 0) {
        std::vector<std::vector<polygon2D> >::iterator op = output.begin();
        for (std::vector<polygon2D>::const_iterator p2p=p2.begin(); p2p!=p2.end(); ++p2p) {
            for (std::vector<polygon2D>::const_iterator p1p=p1.begin(); p1p!=p1.end(); ++p1p) {
                boost::geometry::union_(*p1p,*p2p,*op);
//                 op->push_back(*p2p);
                ++op;
            }
        }
    } else if (operation.compare("intersection") == 0) {
        std::vector<std::vector<polygon2D> >::iterator op = output.begin();
        for (std::vector<polygon2D>::const_iterator p2p=p2.begin(); p2p!=p2.end(); ++p2p) {
            for (std::vector<polygon2D>::const_iterator p1p=p1.begin(); p1p!=p1.end(); ++p1p) {
                boost::geometry::intersection(*p1p,*p2p,*op);
//                 op->push_back(*p2p);
                ++op;
            }
        }
    } else if (operation.compare("difference") == 0) {
        std::vector<std::vector<polygon2D> >::iterator op = output.begin();
        for (std::vector<polygon2D>::const_iterator p2p=p2.begin(); p2p!=p2.end(); ++p2p) {
            for (std::vector<polygon2D>::const_iterator p1p=p1.begin(); p1p!=p1.end(); ++p1p) {
                boost::geometry::difference(*p1p,*p2p,*op);
//                 op->push_back(*p2p);
                ++op;
            }
        }
    } else {
        mexErrMsgTxt("Unknown boolean operator.");
    }
    
    // write the output of all operations
    plhs[0] = mxCreateCellMatrix(p1.size(),p2.size());
    if (plhs[0] == NULL) {
        mexErrMsgTxt("Could not initialize output, probably not enough memory.");
    }
    for (std::vector<std::vector<polygon2D> >::iterator op=output.begin(); op!=output.end(); ++op) {
        // write all output polygons of the current operation
        mxArray* cell = mxCreateCellMatrix(1,op->size());
        if (cell == NULL) {
            mexErrMsgTxt("Could not initialize output, probably not enough memory.");
        }
        for (std::vector<polygon2D>::iterator opp=op->begin(); opp!=op->end(); ++opp) {
            mxArray* polygoncellarray;
            writeBoostPolygon2D(*opp, polygoncellarray);
            mxSetCell(cell,opp-op->begin(),polygoncellarray);
        }
        mxSetCell(plhs[0],op-output.begin(),cell);
    }
}
