#ifndef _MLBOOSTUTILS_H
#define _MLBOOSTUTILS_H

#include "mex.h"

#include <vector>

#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>

typedef boost::geometry::model::d2::point_xy<double> point2D;
typedef boost::geometry::model::polygon<point2D> polygon2D;

void readBoostPointset2D(const mxArray* input, std::vector<point2D>& output);
void readBoostPointset2DCellArray(const mxArray* input, std::vector<std::vector<point2D> >& output);

void readBoostSimplePolygon2D(const mxArray* input, polygon2D& output, bool docorrect=true);
void readBoostSimplePolygon2DCellArray(const mxArray* input, std::vector<polygon2D>& output, bool docorrect=true);

void readBoostPolygon2D(const mxArray* input, polygon2D& output, bool docorrect=true);
void readBoostPolygon2DCellArray(const mxArray* input, std::vector<polygon2D>& output, bool docorrect=true);
void readBoostPolygon2DCellArrayCellArray(const mxArray* input, std::vector<std::vector<polygon2D> >& output, bool docorrect=true);

void writeBoostPolygon2D(const polygon2D& input, mxArray*& output);

#endif // _MLBOOSTUTILS_H
