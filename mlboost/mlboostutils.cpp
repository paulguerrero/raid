#include "mlboostutils.h"

#include <boost/geometry/geometry.hpp> 

void readBoostPointset2D(const mxArray* input, std::vector<point2D>& output)
{
    if (!mxIsClass(input,"double")) {
        mexErrMsgTxt("Type is not double.");
    }
    if (mxGetNumberOfDimensions(input) != 2 || mxGetM(input) != 2) {
        mexErrMsgTxt("Input is not an 2D point set.");
    }
    
    output.resize(mxGetN(input));
    double* input_p = mxGetPr(input);
    for (std::vector<point2D>::iterator it=output.begin(); it!=output.end(); ++it) {
        it->x(*input_p++);
        it->y(*input_p++);
    }
}

void readBoostPointset2DCellArray(const mxArray* input, std::vector<std::vector<point2D> >& output)
{
    if (!mxIsClass(input,"cell")) {
        mexErrMsgTxt("Type is not cell.");
    }

    mwSize nelements = mxGetNumberOfElements(input);
    output.resize(nelements);
    for (mwIndex i=0; i<nelements; i++) {
        const mxArray* cell = mxGetCell(input,i);
        if (cell != NULL) { // && mxGetN(cell) >= 1) {
            readBoostPointset2D(cell,output[i]);
        }
    }
}

void readBoostSimplePolygon2D(const mxArray* input, polygon2D& output, bool docorrect)
{
    if (!mxIsClass(input,"double")) {
        mexErrMsgTxt("Type is not double.");
    }
    if (mxGetNumberOfDimensions(input) != 2 || mxGetM(input) != 2) {
        mexErrMsgTxt("Input is not an 2D polygon.");
    }
    
    output.clear();
    
    auto& exteriorring = boost::geometry::exterior_ring(output);
    exteriorring.resize(mxGetN(input));
    double* input_p=mxGetPr(input);
    for (auto it=boost::begin(exteriorring); it!=boost::end(exteriorring); ++it) {
        it->x(*input_p++);
        it->y(*input_p++);
    }

    if (docorrect) {
        boost::geometry::correct(output);
    }
}

void readBoostSimplePolygon2DCellArray(const mxArray* input, std::vector<polygon2D>& output, bool docorrect)
{
    if (!mxIsClass(input,"cell")) {
        mexErrMsgTxt("Type is not cell.");
    }

    mwSize nelements = mxGetNumberOfElements(input);
    output.resize(nelements);
    for (mwIndex i=0; i<nelements; i++) {
        const mxArray* cell = mxGetCell(input,i);
        if (cell != NULL) { // && mxGetN(cell) >= 1) {
            readBoostSimplePolygon2D(cell,output[i],docorrect);
        }
    }
}

void readBoostPolygon2D(const mxArray* input, polygon2D& output, bool docorrect)
{
    if (!mxIsClass(input,"cell")) {
        mexErrMsgTxt("Type is not cell.");
    }
    
    mwSize nelements = mxGetNumberOfElements(input);
    
    if (nelements == 0) {
        output.clear();
        return;
    }
    
    const mxArray* exteriorcell = mxGetCell(input,0);
    if (exteriorcell == NULL) {
        // no exterior boundary
        output.clear();
        return;
    }
    
    // read exterior boundary
    readBoostSimplePolygon2D(exteriorcell,output);
    
    // read interior boundaries
    output.inners().resize(nelements-1);
    auto iringit = boost::begin(boost::geometry::interior_rings(output));
    for (mwIndex i=1; i<nelements; i++) {
        
        const mxArray* interiorcell = mxGetCell(input,i);
        if (interiorcell != NULL) {
            if (!mxIsClass(interiorcell,"double")) {
                mexErrMsgTxt("Type is not double.");
            }
            if (mxGetNumberOfDimensions(interiorcell) != 2 || mxGetM(interiorcell) != 2) {
                mexErrMsgTxt("Input is not an 2D polygon.");
            }
            
            iringit->resize(mxGetN(interiorcell));
            double* interiorcell_p = mxGetPr(interiorcell);
            for (auto it=boost::begin(*iringit); it!=boost::end(*iringit); ++it) {
                it->x(*interiorcell_p++);
                it->y(*interiorcell_p++);
            }
        }
        
        ++iringit;
    }
    
    if (docorrect) {
        boost::geometry::correct(output);
    }
}

void readBoostPolygon2DCellArray(const mxArray* input, std::vector<polygon2D>& output, bool docorrect)
{
    if (!mxIsClass(input,"cell")) {
        mexErrMsgTxt("Type is not cell.");
    }

    mwSize nelements = mxGetNumberOfElements(input);
    output.resize(nelements);
    for (mwIndex i=0; i<nelements; i++) {
        const mxArray* cell = mxGetCell(input,i);
        if (cell != NULL) { // && mxGetN(cell) >= 1) {
            readBoostPolygon2D(cell,output[i],docorrect);
        }
    }
}

void readBoostPolygon2DCellArrayCellArray(const mxArray* input, std::vector<std::vector<polygon2D> >& output, bool docorrect)
{
    if (!mxIsClass(input,"cell")) {
        mexErrMsgTxt("Type is not cell.");
    }

    mwSize nelements = mxGetNumberOfElements(input);
    output.resize(nelements);
    for (mwIndex i=0; i<nelements; i++) {
        const mxArray* cell = mxGetCell(input,i);
        if (cell != NULL) { // && mxGetN(cell) >= 1) {
            readBoostPolygon2DCellArray(cell,output[i],docorrect);
        }
    }
}

void writeBoostPolygon2D(const polygon2D& input, mxArray*& output)
{
    mwSize ninterior = boost::size(boost::geometry::interior_rings(input));
    output = mxCreateCellMatrix(1,1+ninterior);
    if (output == NULL) {
        mexErrMsgTxt("Could not initialize output, probably not enough memory.");
    }
    
    // write exterior boundary
    auto& exteriorring = boost::geometry::exterior_ring(input);
    mxArray* exteriorcell = mxCreateDoubleMatrix(2,boost::size(exteriorring),mxREAL);
    if (exteriorcell == NULL) {
        mexErrMsgTxt("Could not initialize output, probably not enough memory.");
    }
    double* exteriorcell_p = mxGetPr(exteriorcell);
    for (auto it=boost::begin(exteriorring); it!=boost::end(exteriorring); ++it) {
        *exteriorcell_p++ = it->x();
        *exteriorcell_p++ = it->y();
    }
    mxSetCell(output,0,exteriorcell);
    
    // write interior boundaries
    auto iringit = boost::begin(boost::geometry::interior_rings(input));
    for (mwIndex i=1; i<ninterior+1; i++) {
        
        mxArray* interiorcell = mxCreateDoubleMatrix(2,boost::size(*iringit),mxREAL);
        if (interiorcell == NULL) {
            mexErrMsgTxt("Could not initialize output, probably not enough memory.");
        }
        double* interiorcell_p = mxGetPr(interiorcell);
        for (auto it=boost::begin(*iringit); it!=boost::end(*iringit); ++it) {
            *interiorcell_p++ = it->x();
            *interiorcell_p++ = it->y();
        }
        mxSetCell(output,i,interiorcell);
        
        ++iringit;
    }
}
