function compileMlboost(options,utilpath,boostpath)

    % unload mex library if it has already been used
    % (also unloads all other mex libraries)
    clear mex;  %#ok<CLMEX>
    
    includefilepaths = {...
        [boostpath,'/include/'],...    
        [utilpath,'/']};
        
    includeoptions = cellfun(@(x) ['-I',x],includefilepaths,'UniformOutput',false);
    
    if nargin > 0 && any(strcmp(options,'debug'))
        additionaloptions = {'-g'};
    else
        additionaloptions = {};
    end
    
    % not needed at the moment, mex functions only need boost includes
%     if nargin > 0 
%         [libpathoptions,libfileoptions] = liboptions(options);
%     else
%         [libpathoptions,libfileoptions] = liboptions;
%     end

    try
        mex(includeoptions{:},...
            '-largeArrayDims',...
            additionaloptions{:},...
            'polygonPolygonBoolean_mex.cpp',...
            [utilpath,'/mexutils.cpp'],...
            'mlboostutils.cpp');

        mex(includeoptions{:},...
            '-largeArrayDims',...
            additionaloptions{:},...
            'polygonArea_mex.cpp',...
            [utilpath,'/mexutils.cpp'],...
            'mlboostutils.cpp');

        mex(includeoptions{:},...
            '-largeArrayDims',...
            additionaloptions{:},...
            'polygonsetArea_mex.cpp',...
            [utilpath,'/mexutils.cpp'],...
            'mlboostutils.cpp');
    catch err
        if exist(fullfile(pwd,'precompiled.conf'),'file') == 2
            delete('precompiled.conf')
        end
        rethrow(err);
    end
    
    % update precompiled info
    updatePrecompiledInfo('precompiled.conf',{...
        'polygonPolygonBoolean_mex',...
        'polygonArea_mex',...
        'polygonsetArea_mex'},...
        'C++');
end

function [libpathoptions,libfileoptions] = liboptions(options)
    
    % get architecture
    arch = computer('arch');
    % get compiler runtime
    cc = mex.getCompilerConfigurations('C++');
    if isempty(cc)
        mex -setup
        cc = mex.getCompilerConfigurations('C++');
        if isempty(cc)
            error('Cannot compile, no suitable compiler found.');
        end
    end
    rtime = cc.ShortName;
    
    % get filename and path of precompiled library
    if any(strcmp(arch,{'win32','Win32'}))
        error(['No precompiled boostpolygon libraries available for the selected architecture ''',arch,'''.']);
    elseif any(strcmp(arch,{'win64','Win64'}))
        libarch = 'x64';
    else
        error(['No precompiled boost libraries available for the selected architecture ''',arch,'''.']);
    end
    if any(strcmp(rtime,{'MSVC110','MSVCPP110'}))
        librtime = '-vc110';
    elseif any(strcmp(rtime,{'MSVC120','MSVCPP120'}))
        librtime = '-vc120';
    else
        error(['No precompiled boost libraries available for the selected compiler runtime ''',rtime,'''.']);
    end
    
    if nargin > 0 && any(strcmp(options,'debug'))
        libdebug = '-gd';
    else
        libdebug = '';
    end
   
    libfilepaths = {...
        [boostpath,'/lib/',libarch]};
    
    libfilenames = {...
        ['boost_thread',librtime,'-mt',libdebug,'-1_57'],...
        ['boost_system',librtime,'-mt',libdebug,'-1_57']};
    
    
    libpathoptions = cellfun(@(x) ['-L',x],libfilepaths,'UniformOutput',false);
    libfileoptions = cellfun(@(x) ['-l',x],libfilenames,'UniformOutput',false);
end
