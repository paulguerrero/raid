// output = polygonsetArea_mex(poly)
//
// Sums the areas of all polygons in each set.
//
// poly: input set of polygon sets as cell array. Each polygon in the
// set of sets is a cell array of polygon components, first cell is the
// exterior boundary, the following cells are interior boundaries
// each boundary is 2xn matrix of vertices.
// Polygon boundaries must not self-intersect.
//
// output: 1 x n array of summed polygon areas, where n is the number of polygon
// sets.

#include "mex.h"

#include <functional> // std::multiplies
#include <numeric>    // std::accumulate

#include <boost/geometry.hpp>

#include "mexutils.h"
#include "mlboostutils.h"

/* the gateway function */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    if(nrhs != 1) {
        mexErrMsgTxt("One input required.");
    }
    
    if(nlhs != 1) {
        mexErrMsgTxt("One output required.");
    }
    
    mwIndex inputind = 0;    
    
    // read polygon set
    mwSize ndim = mxGetNumberOfDimensions(prhs[inputind]);
    const mwSize* dims = mxGetDimensions(prhs[inputind]);
    std::vector<std::vector<polygon2D> > p;
    readBoostPolygon2DCellArrayCellArray(prhs[inputind],p);
    if (p.size() != std::accumulate(dims,dims+ndim,1,std::multiplies<double>())) {
        mexErrMsgTxt("Polygons count does not match array size, this should not happen.");
    }
    ++inputind;

    
    // compute area and write outputs
    plhs[0] = mxCreateNumericArray(ndim,dims,mxDOUBLE_CLASS,mxREAL);
    if (plhs[0] == NULL) {
        mexErrMsgTxt("Could not initialize output, probably not enough memory.");
    }
    double* op = mxGetPr(plhs[0]);
    for (std::vector<std::vector<polygon2D> >::const_iterator it=p.begin(); it!=p.end(); ++it) {
        *op = 0;
        for (std::vector<polygon2D>::const_iterator iit=it->begin(); iit!=it->end(); ++iit) {
            *op += boost::geometry::area(*iit);
        }
        ++op;
    }
}
