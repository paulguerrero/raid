classdef DatabaseRelationshipSet < handle
    
properties
    % per image
    imgfilenames = cell(1,0);
    imgrelinds = cell(1,0);
    
    % per relationship
    id = zeros(1,0);
    sourceids = cell(1,0);
    targetids = cell(1,0);
    imageind = zeros(1,0);
    sourcelabels = cell(1,0);
    targetlabels = cell(1,0);
    labels = cell(1,0);
    labelweights = cell(1,0);
    descind = nan(1,0);
    
    % per descriptor
    descriptors = ReldisthistDescriptorSet.empty;
    descsrcarea = zeros(1,0); % area of the source region(s) for each descriptor
    descsrcsaliency = zeros(1,0); % saliency of the source region(s) for each descriptor
    
    lockimgfilenames = false; % if true, filenames can only be modified through addImgfilenames or cleanImgfilenames
end

methods
    
function obj = DatabaseRelationshipSet(varargin)
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'DatabaseRelationshipSet')
        obj.copyFrom(varargin{1});
    else
        error('Invalid arguments.');
    end
end

function delete(obj)
    delete(obj.descriptors(isvalid(obj.descriptors)));
end

function copyFrom(obj,obj2)
    obj.imgfilenames = obj2.imgfilenames;
    obj.imgrelinds = obj2.imgrelinds;
    
    obj.id = obj2.id;
    obj.sourceids = obj2.sourceids;
    obj.targetids = obj2.targetids;
    obj.imageind = obj2.imageind;
    obj.sourcelabels = obj2.sourcelabels;
    obj.targetlabels = obj2.targetlabels;
    obj.labels = obj2.labels;
    obj.labelweights = obj2.labelweights;
    obj.descind = obj2.descind; % 0 if it has no descriptor
    
    delete(obj.descriptors(isvalid(obj.descriptors)));
    if not(isempty(obj2.descriptors))
        obj.descriptors = obj2.descriptors.clone;
    else
        obj.descriptors = ReldisthistDescriptorSet.empty;
    end
    obj.descsrcarea = obj2.descsrcarea;
    obj.descsrcsaliency = obj2.descsrcsaliency;
    
    obj.lockimgfilenames = obj2.lockimgfilenames;
end

function obj2 = clone(obj)
    obj2 = DatabaseRelationshipSet(obj);
end

function addImgfilenames(obj,imgfilenames)
    imgfilenames(ismember(imgfilenames,obj.imgfilenames)) = [];
    obj.imgfilenames = [obj.imgfilenames,imgfilenames];
    obj.imgrelinds = [obj.imgrelinds,repmat({zeros(1,0)},1,numel(imgfilenames))];
%     obj.imgfilenames = unique([obj.imgfilenames,imgfilenames],'stable');
end

function removeImgfilenames(obj,imginds)
    
    % remove relationships but keep filenames the same
    delrelinds = cat(1,obj.imgrelinds{imginds});
    origlock = obj.lockimgfilenames;
    obj.lockimgfilenames = true;
    obj.deleteSubset(delrelinds);
    obj.lockimgfilenames = origlock;
    
    % remove filenames and update indices
    [~,delmask,obj.imageind] = delindmap(imginds,...
        numel(obj.imgfilenames),obj.imageind);
    
    obj.imgfilenames(delmask) = [];
    obj.imgrelinds(delmask) = [];
end

function relinds = keepImgsubset(obj,imginds)
    relinds = cat(1,obj.imgrelinds{imginds});
    
    waslocked = obj.lockimgfilenames;
    obj.lockimgfilenames = true;
    obj.keepSubset(relinds);
    obj.lockimgfilenames = waslocked;
    
    old2newmap = nan(1,numel(obj.imgfilenames));
    old2newmap(imginds) = 1:numel(imginds);
    
    obj.imgfilenames = obj.imgfilenames(imginds);
    obj.imageind = old2newmap(obj.imageind);
    
    if any(isnan(obj.imageind))
        error('This should not happen, relationship set is corrupt.');
    end
end

function [obj2,relinds] = cloneImgsubset(obj,imginds)
    relinds = cat(1,obj.imgrelinds{imginds});
    
    waslocked = obj.lockimgfilenames;
    obj.lockimgfilenames = true;
    obj2 = obj.cloneSubset(relinds);
    obj.lockimgfilenames = waslocked;
    
    old2newmap = nan(1,numel(obj2.imgfilenames));
    old2newmap(imginds) = 1:numel(imginds);
    
    obj2.imgfilenames = obj2.imgfilenames(imginds);
    obj2.imageind = old2newmap(obj2.imageind);
    
    if any(isnan(obj2.imageind))
        error('This should not happen, relationship set is corrupt.');
    end
end

function obj2 = cloneSubset(obj,ind)
    obj2 = DatabaseRelationshipSet;
    
    obj2.lockimgfilenames = obj.lockimgfilenames;
    if obj2.lockimgfilenames
        obj2.imgfilenames = obj.imgfilenames;
    end

    obj2.append(obj,ind);
end

function removeDescriptors(obj,descinds)

    if not(isempty(obj.descriptors))
        
        if nargin < 2
            descinds = 1:size(obj.descriptors.val,2);
        end
    
        % remove relationships but keep filenames the same
        obj.descind(ismember(obj.descind,descinds)) = nan;

        % remove filenames and update indices
        [~,delmask,obj.descind] = delindmap(descinds,...
            size(obj.descriptors.val,2),obj.descind);

        delinds = find(delmask);
        obj.descriptors.deleteSubset(delinds);
        obj.descsrcarea(delinds) = [];
        obj.descsrcsaliency(delinds) = [];
    else
        if nargin >= 2 && not(isempty(descinds))
            error('Invalid descriptor indices.');
        end
    end
end

function append(obj,obj2,ind2)

    if nargin < 3
        ind2 = 1:numel(obj2.id);
    end
    
    obj.id = [obj.id,obj2.id(ind2)];
    obj.sourceids = [obj.sourceids,obj2.sourceids(ind2)];
    obj.targetids = [obj.targetids,obj2.targetids(ind2)];
    obj.sourcelabels = [obj.sourcelabels,obj2.sourcelabels(ind2)];
    obj.targetlabels = [obj.targetlabels,obj2.targetlabels(ind2)];
    obj.labels = [obj.labels,obj2.labels(ind2)];
    obj.labelweights = [obj.labelweights,obj2.labelweights(ind2)];
    
%     obj.imageind = [obj.imageind,obj2.imageind+numel(obj.imgfilenames)];
%     [obj.imgfilenames,~,old2newimginds] = unique([obj.imgfilenames,obj2.imgfilenames]);
    
    if not(obj.lockimgfilenames)
        [~,keepmask,mappedimgind] = keepindmap(obj2.imageind(ind2),...
            numel(obj2.imgfilenames),obj2.imageind(ind2));
        obj.imageind = [obj.imageind,mappedimgind+numel(obj.imgfilenames)];
        obj.imgfilenames = [obj.imgfilenames,obj2.imgfilenames(keepmask)];
        
        [obj.imgfilenames,~,old2newimginds] = unique(obj.imgfilenames,'stable');
        old2newimginds = old2newimginds';
        obj.imageind = old2newimginds(obj.imageind);
    else
        [ism,map] = ismember(obj2.imgfilenames,obj.imgfilenames);
        if not(all(ism(obj2.imageind(ind2))))
            error('Need to add new images but images are locked.');
        end
        obj.imageind = [obj.imageind,map(obj2.imageind(ind2))];
    end
    obj.imgrelinds = array2cell_mex(1:numel(obj.imageind),obj.imageind,numel(obj.imgfilenames));
    
    if not(isempty(obj.descriptors)) && not(isempty(obj2.descriptors))
        [~,keepmask,mappeddescind] = keepindmap(obj2.descind(ind2),...
            size(obj2.descriptors.val,2),obj2.descind(ind2));
        obj.descind = [obj.descind,mappeddescind+size(obj.descriptors.val,2)];
        
        keepinds = find(keepmask);
        obj.descriptors.append(obj2.descriptors,keepinds);
        obj.descsrcarea = [obj.descsrcarea,obj2.descsrcarea(keepinds)];
        obj.descsrcsaliency = [obj.descsrcarea,obj2.descsrcarea(keepinds)];
    elseif not(isempty(obj2.descriptors))
        [~,keepmask,mappeddescind] = keepindmap(obj2.descind(ind2),...
            size(obj2.descriptors.val,2),obj2.descind(ind2));
        obj.descind = [obj.descind,mappeddescind];
        
        keepinds = find(keepmask);
        obj.descriptors = obj2.descriptors.cloneSubset(keepinds);
        obj.descsrcarea = obj2.descsrcarea(keepinds);
        obj.descsrcsaliency = obj2.descsrcarea(keepinds);
    else
        obj.descind = [obj.descind,obj2.descind(ind2)];
    end
end

function delmask = cleanImgfilenames(obj)
    delimagemask = cellisempty_mex(obj.imgrelinds,true);
    
    [~,delmask,obj.imageind] = delindmap(find(delimagemask),numel(obj.imgfilenames),obj.imageind);
    obj.imgfilenames(delmask) = [];
end

function copySubset(obj,ind,obj2,ind2)
    
    if nargin < 4
        ind2 = 1:numel(obj2.id);
    end
    
    obj.id(ind) = obj2.id(ind2);
    obj.sourceids(ind) = obj2.sourceids(ind2);
    obj.targetids(ind) = obj2.targetids(ind2);
    obj.sourcelabels(ind) = obj2.sourcelabels(ind2);
    obj.targetlabels(ind) = obj2.targetlabels(ind2);
    obj.labels(ind) = obj2.labels(ind2);
    obj.labelweights(ind) = obj2.labelweights(ind2);
    
    if not(obj.lockimgfilenames)
        cappedind = ind(ind <= numel(obj.imgind)); % because ind may be past the end
        keepmask = true(1,numel(obj.imageind));
        keepmask(cappedind) = false;
        delimageind = obj.imageind(cappedind(not(ismember(obj.imageind(cappedind),obj.imageind(keepmask)))));
        
        [~,delmask,obj.imageind] = delindmap(delimageind,...
            numel(obj.imgfilenames),obj.imageind);
        obj.imgfilenames(delmask) = [];
        
        [~,keepmask,obj.imageind(ind)] = keepindmap(obj2.imageind(ind2),...
            numel(obj2.imgfilenames),obj2.imageind(ind2));
        obj.imageind(ind) = obj.imageind(ind)+numel(obj.imgfilenames);
        obj.imgfilenames = [obj.imgfilenames,obj2.imgfilenames(keepmask)];
        
        [obj.imgfilenames,~,old2newimginds] = unique(obj.imgfilenames,'stable');
        old2newimginds = old2newimginds';
        obj.imageind = old2newimginds(obj.imageind);
    else
        [ism,map] = ismember(obj2.imgfilenames,obj.imgfilenames);
        if not(all(ism(obj2.imageind(ind2))))
            error('Need to add new images but images are locked.');
        end
        obj.imageind(ind) = map(obj2.imageind(ind2));
    end
    obj.imgrelinds = array2cell_mex(1:numel(obj.imageind),obj.imageind,numel(obj.imgfilenames));
    
    if not(isempty(obj.descriptors)) && not(isempty(obj2.descriptors))
        deldescinds = obj.descind(ind(ind <= numel(obj.descind)));
        [~,delmask,obj.descind] = delindmap(deldescinds,...
            size(obj.descriptors.val,2),obj.descind);
        delinds = find(delmask);
        obj.descriptors.deleteSubset(delinds);
        obj.descsrcarea(delinds) = [];
        obj.descsrcsaliency(delinds) = [];
        
        [~,obj.descind(ind)] = keepindmap(obj2.descind(ind2),...
            size(obj2.descriptors.val,2),obj2.descind(ind2));
        obj.descind(ind) = obj.descind(ind) + size(obj.descriptors.val,2);
        keepinds = find(keepmask);
        obj.descriptors.append(obj2.descriptors,keepinds);
        obj.descsrcarea = [obj.descsrcarea,obj2.descsrcarea(keepinds)];
        obj.descsrcsaliency = [obj.descsrcsaliency,obj2.descsrcsaliency(keepinds)];
    elseif not(isempty(obj2.descriptors))
        [~,keepmask,obj.descind(ind)] = keepindmap(obj2.descind(ind2),...
            size(obj2.descriptors.val,2),obj2.descind(ind2));
        keepinds = find(keepmask);
        obj.descriptors = obj2.descriptors.cloneSubset(keepinds);
        obj.descsrcarea = obj2.descsrcarea(keepinds);
        obj.descsrcsaliency = obj2.descsrcsaliency(keepinds);
    else
        obj.descind(ind) = obj2.descind(ind2);
    end
end

function keepSubset(obj,ind)
    
    if isempty(ind)
        ind = zeros(1,0);
    end
    
    obj.id = obj.id(ind);
    obj.sourceids = obj.sourceids(ind);
    obj.targetids = obj.targetids(ind);
    obj.sourcelabels = obj.sourcelabels(ind);
    obj.targetlabels = obj.targetlabels(ind);
    obj.labels = obj.labels(ind);
    obj.labelweights = obj.labelweights(ind);
    
    if not(obj.lockimgfilenames)
        keepmask = false(1,numel(obj.imageind));
        keepmask(ind) = true;
        keepimageind = obj.imageind(ind(ismember(obj.imageind(ind),obj.imageind(keepmask))));

        [~,keepmask,obj.imageind] = keepindmap(keepimageind,...
            numel(obj.imgfilenames),obj.imageind(ind));
        obj.imgfilenames = obj.imgfilenames(keepmask);
    else
        obj.imageind = obj.imageind(ind);
    end
    obj.imgrelinds = array2cell_mex(1:numel(obj.imageind),obj.imageind,numel(obj.imgfilenames));
    
    if not(isempty(obj.descriptors))
        [~,keepmask,obj.descind] = keepindmap(obj.descind(ind),...
            size(obj.descriptors.val,2),obj.descind(ind));
        
        keepinds = find(keepmask);
        obj.descriptors.keepSubset(keepinds);
        obj.descsrcarea = obj.descsrcarea(keepinds);
        obj.descsrcsaliency = obj.descsrcsaliency(keepinds);
    else
        obj.descind = obj.descind(ind);
    end
end

function deleteSubset(obj,ind)
    obj.id(ind) = [];
    obj.sourceids(ind) = [];
    obj.targetids(ind) = [];
    obj.sourcelabels(ind) = [];
    obj.targetlabels(ind) = [];
    obj.labels(ind) = [];
    obj.labelweights(ind) = [];
    
    if not(obj.lockimgfilenames)
        keepmask = true(1,numel(obj.imageind));
        keepmask(ind) = false;
        delimageind = obj.imageind(ind(not(ismember(obj.imageind(ind),obj.imageind(keepmask)))));

        [~,delmask,obj.imageind] = delindmap(delimageind,...
            numel(obj.imgfilenames),obj.imageind);
        obj.imgfilenames(delmask) = [];
    end
    obj.imageind(ind) = [];
    obj.imgrelinds = array2cell_mex(1:numel(obj.imageind),obj.imageind,numel(obj.imgfilenames));
    
    if not(isempty(obj.descriptors))
        [~,delmask,obj.descind] = delindmap(obj.descind(ind),...
            size(obj.descriptors.val,2),obj.descind);
        
        delinds = find(delmask);
        obj.descriptors.deleteSubset(delinds);
        obj.descsrcarea(delinds) = [];
        obj.descsrcsaliency(delinds) = [];
    end
    obj.descind(ind) = [];
end

function inds = findRelationships(obj,sourceids,targetids,imgind,findmode)
    
    if nargin < 5 || isempty(findmode)
        findmode = 'exact';
    end
    
    subsetinds = obj.imgrelinds{imgind};
    
    if strcmp(findmode,'exact')
        
        bothids = cell(1,numel(sourceids));
        for i=1:numel(sourceids)
            bothids{i} = [-[sourceids{i}]-10,[targetids{i}]+10]; % just to avoid cases where the id is -1 or 0
        end

        setbothids = cell(1,numel(subsetinds));
        for i=1:numel(subsetinds)
            setbothids{i} = [...
                -obj.sourceids{subsetinds(i)}-10,...
                obj.targetids{subsetinds(i)}+10];
        end

        [~,inds] = cellismember(bothids,setbothids,false);
        mask = inds>=1;
        inds(mask) = subsetinds(inds(mask));
    
    elseif strcmp(findmode,'contain_all')
        inds = zeros(1,0);
        for i=1:numel(subsetinds)
            if all(ismember(sourceids,obj.sourceids{subsetinds(i)})) && ...
                    all(ismember(targetids,obj.targetids{subsetinds(i)}))
                inds(:,end+1) = i; %#ok<AGROW>
            end
        end
        mask = inds>=1;
        inds(mask) = subsetinds(inds(mask));
        
    elseif strcmp(findmode,'contain_any')
        inds = zeros(1,0);
        for i=1:numel(subsetinds)
            if any(ismember(sourceids,obj.sourceids{subsetinds(i)})) || ...
                    any(ismember(targetids,obj.targetids{subsetinds(i)}))
                inds(:,end+1) = i; %#ok<AGROW>
            end
        end
        mask = inds>=1;
        inds(mask) = subsetinds(inds(mask));
    else
        error('Unknown find mode.');
    end
end

end

methods(Static)

function obj = saveobj(obj)
    % do nothing for now
end

function obj = loadobj(obj)
    if isstruct(obj)
        error('Could not load the object, need to implement this conversion.');
    else

%         if numel(obj.descsrcarea) ~= size(obj.descriptors.val,2) || numel(obj.descsrcsaliency) ~= size(obj.descriptors.val,2)
%             
%             obj.descsrcarea = nan(1,size(obj.descriptors.val,2));
%             obj.descsrcsaliency = nan(1,size(obj.descriptors.val,2));
%             
%             % get annotation path from user
%             annopath = inputdlg('Annotation path:');
%             if isempty(annopath)
%                 obj = DatabaseRelationhipSet.empty;
%                 return;
%             end
%             annopath = cleanfilename(annopath{1});
%             
%             % get area and saliency of the source regions for all
%             % relationships that have a descriptor
%             annotation = ImageAnnotation.empty;
%             relinds = find(obj.descind >= 1);
%             lastimgfilename = '';
%             for i=1:numel(relinds)
%                 imgfilename = obj.imgfilenames{obj.imageind(relinds(i))};
%                 
%                 if not(strcmp(lastimgfilename,imgfilename))
%                     delete(annotation(isvalid(annotation)));
%                     annofilename = ImageAnnotation.img2annofilename(imgfilename);
%                     annotation = AnnotationImporter.importAnnotation([annopath,'/',annofilename]);
%                 end
%                 
%                 imgobjinds = find(ismember([annotation.imgobjects.id],obj.sourceids{relinds(i)}));
%                 
%                 if numel(imgobjinds) ~= numel(obj.sourceids{relinds(i)})
%                     error('Some image objects were not found or there are duplicate image object ids.');
%                 end
%                 
%                 obj.descsrcarea(obj.descind(relinds(i))) = sum([annotation.imgobjects(imgobjinds).area]);
%                 obj.descsrcsaliency(obj.descind(relinds(i))) = max([annotation.imgobjects(imgobjinds).saliency]);
%                 
%                 lastimgfilename = imgfilename;
%             end
%         end
        
        % do nothing for now
    end
end

end

end
