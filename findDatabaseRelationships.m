% rels = findDatabaseRelationships(imgfilenames,annopath)
% rels = findDatabaseRelationships(imgfilenames,annotations)
% rels = findDatabaseRelationships(...,filteroptions)
% filteroptions may be:
% 'labeled' : ['yes','no','any'], only labeled relationships
% 'sourcelbl' : cell array of strings, only relationships with one of the given source labels
% 'targetlbl' : cell array of strings, only relationships with one of the given target labels
% 'ignoreownlabel' : [true,false], do not inlcude relationships where targetlabel == sourcelabel
function [rels,annos,srcobjs,tgtobjs] = findDatabaseRelationships(varargin)

    if numel(varargin) < 1
        error('Invalid arguments.');
    end
    
    if isa(varargin{2},'ImageAnnotation')
        imgfilenames = varargin{1};
        annotations = varargin{2};
        varargin(1) = [];
    else
        imgfilenames = varargin{1};
        if ischar(imgfilenames)
            imgfilenames = {imgfilenames};
        end
        annotations = ImageAnnotation.empty(1,0);
        annopath = varargin{2};
        varargin(1:2) = [];
    end
    
    options = struct(...
        'labeled','any',...
        'sourcelbl',zeros(1,0),...
        'targetlbl',zeros(1,0),...
        'ignoreownlabel',true);
    options = nvpairs2struct(varargin,options);
    
    rels = DatabaseRelationshipSet;
    rels.imgfilenames = imgfilenames;
    rels.imgrelinds = cell(1,numel(imgfilenames));
    
%     srcids = cell(1,0);
%     tgtids = cell(1,0);
%     imgind = zeros(1,0);
%     relid = zeros(1,0);
    
    annos = ImageAnnotation.empty(1,0);
    srcobjs = cell(1,0);
    tgtobjs = cell(1,0);
    for i=1:numel(imgfilenames)
        % get annotation
        if isempty(annotations)
            annofilename = ImageAnnotation.img2annofilename(imgfilenames{i});
            anno = AnnotationImporter.importAnnotation([annopath,'/',annofilename]);
        else
            anno = annotations(i);
        end
        
        % get image objects and labels
        imgobjects = anno.imgobjects;
        imgobjectlbls = [imgobjects.label];
        
        % get requested source and target labels (all labels in the image if none are given)
        if isempty(options.sourcelbl)
            srclbl = unique(imgobjectlbls);
        else
            srclbl = options.sourcelbl;
        end
        if isempty(options.targetlbl)
            tgtlbl = unique(imgobjectlbls);
        else
            tgtlbl = options.targetlbl;
        end
        
        % one set of target regions for each target label
        imgtgtobjs = cell(1,numel(tgtlbl));
        for j=1:numel(tgtlbl)
            imgtgtobjs{j} = {anno.imgobjects(imgobjectlbls == tgtlbl(j))};
        end

        % one set of source regions for each source label
        imgsrcobjs = cell(1,numel(srclbl));
        for j=1:numel(srclbl)
            imgsrcobjs{j} = num2cell(anno.imgobjects(imgobjectlbls == srclbl(j)));
        end
        
        % each source label with each target label
        [srclbl,tgtlbl] = ndgrid(srclbl,tgtlbl);
        srclbl = srclbl(:)';
        tgtlbl = tgtlbl(:)';
        nt = numel(imgtgtobjs);
        ns = numel(imgsrcobjs);
        imgsrcobjs = repmat(imgsrcobjs',1,nt);
        imgtgtobjs = repmat(imgtgtobjs,ns,1);
        imgsrcobjs = imgsrcobjs(:)';
        imgtgtobjs = imgtgtobjs(:)';

        % remove relations between regions of the same label if requested
        if options.ignoreownlabel
            % remove source label to same label (comment out to have things like
            % person crossing persons)
            mask = tgtlbl == srclbl;
            srclbl(mask) = []; %#ok<NASGU>
            tgtlbl(mask) = []; %#ok<NASGU>
            imgsrcobjs(mask) = [];
            imgtgtobjs(mask) = [];
        end

        % relation from each region in a source set to all regions in the
        % corresponding target set
        for j=1:numel(imgsrcobjs)
            imgtgtobjs{j} = imgtgtobjs{j}(ones(1,numel(imgsrcobjs{j})));
        end

        imgsrcobjs = [imgsrcobjs{:}];
        imgtgtobjs = [imgtgtobjs{:}];

        % remove target regions that are == sourceregion
        for j=1:numel(imgsrcobjs)
            imgtgtobjs{j}(imgtgtobjs{j}==imgsrcobjs{j}) = [];
        end
        
        % get relationship infos
        relinds = anno.relationships.findRelationships(imgsrcobjs,imgtgtobjs,'exact');
        labeledmask = relinds >= 1;
        imgrelids = nan(1,numel(relinds));
        imgrellabels = cell(1,numel(relinds));
        imgrellabelweights = cell(1,numel(relinds));
        imgrelids(labeledmask) = anno.relationships.id(relinds(labeledmask));
        imgrellabels(labeledmask) = anno.relationships.labels(relinds(labeledmask));
        imgrellabelweights(labeledmask) = anno.relationships.labelweights(relinds(labeledmask));
        
        % filter by 'labeled' option
        if strcmp(options.labeled,'yes')
            mask = not(isnan(imgrelids));
        elseif strcmp(options.labeled,'no')
            mask = isnan(imgrelids);
        elseif strcmp(options.labeled,'any')
            mask = true(1,numel(imgrelids));
        else
            error('Unknown labeled option.');
        end
        imgsrcobjs = imgsrcobjs(mask);
        imgtgtobjs = imgtgtobjs(mask);
        imgrelids = imgrelids(mask);
        imgrellabels = imgrellabels(mask);
        imgrellabelweights = imgrellabelweights(mask);
        
        % store relationship info
%         relid = [relid,imgrelids]; %#ok<AGROW>
%         offset = numel(srcids);
%         srcids = [srcids,cell(1,numel(imgsrcobjs))]; %#ok<AGROW>
%         tgtids = [tgtids,cell(1,numel(imgsrcobjs))]; %#ok<AGROW>
%         for j=1:numel(imgsrcobjs)
%             srcids{offset+j} = [imgsrcobjs{j}.id];
%             tgtids{offset+j} = [imgtgtobjs{j}.id];
%         end
%         imgind = [imgind,ones(1,numel(imgsrcobjs)).*i]; %#ok<AGROW>
        
        offset = numel(rels.id);        
        rels.id = [rels.id,imgrelids];
        rels.sourceids = [rels.sourceids,cell(1,numel(imgsrcobjs))];
        rels.targetids = [rels.targetids,cell(1,numel(imgsrcobjs))];
        rels.sourcelabels = [rels.sourcelabels,cell(1,numel(imgsrcobjs))];
        rels.targetlabels = [rels.targetlabels,cell(1,numel(imgsrcobjs))];
        for j=1:numel(imgsrcobjs)
            rels.sourceids{offset+j} = [imgsrcobjs{j}.id];
            rels.targetids{offset+j} = [imgtgtobjs{j}.id];
            rels.sourcelabels{offset+j} = [imgsrcobjs{j}.label];
            rels.targetlabels{offset+j} = [imgtgtobjs{j}.label];
        end
        rels.imageind = [rels.imageind,ones(1,numel(imgsrcobjs)).*i];
        rels.labels = [rels.labels,imgrellabels];
        rels.labelweights = [rels.labelweights,imgrellabelweights];
        rels.descind = [rels.descind,nan(1,numel(imgsrcobjs))];
        rels.imgrelinds{i} = offset+1:offset+numel(imgsrcobjs);
        
        % store annotation, source and target objects if requested
        if nargout >= 2
            annos(i) = anno;
        end
        if nargout >= 3
            srcobjs = [srcobjs,imgsrcobjs]; %#ok<AGROW>
            tgtobjs = [tgtobjs,imgtgtobjs]; %#ok<AGROW>
        end
        
        if nargout < 2 && isempty(annotations)
            delete(anno(isvalid(anno)));
        end
    end
end
