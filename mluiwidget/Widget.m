classdef Widget < handle & matlab.mixin.Heterogeneous
    
properties(SetAccess=protected)
    name = '';
    
    isvisible = false;
end

properties(Access=protected)
    blockcallbacks = false;
end

methods

% obj = Widget
% obj = Widget(name)
function obj = Widget(name)
    if nargin == 0
        % do nothing
    elseif nargin == 1
        obj.name = name;
    else
        error('Invalid input arguments.');
    end
end

function setName(obj,name)
    obj.name = name;
end

function setAxes(obj,ax)
    obj.axes = ax;
end

function delete(obj)
    obj.hide;
end

function mousePressed(obj,src,evt) %#ok<INUSD>
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    % ...

    obj.blockcallbacks = false;
end

function mouseMoved(obj,src,evt) %#ok<INUSD>
    if obj.mousedown
        if obj.blockcallbacks
            return;
        end
        obj.blockcallbacks = true;

        % ...

        obj.blockcallbacks = false;
    end
end

function mouseReleased(obj,src,evt) %#ok<INUSD>
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = false;

    % ...

    obj.blockcallbacks = false;
end

function keyPressed(obj,src,evt) %#ok<INUSD>
end

function keyReleased(obj,src,evt) %#ok<INUSD>
end

function show(obj)
    obj.isvisible = true;
end

function hide(obj)
    obj.isvisible = false;
end

function layoutPanels(obj) %#ok<MANU>
end

end

end
