classdef AxesWidget < Widget

properties(SetAccess=protected)
    axes = gobjects(1,0);
    
    axesgraphics = containers.Map('KeyType','char','ValueType','any');
    axesgraphicsVisible = false(1,0);
end

properties(Access=protected)
    mousedown = false;
    clickedpos = [];
    mousepos = [];
    
    visuigroup = UIHandlegroup.empty;
end

methods
    
% obj = AxesWidget
% obj = AxesWidget(axes)
% obj = AxesWidget(axes,name)
function obj = AxesWidget(varargin)
    if numel(varargin) == 2
        superparams = 2;
    else 
        superparams = [];
    end
    
    obj@Widget(varargin{superparams})
    varargin(superparams) = [];
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1
        obj.axes = varargin{1};
    else
        error('Invalid arguments.');
    end
end

function mousePressed(obj,src,evt) %#ok<INUSD>
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = true;

    if not(isempty(obj.axes))
        obj.mousepos = mouseray(obj.axes);
        obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
        obj.clickedpos = obj.mousepos;
    end

    % ...

    obj.blockcallbacks = false;
end

function mouseMoved(obj,src,evt) %#ok<INUSD>
    if obj.mousedown
        if obj.blockcallbacks
            return;
        end
        obj.blockcallbacks = true;

        if not(isempty(obj.axes))
            obj.mousepos = mouseray(obj.axes);
            obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
        end

        % ...

        obj.blockcallbacks = false;
    end
end

function mouseReleased(obj,src,evt) %#ok<INUSD>
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = false;

    % ...

    obj.blockcallbacks = false;
end
    
end
    
end
