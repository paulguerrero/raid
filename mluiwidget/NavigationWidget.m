classdef NavigationWidget < AxesWidget

properties
    zoommode = false;
    panmode = false;
    orbitmode = false;
    
    clickedpos_screen = [];
    mousepos_screen = [];
    
    lastmousepos = [];
    lastmousepos_screen = [];

    navaxes = gobjects(1,1);
    
    zoomenabled = true;
    panenabled = true;
    orbitenabled = true;
end

properties(Dependent)
    navmode;
end
    
properties(Access=protected)
    navpanel = gobjects(1,1);
    
    buttongroup = UIHandlegroup.empty;
    navbutton = gobjects(1,1);
end
    
methods
    
function obj = NavigationWidget(ax,navpanel)
    obj = obj@AxesWidget(ax,'Navigation Widget');
    
    obj.navpanel = navpanel;
    obj.navaxes = ax;
    
    obj.buttongroup = UIHandlegroup;
    
    obj.navbutton = uicontrol(obj.navpanel,...
        'Style','togglebutton',...
        'Units','normalized',...
        'String',['<html><img src="file:///',pwd,'/nav.png">'],...
        'TooltipString',sprintf([...
            'pan: left mouse\n',...
            'zoom: right mouse (or ctrl + left mouse)\n',...
            'orbit: middle mouse (or shift + left mouse)\n']),...    
        'Position',[0,0,1,1],...
        'Callback', {@(src,evt) obj.navPressed},...
        'Enable','on',...
        'Visible','on');
    obj.buttongroup.addChilds(obj.navbutton);
    
    if nargin >= 4
        obj.levelsets = levelsets;
        obj.levelsetinds = levelsetinds;
    end
    
    obj.hide;
end

function delete(obj)
    obj.hide;
    
    delete(obj.buttongroup(isvalid(obj.buttongroup)));
end

function setZoommode(obj,val)
    obj.zoommode = val;
    obj.panmode = false;
    obj.orbitmode = false;
    set(obj.navbutton,'Value',obj.navmode);
end

function setPanmode(obj,val)
    obj.zoommode = false;
    obj.panmode = val;
    obj.orbitmode = false;
    set(obj.navbutton,'Value',obj.navmode);
end

function setOrbitmode(obj,val)
    obj.zoommode = false;
    obj.panmode = false;
    obj.orbitmode = val;
    set(obj.navbutton,'Value',obj.navmode);
end

function navPressed(obj)
    obj.navmode = logical(get(obj.navbutton,'Value'));
end

function set.zoomenabled(obj,val)
    obj.zoomenabled = val;
end

function set.panenabled(obj,val)
    obj.panenabled = val;
end

function set.orbitenabled(obj,val)
    obj.orbitenabled = val;
end

function set.navaxes(obj,val)
    if obj.navaxes ~= val
        obj.clickedpos = [];
        obj.mousepos = [];
        obj.lastmousepos = []; %#ok<MCSUP>
    end
    obj.navaxes = val;
end

function val = get.navmode(obj)
    val = obj.panmode || obj.zoommode || obj.orbitmode;
end

function set.navmode(obj,val)
    if val && not(obj.panmode || obj.zoommode || obj.orbitmode)
        obj.panmode = true;
    elseif not(val) && (obj.panmode || obj.zoommode || obj.orbitmode)
        obj.panmode = false;
        obj.zoommode = false;
        obj.orbitmode = false;
    end
    if isgraphics(obj.navbutton)
        set(obj.navbutton,'Value',obj.navmode);
    end
end

function unblock(obj)
    obj.blockcallbacks = false;
end

function mousePressed(obj,src,evt) %#ok<INUSD>
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = true;

    obj.lastmousepos = obj.mousepos;
    obj.lastmousepos_screen = obj.mousepos_screen;

%     currentaxes = get(obj.fig,'CurrentAxes');
    obj.mousepos = mouseray(obj.navaxes);
    obj.clickedpos = obj.mousepos;

    obj.mousepos_screen = mouseScreenpos;
    obj.clickedpos_screen = obj.mousepos_screen;
    
    if strcmp(get(src,'SelectionType'),'normal') && obj.panenabled
        obj.setPanmode(true);
    elseif strcmp(get(src,'SelectionType'),'alt') && obj.zoomenabled
        obj.setZoommode(true);
    elseif strcmp(get(src,'SelectionType'),'extend') && obj.orbitenabled
        obj.setOrbitmode(true);
    else
        % do nothing (selection type may also be 'open' when double
        % clicking')
%         error('Unknown selection type.'); 
    end
        

    % ...

    obj.blockcallbacks = false;
end

function mouseMoved(obj,src,evt) %#ok<INUSD>
    if obj.mousedown && not(isempty(obj.clickedpos))
        if obj.blockcallbacks
            return;
        end
        obj.blockcallbacks = true;
        
        obj.lastmousepos = obj.mousepos;
        obj.lastmousepos_screen = obj.mousepos_screen;

%         currentaxes = get(obj.fig,'CurrentAxes');
        obj.mousepos = mouseray(obj.navaxes);
        obj.mousepos_screen = mouseScreenpos;

        if obj.zoommode && obj.zoomenabled
            
            campos = get(obj.navaxes,'CameraPosition')';
            camtarget = get(obj.navaxes,'CameraTarget')';
            camviewangle = get(obj.navaxes,'CameraViewAngle');
            camdist = sqrt(sum((camtarget-campos).^2,1));
            camdir = (camtarget-campos) ./ camdist;
            
            mousemovevec = (obj.mousepos_screen - obj.lastmousepos_screen);
            mousemovelen = dot(mousemovevec,[0;1]);
            
            movevec = camdir .* (camdist * mousemovelen*0.1);
            
            set(obj.navaxes,...
                'CameraPosition',(campos + movevec.*0.1)',...
                'CameraViewAngle',camviewangle); % necessary because it automatically changes even though it should be manual
        elseif obj.panmode && obj.panenabled
            % get mouse positions on camera plane
            movevec = obj.clickedpos(:,2)-obj.mousepos(:,2);
            % move camera so the mouse is over the clicked position again
            
            campos = get(obj.navaxes,'CameraPosition')';
            camtarget = get(obj.navaxes,'CameraTarget')';
            
            set(obj.navaxes,...
                'CameraPosition',(campos + movevec)',...
                'CameraTarget',(camtarget + movevec)');
        elseif obj.orbitmode && obj.orbitenabled
%             movelen = sqrt(sum((obj.mousepos(:,1) - obj.clickedpos(:,1)).^2,1));
            
            mousemovevec = (obj.mousepos_screen - obj.lastmousepos_screen);
            mousemovelenx = dot(mousemovevec,[1;0]);
            mousemoveleny = dot(mousemovevec,[0;1]);
            
%             camorbit(mousemovelenx*0.5,mousemoveleny*0.5,'camera',[0,0,1]);
            camorbit(obj.navaxes,-mousemovelenx*0.5,-mousemoveleny*0.5);
    
        
%             len = tan(get(obj.mainaxes,'CameraViewangle')*0.5*(pi/180)) * camtargetdist;
%             disp(len);
        end

        obj.blockcallbacks = false;
    end
end

function mouseReleased(obj,src,evt) %#ok<INUSD>
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = false;
    
    obj.lastmousepos = obj.mousepos;
    obj.lastmousepos_screen = obj.mousepos_screen;

    obj.mousepos = mouseray(obj.navaxes);
    obj.mousepos_screen = mouseScreenpos;

    % ...

    obj.blockcallbacks = false;
end

function keyPressed(obj,src,evt)
    if strcmp(evt.Key,'delete')
        if not(isempty(obj.createpoly))
            obj.createpoly.scenegroup.removeElements(obj.createpoly);
            obj.editor.hideAllSceneFeatures(obj.createpoly);
            obj.createpoly = ScenePolygon.empty;
        end
    end
end

function keyReleased(obj,src,evt)
    % do nothing
end

function show(obj)
	obj.show@AxesWidget;
   
    if isvalid(obj.buttongroup)
        obj.buttongroup.show;
    end
end

function hide(obj)
    obj.hide@AxesWidget;
    
    if isvalid(obj.buttongroup)
        obj.buttongroup.hide;
    end
end

end

end
