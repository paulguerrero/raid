classdef SceneEditor < AxesWidget
    
properties(SetAccess = protected)
    
    scene = Scene.empty;
    
    toolstr = cell(1,0); % temp
    
    tools = Tool.empty;
    
    % ui elements
    navwidget = NavigationWidget.empty;
    
    polygonsVisible = true;
    pointsVisible = true;
    polylinesVisible = true;
    meshesVisible = true;
    
    cornersVisible = false;
    edgesVisible = false;
    segmentsVisible = false;
    surfacesVisible = false;
    
    normalsVisible = false;
    curvatureVisible = false;
    verticesVisible = false;
    
    polygondetailsVisible = false;
%     polygonskeletonVisible = false;
    
    lightsVisible = true;
    
    sidepanelVisible = false;
end

properties(SetObservable, AbortSet)
    selfeatinds = zeros(1,0);
    
    selpanel = zeros(1,0);
    
    selgroup = zeros(1,0);
    
    seltoolind = zeros(1,0);
    
%     tool = '';
end

properties(Dependent, SetAccess=protected)
%     scenegroup;
    selpointfeatinds;
    selpolylinefeatinds;
    selpolygonfeatinds;
    selmeshfeatinds;
    selelmfeatinds;
    selcornerfeatinds;
    seledgefeatinds;
    selsegmentfeatinds;
    selsurfacefeatinds;
end

properties(Access=protected)
    
    fig = gobjects(1,0);
    toolpanel = gobjects(1,0);
    mainpanel = gobjects(1,0);
    sidepanel = gobjects(1,0);
    
    selfeatindsListener = event.listener.empty;
    seltoolindPreListener = event.listener.empty;
    seltoolindListener = event.listener.empty;
    
    uigroup = UIHandlegroup.empty;
    
    % vis menu
    gmenu_polygons = gobjects(1,1);
    gmenu_points = gobjects(1,1);
    gmenu_polylines = gobjects(1,1);
    gmenu_meshes = gobjects(1,1);
    gmenu_corners = gobjects(1,1);
    gmenu_edges = gobjects(1,1);
    gmenu_segments = gobjects(1,1);
    gmenu_surfaces = gobjects(1,1);
    gmenu_normals = gobjects(1,1);
    gmenu_curv = gobjects(1,1);
    gmenu_verts = gobjects(1,1);
    gmenu_polygondetails = gobjects(1,1);
%     gmenu_polygonskeleton = gobjects(1,1);
    gmenu_lights = gobjects(1,1);
    
    % tools menu
    gmenu_elementselect = gobjects(1,0);
    gmenu_cornerselect = gobjects(1,0);
    gmenu_edgeselect = gobjects(1,0);
    gmenu_segmentselect = gobjects(1,0);
    gmenu_surfaceselect = gobjects(1,0);
    
    % axes content
    ggroups = {SceneGroup.empty(1,0);gobjects(1,0)};
    
    gpoints = {ScenePoint.empty(1,0);gobjects(1,0)};
    gpolylines = {ScenePolyline.empty(1,0);gobjects(1,0)};
    gpolygons = {ScenePolygon.empty(1,0);gobjects(1,0)};
    gmeshes = {SceneMesh.empty(1,0);gobjects(1,0)};
    
    gcorners = {SceneCorner.empty(1,0);gobjects(1,0)};
    gedges = {SceneEdge.empty(1,0);gobjects(2,0)};
    gsegments = {SceneBoundarySegment.empty(1,0);gobjects(2,0)};
    gsurfaces = {SceneSurface.empty(1,0);gobjects(1,0)};
    
    gverts = gobjects(1,1);
    gnormals = gobjects(1,1);
    
    gpolygondetails = {ScenePolygon.empty(1,0);gobjects(6,0)};
    
    glights = gobjects(1,0);
    
    mousepos_screen = [];
    clickedpos_screen = [];
    
    lastsceneloadname = '';
    lastscenesavename = '';
    
    lastrelloadname = '';
    lastrelsavename = '';
    
    lastselfeatinds = zeros(1,0);
end

methods
    
function obj = SceneEditor(ax,navwidget,fig,toolpanel,mainpanel,menus)
    obj@AxesWidget(ax);
    obj.setName('Scene Editor');

    obj.navwidget = navwidget;
    
    obj.fig = fig;
    obj.toolpanel = toolpanel;
    obj.mainpanel = mainpanel;
    
    obj.uigroup = UIHandlegroup;
    
    obj.sidepanel = uipanel(obj.fig,...
        'Position',[0,0,1,1],... % will be adjusted in layout panels
        'BorderType','line',...
        'BorderWidth',5,...
        'HighlightColor',[0.7,0.7,0.7],...
        'Visible','off');
    obj.uigroup.addChilds(obj.sidepanel);
    
    % file menu
    obj.uigroup.addChilds(uimenu(menus.File,...
        'Label','Load Scene...',...
        'Callback', {@(src,evt) obj.loadScene},...
        'Separator','on'));
    obj.uigroup.addChilds(uimenu(menus.File,...
        'Label','Save Scene...',...
        'Callback', {@(src,evt) obj.saveScene}));
    
    % vis menu
    obj.gmenu_points = uimenu(menus.Vis,...
        'Label','Points',...
        'Callback', {@(src,evt) obj.togglePointsVisible},...
        'Checked',iif(obj.pointsVisible,'on','off'),...
        'Separator','on');
    obj.uigroup.addChilds(obj.gmenu_points);
    obj.gmenu_polylines = uimenu(menus.Vis,...
        'Label','Polylines',...
        'Callback', {@(src,evt) obj.togglePolylinesVisible},...
        'Checked',iif(obj.polylinesVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_polylines);
    obj.gmenu_polygons = uimenu(menus.Vis,...
        'Label','Polygons',...
        'Callback', {@(src,evt) obj.togglePolygonsVisible},...
        'Checked',iif(obj.polygonsVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_polygons);
    obj.gmenu_meshes = uimenu(menus.Vis,...
        'Label','Meshes',...
        'Callback', {@(src,evt) obj.toggleMeshesVisible},...
        'Checked',iif(obj.meshesVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_meshes);
    obj.gmenu_corners = uimenu(menus.Vis,...
        'Label','Corners',...
        'Callback', {@(src,evt) obj.toggleCornersVisible},...
        'Checked',iif(obj.cornersVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_corners);
    obj.gmenu_edges = uimenu(menus.Vis,...
        'Label','Edges',...
        'Callback', {@(src,evt) obj.toggleEdgesVisible},...
        'Checked',iif(obj.edgesVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_edges);
    obj.gmenu_segments = uimenu(menus.Vis,...
        'Label','Segments',...
        'Callback', {@(src,evt) obj.toggleSegmentsVisible},...
        'Checked',iif(obj.segmentsVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_segments);
    obj.gmenu_surfaces = uimenu(menus.Vis,...
        'Label','Surfaces',...
        'Callback', {@(src,evt) obj.toggleSurfacesVisible},...
        'Checked',iif(obj.surfacesVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_surfaces);
    obj.gmenu_normals = uimenu(menus.Vis,...
        'Label','Normals',...
        'Callback', {@(src,evt) obj.toggleNormalsVisible},...
        'Checked',iif(obj.normalsVisible,'on','off'),...
        'Separator','on');
    obj.uigroup.addChilds(obj.gmenu_normals);
    obj.gmenu_curv = uimenu(menus.Vis,...
        'Label','Curvature',...
        'Callback', {@(src,evt) obj.toggleCurvatureVisible},...
        'Checked',iif(obj.curvatureVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_curv);
    obj.gmenu_verts = uimenu(menus.Vis,...
        'Label','Vertices',...
        'Callback', {@(src,evt) obj.toggleVerticesVisible},...
        'Checked',iif(obj.verticesVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_verts);
    obj.gmenu_polygondetails = uimenu(menus.Vis,...
        'Label','Polygon Details',...
        'Callback', {@(src,evt) obj.togglePolygondetailsVisible},...
        'Checked',iif(obj.polygondetailsVisible,'on','off'),...
        'Separator','on');
    obj.uigroup.addChilds(obj.gmenu_polygondetails);
%     obj.gmenu_polygonskeleton = uimenu(menus.Vis,...
%         'Label','Polygon Skeleton',...
%         'Callback', {@(src,evt) obj.togglePolygonskeletonVisible},...
%         'Checked',iif(obj.polygonskeletonVisible,'on','off'));
%     obj.uigroup.addChilds(obj.gmenu_polygonskeleton);
    obj.gmenu_lights = uimenu(menus.Vis,...
        'Label','Lights',...
        'Callback', {@(src,evt) obj.toggleLightsVisible},...
        'Checked',iif(obj.lightsVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_lights);
    
    % tools menu
    obj.gmenu_elementselect = uimenu(menus.Tools,...
        'Label','Select Elements',...
        'Checked',iif(obj.seltoolind > numel(obj.tools),...
        iif(not(isempty(obj.seltoolind)) && strcmp(obj.toolstr{obj.seltoolind-numel(obj.tools)},'elementselect'),'on','off'),...
        iif(not(isempty(obj.seltoolind)) && strcmp(obj.tools(obj.seltoolind).name,'elementselect'),'on','off')),...
        'Callback', {@(src,evt) obj.selectElementsPressed});
    obj.uigroup.addChilds(obj.gmenu_elementselect);
    obj.gmenu_cornerselect = uimenu(menus.Tools,...
        'Label','Select Corners',...
        'Checked',iif(obj.seltoolind > numel(obj.tools),...
        iif(not(isempty(obj.seltoolind)) && strcmp(obj.toolstr{obj.seltoolind-numel(obj.tools)},'cornerselect'),'on','off'),...
        iif(not(isempty(obj.seltoolind)) && strcmp(obj.tools(obj.seltoolind).name,'cornerselect'),'on','off')),...
        'Callback', {@(src,evt) obj.selectCornersPressed});
    obj.uigroup.addChilds(obj.gmenu_cornerselect);
    obj.gmenu_edgeselect = uimenu(menus.Tools,...
        'Label','Select Edges',...
        'Checked',iif(obj.seltoolind > numel(obj.tools),...
        iif(not(isempty(obj.seltoolind)) && strcmp(obj.toolstr{obj.seltoolind-numel(obj.tools)},'edgeselect'),'on','off'),...
        iif(not(isempty(obj.seltoolind)) && strcmp(obj.tools(obj.seltoolind).name,'edgeselect'),'on','off')),...
        'Callback', {@(src,evt) obj.selectEdgesPressed});
    obj.uigroup.addChilds(obj.gmenu_edgeselect);
    obj.gmenu_segmentselect = uimenu(menus.Tools,...
        'Label','Select Segments',...
        'Checked',iif(obj.seltoolind > numel(obj.tools),...
        iif(not(isempty(obj.seltoolind)) && strcmp(obj.toolstr{obj.seltoolind-numel(obj.tools)},'segmentselect'),'on','off'),...
        iif(not(isempty(obj.seltoolind)) && strcmp(obj.tools(obj.seltoolind).name,'segmentselect'),'on','off')),...
        'Callback', {@(src,evt) obj.selectSegmentsPressed});
    obj.uigroup.addChilds(obj.gmenu_segmentselect);
    obj.gmenu_surfaceselect = uimenu(menus.Tools,...
        'Label','Select Surfaces',...
        'Checked',iif(obj.seltoolind > numel(obj.tools),...
        iif(not(isempty(obj.seltoolind)) && strcmp(obj.toolstr{obj.seltoolind-numel(obj.tools)},'surfaceselect'),'on','off'),...
        iif(not(isempty(obj.seltoolind)) && strcmp(obj.tools(obj.seltoolind).name,'surfaceselect'),'on','off')),...
        'Callback', {@(src,evt) obj.selectSurfacesPressed});
    obj.uigroup.addChilds(obj.gmenu_surfaceselect);
    
    % event listeners
    obj.selfeatindsListener = obj.addlistener('selfeatinds','PostSet',@(src,evt) obj.selfeatindsChanged);
    obj.seltoolindPreListener = obj.addlistener('seltoolind','PreSet',@(src,evt) obj.seltoolindPreChanged);
    obj.seltoolindListener = obj.addlistener('seltoolind','PostSet',@(src,evt) obj.seltoolindChanged);
    
    obj.toolstr = {'elementselect','cornerselect','edgeselect','segmentselect','surfaceselect'};
    
    obj.hide;
end

function delete(obj)
    obj.hide();
    
    delete(obj.selfeatindsListener(isvalid(obj.selfeatindsListener)));
    delete(obj.seltoolindPreListener(isvalid(obj.seltoolindPreListener)));
    delete(obj.seltoolindListener(isvalid(obj.seltoolindListener)));
    
    delete(obj.scene(isvalid(obj.scene)));
    
    delete(obj.uigroup(isvalid(obj.uigroup)));
end

function clear(obj)
    obj.selfeatinds = zeros(1,0);
    obj.selfeaturetype = 0;
%     obj.tool = '';
    obj.seltoolind = zeros(1,0);
end

% function val = get.scenegroup(obj)
%     if isempty(obj.scene) || isempty(obj.scene.rootgroups)
%         val = SceneGroup.empty;
%     else
%         val = obj.scene.rootgroups(1);
%     end
% end

function val = get.selpointfeatinds(obj)
    if isempty(obj.selfeatinds)
        val = zeros(1,0);
    else
%         pointtype = SceneFeatureType.fromName('point');
        val = obj.selfeatinds(...
            strcmp({obj.scene.features(obj.selfeatinds).type},'point'));
    end
end

function val = get.selpolylinefeatinds(obj)
    if isempty(obj.selfeatinds)
        val = zeros(1,0);
    else
%         polylinetype = SceneFeatureType.fromName('polyline');
        val = obj.selfeatinds(...
            strcmp({obj.scene.features(obj.selfeatinds).type},'polyline'));
    end
end

function val = get.selpolygonfeatinds(obj)
    if isempty(obj.selfeatinds)
        val = zeros(1,0);
    else
%         polygontype = SceneFeatureType.fromName('polygon');
        val = obj.selfeatinds(...
            strcmp({obj.scene.features(obj.selfeatinds).type},'polygon'));
    end
end

function val = get.selmeshfeatinds(obj)
    if isempty(obj.selfeatinds)
        val = zeros(1,0);
    else
%         meshtype = SceneFeatureType.fromName('mesh');
        val = obj.selfeatinds(...
            strcmp({obj.scene.features(obj.selfeatinds).type},'mesh'));
    end
end

function val = get.selelmfeatinds(obj)
    if isempty(obj.selfeatinds)
        val = zeros(1,0);
    else
        val = obj.selfeatinds(...
            arrayfun(@(x) isa(x,'SceneElement'),...
            obj.scene.features(obj.selfeatinds)));
    end
end

function val = get.selcornerfeatinds(obj)
    if isempty(obj.selfeatinds)
        val = zeros(1,0);
    else
%         cornertype = SceneFeatureType.fromName('corner');
        val = obj.selfeatinds(...
            strcmp({obj.scene.features(obj.selfeatinds).type},'corner'));
    end
end

function val = get.seledgefeatinds(obj)
    if isempty(obj.selfeatinds)
        val = zeros(1,0);
    else
%         edgetype = SceneFeatureType.fromName('edge');
        val = obj.selfeatinds(...
            strcmp({obj.scene.features(obj.selfeatinds).type},'edge'));
    end
end

function val = get.selsegmentfeatinds(obj)
    if isempty(obj.selfeatinds)
        val = zeros(1,0);
    else
%         segmenttype = SceneFeatureType.fromName('segment');
        val = obj.selfeatinds(...
            strcmp({obj.scene.features(obj.selfeatinds).type},'segment'));
    end
end

function val = get.selsurfacefeatinds(obj)
    if isempty(obj.selfeatinds)
        val = zeros(1,0);
    else
%         surfacetype = SceneFeatureType.fromName('surface');
        val = obj.selfeatinds(...
            strcmp({obj.scene.features(obj.selfeatinds).type},'surface'));
    end
end

function loadScene(obj,filename,varargin)
    
    if abs(nargin) < 2 || isempty(filename)
        if not(isempty(obj.lastsceneloadname))
            loadname = obj.lastsceneloadname;
        else
            loadname = 'tempscene.svg';
        end

        [fname,pathname,~] = ...
            uigetfile({...
            '*.svg','Scalable Vector Graphics (*.svg)';...
            '*.obj','OBJ Mesh format (*.obj)';...
            '*.dae','Collada (*.dae)'},...
            'Load Scene',loadname);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    disp('importing scene...');
    obj.setScene(SceneImporter.importScene(filename,varargin{:}));
    obj.lastsceneloadname = filename;
    disp('done');
end

function saveScene(obj,filename)

    if nargin < 2 || isempty(filename)
        if not(isempty(obj.lastscenesavename))
            savename = obj.lastscenesavename;
        else
            savename = 'tempscene.svg';
        end
        
        [fname,pathname,~] = ...
            uiputfile({...
            '*.svg','Scalable Vector Graphics (*.svg)';...
            '*.obj','OBJ Mesh format (*.obj)';...
            '*.dae','Collada (*.dae)'},...
            'Save Scene',savename);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    disp('exporting scene...');
    SceneExporter.exportScene(filename,obj.scene);
    obj.lastscenesavename = filename;
    disp('done');
end

function unblock(obj)
    obj.blockcallbacks = false;
    obj.navwidget.unblock;
end

function mousePressed(obj,src,evt)

    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;
    
    obj.selpanel = gparent(obj.fig.CurrentObject,'uipanel',true);
    
    obj.mousedown = true;

    obj.mousepos = mouseray(obj.axes);
    obj.clickedpos = obj.mousepos;
    obj.mousepos_screen = mouseScreenpos;
    obj.clickedpos_screen = obj.mousepos_screen;
    
    if obj.selpanel == obj.mainpanel
        if obj.navwidget.zoommode || ...
           obj.navwidget.panmode || ...
           obj.navwidget.orbitmode

            obj.navwidget.mousePressed(src,evt);
        elseif isempty(obj.seltoolind)
            % do nothing
        elseif not(isempty(obj.seltoolind)) && obj.seltoolind <= numel(obj.tools)
            obj.tools(obj.seltoolind).mousePressed(src,evt);
        elseif strcmp(obj.toolstr{obj.seltoolind-numel(obj.tools)},'elementselect')
            obj.selectClosestFeature('element',src.SelectionType);
        elseif strcmp(obj.toolstr{obj.seltoolind-numel(obj.tools)},'cornerselect')
            obj.selectClosestFeature('corner',src.SelectionType);
        elseif strcmp(obj.toolstr{obj.seltoolind-numel(obj.tools)},'edgeselect')
            obj.selectClosestFeature('edge',src.SelectionType);
        elseif strcmp(obj.toolstr{obj.seltoolind-numel(obj.tools)},'segmentselect')
            obj.selectClosestFeature('segment',src.SelectionType);
        elseif strcmp(obj.toolstr{obj.seltoolind-numel(obj.tools)},'surfaceselect')
            obj.selectClosestFeature('surface',src.SelectionType);
        end
    elseif obj.selpanel == obj.sidepanel
        % do nothing
    else
        % do nothing
    end

    obj.blockcallbacks = false;
end

function mouseMoved(obj,src,evt)

    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousepos = mouseray(obj.axes);
    obj.mousepos_screen = mouseScreenpos;

    if obj.selpanel == obj.mainpanel
        if obj.navwidget.zoommode || ...
           obj.navwidget.panmode || ...
           obj.navwidget.orbitmode
            % camera navigation

            obj.navwidget.mouseMoved(src,evt);
        elseif isempty(obj.seltoolind)
            % do nothing
        elseif not(isempty(obj.seltoolind)) && obj.seltoolind <= numel(obj.tools)
            obj.tools(obj.seltoolind).mouseMoved(src,evt);
        elseif strcmp(obj.toolstr{obj.seltoolind-numel(obj.tools)},'elementselect')
            % do nothing
        elseif strcmp(obj.toolstr{obj.seltoolind-numel(obj.tools)},'cornerselect')
            % do nothing
        elseif strcmp(obj.toolstr{obj.seltoolind-numel(obj.tools)},'edgeselect')
            % do nothing
        elseif strcmp(obj.toolstr{obj.seltoolind-numel(obj.tools)},'segmentselect')
            % do nothing
        elseif strcmp(obj.toolstr{obj.seltoolind-numel(obj.tools)},'surfaceselect')
            % do nothing
        end
    elseif obj.selpanel == obj.sidepanel
        % do nothing
    end

    obj.blockcallbacks = false;
end

function mouseReleased(obj,src,evt)

    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = false;

    if obj.selpanel == obj.mainpanel
        if obj.navwidget.zoommode || ...
           obj.navwidget.panmode || ...
           obj.navwidget.orbitmode

            obj.navwidget.mouseReleased(src,evt);
        elseif isempty(obj.seltoolind)
            % do nothing
        elseif not(isempty(obj.seltoolind)) && obj.seltoolind <= numel(obj.tools)
            obj.tools(obj.seltoolind).mouseReleased(src,evt);
        elseif strcmp(obj.toolstr{obj.seltoolind-numel(obj.tools)},'elementselect')
            % do nothing
        elseif strcmp(obj.toolstr{obj.seltoolind-numel(obj.tools)},'cornerselect')
            % do nothing
        elseif strcmp(obj.toolstr{obj.seltoolind-numel(obj.tools)},'edgeselect')
            % do nothing
        elseif strcmp(obj.toolstr{obj.seltoolind-numel(obj.tools)},'segmentselect')
            % do nothing
        elseif strcmp(obj.toolstr{obj.seltoolind-numel(obj.tools)},'surfaceselect')
            % do nothing
        end
    elseif obj.selpanel == obj.sidepanel
        % do nothing
    end

    obj.blockcallbacks = false;
end

function keyPressed(obj,src,evt)
    if strcmp(evt.Key,'1') % space is bound to pressing keys in matlab
        if obj.navwidget.panmode || obj.navwidget.zoommode || obj.navwidget.orbitmode
            obj.navwidget.setPanmode(false);
        else
            obj.navwidget.setPanmode(true);
        end
    elseif strcmp(evt.Key,'c')
        if isempty(obj.selfeatinds)
            resetCamera(obj.axes);
        else
            selfeats = obj.scene.features(obj.selfeatinds);
            selfeatgroups = [selfeats.scenegroup];
            selfeatgroupposes = [selfeatgroups.globaloriginpose];
            
            % get bounding box of all features
            [bbmin,bbmax] = selfeats.boundingbox;
            bbverts = trimeshAACuboid(bbmin,bbmax);
            selfeatgroupposes = repmat(selfeatgroupposes,8,1);
            selfeatgroupposes = reshape(selfeatgroupposes,size(identpose3D,1),[]);
            bbverts = posetransform(bbverts,selfeatgroupposes,repmat(identpose3D,1,size(bbverts,2)));
            bbmin = min(bbverts,[],2);
            bbmax = max(bbverts,[],2);
            bbcenter = (bbmin + bbmax) ./ 2;
            
            vec = bbcenter - obj.axes.CameraTarget';
            
            set(obj.axes,...
                'CameraTarget',bbcenter',...
                'CameraPosition',obj.axes.CameraPosition+vec');
        end
    elseif not(isempty(obj.seltoolind)) && obj.seltoolind <= numel(obj.tools)
        obj.tools(obj.seltoolind).keyPressed(src,evt);
    end
end

function keyReleased(obj,src,evt)
    if not(isempty(obj.seltoolind)) && obj.seltoolind <= numel(obj.tools)
        obj.tools(obj.seltoolind).keyReleased(src,evt);
    end
end

function [lsegdist,startdist,featclosestpoint] = selectClosestFeature(obj,feattype,seltype)
    
    raystart = obj.clickedpos(:,1);
    rayend = obj.clickedpos(:,1) + (obj.clickedpos(:,2) - obj.clickedpos(:,1)) .* 1000;
    
    if strcmp(feattype,'')
        obj.selfeatinds = zeros(1,0);
    elseif strcmp(feattype,'mesh')
        [meshind,lsegdist,startdist,featclosestpoint] = ...
            SceneEditor.closestFeature(...
            raystart,rayend,...
            obj.scene.meshes,...
            obj.axes);

        featind = obj.scene.elm2featind(obj.scene.msh2elmind(meshind));

        obj.selfeatinds = selectMultiple(...
            obj.selfeatinds,featind,seltype);
    elseif strcmp(feattype,'element')
        [elmind,lsegdist,startdist,featclosestpoint] = ...
            SceneEditor.closestFeature(...
            raystart,rayend,...
            obj.scene.elements,...
            obj.axes);

        featind = obj.scene.elm2featind(elmind);

        obj.selfeatinds = selectMultiple(...
            obj.selfeatinds,featind,seltype);
    elseif strcmp(feattype,'corner')
        [cornerind,lsegdist,startdist,featclosestpoint] = ...
            SceneEditor.closestFeature(...
            raystart,rayend,...
            obj.scene.corners,...
            obj.axes);

        featind = obj.scene.cor2featind(cornerind);

        obj.selfeatinds = selectMultiple(...
            obj.selfeatinds,featind,seltype);
    elseif strcmp(feattype,'edge')
        [edgeind,lsegdist,startdist,featclosestpoint] = ...
            SceneEditor.closestFeature(...
            raystart,rayend,...
            obj.scene.edges,...
            obj.axes);

        featind = obj.scene.edge2featind(edgeind);

        obj.selfeatinds = selectMultiple(...
            obj.selfeatinds,featind,seltype);
    elseif strcmp(feattype,'segment')
        [segind,lsegdist,startdist,featclosestpoint] = ...
            SceneEditor.closestFeature(...
            raystart,rayend,...
            obj.scene.segments,...
            obj.axes);

        featind = obj.scene.seg2featind(segind);

        obj.selfeatinds = selectMultiple(...
            obj.selfeatinds,featind,seltype);
    elseif strcmp(feattype,'surface')
        [surfind,lsegdist,startdist,featclosestpoint] = ...
            SceneEditor.closestFeature(...
            raystart,rayend,...
            obj.scene.surfaces,...
            obj.axes);

        featind = obj.scene.surf2featind(surfind);

        obj.selfeatinds = selectMultiple(...
            obj.selfeatinds,featind,seltype);
    else
        error('Unknown feature type for selection.');
    end
end

function setScene(obj,scene)
    if isempty(scene) ~= isempty(obj.scene) || any(scene ~= obj.scene)
        if not(isempty(scene))
            if isempty(scene.rootgroups)
                warning('Scene does not have groups.');
            elseif numel(scene.rootgroups) > 1
                warning('Scene has multiple groups, using the first one.');
            end
        end

        obj.selfeatinds = zeros(1,0);

        obj.hideAxescontents;
        
        delete(obj.scene(isvalid(obj.scene)));
        
        obj.scene = scene;
        
%         % temporary: set all orientations to zero
%         elms = obj.scene.elements;
%         for i=1:numel(elms)
%             elms(i).defineOrientation(0);
%         end
%         obj.scene.updateElements; % corners and edges are re-computed after defineOrientation
        
        obj.updateMainaxes;
        resetCamera(obj.axes);
        
%         obj.appendSceneGraphics(obj.scene.features);
        
        obj.showAxescontents;
    end
end

function selfeatindsChanged(obj)
    changedinds = setxor(obj.lastselfeatinds,obj.selfeatinds);
    obj.showAllSceneFeatures(obj.scene.features(changedinds));
    obj.lastselfeatinds = obj.selfeatinds;
end

function seltoolindPreChanged(obj)
    if isempty(obj.seltoolind)
        return;
    end
    
    if obj.seltoolind > numel(obj.tools)
        toolname = obj.toolstr{obj.seltoolind - numel(obj.tools)};
    else
        obj.tools(obj.seltoolind).hide;
        toolname = obj.tools(obj.seltoolind).name;
    end
        
    if strcmp(toolname,'elementselect')
        obj.gmenu_elementselect.Checked = 'off';
    end
    if strcmp(toolname,'cornerselect')
        obj.gmenu_cornerselect.Checked = 'off';
        if obj.cornersVisible
            obj.toggleCornersVisible;
        end
    end
    if strcmp(toolname,'edgeselect')
        obj.gmenu_edgeselect.Checked = 'off';
        if obj.edgesVisible
            obj.toggleEdgesVisible;
        end
    end
    if strcmp(toolname,'segmentselect')
        obj.gmenu_segmentselect.Checked = 'off';
        if obj.segmentsVisible
            obj.toggleSegmentsVisible;
        end
    end
    if strcmp(toolname,'surfaceselect')
        obj.gmenu_surfaceselect.Checked = 'off';
        if obj.surfacesVisible
            obj.toggleSurfacesVisible;
        end
    end
end

function seltoolindChanged(obj)
    if isempty(obj.seltoolind)
        return;
    end
    
    if obj.seltoolind > numel(obj.tools)
        toolname = obj.toolstr{obj.seltoolind - numel(obj.tools)};
    else
        obj.tools(obj.seltoolind).show;
        toolname = obj.tools(obj.seltoolind).name;
    end
        
    if strcmp(toolname,'elementselect')
        obj.gmenu_elementselect.Checked = 'on';
        if not(obj.polygonsVisible)
            obj.togglePolygonsVisible;
        end
        if not(obj.pointsVisible)
            obj.togglePointsVisible;
        end
        if not(obj.polylinesVisible)
            obj.togglePolylinesVisible;
        end
        if not(obj.meshesVisible)
            obj.toggleMeshesVisible;
        end
    end
    if strcmp(toolname,'cornerselect')
        obj.gmenu_cornerselect.Checked = 'on';
        if not(obj.cornersVisible)
            obj.toggleCornersVisible;
        end
    end
    if strcmp(toolname,'edgeselect')
        obj.gmenu_edgeselect.Checked = 'on';
        obj.showAllSceneEdges;
        if not(obj.edgesVisible)
            obj.toggleEdgesVisible;
        end
    end
    if strcmp(toolname,'segmentselect')
        obj.gmenu_segmentselect.Checked = 'on';
        obj.showAllSceneSegments;
        if not(obj.segmentsVisible)
            obj.toggleSegmentsVisible;
        end
    end
    if strcmp(toolname,'surfaceselect')
        obj.gmenu_surfaceselect.Checked = 'on';
        obj.showAllSceneSurfaces;
        if not(obj.surfacesVisible)
            obj.toggleSurfacesVisible;
        end
    end
end

function selectElementsPressed(obj)
    ind = find(strcmp(obj.toolstr,'elementselect'),1,'first')+numel(obj.tools);
    if any(obj.seltoolind == ind)
        obj.seltoolind = zeros(1,0);
    else
        obj.seltoolind = ind;
    end
end

function selectCornersPressed(obj)
    ind = find(strcmp(obj.toolstr,'cornerselect'),1,'first')+numel(obj.tools);
    if any(obj.seltoolind == ind)
        obj.seltoolind = zeros(1,0);
    else
        obj.seltoolind = ind;
    end
end

function selectEdgesPressed(obj)
    ind = find(strcmp(obj.toolstr,'edgeselect'),1,'first')+numel(obj.tools);
    if any(obj.seltoolind == ind)
        obj.seltoolind = zeros(1,0);
    else
        obj.seltoolind = ind;
    end
end

function selectSegmentsPressed(obj)
    ind = find(strcmp(obj.toolstr,'segmentselect'),1,'first')+numel(obj.tools);
    if any(obj.seltoolind == ind)
        obj.seltoolind = zeros(1,0);
    else
        obj.seltoolind = ind;
    end
end

function selectSurfacesPressed(obj)
    ind = find(strcmp(obj.toolstr,'surfaceselect'),1,'first')+numel(obj.tools);
    if any(obj.seltoolind == ind)
        obj.seltoolind = zeros(1,0);
    else
        obj.seltoolind = ind;
    end
end

function togglePointsVisible(obj)
    obj.pointsVisible = not(obj.pointsVisible);
    if obj.pointsVisible
        set(obj.gmenu_points,'Checked','on');
        obj.showAllScenePoints;
    else
        set(obj.gmenu_points,'Checked','off');
        obj.hideAllScenePoints;
    end    
end

function togglePolylinesVisible(obj)
    obj.polylinesVisible = not(obj.polylinesVisible);
    if obj.polylinesVisible
        set(obj.gmenu_polylines,'Checked','on');
        obj.showAllScenePolylines;
    else
        set(obj.gmenu_polylines,'Checked','off');
        obj.hideAllScenePolylines;
    end    
end

function togglePolygonsVisible(obj)
    obj.polygonsVisible = not(obj.polygonsVisible);
    if obj.polygonsVisible
        set(obj.gmenu_polygons,'Checked','on');
        obj.showAllScenePolygons;
    else
        set(obj.gmenu_polygons,'Checked','off');
        obj.hideAllScenePolygons;
    end    
end

function toggleMeshesVisible(obj)
    obj.meshesVisible = not(obj.meshesVisible);
    if obj.meshesVisible
        set(obj.gmenu_meshes,'Checked','on');
        obj.showAllSceneMeshes;
    else
        set(obj.gmenu_meshes,'Checked','off');
        obj.hideAllSceneMeshes;
    end    
end

function toggleCornersVisible(obj)
    obj.cornersVisible = not(obj.cornersVisible);
    if obj.cornersVisible
        set(obj.gmenu_corners,'Checked','on');
        obj.showAllSceneCorners;
    else
        set(obj.gmenu_corners,'Checked','off');
        obj.hideAllSceneCorners;
    end  
end

function toggleEdgesVisible(obj)
    obj.edgesVisible = not(obj.edgesVisible);
    if obj.edgesVisible
        set(obj.gmenu_edges,'Checked','on');
        obj.showAllSceneCorners;
        obj.showAllSceneEdges;
    else
        set(obj.gmenu_edges,'Checked','off');
        obj.hideAllSceneCorners;
        obj.hideAllSceneEdges;
    end  
end

function toggleSegmentsVisible(obj)
    obj.segmentsVisible = not(obj.segmentsVisible);
    if obj.segmentsVisible
        set(obj.gmenu_segments,'Checked','on');
        obj.showAllSceneSegments;
    else
        set(obj.gmenu_segments,'Checked','off');
        obj.hideAllSceneSegments;
    end  
end

function toggleSurfacesVisible(obj)
    obj.surfacesVisible = not(obj.surfacesVisible);
    if obj.surfacesVisible
        set(obj.gmenu_surfaces,'Checked','on');
        obj.showAllSceneSurfaces;
    else
        set(obj.gmenu_surfaces,'Checked','off');
        obj.hideAllSceneSurfaces;
    end  
end

function toggleNormalsVisible(obj)
    obj.normalsVisible = not(obj.normalsVisible);
    if obj.normalsVisible
        if obj.curvatureVisible
            obj.toggleCurvatureVisible;
        end
        if not(obj.verticesVisible)
            obj.toggleVerticesVisible;
        end
        set(obj.gmenu_normals,'Checked','on');
        obj.showAllSceneNormals;
    else
        set(obj.gmenu_normals,'Checked','off');
        obj.hideAllSceneNormals;
    end
end

function toggleCurvatureVisible(obj)
    obj.curvatureVisible = not(obj.curvatureVisible);
    if obj.curvatureVisible
        if obj.normalsVisible
            obj.toggleNormalsVisible
        end
        if not(obj.verticesVisible)
            obj.toggleVerticesVisible;
        end
        set(obj.gmenu_curv,'Checked','on');
        obj.showAllSceneCurvature;
    else
        set(obj.gmenu_curv,'Checked','off');
        obj.hideAllSceneCurvature;
    end
end

function toggleVerticesVisible(obj)
    obj.verticesVisible = not(obj.verticesVisible);
    if obj.verticesVisible
        set(obj.gmenu_verts,'Checked','on');
        obj.showAllSceneVerts;
    else
        set(obj.gmenu_verts,'Checked','off');
        obj.hideAllSceneVerts;
    end
end

function togglePolygondetailsVisible(obj)
    obj.polygondetailsVisible = not(obj.polygondetailsVisible);
    if obj.polygondetailsVisible
        set(obj.gmenu_polygondetails,'Checked','on');
        obj.showAllScenePolygonDetails;
    else
        set(obj.gmenu_polygondetails,'Checked','off');
        obj.hideAllScenePolygonDetails;
    end
end

% function togglePolygonskeletonVisible(obj)
%     obj.polygonskeletonVisible = not(obj.polygonskeletonVisible);
%     if obj.polygonskeletonVisible
%         set(obj.gmenu_polygonskeleton,'Checked','on');
%         obj.showAllScenePolygonSkeletons;
%     else
%         set(obj.gmenu_polygonskeleton,'Checked','off');
%         obj.hideAllScenePolygonSkeletons;
%     end
% end

function toggleLightsVisible(obj)
    obj.lightsVisible = not(obj.lightsVisible);
    if obj.lightsVisible
        set(obj.gmenu_lights,'Checked','on');
        obj.showAllSceneLights;
    else
        set(obj.gmenu_lights,'Checked','off');
        obj.hideAllSceneLights;
    end
end

function toggleSidepanelVisible(obj)
    obj.sidepanelVisible = not(obj.sidepanelVisible);
    if obj.sidepanelVisible
%         set(obj.gmenu_sidepanel,'Checked','on');
        obj.showSidepanel;
    else
%         set(obj.gmenu_sidepanel,'Checked','off');
        obj.hideSidepanel;
    end
end

function layoutPanels(obj)
    if isgraphics(obj.toolpanel) && ...
       isgraphics(obj.mainpanel) && ...
       isgraphics(obj.sidepanel)
   
        
        mainpanelpos = obj.mainpanel.Position;
        toolpanelpos = obj.toolpanel.Position;
        sidepanelpos = obj.sidepanel.Position;
        
        if obj.toolpanel.Visible
            sidepanelpos(4) = max(1-toolpanelpos(4),0.0001);
            mainpanelpos(4) = max(1-toolpanelpos(4),0.0001);
        else
            sidepanelpos(4) = 1;
            mainpanelpos(4) = 1;
        end
        
        if obj.sidepanelVisible
            sidepanelpos(1) = 0.6;
            sidepanelpos(3) = 0.4;
            
            mainpanelpos(3) = max(1-sidepanelpos(3),0.0001);
        else
            sidepanelpos(1) = 1;
            sidepanelpos(3) = 0;
            
            mainpanelpos(3) = 1;
        end

        if any(obj.mainpanel.Position ~= mainpanelpos)
            obj.mainpanel.Position = mainpanelpos;
        end
        if any(obj.sidepanel.Position ~= sidepanelpos)
            obj.sidepanel.Position = sidepanelpos;
        end
        if any(obj.toolpanel.Position ~= toolpanelpos)
            obj.toolpanel.Position = toolpanelpos;
        end
    end
end

function showSidepanel(obj)
    if isgraphics(obj.sidepanel)
        if not(strcmp(obj.sidepanel.Visible,'on'))
            obj.sidepanel.Visible = 'on';
            obj.layoutPanels;
        end
    end
end

function hideSidepanel(obj)
    if isgraphics(obj.sidepanel)
        if not(strcmp(obj.sidepanel.Visible,'off'))
            obj.sidepanel.Visible = 'off';
            obj.layoutPanels;
        end
    end
end

function updateMainaxes(obj)
    if not(isempty(obj.axes)) && isgraphics(obj.axes)
    
        if isempty(obj.scene) || isempty(obj.scene.elements)
            bbmin = [-1;-1;-1];
            bbmax = [1;1;1];
        else
            fpadding = [0;0;0.05];

            [bbmin,bbmax] = obj.scene.boundingbox;
            if size(bbmin,1) == 2
                bbmin = [bbmin;zeros(1,size(bbmin,2))];
                bbmax = [bbmax;zeros(1,size(bbmax,2))];
            end
            bbmin = min(bbmin,[],2);
            bbmax = max(bbmax,[],2);
            bbdiag = sqrt(sum((bbmax-bbmin).^2,1));
            bbmax = bbmax + bbdiag .* fpadding;
            bbmin = bbmin - bbdiag .* fpadding;
        end
        
        set(obj.axes,...
            'XLim',[bbmin(1),bbmax(1)],...
            'YLim',[bbmin(2),bbmax(2)],...
            'ZLim',[bbmin(3),bbmax(3)]);
    end
end

function showAxescontents(obj)
    obj.showAllSceneGroups;
    
    obj.showAllSceneFeatures;
    
    if obj.normalsVisible
        obj.showAllSceneNormals;
    end
    if obj.curvatureVisible
        obj.showAllSceneCurvature;
    end
    if obj.verticesVisible
        obj.showAllSceneVerts;
    end
    if obj.lightsVisible
        obj.showAllSceneLights;
    end
end

function hideAxescontents(obj)
    obj.hideAllSceneNormals;
    obj.hideAllSceneCurvature;
    obj.hideAllSceneVerts;
    obj.hideAllSceneLights;
    
    obj.hideAllSceneFeatures;
    
    obj.hideAllSceneGroups;
end

function showAllSceneFeatures(obj,features)
    
    if not(isempty(obj.scene))
        if nargin<2 || isempty(features)
            features = obj.scene.features;
        end

        feattypes = {features.type};
        
        groups = features.getScenegroup;
        groups = unique([groups{:}]);
        
        missingmask = not(ismember(groups,obj.ggroups{1}));
        if any(missingmask)
            obj.showAllSceneGroups(groups(missingmask));
        end
        
        if obj.pointsVisible
            points = features(strcmp(feattypes,'point'));
            if not(isempty(points))
                obj.showAllScenePoints(points);
            end
        end   
        if obj.polylinesVisible
            polylines = features(strcmp(feattypes,'polyline'));
            if not(isempty(polylines))
                obj.showAllScenePolylines(polylines);
            end
        end
        if obj.polygonsVisible
            polygons = features(strcmp(feattypes,'polygon'));
            if not(isempty(polygons))
                obj.showAllScenePolygons(polygons);
            end
        end
        if obj.meshesVisible
            meshes = features(strcmp(feattypes,'mesh'));
            if not(isempty(meshes))
                obj.showAllSceneMeshes(meshes);
                
                meshgroups = meshes.getScenegroup;
                meshgroups = unique([meshgroups{:}]);
                if not(isempty(meshgroups))
                    meshchildgroups = [meshgroups.childgroups];
                    meshchildgroups = meshchildgroups(strcmp({meshchildgroups.type},'surfacegroup'));
                    if not(isempty(meshchildgroups))
                        surfacegroups = meshchildgroups(ismember([meshchildgroups.surface],[meshes.surfaces]));
                        if not(isempty(surfacegroups))
                            obj.showAllSceneGroups(surfacegroups);
                        end
                    end
                end
            end
        end
        if obj.cornersVisible
            corners = features(strcmp(feattypes,'corner'));
            if not(isempty(corners))
                obj.showAllSceneCorners(corners);
            end
        end  
        if obj.edgesVisible
            edges = features(strcmp(feattypes,'edge'));
            if not(isempty(edges))
                obj.showAllSceneEdges(edges);
            end
        end  
        if obj.segmentsVisible
            segments = features(strcmp(feattypes,'segment'));
            if not(isempty(segments))
                obj.showAllSceneSegments(segments);
            end
        end
        if obj.surfacesVisible
            surfaces = features(strcmp(feattypes,'surface'));
            if not(isempty(surfaces))
                obj.showAllSceneSurfaces(surfaces);
            end
        end

        if obj.polygondetailsVisible
            polygons = features(strcmp(feattypes,'polygon'));
            if not(isempty(polygons))
                obj.showAllScenePolygonDetails(polygons);
            end
        end
    end
end

function hideAllSceneFeatures(obj,features)
    
    if nargin >= 2
        if isempty(features)
            return;
        end
        
        validmask = isvalid(features);
        invalidfeatures = features(not(validmask));

        obj.hideAllScenePoints(invalidfeatures);
        obj.hideAllScenePolylines(invalidfeatures);
        obj.hideAllScenePolygons(invalidfeatures);
        obj.hideAllSceneMeshes(invalidfeatures);
        obj.hideAllSceneCorners(invalidfeatures);
        obj.hideAllSceneEdges(invalidfeatures);
        obj.hideAllSceneSegments(invalidfeatures);
        obj.hideAllSceneSurfaces(invalidfeatures);

        obj.hideAllScenePolygonDetails(invalidfeatures);        
        
        
        features = features(validmask);
        feattypes = {features.type};
    
%         groups = features.getScenegroup;
%         groups = [groups{:}];
        
        obj.hideAllScenePoints(features(strcmp(feattypes,'point')));
        obj.hideAllScenePolylines(features(strcmp(feattypes,'polyline')));
        obj.hideAllScenePolygons(features(strcmp(feattypes,'polygon')));
        obj.hideAllSceneMeshes(features(strcmp(feattypes,'mesh')));
        obj.hideAllSceneCorners(features(strcmp(feattypes,'corner')));
        obj.hideAllSceneEdges(features(strcmp(feattypes,'edge')));
        obj.hideAllSceneSegments(features(strcmp(feattypes,'segment')));
        obj.hideAllSceneSurfaces(features(strcmp(feattypes,'surface')));

        obj.hideAllScenePolygonDetails(features(strcmp(feattypes,'polygon')));
        
%         obj.hideAllSceneGroups(groups);
    else
        obj.hideAllScenePoints;
        obj.hideAllScenePolylines;
        obj.hideAllScenePolygons;
        obj.hideAllSceneMeshes;
        obj.hideAllSceneCorners;
        obj.hideAllSceneEdges;
        obj.hideAllSceneSegments;
        obj.hideAllSceneSurfaces;

        obj.hideAllScenePolygonDetails;
        
%         obj.hideAllSceneGroups;
    end
end

function showAllSceneGroups(obj,groups)
    if not(isempty(obj.scene)) && not(isempty(obj.scene.groups))
        
        
        if nargin < 2
            groups = obj.scene.groups;
        else
            if any(not(ismember(groups,obj.scene.groups)))
                error('Given group is not in the scene.');
            end
        end
        
        if isempty(groups)
            return;
        end
        
        parentgroups = unique([groups.parentgroup]);
        missingparentmask = not(ismember(parentgroups,obj.ggroups{1}));
        if any(missingparentmask)
            missingparentgroups = parentgroups(missingparentmask);
            groups(ismember(groups,missingparentgroups)) = []; % remove missing parents from set of groups, because they will already be shown by the recursion
            obj.showAllSceneGroups(missingparentgroups); % recurse to show missing parent groups
        end
        
        
        [~,ginds] = ismember(groups,obj.ggroups{1});
        newinds = find(ginds==0);
        if not(isempty(newinds))
            newginds = numel(obj.ggroups{1})+1 : numel(obj.ggroups{1})+numel(newinds);
            ginds(newinds) = newginds;
            obj.ggroups{1}(newginds) = groups(newinds);
            obj.ggroups{2}(newginds) = gobjects(1,numel(newinds));
        end
        
        % todo: do it hierarchically, parent groups first if they do not exist, then child
        % groups
        
%         groups.hierarchydepth = 
        
        gparents = gobjects(1,numel(groups));
        
        parentgroups = {groups.parentgroup};
        emptyparentmask = cellfun(@(x) isempty(x), parentgroups);
        
        if not(all(emptyparentmask))
            nonaxesgparents = [parentgroups{:}];

            [~,nonaxesgparentind] = ismember(nonaxesgparents,obj.ggroups{1});
            if any(isnan(nonaxesgparentind))
                error('Some groups do not have a graphics parent.');
            end

            gparents = gobjects(1,numel(groups));
            gparents(not(emptyparentmask)) = obj.ggroups{2}(nonaxesgparentind);
        end
        if any(emptyparentmask)
            gparents(emptyparentmask) = obj.axes;
        end
        
        obj.ggroups{2}(ginds) = showSceneGroups(...
            groups,obj.ggroups{2}(ginds),gparents);
    else
        obj.hideAllSceneGroups;
    end
end

function hideAllSceneGroups(obj,groups)
    if nargin >= 2
        [~,inds] = ismember(groups,obj.ggroups{1});
        inds(inds==0) = [];
        delete(obj.ggroups{2}(inds(isgraphics(obj.ggroups{2}(inds)))));
        obj.ggroups{1}(inds) = [];
        obj.ggroups{2}(inds) = [];
    else
        delete(obj.ggroups{2}(isgraphics(obj.ggroups{2})));
        obj.ggroups{1} = SceneGroup.empty(1,0);
        obj.ggroups{2} = gobjects(1,0);
    end
end

function showAllScenePoints(obj,points)
    if not(isempty(obj.scene)) && not(isempty(obj.scene.points))
        
        if nargin < 2
            points = obj.scene.points;
        else
            if any(not(ismember(points,obj.scene.points)))
                error('Given point is not in the scene.');
            end
        end
        
        if isempty(points)
            return;
        end
        
        [~,ginds] = ismember(points,obj.gpoints{1});
        newinds = find(ginds==0);
        if not(isempty(newinds))
            newginds = numel(obj.gpoints{1})+1 : numel(obj.gpoints{1})+numel(newinds);
            ginds(newinds) = newginds;
            obj.gpoints{1}(newginds) = points(newinds);
            obj.gpoints{2}(newginds) = gobjects(1,numel(newinds));
        end
        
        selinds = find(ismember(points,obj.scene.features(obj.selpointfeatinds)));
        
        % make selected point red
        colors = repmat(rgbhex2rgbdec('4D4D4D')',1,numel(points));
        
        colors(:,selinds) = repmat(rgbhex2rgbdec('fb8072')',1,numel(selinds));
        
        obj.gpoints{2}(ginds) = showScenePoints(...
            points,obj.gpoints{2}(ginds),obj.featureGParents(obj.gpoints{1}(ginds)),...
            'markerfacecolor',colors);
    else    
        obj.hideAllScenePoints;
    end
end

function hideAllScenePoints(obj,points)
    if nargin >= 2
        [~,inds] = ismember(points,obj.gpoints{1});
        inds(inds==0) = [];
        delete(obj.gpoints{2}(inds(isgraphics(obj.gpoints{2}(inds)))));
        obj.gpoints{1}(inds) = [];
        obj.gpoints{2}(inds) = [];
    else
        delete(obj.gpoints{2}(isgraphics(obj.gpoints{2})));
        obj.gpoints{1} = ScenePoint.empty(1,0);
        obj.gpoints{2} = gobjects(1,0);
    end
end

function showAllScenePolylines(obj,polylines)
	if not(isempty(obj.scene)) && not(isempty(obj.scene.polylines))
        
        if nargin < 2
            polylines = obj.scene.polylines;
        else
            if any(not(ismember(polylines,obj.scene.polylines)))
                error('Given polyline is not in the scene.');
            end
        end
        
        if isempty(polylines)
            return;
        end
        
        [~,ginds] = ismember(polylines,obj.gpolylines{1});
        newinds = find(ginds==0);
        if not(isempty(newinds))
            newginds = numel(obj.gpolylines{1})+1 : numel(obj.gpolylines{1})+numel(newinds);
            ginds(newinds) = newginds;
            obj.gpolylines{1}(newginds) = polylines(newinds);
            obj.gpolylines{2}(newginds) = gobjects(1,numel(newinds));
        end
        
        selinds = find(ismember(polylines,obj.scene.features(obj.selpolylinefeatinds)));
        
        % make selected polyline red
        colors = repmat([0;0;0],1,numel(polylines));
        colors(:,selinds) = repmat(rgbhex2rgbdec('4D4D4D')',1,numel(selinds));
        
        obj.gpolylines{2}(ginds) = showScenePolylines(...
            polylines,obj.gpolylines{2}(ginds),obj.featureGParents(obj.gpolylines{1}(ginds)),...
            'edgecolor',colors);
    else 
        obj.hideAllScenePolylines;
	end
end

function hideAllScenePolylines(obj,polylines)
    if nargin >= 2
        [~,inds] = ismember(polylines,obj.gpolylines{1});
        inds(inds==0) = [];
        delete(obj.gpolylines{2}(inds(isgraphics(obj.gpolylines{2}(inds)))));
        obj.gpolylines{1}(inds) = [];
        obj.gpolylines{2}(inds) = [];
    else
        delete(obj.gpolylines{2}(isgraphics(obj.gpolylines{2})));
        obj.gpolylines{1} = ScenePolyline.empty(1,0);
        obj.gpolylines{2} = gobjects(1,0);
    end
end

function showAllScenePolygons(obj,polygons)
    if not(isempty(obj.scene)) && not(isempty(obj.scene.polygons))
        
        if nargin < 2
            polygons = obj.scene.polygons;
        else
            if any(not(ismember(polygons,obj.scene.polygons)))
                error('Given polygon is not in the scene.');
            end
        end
        
        if isempty(polygons)
            return;
        end
        
        [~,ginds] = ismember(polygons,obj.gpolygons{1});
        newinds = find(ginds==0);
        if not(isempty(newinds))
            newginds = numel(obj.gpolygons{1})+1 : numel(obj.gpolygons{1})+numel(newinds);
            ginds(newinds) = newginds;
            obj.gpolygons{1}(newginds) = polygons(newinds);
            obj.gpolygons{2}(newginds) = gobjects(1,numel(newinds));
        end
        
        selinds = find(ismember(polygons,obj.scene.features(obj.selpolygonfeatinds)));
        
        % make selected polygon red
%         colors = repmat(rgbhex2rgbdec('80b1d3')',1,numel(polygons));
%         colors = repmat(rgbhex2rgbdec('d9d9d9')',1,numel(polygons));
        colors = repmat(rgbhex2rgbdec('4D4D4D')',1,numel(polygons));
%         colors = repmat(rgbhex2rgbdec('808080')',1,numel(polygons));
        colors(:,selinds) = repmat(rgbhex2rgbdec('fb8072')',1,numel(selinds));
        
        % thick selected lines (but slower)
        linewidths = ones(1,numel(polygons));
        linewidths(selinds) = 2;        
        
        obj.gpolygons{2}(ginds) = showScenePolygons(...
            polygons,obj.gpolygons{2}(ginds),obj.featureGParents(obj.gpolygons{1}(ginds)),...
            'edgecolor',mat2cell(colors,3,ones(1,size(colors,2))),...
            'edgewidth',linewidths);

    else    
        obj.hideAllScenePolygons;
    end
end

function gparents = featureGParents(obj,features)
    parents = features.getScenegroup;
    parents = [parents{:}];
    [~,gparentind] = ismember(parents,obj.ggroups{1});
    if any(isnan(gparentind))
        error('Some features do not have a graphics parent.');
    end
    gparents = obj.ggroups{2}(gparentind);
end

function hideAllScenePolygons(obj,polygons)
    if nargin >= 2
        [~,inds] = ismember(polygons,obj.gpolygons{1});
        inds(inds==0) = [];
        delete(obj.gpolygons{2}(inds(isgraphics(obj.gpolygons{2}(inds)))));
        obj.gpolygons{1}(inds) = [];
        obj.gpolygons{2}(inds) = [];
    else
        delete(obj.gpolygons{2}(isgraphics(obj.gpolygons{2})));
        obj.gpolygons{1} = ScenePolygon.empty(1,0);
        obj.gpolygons{2} = gobjects(1,0);
    end
end

function showAllSceneMeshes(obj,meshes)
    if not(isempty(obj.scene)) && not(isempty(obj.scene.meshes))
        
        if nargin < 2
            meshes = obj.scene.meshes;
        else
            if any(not(ismember(meshes,obj.scene.meshes)))
                error('Given mesh is not in the scene.');
            end
        end
        
        if isempty(meshes)
            return;
        end
        
        [~,ginds] = ismember(meshes,obj.gmeshes{1});
        newinds = find(ginds==0);
        if not(isempty(newinds))
            newginds = numel(obj.gmeshes{1})+1 : numel(obj.gmeshes{1})+numel(newinds);
            ginds(newinds) = newginds;
            obj.gmeshes{1}(newginds) = meshes(newinds);
            obj.gmeshes{2}(newginds) = gobjects(1,numel(newinds));
        end
        
        selinds = find(ismember(meshes,obj.scene.features(obj.selmeshfeatinds)));
        
        % make selected corner red
        meshcolors = repmat(rgbhex2rgbdec('ffffb3')',1,numel(meshes));
%         meshcolors = repmat(rgbhex2rgbdec('80b1d3')',1,numel(meshes));
%         meshcolors(:,selinds) = repmat([0.5;1;0.25],1,numel(selinds)); 
        meshcolors(:,selinds) = repmat(rgbhex2rgbdec('fb8072')',1,numel(selinds)); 
%         
%         if not(isempty(obj.neutralelminds))
%             meshcolors(:,obj.neutralelminds) = repmat([0.7;0.7;0.7],1,numel(obj.neutralelminds)); 
%         end
%         if not(isempty(obj.lockedelminds))
%             meshcolors(:,obj.lockedelminds) = repmat([0.8;0.2;0.2],1,numel(obj.lockedelminds)); 
%         end
%         if not(isempty(obj.editelminds))
%             meshcolors(:,obj.editelminds) = repmat([1;1;0.75],1,numel(obj.editelminds)); 
%         end
        
%         obj.gmeshes = showSceneMeshes(...
%             {meshes},obj.gmeshes,obj.axes,...
%             'fvcolor',{meshcolors},...
%             'facecolor','flat',...
%             'edgecolor','none');%,... % [0.5;0.5;0.5]
%             %'lightingbugWorkaround','on');
            
%         obj.gmeshes(inds) = showSceneMeshes(...
%             meshes,obj.gmeshes(inds),obj.axes,...
%             'fvcolor',mat2cell(meshcolors,3,ones(1,size(meshcolors,2))),...
%             'facecolor','flat',...
%             'edgecolor','none');%,... % [0.5;0.5;0.5]
% %             'lightingbugWorkaround','on');

        obj.gmeshes{2}(ginds) = showSceneMeshes(...
            meshes,obj.gmeshes{2}(ginds),obj.featureGParents(obj.gmeshes{1}(ginds)),...
            'facecolor',meshcolors,...
            'edgecolor','none');%,... % [0.5;0.5;0.5]
%             'lightingbugWorkaround','on');
    else    
        obj.hideAllSceneMeshes;
    end
end

function hideAllSceneMeshes(obj,meshes)
    if nargin >= 2
        [~,inds] = ismember(meshes,obj.gmeshes{1});
        inds(inds==0) = [];
        delete(obj.gmeshes{2}(inds(isgraphics(obj.gmeshes{2}(inds)))));
        obj.gmeshes{1}(inds) = [];
        obj.gmeshes{2}(inds) = [];
    else
        delete(obj.gmeshes{2}(isgraphics(obj.gmeshes{2})));
        obj.gmeshes{1} = SceneMesh.empty(1,0);
        obj.gmeshes{2} = gobjects(1,0);
    end
end

function showAllSceneCorners(obj,corners)
    if not(isempty(obj.scene))
        
        corners = obj.scene.corners;
        
        if isempty(corners)
            obj.hideAllSceneCorners;
            return;
        end
        
        selinds = find(ismember(corners,obj.scene.features(obj.selcornerfeatinds)));

        % make selected corners red
        colors = repmat([0;0;1],1,numel(corners));
        colors(:,selinds) = repmat([1;0;0],1,numel(selinds));
        
        obj.gcorners{2} = showSceneCorners(...
            {corners},obj.gcorners{2},obj.axes,...
            'markerfacecolor','flat',...
            'fvcolor',{colors});
    else
        obj.hideAllSceneCorners;    
    end
end

function hideAllSceneCorners(obj,corners)
    delete(obj.gcorners{2}(isgraphics(obj.gcorners{2})));
    obj.gcorners{2} = gobjects(1,0);
end

function showAllSceneEdges(obj,edges)
    if not(isempty(obj.scene))
        
        edges = obj.scene.features(obj.seledgefeatinds);
        
        if isempty(edges)
            obj.hideAllSceneEdges;
            return;
        end
        
        obj.gedges{2} = showSceneSegments(...
            {edges},obj.gedges{2},obj.axes,...
            'edgecolor',[1;0;0],...
            'edgewidth',3);
    else
        obj.hideAllSceneEdges;
    end
end
function hideAllSceneEdges(obj,edges)
    delete(obj.gedges{2}(isgraphics(obj.gedges{2})));
    obj.gedges{2} = gobjects(2,0);
end

function showAllSceneSegments(obj,segments)
    if not(isempty(obj.scene))
        
        error('not implemented yet.')
        
        % todo: finish
        
        segments = obj.scene.segments;
        
        if isempty(segments)
            obj.hideAllSceneSegments;
            return;
        end
        
        selinds = find(ismember(segments,obj.scene.features(obj.selsegmentfeatinds)));

        % make selected segments red
        colors = repmat([0;0;1],1,numel(segments));
        colors(:,selinds) = repmat([1;0;0],1,numel(selinds));
        
        gparents = obj.gsegments{1}.getScenegroup;
        gparents = [gparents{:}];
        
        [gparents,~,ind] = unique(gparents);
        segments = array2cell(segments,ind);
        colors = array2cell(colors,ind);
        
        obj.gsegments{1} = segments;
        
        % todo : sort by group, one per group
        
        obj.gsegments{2} = showSceneSegments(...
            segments,obj.gsegments{2},obj.paxes,...
            'edgecolor',{colors},...
            'edgewidth',3);
    else
        obj.hideAllSceneSegments;
    end
end
function hideAllSceneSegments(obj,segments)
    delete(obj.gsegments{2}(isgraphics(obj.gsegments{2})));
    obj.gsegments{2} = gobjects(2,0);
end

function showAllSceneSurfaces(obj,surfaces)
    if not(isempty(obj.scene)) && not(isempty(obj.scene.surfaces))
        
        if nargin < 2
            surfaces = obj.scene.surfaces;
        else
            if any(not(ismember(surfaces,obj.scene.surfaces)))
                error('Given surface is not in the scene.');
            end
        end
        
        if isempty(surfaces)
            return;
        end
        
        [~,ginds] = ismember(surfaces,obj.gsurfaces{1});
        newinds = find(ginds==0);
        if not(isempty(newinds))
            newginds = numel(obj.gsurfaces{1})+1 : numel(obj.gsurfaces{1})+numel(newinds);
            ginds(newinds) = newginds;
            obj.gsurfaces{1}(newginds) = surfaces(newinds);
            obj.gsurfaces{2}(newginds) = gobjects(1,numel(newinds));
        end
        
        selinds = find(ismember(surfaces,obj.scene.features(obj.selsurfacefeatinds)));
        
        % make selected corner red
        surfacecolors = repmat(rgbhex2rgbdec('bebada')',1,numel(surfaces));
        surfacecolors(:,selinds) = repmat(rgbhex2rgbdec('6f5aff')',1,numel(selinds)); 
        
        edgecolors = repmat({'none'},1,numel(surfaces));
        edgecolors(selinds) = mat2cell(surfacecolors(:,selinds),3,numel(selinds));

        obj.gsurfaces{2}(ginds) = showSceneSurfaces(...
            surfaces,obj.gsurfaces{2}(ginds),obj.featureGParents(obj.gsurfaces{1}(ginds)),...,...
            'facecolor',surfacecolors,...
            'edgecolor',edgecolors);
        
%         disp(isvalid(obj.gsurfaces{2}))
    else    
        obj.hideAllSceneSurfaces;
    end
end

function hideAllSceneSurfaces(obj,surfaces)
    if nargin >= 2
        [~,inds] = ismember(surfaces,obj.gsurfaces{1});
        inds(inds==0) = [];
        delete(obj.gsurfaces{2}(inds(isgraphics(obj.gsurfaces{2}(inds)))));
        obj.gsurfaces{1}(inds) = [];
        obj.gsurfaces{2}(inds) = [];
    else
        delete(obj.gsurfaces{2}(isgraphics(obj.gsurfaces{2})));
        obj.gsurfaces{1} = SceneSurface.empty(1,0);
        obj.gsurfaces{2} = gobjects(1,0);
    end
end

function showAllSceneVerts(obj)
    if not(isempty(obj.scene)) && not(isempty(obj.scene.polygons))
        if isempty(obj.gverts) || not(isgraphics(obj.gverts))
            obj.gverts = patch([0,0],[0,0],0,...
                'Parent',obj.axes,...
                'FaceColor','none',...
                'LineStyle','none',...
                'Marker','o',...
                'MarkerFaceColor','blue',...
                'Visible','off');
            obj.gverts.addprop('Stackz').SetMethod = @changeStackz;
            obj.gverts.Stackz = -0.1; % above lines does not work with line smoothing
        end

%         allpoints = cell2mat([obj.scene.polygons.visrep]);
        allverts = cell2mat(obj.scene.polygons.verts);
        set(obj.gverts,...
            'XData',allverts(1,:),...
            'YData',allverts(2,:),...
            'ZData',zeros(1,size(allverts,2)),...    
            'Visible','on');
    else
        obj.hideAllSceneVerts;
    end
end

function hideAllSceneVerts(obj)
    if isgraphics(obj.gverts)
        delete(obj.gverts)
    end
end

function showAllSceneNormals(obj)
    if not(isempty(obj.scene))
        if isempty(obj.gnormals) ||  not(isgraphics(obj.gnormals))
            obj.gnormals = line([0,0],[0,0],...
                'Parent',obj.axes,...
                'Color','blue',...
                'Visible','off');
            
            obj.gnormals.addprop('Stackz').SetMethod = @changeStackz;
            obj.gnormals.Stackz = 0.35;
        end
        
        allverts = cell(1,0);
        allnormals = cell(1,0);
        
        if not(isempty(obj.scene.polygons))
            allverts{end+1} = {obj.scene.polygons.verts};
            allnormals{end+1} = cell(1,numel(allverts));

            polylinesclosed = [obj.scene.polygons.closed];
            for i=1:numel(allverts{end})
                allnormals{end}{i} = polylineNormals(...
                    allverts{end}{i},polylinesclosed(i));
            end
            allnormals{end} = [allnormals{end}{:}];
            allverts{end} = [allverts{end}{:}];
            allnormals{end}(3,:) = 0;
            allverts{end}(3,:) = 0;
        end
        
        if not(isempty(obj.scene.meshes))
            allverts{end+1} = [allverts,{obj.scene.meshes.verts}];
            allnormals{end+1} = [allnormals,{obj.scene.meshes.vnormals}];
            
            for i=1:numel(allverts{end})
                if isempty(allnormals{end}{i})
                    allnormals{end}{i} = trimeshVertexNormals(obj.scene.meshes(i).verts,obj.scene.meshes(i).faces,'faceavg_area');
                end
            end
            
            allverts{end} = [allverts{end}{:}];
            allnormals{end} = [allnormals{end}{:}];
        end
        
        allverts = [allverts{:}];
        allnormals = [allnormals{:}];
        
        [~,~,sceneextent] = pointsetBoundingbox(allverts);
        displaysize = sceneextent*0.01;
                     
        xdata = [allverts(1,:);allverts(1,:)+allnormals(1,:).*displaysize;nan(1,size(allverts,2))];
        xdata = xdata(:)';
        ydata = [allverts(2,:);allverts(2,:)+allnormals(2,:).*displaysize;nan(1,size(allverts,2))];
        ydata = ydata(:)';
        zdata = [allverts(3,:);allverts(3,:)+allnormals(3,:).*displaysize;nan(1,size(allverts,2))];
        zdata = zdata(:)';
        set(obj.gnormals,'XData',xdata,'YData',ydata,'ZData',zdata,'Visible','on');
    else
        obj.hideAllSceneNormals;
    end
end

function hideAllSceneNormals(obj)
    delete(obj.gnormals(isgraphics(obj.gnormals)));
end

function showAllSceneCurvature(obj)
    
    if not(isempty(obj.scene)) && not(isempty(obj.scene.polygons))
        if isempty(obj.gnormals) ||  not(isgraphics(obj.gnormals))
            obj.gnormals = line([0,0],[0,0],...
                'Parent',obj.axes,...
                'Color','blue',...
                'Visible','off');
            obj.gnormals.addprop('Stackz').SetMethod = @changeStackz;
            obj.gnormals.Stackz = 0.35;
        end
        
        polylines = {obj.scene.polygons.verts};
        polylinesclosed = [obj.scene.polygons.closed];
        
        allverts = cell2mat(polylines);
        allnormals = [];
        allcurv = [];
        for i=1:numel(polylines)
            [polylinecurv,~,polylinenormals,~] = polylineDiffProperties(...
                polylines{i},polylinesclosed(i));
            allnormals = [allnormals,polylinenormals]; %#ok<AGROW>
            allcurv = [allcurv,polylinecurv]; %#ok<AGROW>
        end
        
        length = sign(allcurv).*log(abs(allcurv).*100+1);
        
        xdata = [allverts(1,:);allverts(1,:)+allnormals(1,:).*length.*10;nan(1,size(allverts,2))];
        xdata = xdata(:)';
        ydata = [allverts(2,:);allverts(2,:)+allnormals(2,:).*length.*10;nan(1,size(allverts,2))];
        ydata = ydata(:)';
        set(obj.gnormals,'XData',xdata,'YData',ydata,'Visible','on');
    else
        obj.hideAllSceneCurvature;
    end
end

function hideAllSceneCurvature(obj)
    if isgraphics(obj.gnormals)
        delete(obj.gnormals);
    end
end

function showAllScenePolygonDetails(obj,polygons)
    if not(isempty(obj.scene)) && not(isempty(obj.selpolygonfeatinds))
        
        polygons = obj.scene.features(obj.selpolygonfeatinds);
        
        if isempty(polygons)
            obj.hideAllScenePolygonDetails;
            return;
        end

        obj.gpolygondetails{2} = showScenePolygonDetails(...
            {polygons},obj.gpolygondetails{2},obj.axes);
    else
        obj.hideAllScenePolygonDetails;
    end
end

function hideAllScenePolygonDetails(obj,polygons)
    delete(obj.gpolygondetails{2}(isgraphics(obj.gpolygondetails{2})));
    obj.gpolygondetails{2} = gobjects(6,0);
end

% function showAllScenePolygonSkeletons(obj,polygons)
%     if not(isempty(obj.scene))
% 
%         polygons = obj.scene.features(obj.selpolygonfeatinds);
%         polygons(not(polygons.hasSkeleton)) = [];
%         skels = [polygons.skeleton];
%         
%         if isempty(skels)
%             obj.hideAllScenePolygonSkeletons;
%             return;
%         end
%         
%         obj.gpolygonskeletons = showScenePolygonSkeletons(...
%             {skels},obj.gpolygonskeletons,obj.axes);
%     end
% end
% 
% function hideAllScenePolygonSkeletons(obj)
%     delete(obj.gpolygonskeletons(isgraphics(obj.gpolygonskeletons)));
%     obj.gpolygonskeletons = gobjects(1,0);
% end

function showAllSceneLights(obj)
    
    if not(isempty(obj.scene))
    
        if numel(obj.glights) ~= 2 || any(not(isgraphics(obj.glights)))
            delete(obj.glights(isgraphics(obj.glights)));
            obj.glights = gobjects(1,0);
            obj.glights(1) = light('Parent',obj.axes,'Style','infinite');
            obj.glights(2) = light('Parent',obj.axes,'Style','infinite');
        end

        % for skyscrapers
        [~,~,bbdiag] = obj.scene.boundingbox;
        
        bbdiag = max(bbdiag,10);

%         light1pos = [75.3255;143.8211;110];
%         light2pos = [-75.3255;-143.8211;110];

        light1pos = [bbdiag,bbdiag*2,-bbdiag*1.5]; % negative z because of lighting bug
        light2pos = [-bbdiag,-bbdiag*2,-bbdiag*1.5]; % negative z because of lighting bug

        obj.glights(1).Position = light1pos';
        obj.glights(2).Position = light2pos';
    
%         show dummy points to see where the lights are
%         delete(obj.hlightdummytemp(isvalid(obj.hlightdummytemp)));
%         obj.hlightdummytemp = gobjects(1,0);
%         obj.hlightdummytemp(1) = patch('XData',light1pos(1),'YData',light1pos(2),'ZData',light1pos(3),'Parent',obj.axes,'LineStyle','none','Marker','o','MarkerFaceColor','red');
%         obj.hlightdummytemp(2) = patch('XData',light2pos(1),'YData',light2pos(2),'ZData',light2pos(3),'Parent',obj.axes,'LineStyle','none','Marker','o','MarkerFaceColor','blue');
    else
        obj.hideAllSceneLights;
    end
end

function hideAllSceneLights(obj)
    delete(obj.glights(isgraphics(obj.glights)));
    obj.glights = gobjects(1,0);
end

function show(obj)
    obj.show@AxesWidget;

    if isvalid(obj.uigroup)
        obj.uigroup.show;
    end
    
    if not(isempty(obj.navwidget)) &&  isvalid(obj.navwidget)
        obj.navwidget.show;
    end
    
    obj.layoutPanels;
    
    if isvalid(obj.selfeatindsListener)
        obj.selfeatindsListener.Enabled = true;
    end
    if isvalid(obj.seltoolindPreListener)
        obj.seltoolindPreListener.Enabled = true;
    end
    if isvalid(obj.seltoolindListener)
        obj.seltoolindListener.Enabled = true;
    end
    
    obj.updateMainaxes;
    obj.showAxescontents;
    if obj.sidepanelVisible
        obj.showSidepanel;
    end
end

function hide(obj)
    obj.hide@AxesWidget;
    
    if isvalid(obj.uigroup)
        obj.uigroup.hide;
    end

    if not(isempty(obj.navwidget)) &&  isvalid(obj.navwidget)
        obj.navwidget.hide;
    end
    
    if isvalid(obj.selfeatindsListener)
        obj.selfeatindsListener.Enabled = false;
    end
    if isvalid(obj.seltoolindPreListener)
        obj.seltoolindPreListener.Enabled = false;
    end
    if isvalid(obj.seltoolindListener)
        obj.seltoolindListener.Enabled = false;
    end
    
    obj.hideAxescontents;
    obj.hideSidepanel;
end

end

methods(Static)
    

function [featind,lsegdist,startdist,featclosestpoint] = closestFeature(lsegstart,lsegend,features,axes)
    
    if isempty(features)
        featind = zeros(1,0);
        lsegdist = zeros(1,0);
        startdist = zeros(1,0);
        featclosestpoint = zeros(3,0);
        return;
    end
    
    % approximate ray with very long line segment
    lsegend_extended = lsegstart+(lsegend-lsegstart).*1000;
    
    % compute candidates for the closest feature based on bounding boxes of
    % objects (all objects where the lower bound for the distance is no
    % farther away than the smallest upper bound over all objects)
    [bbmin,bbmax] = features.boundingbox;
    if size(bbmin,1) == 2
        bbmin(3,:) = 0;
        bbmax(3,:) = 0;
    end
    if not(all(features.hasScenegroup))
        error('Some features are not in a scene group.');
    end
    featgroups = features.getScenegroup;
    featgroups = [featgroups{:}];
    featgrouppose = [featgroups.globaloriginpose];
    
    [dlowerbound,~,dupperbound,~] = linesegBoxDistance3D(...
        lsegstart,lsegend_extended,bbmin,bbmax,featgrouppose);
    
    % for safety
    if any(dupperbound.*1.00001 < dlowerbound)
        error('upper distance bound < lower distance bound, this should not happen.');
    end
    
    candidateinds = find(dlowerbound <= min(dupperbound));
    
    candidatefeatures = features(candidateinds);
    
    % compute distance to actual geometry of candidates
    [d,t,featclosestpoint] = candidatefeatures.linesegDistance(...
        lsegstart,lsegend_extended);
    d = d';
    t = t';
    featclosestpoint = [...
        featclosestpoint(:,:,1)';...
        featclosestpoint(:,:,2)';...
        featclosestpoint(:,:,3)'];
    
    if nargin >= 4 && not(isempty(axes))
        % subtract from d and t of line features (as if they were dilated)
        % from 2D features amount r1, from line elements (1D) amount r2 > r1
        % from points (0D) amount r3 > r2. All radii in screen space.
        [~,screenscale] = axes2screen(axes,featclosestpoint);
        d_screen = d .* screenscale;
        t_screen = t .* screenscale;
        featdims = [candidatefeatures.dimension];

        % subtract the correct amount from d_screen and t_screen for a sphere
        % of radius rad1 around the featclosestpoint
        rad1 = 5; % in pixels
        mask = featdims == 1;
        if any(mask)
            d_screen_original = d_screen(mask);
            d_screen(mask) = max(0,d_screen(mask) - rad1);
            t_screen(mask) = t_screen(mask) - sqrt(max(0,rad1^2 - d_screen_original.^2));
        end

        rad0 = 7; % in pixels
        mask = featdims == 0;
        if any(mask)
            d_screen_original = d_screen(mask);
            d_screen(mask) = max(0,d_screen(mask) - rad0);
            t_screen(mask) = t_screen(mask) - sqrt(max(0,rad0^2 - d_screen_original.^2));
        end
    else
        d_screen = d;
        t_screen = t;
    end
    
    mask = d_screen==0;
    if any(mask)
        featind = find(mask);
        [~,minind] = min(t_screen(featind));
        featind = featind(minind);
        startdist = t(featind);
        lsegdist = 0;
    else
        [~,featind] = min(d_screen);
        startdist = t(featind);
        lsegdist = d(featind);
    end
    
    featclosestpoint = featclosestpoint(:,featind);
    
    featind = candidateinds(featind);
end

function [group,pos,localpos] = closestGroup(groups,lsegstart,lsegend)
%     groups = obj.fplan.allgroups;
    
    group = SceneGroup.empty;
    pos = zeros(3,0);

    if isempty(groups)
        return;
    end
    
%     nonsurfacemask = arrayfun(@(x) isempty(x.surface), groups);
%     nonsurfacemask = true(1,numel(groups));
    surfgroupmask = strcmp({groups.type},'surfacegroup');
    
    % intersect groups that have surfaces
    if any(surfgroupmask)
        surfgroups = groups(surfgroupmask);
        surfaces = [surfgroups.surface];
        
        [surfind,surfdist,~,cp] = SceneEditor.closestFeature(...
            lsegstart,lsegend,surfaces);
%         disp(surfaces(surfind).verts);
        
        if not(isempty(surfdist)) && surfdist == 0
            group = surfgroups(surfind);
            pos = cp;
        end
    end
    
    % if no surface group was hit, try non-surface groups
    if any(not(surfgroupmask)) && isempty(group)
        
        nonsurfacegroups = groups(not(surfgroupmask));

        mint = inf;
        for i=1:numel(nonsurfacegroups)
            [p,t] = SceneEditor.rayGroupplaneIntersection(...
                nonsurfacegroups(i),lsegstart,lsegend);
            if not(isempty(p)) && t < mint
                group = nonsurfacegroups(i);
                pos = p;
                mint = t;
            end
        end

    end
    
    if nargout >= 3 && not(isempty(group))
        localpos = group.localpose(pos);
    end
end

function [pos,t,localpos] = rayGroupplaneIntersection(group,lsegstart,lsegend)
    grppose = group.globaloriginpose;
    
    grpnormal = quat2dcm(grppose(4:7)')' * [0;0;1];
    grporigin = grppose(1:3);
    
    [pos,t,itype] = linePlaneIntersection(grpnormal,grporigin,lsegstart,lsegend);
    
    if not((itype == 1 || itype == 3) && t >= 0)
        % lineseg is parallel (either coplanar or not touching)
        pos = [];
        t = [];
        if nargout >= 3
            localpos = [];
        end
    else
        if nargout >= 3
            localpos = posetransform(pos,identpose3D,grppose);
        end
    end
end

end

end 
