classdef EditorFigure < handle

% if editor is slow:
% com.mathworks.services.Prefs.setBooleanPref('CodeParserServiceOn',false)
    
properties(SetAccess=protected)
    unitsVisible = false;
    toolpanelVisible = true;
    navpanelVisible = true;
    
    editors = cell(2,0); % first row: editors, second row: entries in the editor menu
    
    % figure and axes
    fig = -1;
    mainaxes = -1;
    
    % gui that is displayed outside the axes
    mainpanel = gobjects(1,1);
    navpanel = gobjects(1,1);
    toolpanel = gobjects(1,1);
    fileMenu = gobjects(1,1);
    fileMenu_screenshot = gobjects(1,1);
    visMenu = gobjects(1,1);
    visMenu_toolpanel = gobjects(1,1);
    visMenu_navpanel = gobjects(1,1);
	actionsMenu = gobjects(1,1);
    toolsMenu = gobjects(1,1);
    editorMenu = gobjects(1,1);
    settingsMenu = gobjects(1,1);
    settingsMenu_units = gobjects(1,1);
    helpMenu = gobjects(1,1);
    
end % properties

properties(SetAccess=protected,SetObservable,AbortSet)
    seleditor = Widget.empty;
end

properties(Access=protected)
    toolpanelheight = 100;
    
    seleditorPresetListener = event.listener.empty;
    seleditorPostsetListener = event.listener.empty;
    
    lastscreenshotsavename = [];
    
    clickedpos = [];
    mousepos = [];
    mousedown = false;
    
    shiftkeydown = false;
    ctrlkeydown = false;
    
    stopcallbacks = false;
end

methods
    
function obj = EditorFigure
    
%     % to disable class changed warnings:
%     warning('off','MATLAB:class:cannotUpdateClass:Changed');

    % turn off warning for merging structs with same field names (to merge
    % settings for operations)
    warning('off','catstruct:DuplicatesFound');
   
    obj.fig = figure(...
        'Position', [20,20,1000,1000], ...
        'Renderer','opengl',...
        'Menubar','none',...
        'Toolbar', 'none', ...
        'Name','Editor', ...
        'NumberTitle','off',...
        'DockControls','off',...
        'CloseRequestFcn', {@(src,event) closed(obj,src,event)}, ...    
        'WindowButtonDownFcn', {@(src,event) mousePressed(obj,src,event)}, ...
        'WindowButtonUpFcn', {@(src,event) mouseReleased(obj,src,event)}, ...
        'WindowButtonMotionFcn', {@(src,event) mouseMoved(obj,src,event)}, ...
        'KeyPressFcn', {@(src,event) keyPressed(obj,src,event)}, ...
        'WindowKeyReleaseFcn', {@(src,event) keyReleased(obj,src,event)}, ...
        'SizeChangedFcn', {@(src,event) figureResized(obj,src,event)},...
        'Visible','off');

    obj.mainpanel = uipanel(obj.fig,...
        'BackgroundColor','white',...
        'Position',[0,0,1,0.9],...
        'BorderType','none');
    obj.navpanel = uipanel(obj.fig,...
        'Position',[0,0.9,0.2,0.1],...
        'BorderWidth',5,...
        'BorderType','line',...
        'HighlightColor', get(0, 'DefaultUIControlBackgroundColor'));
    obj.toolpanel = uipanel(obj.fig,...
        'Position',[0.2,0.9,0.8,0.1],...
        'BorderWidth',5,...
        'BorderType','line',...
        'HighlightColor', get(0, 'DefaultUIControlBackgroundColor'));

    obj.mainaxes = axes(...
        'Parent',obj.mainpanel,...
        'DataAspectRatio',[1 1 1],...
        'ALim',[0,1],...
        'CLim',[0,1],...
        'XLim',[-1,1],...
        'YLim',[-1,1],...
        'ZLim',[-1,1],...
        'CameraViewAngleMode','manual',...
        'CameraViewAngle',5,... % small angle so that the near and far clipping planes are not too close, projection is orthogonal anyways
        'CameraPositionMode','manual',...
        'CameraTargetMode','manual',...
        'CameraUpVectorMode','manual',...
        'CameraViewAngleMode','manual',...    
        'FontSize',8,...
        'XTick',[],...
        'YTick',[],...
        'ZTick',[],...
        'XGrid','off',...
        'YGrid','off',...
        'ActivePositionProperty','OuterPosition',...
        'LooseInset',[0,0,0,0],...
        'OuterPosition',[0,0,1,1],...
        'Box','on',...
        'Clipping','off',...
        'Visible','off');

    % File menu
    obj.fileMenu = uimenu(obj.fig,...
        'Label','File');
    obj.fileMenu_screenshot = uimenu(obj.fileMenu,...
        'Label','Screenshot...',...
        'Callback', {@(src,event) obj.saveScreenshot});

    % Vis menu
    obj.visMenu = uimenu(obj.fig,...
        'Label','Vis');
    obj.visMenu_toolpanel = uimenu(obj.visMenu,...
        'Label','Toolpanel',...
        'Callback', {@(src,event) obj.toggleToolpanelVisible},...
        'Checked',iif(obj.toolpanelVisible,'on','off'));
    obj.visMenu_navpanel = uimenu(obj.visMenu,...
        'Label','Navigation Panel',...
        'Callback', {@(src,event) obj.toggleNavpanelVisible},...
        'Checked',iif(obj.navpanelVisible,'on','off'));

    % Actions menu (one-time actions that do not change ui interaction behaviour)
    obj.actionsMenu = uimenu(obj.fig,...
        'Label','Actions');
    
    % Tools menu (tools that change ui interaction behaviour when activated)
    obj.toolsMenu = uimenu(obj.fig,...
        'Label','Tools');
    
    % Editor menu
    obj.editorMenu = uimenu(obj.fig,...
        'Label','Editor');

    % Settings menu
    obj.settingsMenu = uimenu(obj.fig,...
        'Label','Settings');
    obj.settingsMenu_units = uimenu(obj.settingsMenu,...
        'Label','Show Units',...
        'Checked',iif(obj.unitsVisible,'on','off'),...
        'Callback', {@(src,event) obj.toggleDisplayUnits});
    
    % Help menu
    obj.helpMenu = uimenu(obj.fig,...
        'Label','Help');
    
%     % rgb color data is not supported in painters mode, ignore the warning
%     % (probably not needed anyways since everything is done in opengl)
%     warning('off','MATLAB:hg:patch:RGBColorDataNotSupported');

    obj.seleditorPresetListener = obj.addlistener('seleditor','PreSet',@(src,evt) obj.seleditorPreChanged);
    obj.seleditorPostsetListener = obj.addlistener('seleditor','PostSet',@(src,evt) obj.seleditorPostChanged);    
    
    set(obj.fig,'Visible','on');
end

function delete(obj)
    
    delete(obj.seleditorPresetListener(isvalid(obj.seleditorPresetListener)));
    delete(obj.seleditorPostsetListener(isvalid(obj.seleditorPostsetListener)));
    
    % widgets are not owned by this class
    
    % delete main figure
    if isgraphics(obj.fig)
        delete(obj.fig);
    end
end

function addEditor(obj,editor,separator)

    if nargin < 3 || isempty(separator)
        separator = false;
    end
    
    editornames = cellfun(@(x) x.name, obj.editors(1,:), 'UniformOutput',false);
    
    if any(strcmp(editor.name, editornames))
        error('An editor with this name already exists.');
    end
    
    if separator
        separator = 'on';
    else
        separator = 'off';
    end
    
    obj.editors(:,end+1) = {...
        editor,...
        uimenu(obj.editorMenu,...
            'Label',editor.name,...
            'Separator',separator,...
            'Callback',{@(src,evt) obj.editormenuChanged(src,evt)})};
end

function removeEditor(obj,name)
    editornames = cellfun(@(x) x.name, obj.editors(1,:), 'UniformOutput',false);
    
    removeind = find(strcmp(name,editornames));
    
    if not(isempty(removeind))
        editormenu = obj.editors{2,removeind};
        delete(editormenu(isvalid(editormenu)));
        obj.editors(:,removeind) = [];
    else
        % do nothing if the name is not in the list
    end
end

function editormenuChanged(obj,src,evt) %#ok<INUSD>
    editorind = find(src == [obj.editors{2,:}]);
    
    if isempty(editorind)
        error('Menu entry refers to non-existent editor.');
    end
    
    obj.seleditor = obj.editors{1,editorind};
end

function seleditorPreChanged(obj)
    if not(isempty(obj.seleditor))
        editorind = find(cellfun(@(x) obj.seleditor == x,obj.editors(1,:)));

        if isempty(editorind)
            error('Selected editor is not in the editor list.');
        end
        
        obj.editors{1,editorind}.hide;
        obj.editors{2,editorind}.Checked = 'off';
    end
end

function seleditorPostChanged(obj)
    editorind = find(cellfun(@(x) obj.seleditor == x,obj.editors(1,:)));
    
    if isempty(editorind)
        error('Selected editor is not in the editor list.');
    end
    
    obj.fig.Name = obj.seleditor.name;
    
    obj.editors{1,editorind}.show;
    obj.editors{2,editorind}.Checked = 'on';

    obj.layoutPanels;
end

function focusFigure(obj)
    figure(obj.fig);
end

function layoutPanels(obj)
    % do layout
    figureUnits = obj.fig.Units;
    toolpanelUnits = obj.toolpanel.Units;
    mainpanelUnits = obj.mainpanel.Units;
    navpanelUnits = obj.navpanel.Units;

    obj.fig.Units = 'pixels';
    obj.toolpanel.Units = 'pixels';
    obj.navpanel.Units = 'pixels';

    figurepos = obj.fig.Position;
    toolpanelpos = obj.toolpanel.Position;
    navpanelpos = obj.navpanel.Position;

    if obj.toolpanelVisible
        toolpanelpos(2) = figurepos(4)-obj.toolpanelheight;
        toolpanelpos(4) = obj.toolpanelheight;
        toolpanelpos(1) = 50;
        toolpanelpos(3) = figurepos(3)-50;
    else
        toolpanelpos(2) = figurepos(4);
        toolpanelpos(4) = 0;
        toolpanelpos(1) = 50;
        toolpanelpos(3) = figurepos(3)-50;
    end
    obj.toolpanel.Position = toolpanelpos;

    if obj.navpanelVisible
        navpanelpos(2) = figurepos(4)-obj.toolpanelheight;
        navpanelpos(4) = obj.toolpanelheight;
        navpanelpos(1) = 0;
        navpanelpos(3) = 50;
    else
        navpanelpos(2) = figurepos(4)-0;
        navpanelpos(4) = 0;
        navpanelpos(1) = 0;
        navpanelpos(3) = 50;
    end
    obj.navpanel.Position = navpanelpos;

    obj.fig.Units = 'normalized';
    obj.toolpanel.Units = 'normalized';
    obj.mainpanel.Units = 'normalized';
    obj.navpanel.Units = 'normalized';

    toolpanelpos = obj.toolpanel.Position;

    mainpanelpos = obj.mainpanel.Position;
    mainpanelpos(4) = 1-toolpanelpos(4);
    mainpanelpos(3:4) = max(mainpanelpos(3:4),0.0001);
    obj.mainpanel.Position = mainpanelpos;

    obj.fig.Units = figureUnits;
    obj.toolpanel.Units = toolpanelUnits;
    obj.mainpanel.Units = mainpanelUnits;
    obj.navpanel.Units = navpanelUnits;
    
    if obj.navpanelVisible && not(obj.toolpanelVisible)
        obj.navpanel.HighlightColor = 'white';
    else
        obj.navpanel.HighlightColor = obj.toolpanel.HighlightColor;
    end
    
    if not(isempty(obj.seleditor))
        obj.seleditor.layoutPanels;
    end
end

function figureResized(obj,src,evt) %#ok<INUSD>
    obj.layoutPanels;
end

function saveScreenshot(obj,filename,targetresolution)
    
    if nargin < 3 || isempty(targetresoltion)
        targetresolution = [1920,1080];
    end
    
    if nargin < 2 || isempty(filename)
        if not(isempty(obj.lastscreenshotsavename))
            savename = obj.lastscreenshotsavename;
        else
%             [path,name,~] = fileparts(obj.filename);
%             savename = [path,'/',name,'_screenshot.svg'];
            savename = './screenshot.svg';
        end

%         [fname,pathname,ftype] = ...
%             uiputfile({'*.svg','Scalable Vector Graphics (*.svg)';'*.obj','OBJ Mesh File (*.obj)'},'Save Screenshot',savename);
        [fname,pathname,~] = ...
            uiputfile({'*.svg','Scalable Vector Graphics (*.svg)';'*.png','PNG Image File (*.png)'},'Save Screenshot',savename);

        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,'/',fname];
        else
            return;
        end
    end
    
   
    [~,~,ext] = fileparts(filename);
    
    if strcmp(ext,'.png')
        % png screenshot
        currentunits = obj.mainaxes.Units;
        obj.mainaxes.Units = 'pixels';

%         axsize = floor(obj.mainaxes.Position(3:4));
%         margin = (axsize-targetresolution)./2;
%         axrect = [[0,0]+max(0,margin),axsize-max(0,margin)];
%         axrect(3:4) = axrect(3:4)-axrect(1:2); % convert to width,height
%         F = getframe(obj.mainaxes,axrect);
%         if not(isempty(F.colormap))
%             error('Image screenshots from a non-true color system are not supported yet.');
%         end
%         img = ones(targetresolution(2),targetresolution(1),3,'uint8').*255;
%         imgarea = [[0,0]+abs(min(0,margin)),targetresolution-abs(min(0,margin))];
%         imgarea_pixinds = [ceil(imgarea(1:2)+0.5),floor(imgarea(3:4)+0.5)];
%         img(imgarea_pixinds(2):imgarea_pixinds(4),imgarea_pixinds(1):imgarea_pixinds(3),:) = F.cdata;

        F = getframe(obj.mainaxes);
        if not(isempty(F.colormap))
            error('Image screenshots from a non-true color system are not supported yet.');
        end
        F.cdata = imresize(F.cdata,min(targetresolution./[size(F.cdata,2),size(F.cdata,1)]));
        img = ones(targetresolution(2),targetresolution(1),3,'uint8').*255;
        margin = [size(img,2),size(img,1)] - [size(F.cdata,2),size(F.cdata,1)];
        imgarea_pixinds = [ceil(margin./2)+1,ceil(margin./2)+[size(F.cdata,2),size(F.cdata,1)]];
        img(imgarea_pixinds(2):imgarea_pixinds(4),imgarea_pixinds(1):imgarea_pixinds(3),:) = F.cdata;
        
        obj.mainaxes.Units = currentunits;


        imwrite(img,filename,'png');
    elseif strcmp(ext,'.svg')
        % svg screenshot
        graphics2svg(obj.mainaxes,filename,[],[],[],[],[],10); % scaled up 100
    else
        error('Unknown file type.');
    end
    obj.lastscreenshotsavename = filename;
end

function unblock(obj)
    obj.stopcallbacks = false;
    obj.seleditor.unblock;
end

function mousePressed(obj,src,event)

    if not(obj.stopcallbacks)
        obj.stopcallbacks = true;
    else
        return;
    end
    
    obj.mousepos = mouseray(obj.mainaxes);
    obj.clickedpos = obj.mousepos;

    obj.mousedown = true;

    if not(isempty(obj.seleditor))
        obj.seleditor.mousePressed(src,event);
    end
    
    obj.stopcallbacks = false;
end

function mouseMoved(obj,src,event)
    
    if obj.mousedown
        if not(obj.stopcallbacks)
            obj.stopcallbacks = true;
        else
            return;
        end
        
        obj.mousepos = mouseray(obj.mainaxes);

        obj.stopcallbacks = false;    
    end
    
    if not(isempty(obj.seleditor))
        obj.seleditor.mouseMoved(src,event);
    end
end

function mouseReleased(obj,src,event)
    
    if not(obj.stopcallbacks)
        obj.stopcallbacks = true;
    else
        return;
    end

    obj.mousedown = false;
    obj.mousepos = mouseray(obj.mainaxes);

    if not(isempty(obj.seleditor))
        obj.seleditor.mouseReleased(src,event);
    end
    
    obj.stopcallbacks = false;
end

function keyPressed(obj,src,event)
    if strcmp(event.Key,'control') || strcmp(event.Key,'command')
        obj.ctrlkeydown = true;
    elseif strcmp(event.Key,'shift')
        obj.shiftkeydown = true;
    end
    
    if not(isempty(obj.seleditor))
        obj.seleditor.keyPressed(src,event);
    end
end

function keyReleased(obj,src,event)
    if strcmp(event.Key,'control') || strcmp(event.Key,'command')
        obj.ctrlkeydown = false;
    elseif strcmp(event.Key,'shift')
        obj.shiftkeydown = false;
    end
    
    if not(isempty(obj.seleditor))
        obj.seleditor.keyReleased(src,event);
    end
end

function closed(ui,src,event) %#ok<INUSD>
    delete(ui);
end

function toggleDisplayUnits(obj,src,evt) %#ok<INUSD>
    obj.unitsVisible = not(obj.unitsVisible);
    if obj.unitsVisible
        EditorFigure.showAxesUnits(obj.mainaxes);
        obj.settingsMenu_units.Checked = 'on';
    else
        EditorFigure.hideAxesUnits(obj.mainaxes);
        obj.settingsMenu_units.Checked = 'off';
    end
end

function toggleToolpanelVisible(obj)
    obj.toolpanelVisible = not(obj.toolpanelVisible);
    if obj.toolpanelVisible
        obj.visMenu_toolpanel.Checked = 'on';
        obj.toolpanel.Visible = 'on';
        if not(obj.navpanelVisible)
            obj.toggleNavpanelVisible;
        end
    else
        obj.visMenu_toolpanel.Checked = 'off';
        obj.toolpanel.Visible = 'off';
        if obj.navpanelVisible
            obj.toggleNavpanelVisible;
        end
    end
    obj.layoutPanels;
end

function toggleNavpanelVisible(obj)
    obj.navpanelVisible = not(obj.navpanelVisible);
    if obj.navpanelVisible
        obj.visMenu_navpanel.Checked = 'on';
        obj.navpanel.Visible = 'on';
    else
        obj.visMenu_navpanel.Checked = 'off';
        obj.navpanel.Visible = 'off';
    end
    obj.layoutPanels;
end

end % methods

methods(Static)

function showAxesUnits(ax)
    set(ax,...        
        'ActivePositionProperty','OuterPosition',...
        'LooseInset',[0,0,0,0],...
        'OuterPosition',[0,0,1,1],...
        'Box','on',...
        'XTickMode','auto',...
        'YTickMode','auto',...
        'ZTickMode','auto',...
        'XTickLabelMode','auto',...
        'YTickLabelMode','auto',...
        'ZTickLabelMode','auto',...
        'XGrid','on',...
        'YGrid','on',...
        'Visible','on');
end

function hideAxesUnits(ax)
    set(ax,...
        'XTick',[],...
        'YTick',[],...
        'ZTick',[],...
        'XGrid','off',...
        'YGrid','off',...
        'ActivePositionProperty','OuterPosition',...
        'LooseInset',[0,0,0,0],...
        'OuterPosition',[0,0,1,1],...
        'Box','on',...
        'TickLength',[0,0],...
        'Visible','off');
end

end % methods(Static)

end % classdef
