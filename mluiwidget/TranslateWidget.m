classdef TranslateWidget < AxesWidget
    
properties(SetAccess=protected)
    pose = identpose3D;
    grouppose = identpose3D;
    
    posedelta = identpose3D; % in local group coordinates
    
    % modes: 'xy_z','z'
    mode = 'xy_z'; % xy plane and z line
end

properties(Access=protected)
    gplane = gobjects(1,0);
    gline = gobjects(1,0);
    gtvec = gobjects(1,0);
   
    fixedcoords = [false;false;true]; % default is z fixed
    
    widgetclickedpos = zeros(3,0);
    widgetmousepos = zeros(3,0);
end

methods
    
% axes can also be a hgtransform
% obj = TranslateWidget(axes,pose)
% obj = TranslateWidget(axes,pose,grouppose)
function obj = TranslateWidget(axes,pose,grouppose)
    obj = obj@AxesWidget(axes);
    
    obj.pose = pose;
    
    if nargin >= 3
        obj.grouppose = grouppose;
    else
        obj.grouppose = identpose3D;
    end
end

function setPose(obj,pose)
    obj.pose = pose;
    obj.posedelta = identpose3D;
end

function setGroup(obj,grouppose,groupaxes)
    obj.grouppose = grouppose;
    obj.posedelta = identpose3D;
    
    v = obj.isvisible;
    if v
        obj.hide;
    end
    
    obj.axes = groupaxes;
    
    if v
        obj.show;
    end
end

function setMode(obj,mode)
    obj.mode = mode;
end

function mousePressed(obj,src,evt) %#ok<INUSD>
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = true;

    if not(isempty(obj.axes))
        obj.mousepos = mouseray(obj.axes);
        obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
        obj.clickedpos = obj.mousepos;
    end
    
%     obj.mousepos = posetransform(obj.mousepos,identpose3D,obj.grouppose);
%     obj.clickedpos = posetransform(obj.clickedpos,identpose3D,obj.grouppose);
    
    if strcmp(obj.mode,'xy_z')
        [tl,~,~,capoint,sclpoint,scapoint] = obj.lineFreeaxisRelation(...
            [true;true;false],obj.clickedpos(:,1),obj.clickedpos(:,2));
        
        disp(sqrt(sum((sclpoint-scapoint).^2,1)));
        
        if tl > 0 && sqrt(sum((sclpoint-scapoint).^2,1)) < 12
            % z-line
            obj.fixedcoords = [true;true;false];
        else
            % xy-plane
            obj.fixedcoords = [false;false;true];
        end
    elseif strcmp(obj.mode,'z')
        [~,~,~,capoint,~,~] = obj.lineFreeaxisRelation(...
            [true;true;false],obj.clickedpos(:,1),obj.clickedpos(:,2));
        
        obj.fixedcoords = [true;true;false];
    else
        error('Unknown transform widget mode.');
    end
    
    if sum(obj.fixedcoords) == 1
        % plane
        
        [tl,ipoint,itype] = obj.lineFreeplaneRelation(...
            obj.fixedcoords,obj.clickedpos(:,1),obj.clickedpos(:,2));

        if tl > 0 && itype == 1
            % standard intersection
            obj.widgetclickedpos = ipoint;
        end
        
    elseif sum(obj.fixedcoords) == 2
        % line
        
        obj.widgetclickedpos = capoint;
        
    else
        error('One or two coordinates must be fixed.');
    end
    
    obj.widgetmousepos = obj.widgetclickedpos;
    
%     disp('---------------------------');
%     disp(obj.widgetclickedpos);
% 
    obj.blockcallbacks = false;
end

function mouseMoved(obj,src,evt) %#ok<INUSD>
    if obj.mousedown
        if obj.blockcallbacks
            return;
        end
        obj.blockcallbacks = true;

        if not(isempty(obj.axes))
            obj.mousepos = mouseray(obj.axes);
            obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
        end
        
%         obj.mousepos = posetransform(obj.mousepos,identpose3D,obj.grouppose);
        
        if sum(obj.fixedcoords) == 1
            % plane
            
            [tl,ipoint,itype] = obj.lineFreeplaneRelation(...
                obj.fixedcoords,obj.mousepos(:,1),obj.mousepos(:,2));

            if tl > 0 && itype == 1
                % standard intersection
                obj.widgetmousepos = ipoint;
                obj.posedelta(1:3) = obj.widgetmousepos - obj.widgetclickedpos;
                
%                 disp(obj.posedelta);
            end
            
        elseif sum(obj.fixedcoords) == 2
            % line
            
            [tl,~,~,capoint,~,~] = obj.lineFreeaxisRelation(...
                [true;true;false],obj.mousepos(:,1),obj.mousepos(:,2));

            if tl > 0
                obj.widgetmousepos = capoint;
                obj.posedelta(1:3) = obj.widgetmousepos - obj.widgetclickedpos;
            end
            
        else
            error('Unsupported plane type.');
        end
        
%         disp(obj.widgetmousepos);
        
        % update graphics
        if obj.isvisible;
            obj.show();
        end

        obj.blockcallbacks = false;
    end
end

function mouseReleased(obj,src,evt) %#ok<INUSD>
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = false;

    % ...
    
    % update graphics
    if obj.isvisible;
        obj.show();
    end

    obj.blockcallbacks = false;
end

% clpoint: closest line point
% capoint: closest axis point
function [tl,ta,clpoint,capoint,sclpoint,scapoint] = lineFreeaxisRelation(obj,fixedcoords,lp1,lp2)
    if sum(fixedcoords) ~= 2
        error('Must have two fixed coordinates.');
    end
    
    ax1 = double(not(fixedcoords)).*-1;
    ax2 = double(not(fixedcoords));

    ax = posetransform([ax1,ax2],obj.pose);
    ax1 = ax(:,1);
    ax2 = ax(:,2);
    
    [~,tl,ta] = lineLineDistance3D_mex(...
        lp1,lp2,ax1,ax2);

    if nargout >= 3
        clpoint = lp1 + (lp2 - lp1) .* tl;
        capoint = ax1 + (ax2 - ax1) .* ta;
    end

    if nargout >= 5
        scp = axes2screen(obj.axes,[clpoint,capoint]);
        sclpoint = scp(:,1);
        scapoint = scp(:,2);
    end
end

function [tl,ipoint,itype,sipoint] = lineFreeplaneRelation(obj,fixedcoords,lp1,lp2)
    
    planenormal = double(fixedcoords);
    planeorigin = [0;0;0];
    
    planenormal = posetransformNormal(planenormal,obj.pose);
    planeorigin = posetransform(planeorigin,obj.pose);
    
    
    [ipoint,tl,itype] = linePlaneIntersection(...
        planenormal,planeorigin,lp1,lp2);
    
    if nargout >= 4
        sipoint = axes2screen(obj.axes,ipoint);
    end
end

function showPlane(obj)
    if strcmp(obj.mode,'xy_z') || strcmp(obj.mode,'z')
        planeverts = [[-0.6;-0.6;0],[-0.6;0.6;0],[0.6;0.6;0],[0.6;-0.6;0]];
    else
        error('Unknown transform widget mode.');
    end
    
    planefaces = [[1;2;3],[1;3;4]];
    
    planeverts = posetransform(planeverts,obj.pose);
    
    obj.gplane = showPatches(...
        planeverts,planefaces,obj.gplane,obj.axes,...
        'edgecolor','none',...
        'facecolor',[1;0;0],...
        'facealpha','0.2');
end

function hidePlane(obj)
    delete(obj.gplane(isvalid(obj.gplane)));
    obj.gplane = gobjects(1,0);
end

function showLine(obj)
    
    if strcmp(obj.mode,'xy_z') || strcmp(obj.mode,'z')
        lineverts = [[0;0;-0.6],[0;0;0.6]];
    else
        error('Unknown transform widget mode.');
    end
    
    lineverts = posetransform(lineverts,obj.pose);
    
    obj.gline = showPolylines(...
        lineverts,obj.gline,obj.axes,...
        'edgecolor',[1;0;0]);
end

function hideLine(obj)
    delete(obj.gline(isvalid(obj.gline)));
    obj.gline = gobjects(1,0);
end

function showTranslatevec(obj)
    obj.gtvec = showPolylines(...
        [obj.widgetclickedpos,obj.widgetmousepos],obj.gtvec,obj.axes,...
        'edgecolor',[0;0;1]);
end

function hideTranslatevec(obj)
    delete(obj.gtvec(isvalid(obj.gtvec)));
    obj.gtvec = gobjects(1,0);    
end

function show(obj)
	obj.show@AxesWidget;
    
    if obj.mousedown
        if sum(obj.fixedcoords) == 1
            obj.showPlane;
            obj.hideLine;
        elseif sum(obj.fixedcoords) == 2
            obj.showLine;
            obj.hidePlane;
        else
            error('One or two coordinates must be fixed.');
        end
        obj.showTranslatevec;
    else
%         obj.showPlane;
        obj.hidePlane;
        obj.showLine;
        
        obj.hideTranslatevec;
    end
end

function hide(obj)
    obj.hide@AxesWidget;
    
    obj.hidePlane;
    obj.hideLine;
    obj.hideTranslatevec;
end

end

end
