classdef RotateWidget < AxesWidget
    
properties(SetAccess=protected)
    pose = identpose3D;
    grouppose = identpose3D;
    
    posedelta = identpose3D; % in local group coordinates
    
    % modes: 'x','y','z'
    mode = 'z'; % xy plane and z line
end

properties(Access=protected)
    grotplane = gobjects(1,0);
    grotaxis = gobjects(1,0);
    grotangle = gobjects(1,0);
   
    rotaxis = [0;0;1];
    rotangle = 0;
    
    widgetclickedpos = zeros(3,0);
    widgetmousepos = zeros(3,0);
end

methods
    
% obj = RotateWidget(axes,pose)
% obj = RotateWidget(axes,pose,grouppose)
function obj = RotateWidget(axes,pose,grouppose)
    obj = obj@AxesWidget(axes);
    
    obj.pose = pose;
    
    if nargin >= 3
        obj.grouppose = grouppose;
    else
        obj.grouppose = identpose3D;
    end
end
    
function setPose(obj, pose)
    obj.pose = pose;
    obj.posedelta = identpose3D;
end

function setGroup(obj,grouppose,groupaxes)
    obj.grouppose = grouppose;
    obj.posedelta = identpose3D;
    
    v = obj.isvisible;
    if v
        obj.hide;
    end
    
    obj.axes = groupaxes;
    
    if v
        obj.show;
    end
end

function setMode(obj,mode)
    obj.mode = mode;
end

function mousePressed(obj,src,evt) %#ok<INUSD>
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = true;

    if not(isempty(obj.axes))
        obj.mousepos = mouseray(obj.axes);
        obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
        obj.clickedpos = obj.mousepos;
    end

%     obj.mousepos = posetransform(obj.mousepos,identpose3D,obj.grouppose);
%     obj.clickedpos = posetransform(obj.clickedpos,identpose3D,obj.grouppose);
    
    if strcmp(obj.mode,'x')
        obj.rotaxis = [1;0;0];
        obj.rotangle = 0;
    elseif strcmp(obj.mode,'y')
        obj.rotaxis = [0;1;0];
        obj.rotangle = 0;
    elseif strcmp(obj.mode,'z')
        obj.rotaxis = [0;0;1];
        obj.rotangle = 0;
    else
        error('Unknown transform widget mode.');
    end
    
    obj.rotaxis = posetransformNormal(obj.rotaxis,obj.pose);
    
    [~,ipoint,~] = obj.lineRotplaneRelation(...
        obj.rotaxis,obj.clickedpos(:,1),obj.clickedpos(:,2));

    obj.widgetclickedpos = ipoint;
    
    obj.widgetmousepos = obj.widgetclickedpos;

    obj.blockcallbacks = false;
end

function mouseMoved(obj,src,evt) %#ok<INUSD>
    if obj.mousedown
        if obj.blockcallbacks
            return;
        end
        obj.blockcallbacks = true;

        if not(isempty(obj.axes))
            obj.mousepos = mouseray(obj.axes);
            obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
        end
        
%         obj.mousepos = posetransform(obj.mousepos,identpose3D,obj.grouppose);
        
        [~,ipoint,~] = obj.lineRotplaneRelation(...
            obj.rotaxis,obj.mousepos(:,1),obj.mousepos(:,2));
        
        obj.widgetmousepos = ipoint;
        
        planeclickedpos = posetransform(obj.widgetclickedpos,identpose3D,obj.pose);
        planemousepos = posetransform(obj.widgetmousepos,identpose3D,obj.pose);
        widgetclickedangle = cart2pol(planeclickedpos(1),planeclickedpos(2));
        widgetmouseangle = cart2pol(planemousepos(1),planemousepos(2));
        
        obj.rotangle = smod(widgetmouseangle - widgetclickedangle,-pi,pi);

        obj.posedelta(4:7) = angleaxis2quat(obj.rotangle,obj.rotaxis);
        
        % update graphics
        if obj.isvisible;
            obj.show();
        end

        obj.blockcallbacks = false;
    end
end

function mouseReleased(obj,src,evt) %#ok<INUSD>
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = false;

    % ...
    
    % update graphics
    if obj.isvisible;
        obj.show();
    end

    obj.blockcallbacks = false;
end

function [tl,ipoint,itype,sipoint] = lineRotplaneRelation(obj,rotaxis,lp1,lp2)
    
    planenormal = rotaxis;
    planeorigin = [0;0;0];
    
    planenormal = posetransformNormal(planenormal,obj.pose);
    planeorigin = posetransform(planeorigin,obj.pose);
    
    
    [ipoint,tl,itype] = linePlaneIntersection(...
        planenormal,planeorigin,lp1,lp2);
    
    if nargout >= 4
        sipoint = axes2screen(obj.axes,ipoint);
    end
end

function showRotplane(obj)
%     planeverts = [[-1;-1;0],[-1;1;0],[1;1;0],[1;-1;0]];
    planeverts = [[-0.6;-0.6;0],[-0.6;0.6;0],[0.6;0.6;0],[0.6;-0.6;0]];
    
    if strcmp(obj.mode,'x')
        planeverts = planeverts([3,1,2],:); % xy -> yz
    elseif strcmp(obj.mode,'y')
        planeverts = planeverts([1,3,2],:); % xy -> xz
    elseif strcmp(obj.mode,'z')
        % xy -> xy
    else
        error('Unknown transform widget mode.');
    end
    
    planefaces = [[1;2;3],[1;3;4]];
    
    planeverts = posetransform(planeverts,obj.pose);
    
    obj.grotplane = showPatches(...
        planeverts,planefaces,obj.grotplane,obj.axes,...
        'edgecolor','none',...
        'facecolor',[1;0;0],...
        'facealpha','0.2');
end

function hideRotplane(obj)
    delete(obj.grotplane(isvalid(obj.grotplane)));
    obj.grotplane = gobjects(1,0);
end

function showRotaxis(obj)
    
    if strcmp(obj.mode,'x')
        lineverts = [[-0.6;0;0],[0.6;0;0]];
    elseif strcmp(obj.mode,'y')
        lineverts = [[0;-0.6;0],[0;0.6;0]];
    elseif strcmp(obj.mode,'z')
        lineverts = [[0;0;-0.6],[0;0;0.6]];
    else
        error('Unknown transform widget mode.');
    end
    
    lineverts = posetransform(lineverts,obj.pose);
    
    obj.grotaxis = showPolylines(...
        lineverts,obj.grotaxis,obj.axes,...
        'edgecolor',[1;0;0]);
end

function hideRotaxis(obj)
    delete(obj.grotaxis(isvalid(obj.grotaxis)));
    obj.grotaxis = gobjects(1,0);
end

function showRotangle(obj)
	
    planeclickedpos = posetransform(obj.widgetclickedpos,identpose3D,obj.pose);
    planemousepos = posetransform(obj.widgetmousepos,identpose3D,obj.pose);
    [widgetclickedangle,r] = cart2pol(planeclickedpos(1),planeclickedpos(2));
    widgetmouseangle = cart2pol(planemousepos(1),planemousepos(2));
    
    if smod(widgetmouseangle-widgetclickedangle,-pi,pi) <= pi
        [verts(1,:),verts(2,:)] = polylineCirclearc(0,0,r,widgetclickedangle,widgetmouseangle);
    else
        [verts(1,:),verts(2,:)] = polylineCirclearc(0,0,r,widgetmouseangle,widgetclickedangle);
    end
    verts(3,:) = 0;
    
%     disp(widgetclickedangle);
%     disp(widgetmouseangle);
    
    verts = posetransform(verts,obj.pose);
    
%     disp(verts);
    
    obj.grotangle = showPolylines(...
        verts,obj.grotangle,obj.axes,...
        'edgecolor',[0;0;1],...
        'facecolor','none');
end

function hideRotangle(obj)
    delete(obj.grotangle(isvalid(obj.grotangle)));
    obj.grotangle = gobjects(1,0);    
end

function show(obj)
	obj.show@AxesWidget;
    
    if obj.mousedown
        obj.showRotplane;
        obj.showRotaxis;
        obj.showRotangle;
    else
        obj.hideRotplane;
        obj.showRotaxis;
        obj.hideRotangle;
    end
end

function hide(obj)
    obj.hide@AxesWidget;
    
    obj.hideRotplane;
    obj.hideRotaxis;
    obj.hideRotangle;
end

end

end
