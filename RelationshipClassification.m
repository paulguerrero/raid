classdef RelationshipClassification < handle
    
% properties(SetAccess=protected)
properties
    % train data set includes training and validation sets
    % (the set is partitioned into several subset when doing cross-validation)
    trainrelids = zeros(1,0);
    trainimginds = zeros(1,0);
    trainimgfilenames = cell(1,0);
    trainlabels = []; % d x n matrix, where d is the number of classes and n the number of training samples
    traindescriptors = []; % optional, is empty if there are too many training relationships
    
    % result of predictions from cross-validation (i.e. class of sample is
    % predicted with a model trained on a subset of the training set not
    % containing the sample)
    validatelabels = []; % d x n matrix, where d is the number of classes and n the number of training samples
%     validateclasses = []; % temp
    
    % todo: testrels must be stores as sourceids and targetids (like in
    % query), because we would also like to classify relationships that
    % were not annotated
    
    % test data set
    testrelids = zeros(1,0);
    testimginds = zeros(1,0);
    testimgfilenames = cell(1,0);
    testlabels = []; % d x m matrix, where d is the number of classes and m the number of test samples
    testdescriptors = []; % optional, is empty if there are too many test relationships
    
    labelids = zeros(0,1); % d x 1 vector of labels of each class
    
    predictor = [];
    
    settings = struct;
end

properties(Access=protected)
    crossvalpredictor = [];
end

methods(Static)
function s = defaultSettings
    
    s = struct;

    s.niangular = 8;
    s.niradial = 2;%4
    s.noangular = 8;
    s.noradial = 2;%4
    
    s.desctype = 'reldisthist';
    s.descparams = struct;
    s.descsimmethod = 'negativeL1dist';
    
%     s.predtype = 'brsvm'; % binary relevance svm
    s.predtype = 'brknn'; % binary relevance k-nearest-neighbors
    s.predparams = struct;
    
    s.lblfilter = zeros(1,0);
end
end

methods
    
% obj = RelationshipClassification()
% obj = RelationshipClassification(obj2)
% obj = RelationshipClassification(trainrelids,trainimginds,trainingimgfilenames,imgpath,annopath)
% obj = RelationshipClassification(trainrelids,trainimginds,trainingimgfilenames,imgpath,annopath,traindescriptors)
% obj = RelationshipClassification(...,nvpairs)
function obj = RelationshipClassification(varargin)
    
    obj.settings = obj.defaultSettings;
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'RelationshipClassification')
        obj.copyFrom(varargin{1});
    elseif numel(varargin) == 5 || numel(varargin) >= 6 && ischar(varargin{6}) || ...
           numel(varargin) == 6 || numel(varargin) >= 7 && ischar(varargin{7})
        
        if numel(varargin) >= 6 && not(ischar(varargin{6}))
            obj.settings = nvpairs2struct(varargin(7:end),obj.settings);
        else
            obj.settings = nvpairs2struct(varargin(6:end),obj.settings);
        end

        obj.trainrelids = varargin{1};
        obj.trainimginds = varargin{2};
        obj.trainimgfilenames = varargin{3};
        imgpath = varargin{4};
        annopath = varargin{5};
        
        if isempty(obj.trainrelids)
            [obj.trainrelids,obj.trainimginds,rellbls,rellblweights,...
                obj.trainimgfilenames,imgannotations,imgmin,imgmax,imgres] = ...
                obj.relinfo(obj.trainimgfilenames,imgpath,annopath);
        else
            [obj.trainrelids,obj.trainimginds,rellbls,rellblweights,...
                obj.trainimgfilenames,imgannotations,imgmin,imgmax,imgres] = ...
                obj.relinfo(obj.trainrelids,obj.trainimginds,obj.trainimgfilenames,imgpath,annopath);
        end
        
        if numel(varargin) >= 6 && not(ischar(varargin{6}))
            obj.traindescriptors = varargin{6};
        else
            obj.traindescriptors = obj.computeDescriptors(...
                obj.trainrelids,obj.trainimginds,imgannotations,imgmin,imgmax,imgres);            
        end
        
        [obj.trainlabels,obj.labelids] = obj.lblsets2lblmat(rellbls,rellblweights);
       
        % create predictors, one for each class
        % (binary relevance problem transformation method for multi-label
        % learning)
        if strcmp(obj.settings.predtype,'brsvm')
            
            % choose cross-validation method
%             if numel(obj.trainrelids) < 200
                cvalparams = {'Leaveout','on'};
%             else
%                 cvalparams = {'KFold',10};
%             end

            kernelfun = @(U,V) obj.traindescriptors.similarity(U',V',...
                obj.traindescriptors.simmethod2distmethod(...
                obj.settings.descsimmethod));
            
            obj.predictor = cell(1,0);
            for i=1:size(obj.trainlabels,1)
                disp(['Training binary classifier ',num2str(i),' / ',num2str(size(obj.trainlabels,1))]);
                predparams = struct2nvpairs(obj.settings.predparams);
                obj.predictor{i} = fitcsvm(...
                    obj.traindescriptors.val',obj.trainlabels(i,:)'>0.5,...
                    predparams{:},...
                    'ClassNames',[false,true],...
                    'KernelFunction',kernelfun);
                obj.predictor{i} = fitSVMPosterior(obj.predictor{i},...
                    cvalparams{:});
            end
        elseif strcmp(obj.settings.predtype,'brknn')

            descdist = @(u,V) obj.traindescriptors.distance(u',V',...
                obj.traindescriptors.simmethod2distmethod(...
                obj.settings.descsimmethod))';
            
            obj.predictor = cell(1,0);
            for i=1:size(obj.trainlabels,1)
                disp(['Training binary classifier ',num2str(i),' / ',num2str(size(obj.trainlabels,1))]);
                predparams = struct2nvpairs(obj.settings.predparams);
                obj.predictor{i} = fitcknn(...
                    obj.traindescriptors.val',obj.trainlabels(i,:)'>0.5,...
                    predparams{:},...
                    'ClassNames',[false,true],...
                    'Distance',descdist);
            end
        else
            error('Unknown predictor type.');
        end
    else
        error('Invalid input parameters.');
    end
end

function delete(obj)
    if not(isempty(obj.traindescriptors))
        delete(obj.traindescriptors(isvalid(obj.traindescriptors)));
    end
    if not(isempty(obj.testdescriptors))
        delete(obj.testdescriptors(isvalid(obj.testdescriptors)));
    end
%     if not(isempty(obj.predictor))
%         delete(obj.predictor(isvalid(obj.predictor)));
%     end
end

function setClasslabels(obj,labelids)
    obj.labelids = labelids;
end

% function [relids,rellbls,relweights] = trainimgrels(obj,imgind)
%     relinds = find(trainimginds == imgind);
%     
%     relids = trainrelids
% end

function copyFrom(obj,obj2)
    obj.trainrelids = obj2.trainrelids;
    obj.trainimginds = obj2.trainimginds;
    obj.trainimgfilenames = obj2.trainimgfilenames;
    obj.trainlabels = obj2.trainlabels;
    if not(isempty(obj.traindescriptors))
        delete(obj.traindescriptors(isvalid(obj.traindescriptors)));
    end
    obj.traindescriptors = obj2.traindescriptors.clone;
    
    obj.testrelids = obj2.testrelids;
    obj.testimginds = obj2.testimginds;
    obj.testimgfilenames = obj2.testimgfilenames;
    obj.testlabels = obj2.testlabels;
    if not(isempty(obj.testdescriptors))
        delete(obj.testdescriptors(isvalid(obj.testdescriptors)));
    end
    obj.testdescriptors = obj2.testdescriptors.clone;
    
    obj.labelids = obj2.labelids;
    
    obj.predictor = obj2.predictor; % should be value class
    obj.crossvalpredictor = obj2.crossvalpredictor;
    
    obj.settings = obj2.settings;
end

function obj2 = clone(obj)
    obj2 = RelationshipClassification(obj);
end

function clear(obj)
    obj.testimgfilenames = cell(1,0);
    obj.testrelids = cell(1,0);
    obj.testlabels = zeros(size(obj.trainlabels,1),0);
    if not(isempty(obj.testdescriptors))
        delete(obj.testdescriptors(isvalid(obj.testdescriptors)));
    end
    obj.testdescriptors = [];
end

function validate(obj)
    obj.clear;
    
%     obj.testrelids = obj.trainrelids;
%     obj.testimginds = obj.trainimginds;
%     obj.testimgfilenames = obj.trainimgfilenames;
%     obj.testdescriptors = obj.traindescriptors.clone;
%     obj.testlabels = zeros(size(obj.trainlabels,1),0);
    
    if isempty(obj.crossvalpredictor)

        if strcmp(obj.settings.predtype,'brsvm')
            error('The validation for brvsm does currently not work.');
            % always seems to predict a single class when using
            % kfoldPredict
            
            % choose cross-validation method
%             if numel(obj.trainrelids) < 200
                cvalparams = {'Leaveout','on'};
%             else
%                 cvalparams = {'KFold',10};
%             end

            obj.crossvalpredictor = {};
            for i=1:numel(obj.predictor)
                obj.crossvalpredictor{i} = crossval(...
                    obj.predictor{i},...
                    cvalparams{:});
            end
        elseif strcmp(obj.settings.predtype,'brknn')
            % choose cross-validation method
%             if numel(obj.trainrelids) < 200
                cvalparams = {'Leaveout','on'};
%             else
%                 cvalparams = {'KFold',10};
%             end

            obj.crossvalpredictor = {};
            for i=1:numel(obj.predictor)
                disp(['Validating binary classifier ',num2str(i),' / ',num2str(numel(obj.predictor))]);
                obj.crossvalpredictor{i} = crossval(...
                    obj.predictor{i},...
                    cvalparams{:});
            end
        else
            error('Unknown predictor type.');
        end
    end
    
    obj.validatelabels = [];
%     obj.validateclasses = [];
    for i=1:size(obj.trainlabels,1)
        inclassind = find(obj.crossvalpredictor{i}.ClassNames == true);
        [~,posteriors] = obj.crossvalpredictor{i}.kfoldPredict;
        obj.validatelabels(i,:) = posteriors(:,inclassind); %#ok<FNDSB>
%         obj.validateclasses(i,:) = classes; %#ok<FNDSB>
    end
end

% fraction of wrong labels in the total number of labels
% assuming labels are in [0,1]
function [hlmacro,hlmicro] = hammingloss(obj,posteriorthresh)
    if nargin < 2 || isempty(posteriorthresh)
        posteriorthresh = 0.5;
    end
    
    if isempty(obj.validatelabels)
        error('Compute validation first');
    end
    
%     if posteriorthresh == 0
%         vl = double(obj.validatelabels>=posteriorthresh); % temp
%     else
        vl = double(obj.validatelabels>posteriorthresh); % temp
%     end
    tl = double(obj.trainlabels>0.5);
    
    hlmacro = sum(abs(vl - tl),1) ./ ...
        size(obj.trainlabels,1);
    hlmacro = mean(hlmacro);
    
    hlmicro = sum(sum(abs(vl - tl))) ./ ...
        numel(obj.trainlabels);
end

% assuming labels are in [0,1]
function [pmacro,rmacro,f1macro,pmicro,rmicro,f1micro] = precisionrecall(obj,posteriorthresh)
    if nargin < 2 || isempty(posteriorthresh)
        posteriorthresh = 0.5;
    end
    
%     vl = obj.validatelabels;
%     if posteriorthresh == 0
%         vl = double(obj.validatelabels>=posteriorthresh); % temp
%     else
        vl = double(obj.validatelabels>posteriorthresh); % temp
%     end
    tl = double(obj.trainlabels>0.5);
    
    pmacro = sum(tl .* (1 - abs(vl - tl)),1) ./ sum(vl,1);
%     pmacro = sum(tl .* vl,1) ./ sum(vl,1); % only for binary!
    mask = sum(vl,1) == 0;
    pmacro(mask) = 1;
    
    rmacro = sum(tl .* (1 - abs(vl - tl)),1) ./ sum(tl,1);
%     rmacro = sum(tl .* vl,1) ./ sum(tl,1); % only for binary!
    mask = sum(tl,1) == 0;
    rmacro(mask) = 1;
    
    
    f1macro = 2 .* (pmacro.*rmacro) ./ (pmacro+rmacro);
    mask = pmacro+rmacro == 0;
    f1macro(mask) = 0;
    
    pmacro = mean(pmacro);
    rmacro = mean(rmacro);
    f1macro = mean(f1macro);
    
    if sum(vl(:)) == 0
        pmicro = 1;
    else
        pmicro = sum(sum(tl .* (1 - abs(vl - tl)))) ./ sum(vl(:));
%         pmicro = sum(sum(tl .* vl)) ./ sum(vl(:));
    end
    
    if sum(tl(:)) == 0
        rmicro = 0;
    else
        rmicro = sum(sum(tl .* (1 - abs(vl - tl)))) ./ sum(tl(:));
%         rmicro = sum(sum(tl .* vl)) ./ sum(tl(:));
    end
    
    f1micro = 2 .* (pmicro.*rmicro) ./ (pmicro+rmicro);
    mask = pmicro+rmicro == 0;
    f1micro(mask) = 0;
end

% predict(obj,relids,imginds,imgfilenames,imgpath,annopath)
% predict(obj,relids,imginds,imgannotations,imgmin,imgmax,imgres,imgfilenames,imgpath,annopath)
function predict(obj,varargin)
    
    obj.clear;
    
    if numel(varargin{3}) <= 100

        obj.testdescriptor = obj.computeDescriptors(...
            [],[],[],[],[],[]);
        
%         descparams = struct2nvpairs(obj.settings.descparams);
%         if strcmp(obj.settings.desctype,'reldisthist')
%             nbins = obj.settings.niangular * obj.settings.niradial * ...
%                 obj.settings.noangular * obj.settings.noradial;
%             obj.testdescriptors = ReldisthistDescriptorSet(...
%                 obj.settings.niangular,obj.settings.niradial,...
%                 obj.settings.noangular,obj.settings.noradial,...
%                 zeros(1,0),zeros(1,0),zeros(nbins,0),...
%                 descparams{:});
%         elseif strcmp(obj.settings.desctype,'avg')
%             nbins = obj.settings.niangular * obj.settings.niradial;
%             obj.testdescriptors = RegionavgDescriptorSet(...
%                 obj.settings.niangular,obj.settings.niradial,...
%                 zeros(1,0),zeros(nbins,0),...
%                 descparams{:});
%         else
%             error('Unknown descriptor type.');
%         end
    end
    
    obj.testrelids = zeros(1,0);
    obj.testimginds = zeros(1,0);
    obj.testimgfilenames = cell(1,0);
    obj.testlabels = zeros(size(obj.trainlabels,1),0);
    for i=1:numel(varargin{3})
    
        [relids,imginds,~,~,...
            imgfilename,imgannotation,imgmin,imgmax,imgres,imgindmask] = ...
            obj.relinfo(varargin{2:end},i);
        obj.testimgfilenames(i) = imgfilename;
        obj.testrelids(imgindmask) = relids;
        obj.testimginds(imgindmask) = imginds;
        
        if isempty(relids)
            continue;
        end
        
        % only per image, otherwise there might not be enough memory for
        % all descriptors
        imgdescset = obj.computeDescriptors(...
            relids,ones(1,numel(relids)),imgannotation,imgmin,imgmax,imgres);
        
        % predict classification for relationships in the current image
        % (e.g. with svm)
        if strcmp(obj.settings.predtype,'brsvm')
            for j=1:numel(obj.predictor)
                inclassind = find(obj.predictor{j}.ClassNames == true);
                [~,scores] = obj.predictor{j}.predict(imgdescset.val');
                obj.testlabels(j,imgindmask) = scores(:,inclassind); %#ok<FNDSB>
            end
        elseif strcmp(obj.settings.predtype,'brknn')
            for j=1:numel(obj.predictor)
                inclassind = find(obj.predictor{j}.ClassNames == true);
                [~,scores] = obj.predictor{j}.predict(imgdescset.val');
                obj.testlabels(j,imgindmask) = scores(:,inclassind); %#ok<FNDSB>
            end
        else
            error('Unknown predictor type.');
        end

        if not(isempty(obj.testdescriptors))
            obj.testdecsriptors.copySubset(imgindmask,imgdescset);
%             obj.testdecsriptors.val(:,imgindmask) = imgdescset.val;
        end
        
        delete(imgdescset(isvalid(imgdescset)));
    end
end
    
end

methods(Access=protected)

function descset = computeDescriptors(obj,relids,imginds,imgannotations,imgmin,imgmax,imgres)
        
    descparams = struct2nvpairs(obj.settings.descparams);
    if strcmp(obj.settings.desctype,'avg')
        nbins = obj.settings.niangular * obj.settings.niradial;
        descset = RegionavgDescriptorSet(...
            obj.settings.niangular,obj.settings.niradial,...
            zeros(1,0),zeros(2,0),zeros(nbins,0),...
            descparams{:});
    elseif strcmp(obj.settings.desctype,'ibs')
        descparams = nvpairs2struct(descparams);
        ndist = descparams.ndist;
        nnormalangle = descparams.nnormalangle;
        nnormaldiff = descparams.nnormaldiff;
        descparams = rmfield(descparams,{'ndist','nnormalangle','nnormaldiff'});
        descparams = struct2nvpairs(descparams);
        
        nbins = ndist+nnormalangle+nnormaldiff;
        
        descset = IBSDescriptorSet(...
            ndist,nnormalangle,nnormaldiff,...
            cell(1,0),zeros(nbins,0),...    
            descparams{:});
    elseif strcmp(obj.settings.desctype,'reldisthist')
        nbins = obj.settings.niangular * obj.settings.niradial * ...
            obj.settings.noangular * obj.settings.noradial;
        descset = ReldisthistDescriptorSet(...
            obj.settings.niangular,obj.settings.niradial,...
            obj.settings.noangular,obj.settings.noradial,...
            zeros(1,0),zeros(1,0),zeros(2,0),zeros(2,0),zeros(nbins,0),...
            descparams{:});
    else
        error('Unknown descriptor type.');
    end
    
    if isempty(relids)
        return;
    end
    
    rinds = array2cell_mex(1:numel(relids),imginds,numel(imgannotations));
    
    for i=1:numel(imgannotations)
        
        disp(['computing descriptors for image ',num2str(i),' / ',num2str(numel(imgannotations))])
        
        % load annotation and clone image objects
        if isempty(rinds{i})
            continue;
        end

        [m,imgrelinds] = ismember(relids(rinds{i}),imgannotations(i).relationships.id);
        if not(all(m))
            error('Some relationships were not found in an annotation.');
        end
        
        sourceobjs = imgannotations(i).relationships.sourceobjs(imgrelinds);
        targetobjs = imgannotations(i).relationships.targetobjs(imgrelinds);
        
        % compute descriptors
        if strcmp(obj.settings.desctype,'avg')
            imgdescset = RegionavgDescriptorSet(...
                obj.settings.niangular,obj.settings.niradial,...
                sourceobjs,targetobjs,...
                imgmin(:,i),imgmax(:,i),imgres(:,i),...
                descparams{:});
        elseif strcmp(obj.settings.desctype,'ibs')
            imgdescset = IBSDescriptorSet(...
                ndist,nnormalangle,nnormaldiff,...        
                sourceobjs,targetobjs,...
                imgmin(:,i),imgmax(:,i),imgres(:,i),...
                descparams{:});
        elseif strcmp(obj.settings.desctype,'reldisthist')
            imgdescset = ReldisthistDescriptorSet(...
                obj.settings.niangular,obj.settings.niradial,...
                obj.settings.noangular,obj.settings.noradial,...
                sourceobjs,targetobjs,...
                imgmin(:,i),imgmax(:,i),imgres(:,i),...
                descparams{:});
        else
            error('Unknown descriptor type.');
        end
        
%         descset.val(:,rinds{i}) = imgdescset.val;
        descset.append(imgdescset);
        
        delete(imgdescset(isvalid(imgdescset)));
    end
end

% [relids,relimginds,rellbls,rellblweights,imgfilenames,imgannotations,imgmin,imgmax,imgres] = relinfo(...)
% [...] = relinfo(imgfilenames,imgpath,annopath)
% [...] = relinfo(imgannotations,imgmin,imgmax,imgres,imgfilenames,imgpath,annopath)
% [...] = relinfo(relids,relimginds,imgfilenames,imgpath,annopath)
% [...] = relinfo(relids,relimginds,imgannotations,imgmin,imgmax,imgres,imgfilenames,imgpath,annopath)
% [...] = relinfo(...,imginds)
function [...
        relids,relimginds,rellbls,rellblweights,...
        imgfilenames,imgannotations,imgmin,imgmax,imgres,imgindmask] = ...
        relinfo(obj,varargin)

    if isnumeric(varargin{1})
        if numel(varargin) == 6 || numel(varargin) == 10
            imginds = varargin{end};
        else
            imginds = 1:numel(varargin{3});
        end
        relids = varargin{1};
        relimginds = varargin{2};
        imgindmask = ismember(relimginds,imginds);
        relids(not(imgindmask)) = [];
        relimginds(not(imgindmask)) = [];
        rinds = array2cell_mex(1:numel(relids),relimginds,numel(varargin{3}));
        rinds = rinds(imginds);
        vararginoffset = 2;
    else
        if numel(varargin) == 4 || numel(varargin) == 8
            imginds = varargin{end};
        else
            imginds = 1:numel(varargin{1});
        end
        relids = zeros(1,0);
        relimginds = zeros(1,0);
        imgindmask = true(1,0);
        rinds = cell(1,numel(imginds));
        vararginoffset = 0;
    end
    rellbls = cell(1,0);
    rellblweights = cell(1,0);
    
    if iscell(varargin{vararginoffset+1})
        imgfilenames = varargin{vararginoffset+1}(imginds);
        imgpath = varargin{vararginoffset+2};
        annopath = varargin{vararginoffset+3};
        imgannotations = ImageAnnotation.empty(1,0);
        imgmin = zeros(2,numel(imginds));
        imgmax = zeros(2,numel(imginds));
        imgres = zeros(2,numel(imginds));
    else
        imgannotations = varargin{vararginoffset+1}(imginds);
        imgmin = varargin{vararginoffset+2}(:,imginds);
        imgmax = varargin{vararginoffset+3}(:,imginds);
        imgres = varargin{vararginoffset+4}(:,imginds);
        imgfilenames = varargin{vararginoffset+5}(imginds);
        imgpath = varargin{vararginoffset+6};
        annopath = varargin{vararginoffset+7};
    end
    
    reloffset = 0;
    for i=1:numel(imgfilenames)

        if iscell(varargin{vararginoffset+1})
            info = imfinfo([imgpath,'/',imgfilenames{i}]);
            imgres(:,i) = [info.Width;info.Height];
            [imgmin(:,i),imgmax(:,i)] = ImageAnnotation.imres2imsize(imgres);
            annofilename = ImageAnnotation.img2annofilename(imgfilenames{i});
            imgannotations(:,i) = AnnotationImporter.importAnnotation([annopath,'/',annofilename]);
        end
        
        if vararginoffset == 0
            rinds{i} = reloffset+1:reloffset+numel(imgannotations(i).relationships.id);
            imgindmask = [imgindmask,true(1,numel(rinds{i}))]; %#ok<AGROW>
            inds = 1:numel(imgannotations(i).relationships.id);
        else
            [m,inds] = ismember(relids(rinds{i}),imgannotations(i).relationships.id);
            if not(all(m))
                error('Some relationships were not found in an annotation.');
            end
        end

        relids(rinds{i}) = imgannotations(i).relationships.id(inds);
        relimginds(rinds{i}) = i;
        rellbls(rinds{i}) = imgannotations(i).relationships.labels(inds);
        rellblweights(rinds{i}) = imgannotations(i).relationships.labelweights(inds);
        
        reloffset = reloffset + numel(rinds{i});
    end
    
    if not(isempty(obj.settings.lblfilter))
        lblfiltermask = true(1,numel(rellbls));
        for i=1:numel(rellbls)
            mask = not(ismember(rellbls{i},obj.settings.lblfilter));
            rellbls{i}(mask) = [];
            rellblweights{i}(mask) = [];
            lblfiltermask(i) = not(isempty(rellbls{i}));
        end
%         lblfiltermask = ismember(rellbls,obj.settings.lblfilter);
        relids = relids(lblfiltermask);
        relimginds = relimginds(lblfiltermask);
        rellbls = rellbls(lblfiltermask);
        rellblweights = rellblweights(lblfiltermask);
        imgindmask = imgindmask(lblfiltermask);
    end
end

end

methods(Static)

function [lblmat,lblids] = lblsets2lblmat(lblsets,weights)
    lblids = unique([lblsets{:}]);
    classlbls2inds = zeros(1,max(lblids));
    classlbls2inds(lblids) = 1:numel(lblids);
    
    lblmat = zeros(numel(lblids),numel(lblsets));
    for i=1:numel(lblsets)
        lblmat(classlbls2inds(lblsets{i}),i) = weights{i};
    end
end

function [lblset,lblweights] = lblmat2lblset(lblmat,lblids)
    lblset = cell(1,size(lblmat,2));
    lblweights = cell(1,size(lblmat,2));
    
    for i=1:numel(lblset)
        mask = lblmat(:,i)>0;
        lblset{i} = lblids(mask);
        lblweights{i} = lblmat(mask,i)';
    end
end
    
end

methods(Static)

function obj = saveobj(obj)
    % do nothing for now
end

function obj = loadobj(obj)
    if isstruct(obj)
        error('Could not load the object, need to implement this conversion.');
    else
        % do nothing, obj should be a valid RelationshipClassification
    end
end

end

end
