function [poly,ind] = polylineClean(poly,polyclosed,minseglen)
%     cpointhullsize = [max(cellfun(@(x) max(x(1,:)),poly)) - min(cellfun(@(x) min(x(1,:)),poly)),...
%                       max(cellfun(@(x) max(x(2,:)),poly)) - min(cellfun(@(x) min(x(2,:)),poly))];
%     minseglen = norm(cpointhullsize) * 0.000001;
    
    i = 1;
    c = 1;
    while i <= numel(poly)
        if size(poly{i},2) < 2
            poly(i) = [];
        else
            if polyclosed(i)
                poly{i}(:,end+1) = poly{i}(:,1);
            end
            j = 1;
            while j < size(poly{i},2)
                seglen = (poly{i}(1,j+1) - poly{i}(1,j))^2 + ...
                         (poly{i}(2,j+1) - poly{i}(2,j))^2;                
                if seglen < minseglen
                    poly{i}(:,j+1) = [];
                else
                    j = j + 1;
                end
            end
            if polyclosed(i) && not(isempty(poly{i}))
                poly{i}(:,end) = [];
            end
            if size(poly{i},2) < 2
                poly(i) = [];
            else
                ind(i) = c; %#ok<AGROW>
                i = i + 1;
            end
        end
        c = c + 1;
    end
end
