% grid nodes assumed to be at integer coordinates
% x,y,z and v are 2xn vectors where is the number of line segments
function [I,pixcoords] = linesegRasterization3(I,x,y,z,v,zcircular)
    
    if nargin < 6 || isempty(zcircular)
        zcircular = false;
    end

    if not(iscell(v))
        v = {v};
    end

    % dda
    x = round(x);
    y = round(y);
    z = round(z);
    
    dx = abs(x(2,:)-x(1,:));
    dy = abs(y(2,:)-y(1,:));
    dz = abs(z(2,:)-z(1,:));
    
    if zcircular
        % circular z domain (like angle)
        % in a circular domain, the uppermost pixel = the the lowermost
        % pixel, e.g. -pi and pi is same value for circular domain
        mask = dz > (size(I,3)-1)/2;
        dz(mask) = (size(I,3)-1) - dz(mask);
    end
    
	m = max([dx;dy;dz],[],1);
    
%     mask = abs(dx)>=abs(dy);
%     m(mask) = abs(dx(mask));
%     m(not(mask)) = abs(dy(not(mask)));
    
    for i=1:size(x,2)
        sx = round(linspace(x(1,i),x(2,i),m(i)+1));
        sy = round(linspace(y(1,i),y(2,i),m(i)+1));
%         sz = round(linspace(z(i-1),z(i),m(i-1)+1));
        
        if zcircular && abs(z(2,i)-z(1,i)) > (size(I,3)-1)/2
            
            if z(1,i) > z(2,i)
                sz = linspace(z(1,i),z(2,i)+(size(I,3)-1),m(i)+1);
%                 mask = sz > size(I,3);
%                 sz(mask) = sz(mask) - (size(I,3)-1);
            else
                sz = linspace(z(1,i),z(2,i)-(size(I,3)-1),m(i)+1);
            end
            sz = mod(sz-1,(size(I,3)-1))+1;
            sz = round(sz);
        else
            sz = round(linspace(z(1,i),z(2,i),m(i)+1));
        end
        
        % clipping
        clipmask = sx >= 1 & sx <= size(I,2) & ...
                   sy >= 1 & sy <= size(I,1) & ...
                   sz >= 1 & sz <= size(I,3);
        clipmask(1) = false; % don't draw first point of polyline segment
        
        if sum(clipmask) > 0
            for j=1:numel(v)
                sv = linspace(v{j}(1,i),v{j}(2,i),m(i)+1);    
                
                I(sub2ind(size(I),sy(clipmask),sx(clipmask),sz(clipmask),...
                    ones(1,sum(clipmask)).*j)) = sv(clipmask);
                if nargout > 1
                    pixcoords = [pixcoords,[sx(clipmask);sy(clipmask);sz(clipmask)]]; %#ok<AGROW>
                end
            end
        end
    end
end
 