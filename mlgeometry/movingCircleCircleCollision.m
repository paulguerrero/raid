function [t,cind] = movingCircleCircleCollision(startpos, vel, radius, center2,radius2)
    % same as intersecting trajectory with static circle of radius+radius2
    
%     traj = [startpos;startpos+vel];
%     velmag = sqrt(sum(vel.^2,1));
    
%     [id,~,it,~,~] = pointPolylineDistance_mex(...
%         center2(1,:),center2(2,:),...
%         traj(1),traj(2),traj(3),traj(4));
%     
%     cradius = radius+radius2;

    if isempty(center2)
        t = nan;
        cind = nan;
        return;
    end
    
    it = pathCircleCollision(startpos,startpos+vel,center2,radius+radius2);
    
%     imask = it <= cradius;
%     
%     % if the trajectory endpoint is inside the closest point add back the distance from the trajectory endpoint to the closest point
%     it = it(imask);
%     id = id(imask);
%     c2temp = center2(:,imask);
%     mask = it == 1;
%     if any(mask)
%         trajnormal = [-vel(2),vel(1)]./velmag;
%         endpointvec = [traj(3) - c2temp(1,mask);...
%                        traj(4) - c2temp(2,mask)];
%         orthdist = abs(endpointvec(1,:).*trajnormal(1) + endpointvec(2,:).*trajnormal(2));
%         endpointdist = sqrt(sum(endpointvec.^2,1));
%         it(mask) = it(mask) - (sqrt(radius^2-orthdist.^2) - sqrt(endpointdist^2-orthdist.^2)) ./ velmag;
%     end
%     
%     mask = not(mask);
%     if any(mask)
%         it(mask) = it(mask) - sqrt(radius^2-id(mask).^2) ./ velmag;
%     end
    
    [t,cind] = min(it);
    if isnan(t)
        cind = nan;
    end
end
