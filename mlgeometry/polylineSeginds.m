function [svi1,svi2,cvi1,cvi2] = polylineSeginds(nanmask)
    cvi1 = [0,find(nanmask)+1];
    cvi2 = [cvi1(2:end)-2,numel(x1)];
    
    svi1 = 1:numel(x1);
    svi2 = 2:numel(x1)+1;
    
    % segments that end with nan and the last segment
    % are changed to end in the first vertex of the component instead
    svi2(cvi2(:)) = cvi1;
    % segments that start with nan are removed
    nanstartsegs = cvi2(1:end-1)+1;
    svi1(nanstartsegs) = [];
    svi2(nanstartsegs) = [];
end
