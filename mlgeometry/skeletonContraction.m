function [t,skelcenter,sctime] = skeletonContraction(d,efrom,eto,elen,maxspeed)%,sx,sy)
    
    if numel(d) == 1
        t = d;
        return;
    end

    % make connectivity data structures
    vedges = cell(1,numel(d));
%     vedgeleaving = cell(1,numel(d));
    vneighbours = cell(1,numel(d));
    for i=1:numel(efrom)
        vedges{efrom(i)}(end+1) = i;
%         vedgeleaving{efrom(i)}(end+1) = true;
        vneighbours{efrom(i)}(end+1) = eto(i);
        
        vedges{eto(i)}(end+1) = i;
%         vedgeleaving{eto(i)}(end+1) = false;
        vneighbours{eto(i)}(end+1) = efrom(i);
    end
    
%     outstanding = true(1,numel(d));
%     bordermask = cellfun(@(x) sum(outstanding(x)) == 1,vneighbours);
    
    
%     bordermask = d(efrom) == 0 | d(eto) == 0;
    bordermask = d == 0;
%     tstart = inf(1,numel(efrom));
%     tend = inf(1,numel(efrom));
%     tstart(bordermask) = 0;
    
    it = cell(1,numel(d));
    for i=1:numel(it)
        if bordermask(i)
            it{i} = zeros(1,numel(vedges{i}));
        else
            it{i} = nan(1,numel(vedges{i}));
        end
    end

    
%     outstanding = true(1,numel(efrom));
    outstanding = true(1,numel(d));
    
    t = nan(1,numel(d));
    
    mintimeincrement = eps(mean(d)) * 1000;
    
%     figure;
    while sum(outstanding) > 1
        % select vertex with minimum highest time and max. one outstanding edge
        oinds = find(outstanding);
        borderinds = cellfun(@(x) sum(isnan(x)) <= 1 && not(all(isnan(x))) ,it(oinds));
        borderinds = oinds(borderinds);
        [~,vind] = min(cellfun(@(x) max(x) ,it(borderinds)));
        vind = borderinds(vind);
        
        if isempty(vind)
            error('skeleton seems to have disconnected components');
        end
        
        outstanding(vind) = false;
        
        % set time of vertex to max. time of all incident edges
        t(vind) = max(it{vind});
        
%         disp(['final time at ',num2str(vind),':']);
%         disp(t(vind));
        
        % for each outstanding neighboring vertex that has not yet
        % got an incident time from this vertex
        v1eoutstanding = find(outstanding(vneighbours{vind}));
        for v1eind=v1eoutstanding
            eind = vedges{vind}(v1eind);
            v2ind = vneighbours{vind}(v1eind);
            v2eind = find(vedges{v2ind} == eind);
            if isnan(it{v2ind}(v2eind))
                mintime = max(mintimeincrement,elen(eind)/maxspeed);
                it{v2ind}(v2eind) = max(d(v2ind),t(vind)+mintime);
                
%                 disp(['candidate time at ',num2str(v2ind),':']);
%                 disp(it{v2ind}(v2eind));
%             
%                 cla;
%                 hold on;
%                 xdata = [sx(efrom);sx(eto);nan(1,numel(efrom))];
%                 ydata = [sy(efrom);sy(eto);nan(1,numel(efrom))];
%                 xdata = xdata(:)';
%                 ydata = ydata(:)';
%                 line(xdata,ydata,'Color','red');
%                 scatter(sx(outstanding),sy(outstanding),max(0.000001,d(outstanding)),repmat([0,0,1],sum(outstanding),1));
%                 scatter(sx(not(outstanding)),sy(not(outstanding)),max(0.000001,t(not(outstanding))),repmat([1,0,0],sum(not(outstanding)),1));
%                 scatter(sx([vind,v2ind]),sy([vind,v2ind]),max(0.000001,[t(vind),it{v2ind}(v2eind)]),repmat([0,1,0],2,1));
%                 line(sx([vind,v2ind]),sy([vind,v2ind]),'Color','green','LineWidth',2);
%                 hold off;   
            end
        end
    end
    
    
    vind = find(outstanding);
    if any(isnan(it{vind}))
        error('last vertex is missing incident time entry');
    end
    
    if numel(it{vind}) == 1
        eind = vedges{vind};
        t(vind) = it{vind};
    else
        [~,veind] = max(it{vind}); % edge containing skeleton center = edge with largest incident time
        eind = vedges{vind}(veind);
        
        it{vind}(veind) = nan;
        t(vind) = max(it{vind}); % time at last vertex = second largest incident time
    end
    v1ind = efrom(eind);
    v2ind = eto(eind);
%     if numel(it{vind} == 1 || 
    
%     % the remaining edge contains the skeleton center
%     eind = find(outstanding);
%     v1ind = efrom(eind);
%     v2ind = eto(eind);
    
    mintime = elen(eind)/maxspeed;
    
    if t(v1ind)-t(v2ind) > mintime
        sctime = t(v1ind);
        skelcenter = [eind;0];
    elseif t(v2ind)-t(v1ind) > mintime
        sctime = t(v2ind);
        skelcenter = [eind;1];
    else
        sctime = (mintime+t(v1ind)+t(v2ind)) / 2;
        skelcenter = [eind;(sctime-t(v1ind))/mintime];
    end
    
%     % remove edge containing skeleton center (because it is the only edge
%     % without defined orientation and we don't need it anymore)
%     efrom(skelcenter(1)) = [];
%     eto(skelcenter(1)) = [];
    
%     % orient all skeleton edges in direction of increasing time
%     mask = t(eto) < t(efrom);
%     temp = eto(mask);
%     eto(mask) = efrom(mask);
%     efrom(mask) = temp;
    
%     % re-make connectivity data structures
%     vedges = cell(1,numel(d));
%     vedgeleaving = cell(1,numel(d));
%     vneighbours = cell(1,numel(d));
%     for i=1:numel(efrom)
%         vedges{efrom(i)}(end+1) = i;
%         vedgeleaving{efrom(i)}(end+1) = true;
%         vneighbours{efrom(i)}(end+1) = eto(i);
%         
%         vedges{eto(i)}(end+1) = i;
%         vedgeleaving{eto(i)}(end+1) = false;
%         vneighbours{eto(i)}(end+1) = efrom(i);
%     end
    
    vedgeleaving = cell(1,numel(d));
    for i=1:numel(efrom)
        vedgeleaving{efrom(i)}(end+1) = t(efrom(i)) <= t(eto(i));
        vedgeleaving{eto(i)}(end+1) = t(efrom(i)) > t(eto(i));
    end
    
    % so the skeleton center edge is left out
    vemask = vedges{efrom(skelcenter(1))} == skelcenter(1);
    vedgeleaving{efrom(skelcenter(1))}(vemask) = true;
    vemask = vedges{eto(skelcenter(1))} == skelcenter(1);
    vedgeleaving{eto(skelcenter(1))}(vemask) = true;
    
    % walk through all vertices of order > 2 and backpropagate times to
    % subtrees so that all borders arrive at the vertices at the same time
    itinds = find(cellfun(@(x) sum(not(x)) >= 2,vedgeleaving));
    for i=itinds
        arrivingeind = find(not(vedgeleaving{i}));
        scalefactors = max(it{i}(arrivingeind)) ./ it{i}(arrivingeind);

        % for all subtrees
        for j=1:numel(scalefactors)
            if scalefactors(j) == 1
                continue;
            end
            
            % find all vertex indices in the subtree
            vqueue = vneighbours{i}(arrivingeind(j));
            while not(isempty(vqueue))
                vind = vqueue(1);
                vqueue(1) = [];
                
                t(vind) = t(vind) * scalefactors(j);
                
                vqueue = [vqueue,vneighbours{vind}(not(vedgeleaving{vind}))]; %#ok<AGROW>
            end
        end
    end

end
