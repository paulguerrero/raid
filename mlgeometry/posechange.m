function pose = posechange(pose,change)
    if size(pose,1) == 11
        % 3d pose
        pose(1:3) = pose(1:3)+ change(1:3);
        pose(4:7) = quatmultiply(change(4:7)',pose(4:7)')';
        pose(8:10) = pose(8:10) .* change(8:10);
        pose(11) = xor(pose(11),change(11));
    elseif size(pose,1) == 6
        % 2d pose
        error('Not yet implemented.');
    else
        error('Unknown pose type.');
    end
end
