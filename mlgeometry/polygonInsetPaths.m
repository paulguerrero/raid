% polygonInsetPaths(px,py,type,originx,originy,angleres)
% polygonInsetPaths(px,py,type,originangle,angleres) % angle from skeleton center
function [pathstart,pathend,pathstarttime,pathendtime,origintraj] = ...
    polygonInsetPaths(in1,in2,in3,origintype,originin1,originin2,originin3)
        
    if isa(in1,'PolygonSkeleton')
        if not(isempty(in2)) || not(isempty(in3))
            error(['Invalid parameters, when passing a polygon skeleton, ' ...
                   'the 2nd and 3rd parameter must be empty.']);
        end
        
        sx = in1.verts(1,:);
        sy = in1.verts(2,:);
        st = in1.verttime;
        binds = in1.boundaryvertinds;
        efrom = in1.edges(1,:);
        eto = in1.edges(2,:);
        
        px = in1.polygon(1,:);
        py = in1.polygon(2,:);

    else
        
        px = in1;
        py = in2;
        timetype = in3;
        
        if any(isnan(px)) || any(isnan(px))
            error('polygons with holes or multiple components not supported.');
        end
        
%         if ispolycw(px,py)
%             [px,py] = poly2ccw(px,py);
%         end
        
        % merge vertices that are too close together (that would be less than
        % 10 units apart in integer coordinates) to avoid numeric issues with
        % the skeleton
        [bbmax,bbmin] = pointsetBoundingbox([px;py]);

        maxcoord = max(max(abs([bbmax,bbmin])));

        % leave room for about 7 bit for larger values, that leaves
        % about 25 bits for precision (or 24 because signed),
        % that are about 7 significant decimal digits
        scale = (double(intmax('int32') / 128.0)) / maxcoord;
        ipx = round(px.*scale);
        ipy = round(py.*scale);

        [mindiff,mindiffind] = min(sqrt(sum(diff([ipx([1:end,1]);ipy([1:end,1])],1,2).^2,1)));

        while mindiff < 10 && size(ipx,2) > 3
            mergeind1 = mindiffind;
            mergeind2 = mod(mindiffind,size(ipx,2))+1;
            ipx(mergeind1) = (ipx(mergeind1) + ipx(mergeind2))/2;
            ipy(mergeind1) = (ipy(mergeind1) + ipy(mergeind2))/2;
            ipx(mergeind2) = [];
            ipy(mergeind2) = [];
            px(mergeind1) = (px(mergeind1) + px(mergeind2))/2;
            py(mergeind1) = (py(mergeind1) + py(mergeind2))/2;
            px(mergeind2) = [];
            py(mergeind2) = [];

            [mindiff,mindiffind] = min(sqrt(sum(diff([ipx([1:end,1]);ipy([1:end,1])],1,2).^2,1)));
        end

        if mindiff < 10
            error('Numeric issues with this shape, vertices are too close together.')
        end

        [sx,sy,st,binds,efrom,eto] = polygonInnerVoronoiDiagram(px,py,timetype);    
    end
    
    if nargin < 4 || isempty(origintype)
        origintype = 'firstpoint';
    end
    
    if strcmp(origintype,'firstpoint')
        originx = px(1);
        originy = py(1);
        if nargin < 5 || isempty(originin1)
            angleres = 64;
        else
            angleres = originin1;
        end
    elseif strcmp(origintype,'centerangle')
        if isempty(originin1)
            error('invalid parameters for given origin type')
        end
        
        % find the origin
        [~,~,pextent] = pointsetBoundingbox([px;py]);
        [dirx,diry] = pol2cart(originin1,pextent*2);
        dir = [dirx;diry];
        [~,scenterind] = max(st);
        scenter = [sx(scenterind);sy(scenterind)];
        start = scenter;

        [intersects,ip] = linesegLinesegIntersection2D_mex(...
            [px;py],[px([2:end,1]);py([2:end,1])],...
            start,start+dir);
        isegind = find(intersects>0);
        if isempty(isegind)
            error('no intersection with polygon, skeleton center must be outside the polygon');
        end
        ix = ip(:,:,1);
        ix = ix(isegind);
        iy = ip(:,:,2);
        iy = iy(isegind);
%         ix = intersects.intMatrixX(isegind);
%         iy = intersects.intMatrixY(isegind);
        idists = sqrt((ix-start(1)).^2 + (iy-start(2)).^2);
        [~,maxdistind] = max(idists);
        originx = ix(maxdistind);
        originy = iy(maxdistind);
        
        if nargin < 6 || isempty(originin2)
            angleres = 64;
        else
            angleres = originin3;
        end
    elseif strcmp(origintype,'borderpoint')
        if isempty(originin1) || isempty(originin2)
            error('invalid parameters for given origin type')
        end
        originx = originin1;
        originy = originin2;
        if nargin < 7 || isempty(originin3)
            angleres = 64;
        else
            angleres = originin3;
        end
    else
        error('unknown origin type')
    end
    
    if nargout >= 5
        % build the inset paths
        [pathstart,pathend,pathstarttime,pathendtime,origintraj] = ...
            skeletonInsetPaths(sx,sy,st,binds,efrom,eto,angleres,originx,originy);
    else
        % build the inset paths
        [pathstart,pathend,pathstarttime,pathendtime] = ...
            skeletonInsetPaths(sx,sy,st,binds,efrom,eto,angleres,originx,originy);    
    end
end
