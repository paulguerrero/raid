function p = identpose2D
    p = [...
       0;0;... % position
       0;...   % rotation
       1;1;... % scale
       0];     % mirrored
end
