#include "mex.h"

#include<vector>

#include "mexutils.h"
#include "geom.h"

/* the gateway function */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    if(nrhs != 3) {
        mexErrMsgTxt("Three inputs required.");
    }
    if(nlhs < 1 || nlhs > 2) {
        mexErrMsgTxt("One or two outputs required.");
    }
    
    mwSize dim ,n, np, nl;
    
    // read points
    std::vector<double> p;
    readDoubleMatrix(prhs[0], p, dim, np);
    if (dim != 3) {
        mexErrMsgTxt("Input must be three-dimensional.");
    }
    
    // read linesegs
    std::vector<double> l1;
    std::vector<double> l2;
    readDoubleMatrix(prhs[1], l1, dim, nl);
    if (dim != 3) {
        mexErrMsgTxt("Input must be three-dimensional.");
    }
    
    readDoubleMatrix(prhs[2], l2, dim, n);
    if (dim != 3) {
        mexErrMsgTxt("Input must be three-dimensional.");
    }
    if (n != nl) {
        mexErrMsgTxt("Input size does not match.");
    }
    
    // compute distance
    std::vector<double> dist(np*nl);
    std::vector<double> t(np*nl);
    if (np >= 1 && nl >= 1) {
        mwIndex ind;
        Vector point;
        Lineseg lseg;
        for (mwIndex li=0; li<nl; li++) {
            for (mwIndex pi=0; pi<np; pi++) {
                
                ind = pi*dim;
                point = Vector(p[ind],p[ind+1],p[ind+2]);

                ind = li*dim;
                lseg.P0 = Vector(l1[ind],l1[ind+1],l1[ind+2]);
                lseg.P1 = Vector(l2[ind],l2[ind+1],l2[ind+2]);
                
                ind = li*np+pi;
                dist[ind] = pointLinesegDistance3D(point,lseg,t[ind]);
            }
        }
    }
    p.clear();
    l1.clear();
    l2.clear();
    
    // write outputs
    plhs[0] = mxCreateDoubleMatrix(np,nl,mxREAL);
    std::copy(dist.begin(),dist.end(),mxGetPr(plhs[0]));
    dist.clear();
    
    // t is normalized distance of closest point along line segment
    if (nlhs >= 2) {
        plhs[1] = mxCreateDoubleMatrix(np,nl,mxREAL);
        std::copy(t.begin(),t.end(),mxGetPr(plhs[1]));
    }
    t.clear();
}
