% returns the collision time t (= 0 at start of movement or 1 at end)
function [t,f] = morphingSegPointCollision(v1start,v1end,v2start,v2end,p)

    xa = v1start(1,:);
    xb = v2start(1,:);
    xc = v1end(1,:);
    xd = v2end(1,:);
    
    ya = v1start(2,:);
    yb = v2start(2,:);
    yc = v1end(2,:);
    yd = v2end(2,:);
    
    xh = p(1,:);
    yh = p(2,:);
    
    % mathematica solution for f of:
    % (pa*(1-t)+pc*t) * (1-f) + (pb*(1-t)+pd*t) * f = ph
    % where pa = v1start pb = v2start, pc = v1end, pd = v2end and ph = p
    f_term1 = -1./(2.*((xc-xd).*(ya-yb)-(xa-xb).*(yc-yd)));
    f_term2 = -2.*xc.*ya + xd.*ya + xh.*ya + xc.*yb - xh.*yb + 2.*xa.*yc - xb.*yc - xh.*yc - xa.*yd + xh.*yd - xa.*yh + xb.*yh + xc.*yh - xd.*yh;
    term3 = sqrt((2.*xa.*yc - xb.*yc - xa.*yd + xh.*(ya - yb - yc + yd) + xd.*(ya - yh) - xa.*yh + xb.*yh + xc.*(-2.*ya + yb + yh)).^2 - ...
                 4.*((xc - xd).*(ya - yb) - (xa - xb).*(yc - yd)) .* ...
            	 (xh.*(-ya + yc) + xc.*(ya - yh) + xa.*(-yc + yh)));

    f1 = real(f_term1 .* (f_term2+term3));
    f2 = real(f_term1 .* (f_term2-term3)); % optional
    
    t1mask = f1 >= 0 & f1 <= 1;
    t2mask = f2 >= 0 & f2 <= 1;
    t12inds = find(t1mask | t2mask);
    t1mask = t1mask & not(t2mask);
    t2mask = t2mask & not(t1mask);
    
    % mathematica solution for t of:
    % (pa*(1-t)+pc*t) * (1-f) + (pb*(1-t)+pd*t) * f = ph
    % where pa = v1start pb = v2start, pc = v1end, pd = v2end and ph = p
    t_term1 = 1./(2.*((xb-xd).*(ya-yc)-(xa-xc).*(yb-yd)));
    t_term2 = 2.*xb.*ya - xd.*ya - xh.*ya - 2.*xa.*yb + xc.*yb + xh.*yb - xb.*yc + xh.*yc + xa.*yd - xh.*yd + xa.*yh - xb.*yh - xc.*yh + xd.*yh;
			 
    % two solutions:
	t1 = real(t_term1 .* (t_term2+term3));
    t2 = real(t_term1 .* (t_term2-term3));
    
    negmask = t1<0;
    t1(negmask) = nan;
    f1(negmask) = nan;
    negmask = t2<0;
    t2(negmask) = nan;
    f2(negmask) = nan;
    
    t = nan(1,numel(xa));
    f = nan(1,numel(xa));
    t(t2mask) = t2(t2mask);
    t(t1mask) = t1(t1mask);
    f(t2mask) = f2(t2mask);
    f(t1mask) = f1(t1mask);
    t1lt2mask = not(isnan(t1(t12inds))) & not(t12inds > t2(t12inds));
    t(t12inds(t1lt2mask)) = t1(t12inds(t1lt2mask));
    t(t12inds(not(t1lt2mask))) = t2(t12inds(not(t1lt2mask)));
    f(t12inds(t1lt2mask)) = f1(t12inds(t1lt2mask));
    f(t12inds(not(t1lt2mask))) = f2(t12inds(not(t1lt2mask)));
end
