function [area,a,b,c] = triangleArea(p1,p2,p3)

    % A = tri_area(P1, P2, P3)
    %
    % DESC:
    % calculates the triangle area given the triangle vertices (using Heron's
    % formula)
    %
    % AUTHOR
    % Marco Zuliani - zuliani@ece.ucsb.edu
    %
    % VERSION:
    % 1.0
    %
    % INPUT:
    % P1, P2, P3 = triangle vertices
    %
    % OUTPUT:
    % A          = triangle area

    u1 = p1 - p2;
    u2 = p1 - p3;
    u3 = p3 - p2;

    a = sqrt(sum(u1.^2,1));
    b = sqrt(sum(u2.^2,1));
    c = sqrt(sum(u3.^2,1));

    % Stabilized Heron formula
    % see: http://http.cs.berkeley.edu/%7Ewkahan/Triangle.pdf
    %
    % s = semiperimeter
    % A = sqrt(s * (s-a) * (s-b) * (s-c))

    % sort the elements
    v = sort([a;b;c],1);
    a = v(3,:);
    b = v(2,:);
    c = v(1,:);

    temp = b + c;
    v1 = a + temp;
    temp = a - b;
    v2 = c - temp;
    v3 = c + temp;
    temp = b - c;
    v4 = a + temp;
    area = 0.25 .* sqrt(max(0,v1.*v2.*v3.*v4)); % max necessary for degenerate cases where the triangle is a line

return