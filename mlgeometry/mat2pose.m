% find best matching pose for a 4x4 transformation matrix
function [targetpose,err] = mat2pose(mat,currentpose)
    
    % try to find best matching decomposition of upper left 3x3 part into:
    % rotation * scaling * mirroring
    % where
    % mirroring = diag([1,m,1]), m=-1 if mirroring, 1 otherwise
    % scaling = diag([s1,s2,s3])
    % rotation = is a rotation matrix (orthonormal columns)
    % this is not always possible, as poses cannot represent shearing or
    % anything that results in a homogeneous coordinate ~= 1
    % (would need rotation after AND before the non-uniform scaling for general shearing)

    rm = mat(1:3,1:3);
    
    mirroring = dot(cross(rm(1:3,1),rm(1:3,2)),rm(1:3,3)) < 0;

    [u,s,v] = svd(rm(1:3,1:3));
    
    err = max(abs([s(1,1)-s(2,2),s(1,1)-s(3,3)])); % deviation from uniform scaling

    if err < 0.0001
        % uniform scaling
        scaling = ones(3,1) .* mean(diag(s));
    else
        % non-uniform scaling
        
        % v must be a permutation matrix, since shearing is not supported
        % if it is not, find the closest thing to a permutation possible

        % since v is orthonormal, cols and rows must sum to 1, find difference
        % to a permutation matrix as difference of the largest elements in each
        % row and column from 1.
        err = sum(max(1-max(abs(v),[],1),1-max(abs(v),[],2)'));
        
        if err < 0.00001
            v = round(v);
        else
            if err < 0.01
                v = round(v);
            else
                % todo: find closest permutation with hungarian method
                % (same thing as assignment problem)
                error('Finding the closest permutation is not implemented yet.');
            end
        end

        % because u * s * v' = u * v' * diag(diag(s)'*abs(v')) if abs(v) is just a permutation matrix
        % diag( diag(s)'*abs(v') ) = diag( abs(v) * diag(s) ) and scaling is the
        % diag of that matrix
        scaling = abs(v) * diag(s);
    end
    
    rotmat = u * v';
    
    if mirroring
        % otherwise it would not be a rotation matrix (change of handedness of basis)
        % and dcm2quat expects a rotation matrix
        rotmat(:,2) = rotmat(:,2) .*-1;
    end
    rotquat = dcm2quat(rotmat')'; % transpose because dcm2quat produces angles*-1 otherwise
    
    translate = mat(1:3,4);
    
    targetpose = [translate;rotquat;scaling;mirroring];
    if nargin >= 2 && not(isempty(currentpose))
        targetpose = posetransform(currentpose,targetpose);
    end
    
end
