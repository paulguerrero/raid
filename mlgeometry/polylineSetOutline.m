% Holes are only found if they are not connected to the outline through a
% polyline.
% Also separate components inside holes are not supported.
function outline = polylineSetOutline(poly,closed,keeptails,keepholes,snapdist)
    if not(iscell(poly))
        poly = {poly};
    end
    
    if nargin < 3
        keeptails = false;
    end
    
    if nargin < 4
        keepholes = false;
    end
    
    if nargin < 5
        snapdist = 0;
    end
    
    outline = {};
    outlinecompind = [];
    
    if all(cellfun(@(x) isempty(x),poly))
        return;
    end
    
    % create a graph of all vertices, intersections and connecting edges
    [verts,edges] = polylineSet2Graph(poly,closed,snapdist);
    
    [compverts,compedges] = graphConnectedComponents(verts,edges);
    
    for k=1:numel(compverts)
        verts = compverts{k};
        edges = compedges{k};
        
        % find adjacent vertices and angles of adjacent vertices
        adjvinds = cell(1,size(verts,2));
        adjvertangles = cell(1,size(verts,2));
        adjeinds = cell(1,size(verts,2));
        for i=1:size(edges,2)
            adjvinds{edges(1,i)}(end+1) = edges(2,i);
            adjvinds{edges(2,i)}(end+1) = edges(1,i);
            adjeinds{edges(1,i)}(end+1) = i;
            adjeinds{edges(2,i)}(end+1) = i;
        end
        for i=1:size(verts,2)
            vecs = bsxfun(@minus,verts(:,adjvinds{i}),verts(:,i));
            [angles,~] = cart2pol(vecs(1,:),vecs(2,:));
            [~,perm] = sort(angles,'ascend');
            adjvertangles{i} = angles(perm);
            adjvinds{i} = adjvinds{i}(perm);
            adjeinds{i} = adjeinds{i}(perm);
        end
        
        % pick rightmost vertex (at the right edge of the bounding box)
        % as starting vertex (is guaranteed to be on outline)
        % and use angle pointing to the right as starting angle
        % (is guaranteed to point outwards)
        [~,startvind] = max(verts(1,:));
        startangle = 0;
        
        % walk along the outline and at intersections keep turning left
        % (leftmost segment) to get a clockwise outline
        vind = startvind;
        angle = startangle;
        vinds = [];
        einds = [];
        eind = [];
        tournumber = nan(1,size(verts,2));
        edgevisits = zeros(1,size(edges,2));
        lastnind = [];
        while true
            vinds = [vinds,vind]; %#ok<AGROW>
            einds = [einds,eind]; %#ok<AGROW>
            edgevisits(eind) = edgevisits(eind)+1;
            if isnan(tournumber(vind))
                tournumber(vind) = numel(vinds);
            end

            relangles = mod((adjvertangles{vind}-angle)+pi,2*pi)-pi;
            relangles(lastnind) = -2*pi;
            mask = relangles >= 0;
            relangles(mask) = relangles(mask)-2*pi;
            [~,ind] = max(relangles);
            eind = adjeinds{vind}(ind);
            lastvind = vind;
            vind = adjvinds{vind}(ind);
            
            % check if the last two vertices have been visited in the same
            % order already; if so -> done
            if (tournumber(vind) == tournumber(lastvind)+1) || ...
               (numel(vinds) > 2 && isnan(vinds(end-1)) && tournumber(vind) == tournumber(lastvind)+2)
                vinds(end) = [];
                break;
            end

            lastnind = find(adjvinds{vind} == lastvind);
            if isempty(lastnind)
                error('Invalid connectivity, neigbour not connected.')
            end
            angle = adjvertangles{vind}(lastnind);
        end
        
        
        if keeptails
            outline{end+1} = verts(:,vinds); %#ok<AGROW>
            outlinecompind(end+1) = k; %#ok<AGROW>
        else
            % remove inner edges
            outlinevinds = vinds;
            outlineeinds = einds;
            innereindmask = true(1,size(edges,2));
            innereindmask(outlineeinds) = false;
            for i=1:numel(adjvinds)
                mask = innereindmask(adjeinds{i});
                adjvinds{i}(mask) = [];
                adjvertangles{i}(mask) = [];
                adjeinds{i}(mask) = [];
            end
            
            unvisitededges = true(1,size(edges,2));
            
            % do not use twice-visited edges as starting edges, these edges
            % must be part of a tail
            unvisitededges(edgevisits > 1) = false;

            % flip edges so they have the direction of the outline (edges
            % visisted twice won't be used anyway)
            for i=1:size(outlineeinds,2)
                if edges(1,outlineeinds(i)) ~= outlinevinds(i) || ...
                   edges(2,outlineeinds(i)) ~= outlinevinds(mod(i,size(outlineeinds,2))+1)
                    edges(:,outlineeinds(i)) = edges([2,1],outlineeinds(i));
                end
                
                % just for debug
                if edges(1,outlineeinds(i)) ~= outlinevinds(i) || ...
                   edges(2,outlineeinds(i)) ~= outlinevinds(mod(i,size(outlineeinds,2))+1)
                    error('edge still not right.');
                end
            end
            
            
            % walk among the outer vertices in the opposite direction
            % (opposite starting angle) still always turning left
            % to get the faces of the outer vertices
            while true
                % pick an unvisited edge of the outline
                starteind = outlineeinds(find(unvisitededges(outlineeinds),1,'first'));
                if isempty(starteind)
                    break;
                end
                
                % start: first vertex of edge and angle from first to second vertex,
                % so the walk goes opposite to the outline
                vind = edges(1,starteind);
                dir = verts(:,edges(2,starteind)) - ...
                      verts(:,edges(1,starteind)); % away from start vertex
                angle = cart2pol(dir(1),dir(2));
                vinds = [];
                eind = [];
                tournumber = nan(1,size(verts,2));
                lastnind = [];
                while true
                    vinds = [vinds,vind]; %#ok<AGROW>
                    unvisitededges(eind) = false;
                    if isnan(tournumber(vind))
                        tournumber(vind) = numel(vinds);
                    end

                    relangles = mod((adjvertangles{vind}-angle)+pi,2*pi)-pi;
                    relangles(lastnind) = -2*pi;
                    mask = relangles >= 0;
                    relangles(mask) = relangles(mask)-2*pi;
                    [~,ind] = max(relangles);
                    eind = adjeinds{vind}(ind);
                    lastvind = vind;
                    vind = adjvinds{vind}(ind);

                    % if at a vertex already visited => done and the vertex is
                    % the starting vertex
                    if not(isnan(tournumber(vind)))
                        vinds = vinds(tournumber(vind):end);
                        unvisitededges(eind) = false;
                        break;
                    end

                    lastnind = find(adjvinds{vind} == lastvind);
                    if isempty(lastnind)
                        error('Invalid connectivity, neigbour not connected.')
                    end
                    angle = adjvertangles{vind}(lastnind);
                end
                
                outline{end+1} = verts(:,vinds); %#ok<AGROW>
                outlinecompind(end+1) = k; %#ok<AGROW>
            end
        end
    end
    
    % remove components completely contained in other components
    % (components cannot intersect, otherwise they would be in the same
    % component)
    % these components would be holes, but some holes are not found anyway
    % (the ones connected to the outline through lines), so holes are not
    % supported for now
    firstoutlineverts = cell2mat(cellfun(@(x) x(1:2,1),outline,'UniformOutput',false));
%     deletecomps = false(1,numel(outline));
    contains = false(numel(outline));
    for i=1:numel(outline)
        in = inpoly(firstoutlineverts',outline{i}')';
        in(i) = false; % outline not inside itself
        % outline not inside other outline of same component,
        % holes in same component as border are not supported
        % (would be done differently => inner edges of outline
        % new component, iterate until all edges visited. This new
        % component is always inside the parent component. If outline is
        % split, the resulting outlines do not have containment relations)
        in(outlinecompind == outlinecompind(i)) = false;
        contains(i,:) = in;
    end
    
    deletecomps = false(1,numel(outline));
    for i=1:numel(outline)
        containedby = find(contains(:,i));
        if numel(containedby) == 1
            deletecomps(i) = true;
            if keepholes
                [outline{i}(1,:),outline{i}(2,:)] = poly2ccw(outline{i}(1,:),outline{i}(2,:)); %#ok<AGROW>
                outline{containedby} = [outline{containedby},nan(2,1),outline{i}]; %#ok<AGROW>
            end
        elseif numel(containedby) > 1
            % contained in multiple component => this means it is a hole in
            % a hole, i.e. a separate component these are not supported
            % right now
            deletecomps(i) = true;
        end
    end
    
    outline(deletecomps) = [];
end
