% POLYGONMINKOWSKISUM Minkowski sum of two polygons.
%   [ox,oy] = POLYGONMINKOWSKISUM(px,py,qx,qy) computes the minkowski sum
%   P+Q of the two polygons P and Q given by px,py and qx,qy. If P is a
%   polygon with N vertices and Q a polygon with M vertices, px and py must
%   be 1xN arrays, qx and qy 1xM arrays of the x and y coordinates of each
%   vertex. px,py and qx,qy can contain multiple polygon contours, either
%   separated by NaNs or each contour in a separate cell. The contours
%   should form a single simple polygon, possibly with holes.
%   The output ox and oy are 1xM cell arrays, where M is the number of
%   output polygons. Each cell contains an 1xL cell array, where L is the
%   number of contours of the output polygon. The first contour is always
%   the outer contour, the remaining contours are inner contours (holes).
%
%   [ox,oy] = POLYGONMINKOWSKISUM(px,py,qx,qy,dilation) additionally
%   specifies if the operations should be a dilation (dilation = true)or an
%   erosion (dilation = false). In case of an erosion, Q-P is computed
%   instead of P+Q.
%
%   See also POLYGONOFFSET
function [ox,oy] = polygonMinkowskiSum(px,py,qx,qy,dilation,negatep,negateq)
    
    if not(iscell(px))
        [px,py] = polysplit(px,py);
    end
    if not(iscell(qx))
        [qx,qy] = polysplit(qx,qy);
    end
    
    pn = cellfun(@(a) numel(a),px);
    px = cell2mat(px);
    py = cell2mat(py);
    
    qn = cellfun(@(a) numel(a),qx);
    qx = cell2mat(qx);
    qy = cell2mat(qy);
    
    % keep winding order of polygon Q
    % (minkowski may returns contours in any winding order)
    if ispolycw(qx(1:qn(1)),qy(1:qn(1)))
        outercw = true;
    else
        outercw = false;
    end
    
    if nargin >= 7
        [sumx,sumy,sumn] = polygonMinkowskiSum_mex(px,py,pn,qx,qy,qn,dilation,negatep,negateq);
    elseif nargin >= 6
        [sumx,sumy,sumn] = polygonMinkowskiSum_mex(px,py,pn,qx,qy,qn,dilation,negatep);
    elseif nargin >= 5
        [sumx,sumy,sumn] = polygonMinkowskiSum_mex(px,py,pn,qx,qy,qn,dilation);
    else
        [sumx,sumy,sumn] = polygonMinkowskiSum_mex(px,py,pn,qx,qy,qn);
    end
    
    % change polygon format to 1 cell per contour per polygon
    ox = cell(1,numel(sumn));
    oy = cell(1,numel(sumn));
    offset = 0;
    for i=1:numel(sumn)
        ox{i} = cell(1,numel(sumn{i}));
        oy{i} = cell(1,numel(sumn{i}));
        for j=1:numel(sumn{i})
            if (j==1 && outercw) || (j~=1 && not(outercw))
                [ox{i}{j},oy{i}{j}] = poly2cw(...
                    sumx(offset+1:offset+sumn{i}(j)),...
                    sumy(offset+1:offset+sumn{i}(j)));
            else
                [ox{i}{j},oy{i}{j}] = poly2ccw(...
                    sumx(offset+1:offset+sumn{i}(j)),...
                    sumy(offset+1:offset+sumn{i}(j)));
            end
            offset = offset + sumn{i}(j);
        end
    end
end
