#include "mex.h"

#include<vector>

#include "mexutils.h"
#include "geom.h"


/* the gateway function */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    if(nrhs != 4) {
        mexErrMsgTxt("Four inputs required.");
    }
    if(nlhs < 1 || nlhs > 3) {
        mexErrMsgTxt("One to three outputs required.");
    }
    
    mwSize dim ,n, nl1, nl2;
    
    // read lineseg1
    std::vector<double> l1p1;
    std::vector<double> l1p2;
    readDoubleMatrix(prhs[0], l1p1, dim, nl1);
    if (dim != 3) {
        mexErrMsgTxt("Input must be three-dimensional.");
    }
    
    readDoubleMatrix(prhs[1], l1p2, dim, n);
    if (dim != 3) {
        mexErrMsgTxt("Input must be three-dimensional.");
    }
    if (n != nl1) {
        mexErrMsgTxt("Input size does not match.");
    }
    
    // read lineseg2
    std::vector<double> l2p1;
    std::vector<double> l2p2;
    readDoubleMatrix(prhs[2], l2p1, dim, nl2);
    if (dim != 3) {
        mexErrMsgTxt("Input must be three-dimensional.");
    }
    
    readDoubleMatrix(prhs[3], l2p2, dim, n);
    if (dim != 3) {
        mexErrMsgTxt("Input must be three-dimensional.");
    }
    if (n != nl2) {
        mexErrMsgTxt("Input size does not match.");
    }
        
    // compute distance
    std::vector<double> dist(nl1*nl2);
    std::vector<double> t1(nl1*nl2);
    std::vector<double> t2(nl1*nl2);
    if (nl1 >= 1 && nl2 >= 1) {
        mwIndex ind;
        Lineseg lseg1;
        Lineseg lseg2;
        for (mwIndex l2i=0; l2i<nl2; l2i++) {
            for (mwIndex l1i=0; l1i<nl1; l1i++) {
                
                ind = l1i*dim;
                lseg1.P0 = Vector(l1p1[ind],l1p1[ind+1],l1p1[ind+2]);
                lseg1.P1 = Vector(l1p2[ind],l1p2[ind+1],l1p2[ind+2]);

                ind = l2i*dim;
                lseg2.P0 = Vector(l2p1[ind],l2p1[ind+1],l2p1[ind+2]);
                lseg2.P1 = Vector(l2p2[ind],l2p2[ind+1],l2p2[ind+2]);
                
                ind = l2i*nl1+l1i;
                dist[ind] = lineLineDistance3D(lseg1,lseg2,t1[ind],t2[ind]);
            }
        }
    }
    l1p1.clear();
    l1p2.clear();
    l2p1.clear();
    l2p2.clear();
    
    // write outputs
    plhs[0] = mxCreateDoubleMatrix(nl1,nl2,mxREAL);
    std::copy(dist.begin(),dist.end(),mxGetPr(plhs[0]));
    dist.clear();
    
    if (nlhs >= 2) {
        plhs[1] = mxCreateDoubleMatrix(nl1,nl2,mxREAL);
        std::copy(t1.begin(),t1.end(),mxGetPr(plhs[1]));
    }
    t1.clear();
    
    if (nlhs >= 3) {
        plhs[2] = mxCreateDoubleMatrix(nl1,nl2,mxREAL);
        std::copy(t2.begin(),t2.end(),mxGetPr(plhs[2]));
    }
    t2.clear();
}

