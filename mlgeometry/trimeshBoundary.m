function bndry = trimeshBoundary(face)
%     if size(face,1)<size(face,2)
%         face=face';
%     end
%     warning('this problably does not work in every case and only seems to find one boundary');

    face = face';

    nvert=max(max(face));
    nface=size(face,1);

    % compute number of incident faces for each edge
    E=sparse(nvert,nvert);
    for i=1:nface
        f=face(i,:);
        E(f(1),f(2))=E(f(1),f(2))+1;
        E(f(1),f(3))=E(f(1),f(3))+1;
        E(f(3),f(2))=E(f(3),f(2))+1;
    end
    E=E+E';

    % find vertex on the boundary
%     bvertinds = find(any(E==1,2));
%     [bedgefrom,bedgeto] = find(E==1);
    
%     bndry = {};
%     for i=1:nvert
%         u=find(E(i,:)==1);
%         if ~isempty(u)
%             bndry=[i u(1)];
%             break;
%         end
%     end
    
%     if isempty(bndry)
%         % no boundary vertices found, no boundary exists
%         return;
%     end


    bndry = {};
    unvisited = false(1,nvert);
    unvisited(any(E==1,2)) = true;
    while any(unvisited)
        % find unvisited boundary vertex
        bndry{end+1} = find(unvisited,1,'first'); %#ok<AGROW>
        unvisited(bndry{end}) = false;
        
        while numel(bndry{end}) <= nvert
            
            % find neighbor boundary vertices
            neighborbverts=find(E(bndry{end}(end),:)==1);
            if numel(neighborbverts)~=2
                warning('Problem in boundary: Boundary vertex adjacent to less or more than two boundary edges.');
            end
            
            % pick neighbor boundary vertex that is not the vertex we just
            % came from
            if numel(bndry{end}) > 1 && neighborbverts(1) == bndry{end}(end-1)
                nextvert=neighborbverts(2);
            else
                nextvert=neighborbverts(1);
            end
            
            if nextvert==bndry{end}(1)
                % reached start of boundary: boundary is closed
                break;
            end
            
            if numel(bndry{end}) == nvert
                warning('Problem in boundary: boundary is not closed.');
            end
            
            bndry{end}(end+1) = nextvert;
            unvisited(nextvert) = false;
        end
    end

%     nextvert=bndry(2);
%     i=2;
%     while(i<=nvert)
%         neighborbverts=find(E(nextvert,:)==1);
%         if length(neighborbverts)~=2
%             warning('problem in boundary');
%         end
%         if neighborbverts(1)==bndry(i-1)
%             nextvert=neighborbverts(2);
%         else
%             nextvert=neighborbverts(1);
%         end
%         if nextvert~=bndry(1)
%             bndry=[bndry nextvert];
%         else
%             break;
%         end
%         i=i+1;
%     end
% 
%     if i>nvert
%         warning('problem in boundary');
%     end
end
