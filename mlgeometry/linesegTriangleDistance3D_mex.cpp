#include "mex.h"

#include<vector>

#include "mexutils.h"
#include "geom.h"

/* the gateway function */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    if(nrhs != 5) {
        mexErrMsgTxt("Five inputs required.");
    }
    if(nlhs < 1 || nlhs > 5) {
        mexErrMsgTxt("One to five outputs required.");
    }
    
    mwSize dim ,n, nl, nt;
    
    // read lines
    std::vector<double> p1;
    std::vector<double> p2;
    readDoubleMatrix(prhs[0], p1, dim, nl);
    if (dim != 3) {
        mexErrMsgTxt("Input must be three-dimensional.");
    }
    
    readDoubleMatrix(prhs[1], p2, dim, n);
    if (dim != 3) {
        mexErrMsgTxt("Input must be three-dimensional.");
    }
    if (n != nl) {
        mexErrMsgTxt("Input size does not match.");
    }
    
    // read triangles
    std::vector<double> t1;
    std::vector<double> t2;
    std::vector<double> t3;
    readDoubleMatrix(prhs[2], t1, dim, nt);
    if (dim != 3) {
        mexErrMsgTxt("Input must be three-dimensional.");
    }
    
    readDoubleMatrix(prhs[3], t2, dim, n);
    if (dim != 3) {
        mexErrMsgTxt("Input must be three-dimensional.");
    }
    if (n != nt) {
        mexErrMsgTxt("Input size does not match.");
    }
    
    readDoubleMatrix(prhs[4], t3, dim, n);
    if (dim != 3) {
        mexErrMsgTxt("Input must be three-dimensional.");
    }
    if (n != nt) {
        mexErrMsgTxt("Input size does not match.");
    }
    
    // compute distance
    std::vector<double> dist(nl*nt);
    std::vector<double> t(nl*nt);
    std::vector<double> b1(nl*nt);
    std::vector<double> b2(nl*nt);
    std::vector<double> b3(nl*nt);
    if (nl >= 1 && nt >= 1) {
        mwIndex ind;
        Lineseg lseg;
        Triangle tri;
        for (mwIndex ti=0; ti<nt; ti++) {
            for (mwIndex li=0; li<nl; li++) {
                
                ind = li*dim;
                lseg.P0 = Vector(p1[ind],p1[ind+1],p1[ind+2]);
                lseg.P1 = Vector(p2[ind],p2[ind+1],p2[ind+2]);

                ind = ti*dim;
                tri.P0 = Vector(t1[ind],t1[ind+1],t1[ind+2]);
                tri.P1 = Vector(t2[ind],t2[ind+1],t2[ind+2]);
                tri.P2 = Vector(t3[ind],t3[ind+1],t3[ind+2]);
                
                ind = ti*nl+li;
                dist[ind] = linesegTriangleDistance3D(lseg,tri,t[ind],b1[ind],b2[ind],b3[ind]);
            }
        }
    }
    p1.clear();
    p2.clear();
    t1.clear();
    t2.clear();
    t3.clear();
    
    // write outputs
    plhs[0] = mxCreateDoubleMatrix(nl,nt,mxREAL);
    std::copy(dist.begin(),dist.end(),mxGetPr(plhs[0]));
    dist.clear();
    
    if (nlhs >= 2) {
        plhs[1] = mxCreateDoubleMatrix(nl,nt,mxREAL);
        std::copy(t.begin(),t.end(),mxGetPr(plhs[1]));
    }
    t.clear();
    
    if (nlhs >= 3) {
        plhs[2] = mxCreateDoubleMatrix(nl,nt,mxREAL);
        std::copy(b1.begin(),b1.end(),mxGetPr(plhs[2]));
    }
    b1.clear();
    
    if (nlhs >= 4) {
        plhs[3] = mxCreateDoubleMatrix(nl,nt,mxREAL);
        std::copy(b2.begin(),b2.end(),mxGetPr(plhs[3]));
    }
    b2.clear();
    
    if (nlhs >= 5) {
        plhs[4] = mxCreateDoubleMatrix(nl,nt,mxREAL);
        std::copy(b3.begin(),b3.end(),mxGetPr(plhs[4]));
    }
    b3.clear();
}
