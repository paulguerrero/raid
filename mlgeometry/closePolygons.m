% px,py: nan-separated polygons
function [x,y] = closePolygons(px,py)
    [x,y] = polysplit(px,py);
    x = cell2mat(cellfun(@(x) [x([1:end,1]),nan],x','UniformOutput',false));
    y = cell2mat(cellfun(@(y) [y([1:end,1]),nan],y','UniformOutput',false));
    x = x(1:end-1);
    y = y(1:end-1);
end
