% polygon interaction bisector surface
% computes an approximation using image processing functions
% needs the edgelink, findendsjunctions, lineseg and maxlinedev functions
% overlapstrategy: how to remove overlaps between the polygon sets
% 'ordered' : subtract polygon set 2 from polygon set 1 
% 'area' : subtract polygon set with smaller area from set with larger area
% segtype is:
%  0  - Start free, end free
%  1  - Start free, end junction
%  2  - Start junction, end free (should not happen)
%  3  - Start junction, end junction
%  5  - Loop
function [ibs,segtype] = polygonIBS(polys1,polys2,reshint,overlapstrategy)

    if nargin < 3 || isempty(reshint)
        reshint = 1000000; % one megapixel
    end
    
    if nargin < 4 || isempty(overlapstrategy)
        overlapstrategy = 'area';
    end
    
    % make polygons non-overlapping
    poly1 = polys1{1};
    for i=2:numel(polys1)
        [x,y] = polybool('union',polys1{i}(1,:),polys1{i}(2,:),poly1(1,:),poly1(2,:));
        poly1 = [x;y];
    end
    poly2 = polys2{1};
    for i=2:numel(polys2)
        [x,y] = polybool('union',polys2{i}(1,:),polys2{i}(2,:),poly2(1,:),poly2(2,:));
        poly2 = [x;y];
    end
    
    if strcmp(overlapstrategy,'area')
        poly1area = 0;
        for i=1:numel(polys1)
            poly1area = poly1area + abs(polyarea(polys1{i}(1,:),polys1{i}(2,:)));
        end
        poly2area = 0;
        for i=1:numel(polys2)
            poly2area = poly2area + abs(polyarea(polys2{i}(1,:),polys2{i}(2,:)));
        end
        
        if poly1area > poly2area
            [x,y] = polybool('subtraction',poly1(1,:),poly1(2,:),poly2(1,:),poly2(2,:));
            poly1 = [x;y];
        else
            [x,y] = polybool('subtraction',poly2(1,:),poly2(2,:),poly1(1,:),poly1(2,:));
            poly2 = [x;y];
        end
    elseif strcmp(overlapstrategy,'ordered')
        [x,y] = polybool('subtraction',poly2(1,:),poly2(2,:),poly1(1,:),poly1(2,:));
        poly2 = [x;y];
    else
        error('Unknown overlap strategy.');
    end
    
    if isempty(poly2)
        % target region is completely covered by source region
        ibs = cell(1,0);
        segtype = zeros(1,0);
        return;
    end
    
    [x,y] = polysplit(poly1(1,:),poly1(2,:));
    polys1 = cell(1,numel(x));
    for i=1:numel(x)
        polys1{i} = [x{i};y{i}];
    end
    [x,y] = polysplit(poly2(1,:),poly2(2,:));
    polys2 = cell(1,numel(x));
    for i=1:numel(x)
        polys2{i} = [x{i};y{i}];
    end
        
%     sourcepolys_old = sourcepolys;
%     sourcepolys = cell(1,0);
%     for i=1:numel(sourcepolys_old)
%         [x,y] = polygonOffset(sourcepolys_old{i}(1,:),sourcepolys_old{i}(2,:),-separationinset);
%         x = [x{:}];
%         y = [y{:}];
%         p = cell(1,numel(x));
%         for j=1:numel(x)
%             p{j} = [x{j};y{j}];
%         end
%         sourcepolys = [sourcepolys,p]; %#ok<AGROW>
%     end
% 
%     targetpolys_old = targetpolys;
%     targetpolys = cell(1,0);
%     for i=1:numel(targetpolys_old)
%         [x,y] = polygonOffset(targetpolys_old{i}(1,:),targetpolys_old{i}(2,:),-separationinset);
%         x = [x{:}];
%         y = [y{:}];
%         p = cell(1,numel(x));
%         for j=1:numel(x)
%             p{j} = [x{j};y{j}];
%         end
%         targetpolys = [targetpolys,p]; %#ok<AGROW>
%     end

    % get pixel grid resoution
    [bbmin,bbmax] = pointsetBoundingbox([polys1,polys2]);
    res = imgres((bbmax(1)-bbmin(1)) ./ (bbmax(2)-bbmin(2)),reshint);
    res = max(5,res); % at least 5 pixels in both dimensions    

    % rasterize polygons to images
    img1 = zeros(res(2),res(1));
    img2 = zeros(res(2),res(1));
    
    for i=1:numel(polys1)
        poly_img = bsxfun(@minus,polys1{i},bbmin) .* min((res-1)./(bbmax-bbmin)) + 1; % + 1 to get 1-based pixel indices
        img1 = polygonRasterization(img1,poly_img(1,:),poly_img(2,:),1,[],@xor); % xor should cut out holes correctly
    end
    for i=1:numel(polys2)
        poly_img = bsxfun(@minus,polys2{i},bbmin) .* min((res-1)./(bbmax-bbmin)) + 1; % + 1 to get 1-based pixel indices
        img2 = polygonRasterization(img2,poly_img(1,:),poly_img(2,:),1,[],@xor); % xor should cut out holes correctly
    end
    
    % erode rasterized polygons by one pixel to create space for the
    % skeleton
    img1 = bwmorph(img1,'erode',1);
    img2 = bwmorph(img2,'erode',1);

    % compute minimum distance to both polygon sets
    img1 = bwdist(img1);
    img2 = bwdist(img2);
    
    % compute skeleton on part where distance transforms to both polygons
    % sets are almost equal
    imgskel = bwmorph(abs(img1-img2) <= 1,'skel',inf);
    
    if not(any(imgskel(:)))
        ibs = cell(1,0);
        segtype = zeros(1,0);
        return;
    end
    
    % trace the skeleton to get the skeleton as polyline
    [edgelist,~,segtype] = edgelink(imgskel');
    ibs = lineseg(edgelist,1);
    
    % transform back to original coordinates
    for i=1:numel(ibs)
        ibs{i} = bsxfun(@plus,(ibs{i}'-1) .* max((bbmax-bbmin)./(res-1)),bbmin);
    end
end
