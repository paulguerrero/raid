% Relationship Distribution Histogram
classdef RegionavgDescriptorSet < RegionrelDescriptorSet
    
properties
    % values are ordered: innermost ring, 2nd innermost ring, etc.
    % and counterclockwise inside each ring, starting at direction [1,0]
    val = [];
    
    rad = zeros(1,0);
    
    pos = cell(1,0);
    
    nangular = 0;
    nradial = 0;
end

methods(Static)
function s = defaultSettings
    s = RegionrelDescriptorSet.defaultSettings;
    
    s.pointdensity = 10000; % points per image area
%     s.normalizebinarea = true; % normalize the histogram bins by the area of the bins
%     s.radius = 0.5; % radius in normalized annotation space (where the image is in [0,1])
    
    s.radf = 0.5;
    s.radlin = 0;
    
    s.normalizehist = 'binarea'; % normalization of histogram, see PointrelDecsriptorSet
    s.normalizeregion = 'regionarea'; % normalization of sum over region
    % - none: no normalization (just sum up descriptors, resulting values
    % depend on the size of the region)
    % - regionarea: normalize by region area (in practice, points are
    % assumed to be distributed equally over the area, su just divide by
    % number of points)
    % - descsum: normalize by sum of descriptor values (if used with 'none'
    % histogram normalization: distribution of relative position of points
    % in the target region relative to points in the source region)
end
end

methods

% obj = RegionavgDescriptorSet()
% obj = RegionavgDescriptorSet(obj2)
% obj = RegionavgDescriptorSet(nangular,nradial)
% obj = RegionavgDescriptorSet(nangular,nradial,rad,pos,val)
% obj = RegionavgDescriptorSet(nangular,nradial,sourceregions,targetregions,imgmin,imgmax,imgres)
% obj = RegionavgDescriptorSet(...,nvpairs)
function obj = RegionavgDescriptorSet(varargin)
    
    obj = obj@RegionrelDescriptorSet;
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'RegionavgDescriptorSet')
        obj.copyFrom(varargin{1});
    elseif numel(varargin) == 2 || (numel(varargin) >= 3 && ischar(varargin{3})) || ...
           numel(varargin) == 5 || (numel(varargin) >= 6 && ischar(varargin{6})) || ...
           numel(varargin) == 7 || (numel(varargin) >= 8 && ischar(varargin{8}))
        
        if numel(varargin) >= 7 && not(ischar(varargin{3})) && not(ischar(varargin{6}))
            obj.settings = nvpairs2struct(varargin(8:end),obj.settings);
        elseif numel(varargin) >= 5 && not(ischar(varargin{3}))
            obj.settings = nvpairs2struct(varargin(6:end),obj.settings);
        else
            obj.settings = nvpairs2struct(varargin(3:end),obj.settings);
        end
        
        obj.nangular = varargin{1};
        obj.nradial = varargin{2};
        
        obj.val = zeros(obj.nangular*obj.nradial,0);
        
        if numel(varargin) >= 7 && not(ischar(varargin{3})) && not(ischar(varargin{6}))
            % compute from image regions
            obj.compute(varargin{3:7})
        elseif numel(varargin) >= 5 && not(ischar(varargin{3}))
            % set manually
            if size(varargin{5},2) ~= size(varargin{3},2) || ...
               size(varargin{5},1) ~= obj.nangular * obj.nradial
                error('Invalid size for descriptor values.');
            end
            
            if size(varargin{4},2) ~= size(varargin{3},2)
                error('Invalid size for descriptor positions.');
            end
            
            obj.rad = varargin{3};
            obj.pos = varargin{4};
            obj.val = varargin{5};
        end
    else
        error('Invalid input arguments.');
    end
end

function delete(obj)
    % do nothing
    obj.clear;
end

function clear(obj)
    obj.clear@RegionrelDescriptorSet;
    
    obj.rad = zeros(1,0);
    obj.val = zeros(obj.nangular*obj.nradial,0);
    obj.pos = cell(1,0);
end

function copyFrom(obj,obj2)
    obj.copyFrom@RegionrelDescriptorSet(obj2);
    
    obj.val = obj2.val;
    
    obj.rad = obj2.rad;
    obj.pos = obj2.pos;
    
    obj.nangular = obj2.nangular;
    obj.nradial = obj2.nradial;
end

function obj2 = clone(obj)
    obj2 = RegionavgDescriptorSet(obj);
end

function obj2 = cloneSubset(obj,ind)
    obj2 = RegionavgDescriptorSet;
    
    obj2.nangular = obj.nangular;
    obj2.nradial = obj.nradial;
    
    obj2.settings = obj.settings;
    
    obj2.append(obj,ind);
end

% todo:
% - when averaging the histograms over A, use average weighted by (voronoi
% area if histogram position inside A) / (area of region A) to approximate
% the integral over A normalized by the area of A
% => for now we assume the points are approx. equally spaced in A = they
% have approx. the same voronoi area
function compute(obj,sourceregions,targetregions,imgmin,imgmax,imgres)
    obj.clear;
    
    if isempty(sourceregions)
        return;
    end
    
    
%     obj.rad = ones(1,numel(sourceregions)).*obj.settings.radius;
    
%     % get histogram radii
%     radl = zeros(1,numel(sourceregions));
%     % find maxmimally distant points in each source region
%     for i=1:numel(sourceregions)
%         sourcepolys = ImageAnnotation.an2imcoords([sourceregions{i}.polygon],imgmin,imgmax);
%         allpoints = [sourcepolys{:}];
%         sqdists = ...
%             bsxfun(@minus,allpoints(1,:)',allpoints(1,:)).^2 + ...
%             bsxfun(@minus,allpoints(2,:)',allpoints(2,:)).^2;
%         
%         radl(i) = sqrt(max(sqdists(:)))*0.5;
%     end
    
    obj.val = zeros(obj.nangular*obj.nradial,numel(sourceregions));    
    obj.rad = zeros(1,numel(sourceregions));
    obj.pos = cell(1,numel(sourceregions));
    for i=1:numel(sourceregions)

        if isempty(sourceregions{i})
            error('Empty source region.');
        end
        
        sourcepolys = ImageAnnotation.an2imcoords([sourceregions{i}.polygon],imgmin,imgmax);
        
        % get position and radius of point histograms
        if obj.settings.pointdensity == 1
            % get source region polygons without self-intersections
            mergedsourcepolyx = {sourcepolys{1}(1,:)};
            mergedsourcepolyy = {sourcepolys{1}(2,:)};
            if numel(sourcepolys) == 1
                [mergedsourcepolyx,mergedsourcepolyy] = polybool('union',...
                    mergedsourcepolyx,mergedsourcepolyy,...
                    mergedsourcepolyx,mergedsourcepolyy);
            elseif numel(sourcepolys) > 1
                for j=2:numel(sourcepolys) % also merge first polygon with itself to get rid of self-intersections
                    [mergedsourcepolyx,mergedsourcepolyy] = polybool('union',...
                        mergedsourcepolyx,mergedsourcepolyy,...
                        sourcepolys{j}(1,:),sourcepolys{j}(2,:));
                end
            end
            
            % one outer histogram at the region centroid
            suma = 0;
            posl = zeros(2,1);
            for j=1:numel(mergedsourcepolyx)
                [cx,cy,a] = polygonCentroid(mergedsourcepolyx{j},mergedsourcepolyy{j});
                posl = posl + [cx;cy] .* a; % inner contours (holes) are counter-clockwise and weighted by negative area
                suma = suma + a;
            end
            possize = suma;
            posl = posl ./ suma;
        else
            [posl,~,~,~,possize] = regiongridsamples(sourceregions{i},...
                imgmin,imgmax,obj.settings.pointdensity);
        end
        
        allpoints = [sourcepolys{:}];
        sqdists = ...
            bsxfun(@minus,allpoints(1,:)',allpoints(1,:)).^2 + ...
            bsxfun(@minus,allpoints(2,:)',allpoints(2,:)).^2;
        radl = sqrt(max(sqdists(:)))*obj.settings.radf;
        
        
        obj.pos{i} = ImageAnnotation.im2ancoords(posl,imgmin,imgmax);
        obj.rad(i) = ImageAnnotation.im2anradius(radl,imgmin,imgmax);
        
        if isempty(targetregions{i}) || isempty(posl)
            continue;
        end

        % approximate source region area as sum of area of rectangular
        % vornoi-like cells around outer positions
        sourceregionarea = prod(possize).*size(posl,2);
        posarea = repmat(sourceregionarea / size(posl,2),1,size(posl,2));
        
        pointdescriptors = PointrelDescriptorSet(...
            obj.nangular,obj.nradial,obj.rad(i),obj.pos{i},targetregions{i},...
            imgmin,imgmax,imgres,...
            'normalize',obj.settings.normalizehist,...
            'radlin',obj.settings.radlin);

        obj.val(:,i) = sum(bsxfun(@times,pointdescriptors.val,posarea),2);
        
        delete(pointdescriptors(isvalid(pointdescriptors)));
        
        if strcmp(obj.settings.normalizeregion,'none')
            % do nothing
        elseif strcmp(obj.settings.normalizeregion,'regionarea')
            obj.val(:,i) = obj.val(:,i) ./ sum(posarea);
        elseif strcmp(obj.settings.normalizeregion,'descsum')
            descsum = sum(obj.val(:,i));
            if descsum > 0
                obj.val(:,i) = bsxfun(@rdivide,obj.val(:,i),descsum);
            end
        else
            error('Unknown normalization method.');
        end
        
%         if not(obj.settings.normalizebinvol)
%             binarea = obj.binareas(angularpos,radialpos);
%             obj.val = bsxfun(@times,obj.val,binarea);
%         end
        
        
    end
end

function [d,dbin] = distance(obj,template,val,method)
    
    binweights = ones(size(val,1),1);
    
%     [angularpos,radialpos] = RegionavgDescriptorSet.binaxes(...
%             obj.nangular,obj.nradial,1);
    
%     % factor in bin area weight
%     binarea = RegionavgDescriptorSet.binareas(angularpos,radialpos);
%     binarea = binarea ./ sum(binarea); % divide by total descriptor area
%     binweights = binweights .* binarea;
%     
%     % factor in bin distance weight
%     bindist = repmat(radialpos,numel(angularpos),1);
%     bindist = bindist(:)./radialpos(end);
%     binweights = binweights .* (1./bindist);
    
    params = cell(1,0);
    if strcmp(method,'EMD')
        params{end+1} = PointrelDescriptorSet.bindistance(obj.nangular,obj.nradial);
    end
    
    d = zeros(size(template,2),size(val,2));
    for i=1:size(template,2)
        if nargout >= 2
            [d(i,:),dbin] = weighteddist(template(:,i),val,binweights,method,params{:});
        else
            d(i,:) = weighteddist(template(:,i),val,binweights,method,params{:});
        end
    end
end

function [s,sbin] = similarity(obj,template,val,method)
    
    binweights = ones(size(val,1),1);
    
    params = cell(1,0);
    if strcmp(method,'negativeEMD')
        % negative earth mover's distance
        
        params{end+1} = PointrelDescriptorSet.bindistance(obj.nangular,obj.nradial);
    end
    
    s = zeros(size(template,2),size(val,2));
    for i=1:size(template,2)
        if nargout >= 2
            [s(i,:),sbin] = weightedsim(template(:,i),val,binweights,method,params{:});
        else
            s(i,:) = weightedsim(template(:,i),val,binweights,method,params{:});
        end
    end
end

function b = iscompatible(obj,obj2)
    b = obj.nangular == obj2.nangular && ...
        obj.nradial == obj2.nradial && ...
        isequaln(obj.settings,obj2.settings);
end

function append(obj,obj2,ind2)
    if not(iscompatible(obj,obj2))
        error('Descriptors are not compatible.');
    end
    
    if nargin < 3
        ind2 = 1:size(obj2.val,2);
    end
    
    obj.val = [obj.val,obj2.val(:,ind2)];
    obj.rad = [obj.rad,obj2.rad(:,ind2)];
    obj.pos = [obj.pos,obj2.pos(:,ind2)];
end

function keepSubset(obj,ind)
    obj.val = obj.val(:,ind);
    obj.rad = obj.rad(:,ind);
    obj.pos = obj.pos(:,ind);
end

function deleteSubset(obj,ind)
    obj.val(:,ind) = [];
    obj.rad(:,ind) = [];
    obj.pos(:,ind) = [];
end

function copySubset(obj,ind,obj2,ind2)
    if not(iscompatible(obj,obj2))
        error('Descriptors are not compatible.');
    end
    
    if nargin < 4 || isempty(ind2)
        ind2 = 1:size(obj2.val,2);
    end
    
    obj.val(:,ind) = obj2.val(:,ind2);
    obj.rad(:,ind) = obj2.rad(:,ind2);
    obj.pos(:,ind) = obj2.pos(:,ind2);
end

end % methods

methods(Static)
    
function [angularpos,radialpos] = binaxes(nangular,nradial,rad,radlin)
    [angularpos,radialpos] = PointrelDescriptorSet.binaxes(nangular,nradial,rad,radlin);
end

function binarea = binareas(angularpos,radialpos)
    binarea = PointrelDescriptorSet.binareas(angularpos,radialpos);
end

function distmethod = simmethod2distmethod(simmethod)
    if strcmp(simmethod,'negativeEMD')
        distmethod = 'EMD';
    elseif strcmp(simmethod,'negativeL1dist')
        distmethod = 'L1dist';
    elseif strcmp(simmethod,'negativeL2dist')
        distmethod = 'L2dist';
    elseif strcmp(simmethod,'product')
        distmethod = 'none';
    else
        error('No distance method for given similarity method found.');
    end
end

function obj = saveobj(obj)
    % do nothing for now
end

function obj = loadobj(obj)
    if isstruct(obj)
        error('Could not load the object, need to implement this conversion.');
    else
        if not(isfield(obj.settings,'radf')) % legacy
            obj.settings.radf = 0.5; % default value
        end
        if not(isfield(obj.settings,'radlin')) % legacy
            obj.settings.radlin = 0; % default value
        end
    end
end

end % methods(Static)

end % classdef
