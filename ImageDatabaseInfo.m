classdef ImageDatabaseInfo < handle

properties
    categories = cell(0,1);
    relcategories = cell(0,1);
    categorycolors = zeros(0,3);
    
    imgfilenames = cell(1,0);
    imghasrels = zeros(1,0);
end

methods

% obj = ImageDatabaseInfo
% obj = ImageDatabaseInfo(obj2)
function obj = ImageDatabaseInfo(varargin)
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'ImageDatabaseInfo')
        obj.copyFrom(obj2);
    else
        error('Invalid arguments.');
    end
end

function copyFrom(obj,obj2)
    obj.categories = obj2.categories;
    obj.relcategories = obj2.relcategories;
    obj.categorycolors = obj2.categorycolors;
    
    obj.imgfilenames = obj2.imgfilenames;
    obj.imghasrels = obj2.imghasrels;
end

function obj2 = clone(obj)
    obj2 = ImageDatabaseInfo(obj);
end
    
end

methods(Static)

function [filenames,indbpath] = dbfilenames(filenames,dbpath)
    filenames = cleanfilename(filenames);
    dbp = cleanfilename(absolutepath(dbpath));
    pos = strfind(lower(filenames),lower(dbp)); % matlab sometimes seems to use lower case letter for the path, even though they should be upper case
    indbpath = false(1,numel(filenames));
    for i=1:numel(filenames)
        if not(isempty(pos{i}))
            filenames{i} = cleanfilename(filenames{i}(pos{i}(1)+numel(dbp):end));
            indbpath(i) = true;
        end
    end
end
    
end

end
