function closeEditor
    
    vars = evalin('base','who');
    for i=1:numel(vars)
        evalin('base',['if isa(',vars{i},',''StructuralImageEditor'') && isvalid(',vars{i},'); delete(',vars{i},'); end;']);
        evalin('base','drawnow;');
        evalin('base',['clear ',vars{i},';']);
    end
    
end
