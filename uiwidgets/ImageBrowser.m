classdef ImageBrowser < AxesWidget

properties(SetAccess=protected)
    parentpanel = gobjects(1,0);
    
    imgpadding = 0.1; % in multiples of the viewport height
    
    onechannel2rgbgrayscale = true; % true to convert to rgb grayscale, false to use an intensity image
    
    maxmemory = 500e6; % max. thumbnail memory in bytes (may be exceeded if visible thumbnails need more than that)
    
    imgtext = {};
    imgbordercolor = nan(3,0);
end

properties(SetAccess=protected,SetObservable,AbortSet)
    imgfilenames = {};
    scrollpos = 1; % scroll coordinates (piecewise linear: the image index at left corner of each image, linear in between)
    
%     imginfos = struct(...
%         'imgmin',{},...
%         'imgmax',{},...
%         'imgres',{},...
%         'filename',{});
end

properties(SetObservable,AbortSet)
    selimginds = zeros(1,0);
    openedimgind = zeros(1,0);
end

properties(Access=protected)
    
    scrollposListener = event.listener.empty;
    
    uigroup = UIHandlegroup;
    
    gimgthumbs = gobjects(1,0);
    gimgtext = gobjects(1,0);
    gimgborder = gobjects(1,0);
    gimginds = zeros(1,0);
    
%     gtemp = gobjects(1,0);
    
    lastimgaddname = '';
    
    lastmousepos = [];
end

methods

% fills out all of parent panel (pass an empty panel)
function obj = ImageBrowser(parentpanel)
    % ignore warnings that the exif information of a file is wrong or missing
    % and also ignore some warning about tiff tags
    warning('off','MATLAB:imagesci:imjpginfo:exif');
    warning('off','MATLAB:imagesci:tifftagsread:numDirectoryEntriesIsZero');
    warning('off','MATLAB:imagesci:tifftagsread:badTagValueDivisionByZero');
    
    
    obj@AxesWidget(gobjects(1,0),'Image Browser');

    obj.parentpanel = parentpanel;
    
    obj.uigroup = UIHandlegroup;

    obj.scrollposListener = obj.addlistener('scrollpos','PostSet',@(src,evt) obj.scrollposChanged);
    
    obj.hide;
end

function delete(obj)
    obj.hide();
    
    delete(obj.scrollposListener(isvalid(obj.scrollposListener)));
    
    delete(obj.uigroup(isvalid(obj.uigroup)));
end

function scrollto(obj,pos)
    if not(isscalar(pos))
        error('scroll position must be a scalar.');
    end
    
    obj.scrollpos = pos;
end

function filenames = addImages(obj,filenames,text,bordercolor)
    
    if nargin < 2 || isempty(filenames)
        if not(isempty(obj.lastimgaddname))
            loadname = obj.lastimgaddname;
        else
            loadname = 'image.png';
        end

        [fname,pathname,~] = ...
            uigetfile(...
            {'*.png;*.jpg;*.jpeg;*.bmp','Image files (*.png,*.jpg,*.jpeg,*.bmp)'},...
            'Load Image',...
            loadname,...
            'MultiSelect','on');
        
        if not(isnumeric(fname) && fname == 0)
            if not(iscell(fname))
                fname = {fname};
            end
            for i=1:numel(fname)
                filenames = [pathname,fname{i}];
            end
        else
            return;
        end
    end
    
    if not(iscell(filenames))
        filenames = {filenames};
    end
    
    if nargin < 3 || isempty(text)
        text = repmat({''},1,numel(filenames));
    end
    
    if nargin < 4 || isempty(bordercolor)
        bordercolor = nan(3,numel(filenames));
    end
    
    obj.imgfilenames = [obj.imgfilenames,filenames];
    obj.imgtext = [obj.imgtext,text];
    obj.imgbordercolor = [obj.imgbordercolor,bordercolor];
    
    obj.lastimgaddname = filenames{1};
    
    obj.updateAxes;
    
    obj.showImagethumbs;
end

function removeImages(obj,ind)
    delind = ismember(obj.gimginds,ind);
    
    delete(obj.gimgthumbs(:,delind(isvalid(obj.gimgthumbs(:,delind)))));
    obj.gimgthumbs(:,delind) = [];
    delete(obj.gimgtext(:,delind(isvalid(obj.gimgtext(:,delind)))));
    obj.gimgtext(:,delind) = [];
    delete(obj.gimgborder(:,delind(isvalid(obj.gimgborder(:,delind)))));
    obj.gimgborder(:,delind) = [];
    obj.gimginds(:,delind) = [];
    obj.imgfilenames(ind) = [];
    obj.imgtext(ind) = [];
    obj.imgbordercolor(:,ind) = [];
    
    obj.showImagethumbs;
    
    obj.selimginds = zeros(1,0);
    obj.openedimgind = zeros(1,0);
    
    obj.scrollpos = max(1,min(numel(obj.imgfilenames)+0.99,obj.scrollpos));
end

function setImgtext(obj,ind,txt)
    if any(ind < 1) || any(ind > numel(obj.imgtext))
        error('Invalid image index.');
    end
    if numel(ind) == 1 && not(iscell(txt))
        txt = {txt};
    end
    
    obj.imgtext(ind) = txt;
    
    obj.showImagethumbs;
end

function setImgbordercolors(obj,ind,clr)
    if ind < 1 || ind > numel(obj.imgbordercolor)
        error('Invalid image index.');
    end
    if size(clr,1) ~= 3
        error('Must pass colors.');
    end
    
    obj.imgbordercolor(:,ind) = clr;
    
    obj.showImagethumbs;
end

function unblock(obj)
    obj.blockcallbacks = false;
end

function mousePressed(obj,src,evt) %#ok<INUSD>

    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = true;
    
    if not(isempty(obj.axes))
        obj.lastmousepos = obj.mousepos;
        obj.mousepos = mouseposOnPlane(obj.axes);
        obj.clickedpos = obj.mousepos;
        
%         disp('currentpoint:');
%         disp(obj.axes.CurrentPoint);
%         disp('mousepos:');
%         disp(obj.mousepos);
        
        if strcmp(src.SelectionType,'normal')
            % ...
        elseif strcmp(src.SelectionType,'extend')
            % ...
        elseif strcmp(src.SelectionType,'alt')
            % ...
        elseif strcmp(src.SelectionType,'open') % double click
%             disp('doubleclick mousepos:');
%             disp(obj.mousepos);
            
            [~,~,axesvpmin,~,~] = obj.viewportinfo;
            clickedimgind = obj.viewrangeinfo(obj.scrollpos,0,obj.clickedpos(1)-axesvpmin(1));
            clickedimgind = floor(clickedimgind);
            if clickedimgind >= 1 && clickedimgind <= numel(obj.imgfilenames)
                obj.openedimgind = clickedimgind;
            end
        end
    end

    obj.blockcallbacks = false;
end

function mouseMoved(obj,src,evt) %#ok<INUSD>

    if obj.mousedown

        if obj.blockcallbacks
            return;
        end
        obj.blockcallbacks = true;

        if not(isempty(obj.axes))
            obj.lastmousepos = obj.mousepos;
            obj.mousepos = mouseposOnPlane(obj.axes);

            if strcmp(src.SelectionType,'normal')
                % ...
            elseif strcmp(src.SelectionType,'extend')
                % ...
            elseif strcmp(src.SelectionType,'alt')
%                 disp(['scrollpos before: ',num2str(obj.scrollpos)]);
                obj.scrollpos = obj.viewrangeinfo(...
                    obj.scrollpos,0,obj.lastmousepos(1) - obj.mousepos(1));
                
%                 disp(['scrollpos after: ',num2str(obj.scrollpos)]);
            end
        end

        obj.blockcallbacks = false;
    end
end

function mouseReleased(obj,src,evt) %#ok<INUSD>

    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = false;

    if not(isempty(obj.axes))
        if strcmp(src.SelectionType,'normal')
            % ...
        elseif strcmp(src.SelectionType,'extend')
            % ...
        elseif strcmp(src.SelectionType,'alt')
            % ...
        end
    end

    obj.blockcallbacks = false;
end

function keyPressed(obj,src,evt) %#ok<INUSL>
    if strcmp(evt.Key,'leftarrow')
        obj.scrollto(obj.scrollpos-10);
    elseif strcmp(evt.Key,'rightarrow')
        obj.scrollto(obj.scrollpos+10);
    end
end

function keyReleased(obj,src,evt)
    obj.pimgcanvas.keyReleased(src,evt);
end

function scrollposChanged(obj)
    if obj.scrollpos < 1
        % ok because callback is not recursive, should also be called
        % before all other listeners, because it was added first
        obj.scrollpos = 1;
    elseif obj.scrollpos > max(1,numel(obj.imgfilenames)+0.999)
        % ok because callback is not recursive, should also be called
        % before all other listeners, because it was added first
        obj.scrollpos = max(1,numel(obj.imgfilenames)+0.999);
    end
    
    obj.showAxescontents;
end

function showAxescontents(obj)
    obj.showImagethumbs;
end

function hideAxescontents(obj)
    obj.hideImages;
end

function updateAxes(obj)
    if not(isempty(obj.axes)) && isgraphics(obj.axes)
        
        % X and Y Lim always remain the same
        
%         fpadding = [0;0;0.05];
%         
%         imgx = [obj.gimgthumbs.XData];
%         imgy = [obj.gimgthumbs.YData];
%         imgz = [obj.gimgthumbs.ZData];
%         bbmin = [min(imgx);min(imgy);min(imgz)];
%         bbmax = [max(imgx);max(imgy);max(imgz)];
%         
%         bbdiag = sqrt(sum((bbmax-bbmin).^2,1));
%         bbmax = bbmax + bbdiag .* fpadding;
%         bbmin = bbmin - bbdiag .* fpadding;
%        
%         set(obj.axes,'XLim',[bbmin(1),bbmax(1)]);
%         set(obj.axes,'YLim',[bbmin(2),bbmax(2)]);
%         set(obj.axes,'ZLim',[bbmin(3),bbmax(3)]);
    end
end

function resetAxes(obj)
    % X and Y Lim always remain the same
    
    resetCamera(obj.axes,'y');
end

function showAxes(obj)
    if not(isempty(obj.parentpanel))
        if isempty(obj.axes) || not(isgraphics(obj.axes))
            obj.axes = axes(...
                'Parent',obj.parentpanel,...
                'DataAspectRatio',[1 1 1],...
                'ALim',[0,1],...
                'CLim',[0,1],...
                'XLim',[0,1],...
                'YLim',[0,1],...
                'ZLim',[0,1],...
                'CameraViewAngleMode','manual',...
                'CameraViewAngle',5,... % small angle so that the near and far clipping planes are not too close, projection is orthogonal anyways
                'CameraPositionMode','manual',...
                'CameraTargetMode','manual',...
                'CameraUpVectorMode','manual',...
                'CameraViewAngleMode','manual',...    
                'FontSize',8,...
                'XTick',[],...
                'YTick',[],...
                'ZTick',[],...
                'XGrid','off',...
                'YGrid','off',...
                'ActivePositionProperty','OuterPosition',...
                'LooseInset',[0,0,0,0],...
                'OuterPosition',[0,0.01,1,0.99],...
                'Box','on',...
                'Clipping','off',...
                'Visible','off');
            
            obj.resetAxes;
        else
            obj.updateAxes;
        end
        
        obj.showAxescontents;
    else
        obj.hideAxes;
    end
end

function hideAxes(obj)
    delete(obj.axes(isvalid(obj.axes))); % should also delete all axes contents
    obj.axes = gobjects(1,0);
end

function showImagethumbs(obj)
    if not(isempty(obj.parentpanel)) && not(isempty(obj.axes))
        
        % get viewport infos
        [vpmin,vpmax,axesvpmin,axesvpmax,vppixheight] = obj.viewportinfo;
        viewportwidth = vpmax(1)-vpmin(1);
        viewportheight = vpmax(2)-vpmin(2);
        axesvpsize = axesvpmax-axesvpmin;
        padding = viewportheight * obj.imgpadding;
        imgpixheight = max(1,floor(vppixheight*(1-2*obj.imgpadding)));
        
        % remove invalid images
        invalidinds = find(...
            not(isgraphics(obj.gimgthumbs)) | ...
            not(isgraphics(obj.gimgtext)) | ...
            not(isgraphics(obj.gimgborder)));
        delete(obj.gimgthumbs(invalidinds(isvalid(obj.gimgthumbs(invalidinds)))));
        obj.gimgthumbs(invalidinds) = [];
        delete(obj.gimgtext(invalidinds(isvalid(obj.gimgtext(invalidinds)))));
        obj.gimgtext(invalidinds) = [];
        delete(obj.gimgborder(invalidinds(isvalid(obj.gimgborder(invalidinds)))));
        obj.gimgborder(invalidinds) = [];
        obj.gimginds(invalidinds) = [];
        
        % get information about images in the viewport
        [~,imgind,imgcellmin,imgcellmax,imgresfull] = obj.viewrangeinfo(...
            obj.scrollpos,0,viewportwidth);
        
        % free memory as needed
        imgresthumb = [...
            round(bsxfun(@times,imgresfull(1:2,:),(imgpixheight ./ imgresfull(2,:))));...
            imgresfull(3,:)];
        visiblememused = sum(prod(imgresthumb,1))*8;
        maxnonvisiblemem = obj.maxmemory - visiblememused;
        nonvisibleinds = find(not(ismember(obj.gimginds,imgind)));
        if isempty(imgind)
            scrolldist = zeros(1,0);
        else
            scrolldist = max(...
                min(imgind)-(obj.gimginds(nonvisibleinds)+1), ...
                obj.gimginds(nonvisibleinds)-(max(imgind)+1));
        end
        obj.freethumbmemory(maxnonvisiblemem,nonvisibleinds,scrolldist);

        % adjust image position and load new images as needed
        visiblemask = false(1,numel(obj.gimgthumbs));
        for i=1:numel(imgind)
            if imgind(i) < 1 || imgind(i) > numel(obj.imgfilenames)
                continue;
            end
            
            ind = find(obj.gimginds == imgind(i),1,'first');
            
            % check if the image has the correct height; if not, reload,
            % need to resample the full-resolution image)
            if not(isempty(ind)) && size(obj.gimgthumbs(ind).CData,1) ~= imgpixheight
                delete(obj.gimgthumbs(ind(isvalid(obj.gimgthumbs(ind)))));
                obj.gimgthumbs(ind) = [];
                delete(obj.gimgtext(ind(isvalid(obj.gimgtext(ind)))));
                obj.gimgtext(ind) = [];
                delete(obj.gimgborder(ind(isvalid(obj.gimgborder(ind)))));
                obj.gimgborder(ind) = [];
                obj.gimginds(ind) = [];
                visiblemask(ind) = [];
                ind = zeros(1,0);
            end
            
            imgmin = (axesvpmin + (imgcellmin(:,i)./[viewportwidth;viewportheight]) .* axesvpsize) + padding;
            imgmax = (axesvpmin + (imgcellmax(:,i)./[viewportwidth;viewportheight]) .* axesvpsize) - padding;
            
            if isempty(ind)
                if not(exist(obj.imgfilenames{imgind(i)},'file')) || imgmax(1)<=imgmin(1)
                    warning(['Skipping file (not found or 0 size): ',obj.imgfilenames{imgind(i)}]);
                    obj.gimgthumbs(:,end+1) = gobjects(1,1);
                    obj.gimgtext(:,end+1) = gobjects(1,1);
                    obj.gimgborder(:,end+1) = gobjects(1,1);
                    obj.gimginds(:,end+1) = imgind(i);
                else
                    [img,cmap,alpha] = imread(obj.imgfilenames{imgind(i)});
                    if not(isempty(cmap))
                        img = ind2rgb(img,cmap);
                    end
                    img = cat(3,img,alpha);

                    img = imresize(img,[imgpixheight,nan],'bilinear');
                    img = im2double(flipud(img));
                    
                    if size(img,3) == 1 && obj.onechannel2rgbgrayscale
                        img = bsxfun(@times,img,ones(1,1,3));
                    elseif size(img,3) == 2 && obj.onechannel2rgbgrayscale
                        img = cat(3,bsxfun(@times,img(:,:,1),ones(1,1,3)),img(:,:,2));
                    end
                    
                    obj.gimgthumbs(:,end+1) = showImages(...
                        img,imgmin,imgmax,[],obj.axes);
                    obj.gimgtext(:,end+1) = showText(...
                        obj.imgtext{imgind(i)},[imgmin(1);imgmax(2);0.1],[],obj.axes,...
                        'Color','red');
                    brdclr = obj.imgbordercolor(:,imgind(i));
                    brdclr(isnan(brdclr)) = 0;
                    obj.gimgborder(:,end+1) = showFaces(...
                        [imgmin(1),imgmin(1),imgmax(1),imgmax(1);...
                        imgmin(2),imgmax(2),imgmax(2),imgmin(2);...
                        0.2,0.2,0.2,0.2],...
                        [],obj.axes,...
                        'facecolor','none',...
                        'edgecolor',brdclr,...
                        'edgewidth',2);
                    brdvisible = not(any(isnan(obj.imgbordercolor(:,imgind(i)))));
                    if not(brdvisible)
                        set(obj.gimgborder(:,end),'Visible','off');
                    end
                    obj.gimginds(:,end+1) = imgind(i);
                    visiblemask(:,end+1) = true; %#ok<AGROW>
                end
            else
                set(obj.gimgthumbs(ind),...
                    'XData',[imgmin(1),imgmax(1)],...
                    'YData',[imgmin(2),imgmax(2)],...
                    'Visible','on');
                set(obj.gimgtext(ind),...
                    'Position',[imgmin(1);imgmax(2);0.1]',...
                    'Visible','on');
                if not(strcmp(obj.gimgtext(ind).String,obj.imgtext{imgind(i)}))
                    obj.gimgtext(ind).String = obj.imgtext{imgind(i)};
                end
                brdvisible = not(any(isnan(obj.imgbordercolor(:,imgind(i)))));
                if brdvisible
                    set(obj.gimgborder(ind),...
                        'Vertices',...
                        [imgmin(1),imgmin(1),imgmax(1),imgmax(1);...
                         imgmin(2),imgmax(2),imgmax(2),imgmin(2);...
                         0.2,0.2,0.2,0.2]',...
                        'Visible','on');
                    if not(all(obj.gimgborder(ind).EdgeColor == obj.imgbordercolor(:,imgind(i))'))
                        obj.gimgborder(ind).EdgeColor = obj.imgbordercolor(:,imgind(i))';
                    end
                else
                    obj.gimgborder(ind).Visible = 'off';    
                end
                visiblemask(ind) = true;
            end
        end
        
        % hide images outside the viewport
        nonvisibleimgs = obj.gimgthumbs(not(visiblemask));
        nonvisibletxt = obj.gimgtext(not(visiblemask));
        nonvisiblebrd = obj.gimgborder(not(visiblemask));
        for i=1:numel(nonvisibleimgs)
            nonvisibleimgs(i).Visible = 'off';
            nonvisibletxt(i).Visible = 'off';
            nonvisiblebrd(i).Visible = 'off';
        end
    else
        obj.hideImages;
    end
end

function hideImages(obj)
    delete(obj.gimgthumbs(isvalid(obj.gimgthumbs)));
    obj.gimgthumbs = gobjects(1,0);
    delete(obj.gimgtext(isvalid(obj.gimgtext)));
    obj.gimgtext = gobjects(1,0);
    delete(obj.gimgborder(isvalid(obj.gimgborder)));
    obj.gimgborder = gobjects(1,0);
    obj.gimginds = zeros(1,0);
end

function [vpmin,vpmax,axesvpmin,axesvpmax,vppixheight] = viewportinfo(obj)
    panelunits = obj.parentpanel.Units;
    obj.parentpanel.Units = 'points';
    panelpos = obj.parentpanel.Position;
    panelaspect = panelpos(3) / panelpos(4);
    obj.parentpanel.Units = 'pixels';
    vppixheight = obj.parentpanel.Position(4);
    obj.parentpanel.Units = panelunits;

    % viewportheight is always 1 in axes units
    vpmin = [0;0];
    vpmax = [panelaspect;1];
    
    axesvpmin = [0.5-(vpmax(1)-vpmin(1))/2;0]; % viewport min in axes coordinates
    axesvpmax = axesvpmin+(vpmax-vpmin); % viewport max in axes coordinates
end

function [scrollend,imgind,imgcellmin,imgcellmax,imgresfull] = viewrangeinfo(...
        obj,scrollstart,viewstart,viewend)
    
    scrlpos = scrollstart; % in scroll coordinates
    viewpos = viewstart;
    
    imgind = zeros(1,0);
    imgcellmin = zeros(2,0);
    imgcellmax = zeros(2,0);
    imgresfull = zeros(3,0);
    iind = floor(scrlpos);
    while scrlpos >= 1 && scrlpos < numel(obj.imgfilenames)+1
        ipos = scrlpos - iind; % in local coordinates of image
        
        if iind < 1
            icellwidth = 1;
            ires = [0;0;0];
        else
            ind = find(obj.gimginds==iind,1,'first');
            if not(isempty(ind)) && isgraphics(obj.gimgthumbs(ind))
                imgxdata = obj.gimgthumbs(ind).XData;
                icellwidth = (imgxdata(end)-imgxdata(1)) + obj.imgpadding*2;
                ires = [...
                    size(obj.gimgthumbs(ind).CData,2);...
                    size(obj.gimgthumbs(ind).CData,1);...
                    size(obj.gimgthumbs(ind).CData,3)];
                if not(isempty(obj.gimgthumbs(ind).AlphaData))
                    ires(3) = ires(3)+1;
                end
            elseif not(exist(obj.imgfilenames{iind},'file'))
                icellwidth = 0;
                ires = [0;0;0];
            else
                info = imfinfo(obj.imgfilenames{iind});
                imgaspect = info.Width / info.Height;
                imgheight = 1 - obj.imgpadding*2;
                icellwidth = imgheight*imgaspect + obj.imgpadding*2;
                ires = [info.Width;info.Height;info.BitDepth/8];
            end
        end
        
        imgind(:,end+1) = iind; %#ok<AGROW>
        imgcellmin(:,end+1) = [viewpos - ipos*icellwidth;0]; %#ok<AGROW>
        imgcellmax(:,end+1) = [imgcellmin(1,end) + icellwidth;1]; %#ok<AGROW>
        imgresfull(:,end+1) = ires; %#ok<AGROW>
        
        imove = ipos + (viewend-viewpos)/icellwidth;
        imove = max(0,min(1,imove));

        viewpos = viewpos + icellwidth*(imove-ipos);
        scrlpos = iind+imove;
        
        if abs(viewend-viewpos) < 0.0001
            break;
        elseif viewend-viewpos < 0 && imove == 0
            iind = iind - 1;
        else
            iind = floor(scrlpos);
        end
    end
    
    scrollend = scrlpos;
end

function freethumbmemory(obj,maxthumbmemory,thumbinds,scrolldist)
    % get memory usage of the image data
    gimgmem = zeros(1,numel(obj.gimgthumbs));
    for i=thumbinds
        gimgmem(i) = (...
            numel(obj.gimgthumbs(i).AlphaData) + ...
            numel(obj.gimgthumbs(i).CData)) * 8;
    end
    memused = sum(gimgmem);
%     disp(['memused: ',num2str(memused),' - available: ',num2str(maxthumbmemory)]);
    
    % iteratively delete thumbs with the largest scroll distance until
    % memory requirements are met
    while memused > maxthumbmemory && not(isempty(thumbinds))
        [~,maxind] = max(scrolldist);
        delete(obj.gimgthumbs(thumbinds(maxind)));
        delete(obj.gimgtext(thumbinds(maxind)));
        delete(obj.gimgborder(thumbinds(maxind)));
        memused = memused - gimgmem(thumbinds(maxind));

        obj.gimgthumbs(thumbinds(maxind)) = [];
        obj.gimgtext(thumbinds(maxind)) = [];
        obj.gimgborder(thumbinds(maxind)) = [];
        obj.gimginds(thumbinds(maxind)) = [];
        gimgmem(thumbinds(maxind)) = [];
        thumbinds(maxind) = [];
    end
end

function show(obj)
    obj.show@AxesWidget;

    if isvalid(obj.uigroup)
        obj.uigroup.show;
    end
    
    if isvalid(obj.scrollposListener)
        obj.scrollposListener.Enabled = true;
    end
    
    obj.showAxes;
end

function hide(obj)
    obj.hide@AxesWidget;
    
    if isvalid(obj.uigroup)
        obj.uigroup.hide;
    end
    
    if isvalid(obj.scrollposListener)
        obj.scrollposListener.Enabled = false;
    end
    
    obj.hideAxes;
end

end

end
