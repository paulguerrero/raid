classdef ImageCanvas < AxesWidget

properties(SetAccess=protected)
	img = [];
	imgmin = [];
	imgmax = [];
	
	brushrad = 0.05; % in multiples of the canvas diagonal
	brushhardness = 0.3; % falloff from this*rad to 1*rad
    brushval = [];
    brushop = 'max';
    
    fixbrushrad = false;
    fixbrushhardness = false;
    fixbrushval = false;
    fixbrushop = false;
	
    tool = 'brush';
    
%     onechannelmode = 'colormap';
    onechannel2rgbgrayscale = true; % true to convert to rgb grayscale, false to use an intensity image
    
    clearval = [];
end

properties(Access=protected)
    gbrush = gobjects(1,1);
    gbrushouter = gobjects(1,1);
    gimg = gobjects(1,0);
    
    brushstrokelayer = [];
    
    brushradmode = false;
    brushvalmode = false;
    
    oldbrushval = [];
    oldbrushrad = [];
    
    mousepos_img = [];
    clickedpos_img = [];
    
    dirtypix = zeros(1,0);
    brushstrokedirtypix = zeros(1,0);;
    
    maxtilesize = [500;500];
%     maxtilesize = [2000;2000];
end

methods
   
    function saveToFile(obj,filename)
        imwrite(flipud(obj.img(:,:,1:end-1)),filename,'Alpha',flipud(obj.img(:,:,end)));
    end
    
    function loadFromFile(obj,filename,imgmin,imgmax)
        [im,indmap,ima] = imread(filename);
        if not(isempty(indmap))
            error('Invalid format, image should be rgb or grayscale (not indexed).');
        end
        
        im = im2double(flipud(im));
        
        if size(im,3) == 1 && obj.onechannel2rgbgrayscale
            im = bsxfun(@times,im(:,:,1),ones(1,1,3));
        end
        if isempty(ima)
            ima = ones(size(im,1),size(im,2));
        else
            ima = im2double(flipud(ima));
        end
        obj.img = cat(3,im,ima);
        
        if nargin < 4 || isempty(imgmin) || isempty(imgmax)
            if size(obj.img,2) > size(obj.img,1)
                % width is larger
                xsize = 2;
                ysize = 2 .* (size(obj.img,1) / size(obj.img,2));
            else
                % height is larger
                xsize = 2 .* (size(obj.img,2) / size(obj.img,1));
                ysize = 2;
            end

            imgmin = -[xsize;ysize]./2;
            imgmax = [xsize;ysize]./2;
        end
        
        obj.imgmin = imgmin;
        obj.imgmax = imgmax;
        
        obj.brushstrokelayer = zeros(size(obj.img));

        if size(obj.img,3) == 4 && numel(obj.brushval) == 2
            cmap = colormap;
            obj.brushval = [reshape(nind2rgb(reshape(obj.brushval(1:end-1),1,1,[]),cmap),[],1,1);obj.brushval(end)];
            obj.clearval = [reshape(nind2rgb(reshape(obj.clearval(1:end-1),1,1,[]),cmap),[],1,1);obj.clearval(end)];
        elseif size(obj.img,3) == 2 && numel(obj.brushval) == 4
            cmap = colormap;
            obj.brushval = [reshape(rgb2nind(reshape(obj.brushval(1:end-1),1,1,[]),cmap),[],1,1);obj.brushval(end)];
            obj.clearval = [reshape(rgb2nind(reshape(obj.clearval(1:end-1),1,1,[]),cmap),[],1,1);obj.clearval(end)];
        end
        
        obj.dirtypix = 1:size(obj.img,1)*size(obj.img,2);
        
        if obj.isvisible
            obj.showImage;
        end
    end
    
    % obj = ImageCanvas(ax,imin,imax,img,clearval)
    % obj = ImageCanvas(ax,imin,imax,res,clearval)
    function obj = ImageCanvas(varargin)
        
        obj@AxesWidget(varargin{1},'Image Canvas');
		
        if numel(varargin) >= 5 && not(isempty(varargin{5}))
            if isrow(varargin{5})
                obj.clearval = varargin{5}';
            elseif iscolumn(varargin{5})
                obj.clearval = varargin{5};
            else
                error('Invalid clear value.');
            end
        else
            obj.clearval = [];
        end
        
        obj.setImage(varargin{2:4});
        
%         obj.brushtimer = timer(...
%             'Name','BrushTimer',...
%             'Period',0.05,...
%             'TimerFcn',{@(x,y)ui.stepCrowdsims},...
%             'StartFcn',{@(x,y)set(ui.crowdsimstatustext,'String','Running...')},...
%             'StopFcn',{@(x,y)set(ui.crowdsimstatustext,'String','Stopped')},...
%             'ExecutionMode','fixedSpacing',...
%             'TasksToExecute',9999,...
%             'BusyMode','drop');
	end
	
	function delete(obj)
        obj.hide();
    end
    
    function setTool(obj,tool)
        obj.tool = tool;
    end
    
    function d = diagonal(obj)
        d = sqrt(sum((obj.imgmax-obj.imgmin).^2,1));
    end
    
    function clear(obj,clearval)
        if nargin < 2 || isempty(clearval)
            clearval = obj.clearval;
        end
        
        for i=1:size(obj.img,3)
            obj.img(:,:,i) = clearval(i);
        end
        
        obj.dirtypix = 1:size(obj.img,1)*size(obj.img,2);
        
%         obj.showImage;
    end
    
    % setImage(obj,imin,imax,img)
    % setImage(obj,imin,imax,res)
    function setImage(obj,varargin)
		obj.imgmin = varargin{1};
		obj.imgmax = varargin{2};
        
        if size(varargin{3},2) == 1
            % empty image (resolution given)
            
            res = varargin{3};
            
            if numel(res) == 1
                reshint = res;
                aspectratio = (obj.imgmax(1)-obj.imgmin(1))/(obj.imgmax(2)-obj.imgmin(2));
                f = sqrt((aspectratio-1)^2+4*aspectratio*reshint);
                resx  = max(2,round((1-aspectratio+f)/2));
                resy  = max(2,round((-1+aspectratio+f)/(2*aspectratio)));
                resc = 2;
            elseif numel(res) == 2
                resx = res(1);
                resy = res(2);
                resc = 2;
            elseif numel(res) == 3
                if res(3) ~= 2 && res(3) ~= 4
                    error('Image must have 1 channel + 1 alpha or 3 channels + 1 alpha.');
                end
                resx = res(1);
                resy = res(2);
                resc = res(3);
            end
            
            if numel(obj.clearval) ~= resc;
                obj.clearval = zeros(resc,1);
            end
            
            obj.img = bsxfun(@times,ones(resy,resx,resc),reshape(obj.clearval,1,1,[]));
        else
            % given image
            
            if size(varargin{3},3) == 1
                resc = 2;
            elseif size(varargin{3},3) == 2
                resc = 2;
            elseif size(varargin{3},3) == 3
                resc = 4;
            elseif size(obj.img,3) == 4
                resc = 4;
            else
                error('Image must have 1 or 3 channels (optionally + 1 alpha).');
            end
            
            if numel(obj.clearval) ~= resc;
                obj.clearval = zeros(resc,1);
            end
            
            obj.img = cat(3,...
                varargin{3},...
                ones(size(varargin{3},1),size(varargin{3},2),resc-size(varargin{3},3)) .* obj.clearval(end));
        end
        
        obj.brushval = ones(size(obj.img,3),1);

        obj.brushstrokelayer = zeros(size(obj.img));
        
        obj.dirtypix = 1 : size(obj.img,1)*size(obj.img,2);
    end
    
    function applyEraser(obj,pos)
        [gridx,gridy] = meshgrid(...
            linspace(obj.imgmin(1),obj.imgmax(1),size(obj.img,2)),...
            linspace(obj.imgmin(2),obj.imgmax(2),size(obj.img,1)));
        
        rad = obj.brushrad*obj.diagonal;
        
        dist = sqrt((gridx-pos(1)).^2 + (gridy-pos(2)).^2);
      
        pixinds = find(dist <= rad);
        imginds = pixinds + size(obj.img,1) * size(obj.img,2) * (size(obj.img,3)-1);
        
        eraseralpha = max(0,min(1,(rad - dist(pixinds)) ./ ((1-obj.brushhardness)*rad)));
        
        obj.img(imginds) = obj.img(imginds) .* (1-eraseralpha);
        
        obj.dirtypix = unique([obj.dirtypix,pixinds']);
    end
    
    function applyBrush(obj,pos,brushval)

        if nargin < 3 || isempty(brushval)
            brushval = obj.brushval;
        end
        
        rad = obj.brushrad*obj.diagonal;
        
        brushmin = pos-rad;
        brushmax = pos+rad;
        
        stepsize = (obj.imgmax-obj.imgmin) ./ ([size(obj.img,2);size(obj.img,1)]-1);
        
        submin = max(1,floor((brushmin-obj.imgmin) ./ stepsize) + 1);
        submax = min([size(obj.img,2);size(obj.img,1)],ceil((brushmax-obj.imgmin) ./ stepsize) + 1);
        
        minsamplepos = obj.imgmin + (submin-1) .* stepsize;
        maxsamplepos = obj.imgmin + (submax-1) .* stepsize;
        
        [subx,suby] = meshgrid(...
            submin(1):submax(1),...
            submin(2):submax(2));
        [gridx,gridy] = meshgrid(...
            linspace(minsamplepos(1),maxsamplepos(1),size(subx,2)),...
            linspace(minsamplepos(2),maxsamplepos(2),size(subx,1)));

        
        pixinds = sub2ind(size(obj.img),suby,subx);

        
        dist = sqrt((gridx-pos(1)).^2 + (gridy-pos(2)).^2);
        
        mask = dist <= rad;
        
        dist = dist(mask);
        pixinds = pixinds(mask);
        
        if isempty(pixinds)
            return;
        end

        balpha = max(0,min(1,(rad - dist) ./ ((1-obj.brushhardness)*rad)));
        
        bval = bsxfun(@times,reshape(brushval,1,1,[]),ones(size(balpha)));
        bval(:,:,end) = bval(:,:,end) .* balpha;
        
        imginds = sub2ind([size(obj.img,1)*size(obj.img,2),size(obj.img,3)],...
            repmat(pixinds,size(obj.img,3),1),...
            reshape(bsxfun(@times,1:size(obj.img,3),ones(numel(pixinds),1)),[],1));
        
        if strcmp(obj.brushop,'add')
            obj.img(imginds) = imblend(...
                reshape(obj.img(imginds),numel(pixinds),1,size(obj.img,3)),...
                bval,...
                obj.brushop);
        elseif strcmp(obj.brushop,'max')
            obj.brushstrokelayer(imginds) = imblend(...
                reshape(obj.brushstrokelayer(imginds),numel(pixinds),1,size(obj.img,3)),...
                bval,...
                obj.brushop);
        else
            error('Unkown brush operation.');
        end

        obj.dirtypix = unique([obj.dirtypix,pixinds']);
    end
    
    function setBrushval(obj,val)
        obj.brushval = val;
    end
    
    function setBrushrad(obj,rad)
        obj.brushrad = rad;
    end
    
    function setBrushhardness(obj,h)
        obj.brushhardness = h;
    end
    
    function setBrushop(obj,op)
        obj.brushop = op;
    end
    
    function setFixbrushrad(obj,b)
        obj.fixbrushrad = b;
    end
    
    function setFixbrushhardness(obj,b)
        obj.fixbrushhardness = b;
    end
    
    function setFixbrushval(obj,b)
        obj.fixbrushval = b;
    end
    
    function setFixbrushop(obj,b)
        obj.fixbrushop = b;
    end
    
%     function data = imgdata2rgb(obj,data)
%         if size(data,3) == 1
%             h = size(data,1);
%             w = size(data,2);
%             cmapsize = size(colormap,1);
%             maxval = max(1,max(max(data,[],1),[],2));
%             if maxval > 0
%                 data = (data./maxval).*(cmapsize-1)+1;
%             else
%                 data = data .*(cmapsize-1)+1;
%             end
%             cmap = colormap;
%             data = reshape(...
%                 cmap(min(size(colormap,1),max(1,round(data))),:),h,w,3);
%         elseif size(data,3) == 2
%             data = cat(3,data,zeros(size(data,1),size(data,2)));
%         elseif size(data,3) == 3
%             % do nothing
%         elseif size(data,3) > 3
%             % showing only the first three channels
%             data = data(:,:,1:3);
%         end
%     end
    
    function unblock(obj)
        obj.blockcallbacks = false;
    end

    function mousePressed(obj,src,evt) %#ok<INUSD>
        
        if obj.blockcallbacks
            return;
        end
        obj.blockcallbacks = true;
        
        obj.mousedown = true;
        
        obj.mousepos = mouseray(obj.axes);
        obj.clickedpos = obj.mousepos;
        obj.mousepos_img = mouseposOnPlane(obj.axes);
        obj.clickedpos_img = obj.mousepos_img;
        
        if strcmp(obj.tool,'brush')
            if strcmp(get(src,'SelectionType'),'extend') && not(obj.fixbrushrad)
                % ctrl: change brush width
                obj.brushradmode = true;
                obj.oldbrushrad = obj.brushrad;
                obj.showBrush;
            elseif strcmp(get(src,'SelectionType'),'alt') && not(obj.fixbrushval)
                % shift: change brush value
                obj.brushvalmode = true;
                obj.oldbrushval = obj.brushval;
                obj.showBrush;
            else
                % start drawing
                obj.applyBrush(obj.mousepos_img(1:2));
                obj.showImage;
            end
        elseif strcmp(obj.tool,'eraser')
            if strcmp(get(src,'SelectionType'),'extend') && not(obj.fixbrushrad)
                % ctrl: change brush width
                obj.brushradmode = true;
                obj.oldbrushrad = obj.brushrad;
                obj.showBrush;
            end
        else
            % do nothing, no mode selected
        end
        
        obj.blockcallbacks = false;
	end

	function mouseMoved(obj,src,evt)
        
        if obj.mousedown
            
            if obj.blockcallbacks
                return;
            end
            obj.blockcallbacks = true;

            obj.mousepos = mouseray(obj.axes);
            obj.mousepos_img = mouseposOnPlane(obj.axes);

            if strcmp(obj.tool,'brush')
                if obj.brushradmode
                    % ctrl: change brush width
%                     movedist = sqrt(sum((obj.mousepos_img(1:2)-obj.clickedpos_img(1:2)).^2,1));
%                     movedist = (obj.mousepos_img(2)-obj.clickedpos_img(2));
%                     obj.brushrad = max(0.0001,movedist / obj.diagonal);
                    obj.brushrad = max(0.0000001,obj.oldbrushrad + min(1,(obj.mousepos_img(2)-obj.clickedpos_img(2)) / (obj.imgmax(2)-obj.imgmin(2))));
                    
                    obj.showBrush;
                elseif obj.brushvalmode
                    if numel(obj.brushval) == 2
                        % shift: change brush value
                        valchange = (obj.mousepos_img(2)-obj.clickedpos_img(2)) / (obj.diagonal * 0.2);
                        obj.brushval = max(0,min(1,obj.oldbrushval + [valchange;0]));
                        obj.showBrush;
                    elseif numel(obj.brushval) == 4
                        valchange = (obj.mousepos_img(2)-obj.clickedpos_img(2)) / (obj.diagonal * 0.2);
                        obj.brushval = max(0,min(1,obj.oldbrushval + [valchange;0]));
                    else
                        error('Invalid brush value.');
                    end
                else
                    % draw
                    obj.applyBrush(obj.mousepos_img(1:2));
                    obj.showImage;
                end
            elseif strcmp(obj.tool,'eraser')
                if obj.brushradmode
                    obj.brushrad = max(0.0000001,obj.oldbrushrad + min(1,(obj.mousepos_img(2)-obj.clickedpos_img(2)) / (obj.imgmax(2)-obj.imgmin(2))));
                    obj.showBrush;
                else
                    obj.applyEraser(obj.mousepos_img(1:2));
                    obj.showImage;
                end
            else
                % do nothing, no mode selected
            end

            obj.blockcallbacks = false;
        
        end
    end

	function mouseReleased(obj,src,evt)
        
        if obj.blockcallbacks
            return;
        end
        obj.blockcallbacks = true;
        
        obj.mousedown = false;
        
        if strcmp(obj.tool,'brush')
            if strcmp(obj.brushop,'max')
                obj.img = imblend(obj.img,obj.brushstrokelayer,'add');
                
                for i=1:size( obj.brushstrokelayer,3)-1
                    obj.brushstrokelayer(:,:,i) = obj.clearval(i);
                end
                obj.brushstrokelayer(:,:,end) = 0; % has to be zero, otherwise showImage would show it
                
                obj.showImage;
            end
            
            obj.brushradmode = false;
            obj.brushvalmode = false;
            obj.hideBrush;
        elseif strcmp(obj.tool,'eraser')
            obj.brushradmode = false;
            obj.brushvalmode = false;
            obj.hideBrush;
        else
            % do nothing, no mode selected
        end
        
        obj.blockcallbacks = false;
    end

    function keyPressed(obj,src,evt)
        if strcmp(evt.Key,'b')
            obj.tool = 'brush';
        elseif strcmp(evt.Key,'d')
            obj.tool = 'eraser';
        elseif strcmp(evt.Key,'delete')
            obj.clear;
            obj.showImage;
        end
        
        if strcmp(obj.tool,'brush')
            if strcmp(evt.Key,'delete')
                obj.clear;
                obj.showImage;
            end
        end
    end
    
    
    function setAxes(obj,ax)
        obj.hide;
        obj.setAxes@AxesWidget(ax);
    end
    
    function show(obj)
        obj.show@AxesWidget;
        
        obj.showImage;
        obj.showBrush;
    end
    
    function hide(obj)
        obj.hide@AxesWidget;
        
        obj.hideBrush;
        obj.hideImage;
    end
    
    function showBrush(obj)
        if not(isempty(obj.mousepos_img)) && (strcmp(obj.tool,'brush')||strcmp(obj.tool,'eraser')) && (obj.brushvalmode || obj.brushradmode)
            rad = obj.brushrad*obj.diagonal;
            
            color = obj.brushval(1:end-1);
            if numel(color) == 1
                cmap = colormap;
                color = reshape(nind2rgb(color,cmap),[],1,1);
            end
            
            v = zeros(3,64);
            [v(1,:),v(2,:)] = polygonCircle(...
                obj.mousepos_img(1),obj.mousepos_img(2),rad,64,'samplecount');

            v(3,:) = 0.005;
            obj.gbrushouter = showPatches(...
                v,(1:size(v,2))',obj.gbrushouter,obj.axes,...
                'facecolor','none',...
                'edgecolor',[0;0;0],...
                'edgewidth',5);
            
            v(3,:) = 0.01;
            obj.gbrush = showPatches(...
                v,(1:size(v,2))',obj.gbrush,obj.axes,...
                'facecolor','none',...
                'edgecolor',color,...
                'edgewidth',3);
        else
            obj.hideBrush;
        end
    end
    
    function hideBrush(obj)
        delete(obj.gbrush(isgraphics(obj.gbrush)));
        obj.gbrush = gobjects(1,0);
        delete(obj.gbrushouter(isgraphics(obj.gbrushouter)));
        obj.gbrushouter = gobjects(1,0);
    end
    
    function showImage(obj)
        
        if not(isempty(obj.img))
            
            cdata = obj.img;
            
            % add brush layer
            if strcmp(obj.tool,'brush')
                cdata = imblend(cdata,obj.brushstrokelayer,'add');
%                 pixinds = find(obj.brushstrokelayer(:,:,end) > 0);                
%                 cdata = imblend(cdata,obj.brushstrokelayer,'add',pixinds);
            end
            
            ntiles = max([1;1],ceil([size(obj.img,1);size(obj.img,2)]./obj.maxtilesize));
            ytilesize = [ones(ntiles(1)-1,1).*obj.maxtilesize(1);size(obj.img,1)-((ntiles(1)-1)*obj.maxtilesize(1))]';
            xtilesize = [ones(ntiles(2)-1,1).*obj.maxtilesize(2);size(obj.img,2)-((ntiles(2)-1)*obj.maxtilesize(2))]';
            
            % merge last tile if it would only be one pixel wide
            if numel(ytilesize) > 1 && ytilesize(end) == 1
                ntiles(1) = ntiles(1)-1;
                ytilesize(end-1) = ytilesize(end-1)+1;
                ytilesize(end) = [];
            end
            if numel(xtilesize) > 1 && xtilesize(end) == 1
                ntiles(2) = ntiles(2)-1;
                xtilesize(end-1) = xtilesize(end-1)+1;
                xtilesize(end) = [];
            end
            
            % get pixel centers coordinates
            pixcenterx = linspace(obj.imgmin(1),obj.imgmax(1),size(obj.img,2));
            pixcentery = linspace(obj.imgmin(2),obj.imgmax(2),size(obj.img,1));
            
            ytileoffset = [0,cumsum(ytilesize)];
            xtileoffset = [0,cumsum(xtilesize)];
            
            [tileminy,tileminx] = ndgrid(pixcentery(ytileoffset(1:end-1)+1),pixcenterx(xtileoffset(1:end-1)+1));
            [tilemaxy,tilemaxx] = ndgrid(pixcentery(ytileoffset(2:end)),pixcenterx(xtileoffset(2:end)));
            
            dirtypixmask = false(size(obj.img,1),size(obj.img,2));
            dirtypixmask(obj.dirtypix) = true;
            changedtileinds = find(cellfun(@(x) any(x(:)),mat2cell(dirtypixmask,ytilesize,xtilesize)));
            
            if not(isempty(changedtileinds))
                changedtiles = cell(1,numel(changedtileinds));
                for i=1:numel(changedtileinds);
                    [indi,indj] = ind2sub(ntiles',changedtileinds(i));
                    changedtiles{i} = cdata(...
                        ytileoffset(indi)+1:ytileoffset(indi)+ytilesize(indi),...
                        xtileoffset(indj)+1:xtileoffset(indj)+xtilesize(indj),...
                        :);
                end
                
                tileminx = tileminx(:);
                tileminy = tileminy(:);
                tilemaxx = tilemaxx(:);
                tilemaxy = tilemaxy(:);
                tilemin = [tileminx(changedtileinds),tileminy(changedtileinds)]';
                tilemax = [tilemaxx(changedtileinds),tilemaxy(changedtileinds)]';
                tilemin = mat2cell(tilemin,2,ones(1,size(tilemin,2)));
                tilemax = mat2cell(tilemax,2,ones(1,size(tilemax,2)));

                if numel(obj.gimg) == ntiles(1)*ntiles(2)
                    him = obj.gimg(changedtileinds);
                else
                    delete(obj.gimg(isgraphics(obj.gimg)));
                    him = gobjects(1,0);
                end

%                 disp(['updating tiles ',num2str(find(changedtilemask)')]);

                obj.gimg(changedtileinds) = showImages(...
                    changedtiles,tilemin,tilemax,him,...
                    obj.axes(1,ones(1,numel(changedtiles))),...
                    'stackz',-0.1);
            end

            obj.dirtypix = zeros(1,0);
        else
            obj.hideImage;
        end
    end

    function hideImage(obj)
        delete(obj.gimg(isgraphics(obj.gimg)));
        obj.dirtypix = 1:size(obj.img,1)*size(obj.img,2);
    end
end

end