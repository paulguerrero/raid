classdef ImageEditor < AxesWidget

% image databases are assumed to have a folder images and a folder
% annotations. Annotations are one xml file and one image file for the mask
% (xml file has location of corresponding mask file)
    
properties(SetAccess = protected)
    
    dbfilename = '../data/COCO_first10k_manual.txt';
%     dbfilename = '../data/COCO_first10k_auto.txt';
%     dbfilename = '../data/COCO_first10k_manual_noise_level1.txt';
%     dbfilename = '../data/Synthetic_manual.txt';
%     dbfilename = '../data/Synthetic_manual_noise_level1.txt';
%     dbfilename = '../data/Webimage_manual.txt';
%     dbfilename = '../data/Webimage_manual_noise_level1.txt';
    
    % database info filenames
    imgpath = '';
    annopath = '';
    dbinfofilenames = struct(...
        'dbinfo','',...
        'categories','',...
        'relcategories','',...
        'categorycolors','');
    workingsetfilename = '';
    trainingsetfilename = '';
    descriptorsfilename = '';

    % database meta-info
    dbinfo = ImageDatabaseInfo.empty;
    
    dbrels = DatabaseRelationshipSet.empty; % realations that have been computed, usually a subset of the database
    
    % working set of the database
    wsimginds = zeros(1,0); % current working set
    origwsimginds = zeros(1,0); % workingset when image editor was created
    wsrels = DatabaseRelationshipSet.empty;
    
    % current query
    query = RelationshipQuery.empty;
    
    % current classification
    classification = RelationshipClassification.empty;
    
    % available descriptors
    descriptors = ReldisthistDescriptorSet.empty;
    descsourceids = cell(1,0);
    desctargetids = cell(1,0);
    descimageinds = zeros(1,0);
    descimgfilenames = cell(1,0);
    
    % info about the currently selected database entry
    imgfilename = '';
    annofilename = '';
    annotation = ImageAnnotation.empty;
    
    queryrels = DatabaseRelationshipSet.empty;
    querylikelihoods = zeros(1,0);
    querydescriptorsim = [];
    
    clsfrelid = zeros(1,0);
    clsfrellbls = cell(1,0);
    clsfrellblweights = cell(1,0);
    
    % relationship templates
    reltemplates = cell(0,2);
    
    % parameters of the descriptor and class label predictor
    % (these are overwritten in the functions descriptorparams and
    % predictorparams)
    niangularbins = 8;
    niradialbins = 2;%4
    noangularbins = 8;
    noradialbins = 2;%4
   
    desctype = 'reldisthist';
    predtype = 'brknn';
    descsimmethod = 'negativeL1dist';
%     descsimmethod = 'negativeEMD'; % nhere
    
    % ui elements
    navwidget = NavigationWidget.empty;
    imgcanvas = ImageCanvas.empty;
    drawcanvas = ImageCanvas.empty;
    imgbrowser = ImageBrowser.empty;
    
    imageVisible = true;
    annopolysVisible = false;
    annorelsVisible = false;
    querydescriptorsVisible = false;
    querytemplateVisible = false;
    querydescbinsimVisible = false;
    querylikelihoodsVisible = false;
    imgbrowserpanelVisible = true;
    examplesVisible = true;
    innerdescriptorVisible = false;
    outerdescriptorVisible = false;
    classificationvalidationVisible = true;
    classificationVisible = false;
    resultsVisible = false;
    querydrawingVisible = false;
end

properties(SetObservable, AbortSet)
    selobjectinds = zeros(1,0);
    selcomponentinds = zeros(1,0);
    selsourceinds = zeros(1,0);
    seltargetinds = zeros(1,0);
    selqueryrel = zeros(1,0);
    selibinind = zeros(1,0);
    selobinind = zeros(1,0);
    selpanel = gobjects(1,0);
    selrel = zeros(2,0); % format: [relationind;relationlabelind]
    
    tool = '';
end

% properties(Access=protected, SetObservable, AbortSet)
%     queryimgind = 0;
%     
% end

properties(Access=protected)
    
    fig = gobjects(1,0);
    toolpanel = gobjects(1,0);
    mainpanel = gobjects(1,0);
    imgbrowserpanel = gobjects(1,0);
    relpanel = gobjects(1,0);
    editorfig = gobjects(1,0);
    
    selobjectindsListener = event.listener.empty;
    selcomponentindsListener = event.listener.empty;
    selqueryrelListener = event.listener.empty;
    selibinindListener = event.listener.empty;
    selobinindListener = event.listener.empty;
    selrelListener = event.listener.empty;
    selsourceindsListener = event.listener.empty;
    seltargetindsListener = event.listener.empty;
    openedimgindListener = event.listener.empty;
    toolListener = event.listener.empty;
    
    uigroup = UIHandlegroup.empty;
    gmenu_image = gobjects(1,0);
    gmenu_annopolys = gobjects(1,0);
    gmenu_annorels = gobjects(1,0);
    gmenu_descriptors = gobjects(1,0);
    gmenu_querytemplate = gobjects(1,0);
    gmenu_descbinsim = gobjects(1,0);
    gmenu_innerdescriptor = gobjects(1,0);
    gmenu_outerdescriptor = gobjects(1,0);
    gmenu_likelihoods = gobjects(1,0);
    gmenu_examples = gobjects(1,0);
    gmenu_results = gobjects(1,0);
    gmenu_querydrawing = gobjects(1,0);
    gmenu_drawquerysrc = gobjects(1,0);
    gmenu_drawquerytgt = gobjects(1,0);
    gmenu_marksrc = gobjects(1,0);
    gmenu_marktgt = gobjects(1,0);
    gmenu_selannno = gobjects(1,0);
    gmenu_seldesc = gobjects(1,0);
    gmenu_drawannno = gobjects(1,0);
    
    gtoolbuttons = gobjects(1,0);
    grellist = gobjects(1,0);
    
    gannopolys = gobjects(1,0);
    gannorelsource = gobjects(1,0);
    gannoreltarget = gobjects(1,0);
    gdescriptorbins = gobjects(1,0);
    godescriptorbins = gobjects(1,0);
    glikelihoods = gobjects(1,0);
    gexamplesubjects = gobjects(1,0);
    gexampleobjects = gobjects(1,0);
    geditpoly = gobjects(1,0);
    geditpolystart = gobjects(1,0);
    gresultsrcregions = gobjects(1,0);
    gresulttgtregions = gobjects(1,0);
    gresultimg = gobjects(1,0);
    gresultrect = gobjects(1,0);
    
    
    lastdatabasesavename = '';
    lastdatabaseloadname = '';
    lastimageloadname = '';
    lastimagesavename = '';
    lastannotationloadname = '';
    lastannotationsavename = '';
    lastqueryloadname = '';
    lastquerysavename = '';
    lastdbaddloadname = '';
    lastworkingsetloadname = '';
    lastworkingsetsavename = '';
    lastwsentryloadname = '';
    lastdescloadname = '';
    lastdescsavename = '';
    lastquerydescsavename = '';
    lastclassificationsavename = '';
    lastclassificationloadname = '';
    lastqueryresultimgsavename = '';
    lastwsimgsavename = '';
    lastcomputedescsavename = '';
    
    editpoly = zeros(2,0);
    wsentrydirty = false;
    
    imgbordercolor = [0;0.5804;0.2667];
    
    drawsrclbl = [];
    drawtgtlbl = [];

%     drawquerysrcpts = cell(1,0);
%     drawquerytgtpts = cell(1,0);
end

methods
   
function obj = ImageEditor(ax,editorfig,navwidget,imgcanvas,fig,toolpanel,mainpanel,menus)
    obj@AxesWidget(ax,'Image Editor');

    obj.navwidget = navwidget;
    obj.imgcanvas = imgcanvas;
    obj.editorfig = editorfig;
    
    obj.drawcanvas = ImageCanvas(obj.axes,...
        imgcanvas.imgmin,imgcanvas.imgmax,[1000;1000;4]);

    obj.drawcanvas.setBrushrad(0.05);
    obj.drawcanvas.setBrushhardness(1);
    obj.drawcanvas.setBrushval([rgbhex2rgbdec('FFBA5F')';1]); % orange
    obj.drawcanvas.setBrushop('add');
    
    obj.drawcanvas.setFixbrushrad(false);
    obj.drawcanvas.setFixbrushhardness(true);
    obj.drawcanvas.setFixbrushval(true);
    obj.drawcanvas.setFixbrushop(true);
    
    obj.fig = fig;
    obj.toolpanel = toolpanel;
    obj.mainpanel = mainpanel;
    
    obj.uigroup = UIHandlegroup;
    
    obj.imgbrowserpanel = uipanel(obj.fig,...
        'BackgroundColor',[0.5,0.5,0.5],...
        'Position',[0,0,1,1],... % will be adjusted in layout panels
        'BorderType','line',...
        'BorderWidth',0,...
        'HighlightColor','white',...
        'SizeChangedFcn', {@(src,evt) obj.imgbrowserpanelResized}, ...
        'Visible','off');
    obj.uigroup.addChilds(obj.imgbrowserpanel);
    
    obj.imgbrowser = ImageBrowser(obj.imgbrowserpanel);
    
    obj.relpanel = uipanel(obj.fig,...
        'Position',[0,0,1,1],... % will be adjusted in layout panels
        'BorderType','line',...
        'BorderWidth',5,...
        'HighlightColor',[0.7,0.7,0.7],...
        'Visible','off');
    obj.uigroup.addChilds(obj.relpanel);
    obj.grellist = uicontrol('Parent',obj.relpanel,...
        'Style','listbox',...
        'Min',0,...
        'Max',2,... % multi-select if Max-Min > 1
        'HorizontalAlignment','left',...
        'Value',[],...
        'String',{},...
        'Units','normalized',...
        'Position',[0,0.1,1,0.9],...
        'BackgroundColor',[1,1,1],...    
        'Callback',{@(src,evt) obj.rellistChanged});
    uicontrol('Parent',obj.relpanel,...
        'Style','pushbutton',...
        'String','Add',...
        'Callback',{@(src,evt) obj.addrelbuttonPressed},...
        'Units','normalized',...    
        'Position',[0,0,0.5,0.1]);
    uicontrol('Parent',obj.relpanel,...
        'Style','pushbutton',...
        'String','Remove',...
        'Callback',{@(src,evt) obj.removerelbuttonPressed},...
        'Units','normalized',...    
        'Position',[0.5,0,0.5,0.1]);
    
    obj.uigroup.addChilds(uimenu(menus.File,...
        'Label','Load Database...',...
        'Callback', {@(src,evt) obj.loadDatabase},...
        'Separator','on'));
    obj.uigroup.addChilds(uimenu(menus.File,...
        'Label','Save Database...',...
        'Callback', {@(src,evt) obj.saveDatabase}));
    obj.uigroup.addChilds(uimenu(menus.File,...
        'Label','Load Descriptors...',...
        'Callback', {@(src,evt) obj.loadDescriptors},...
        'Separator','on'));
    obj.uigroup.addChilds(uimenu(menus.File,...
        'Label','Save Descriptors...',...
        'Callback', {@(src,evt) obj.saveDescriptors}));
    obj.uigroup.addChilds(uimenu(menus.File,...
        'Label','Load Query...',...
        'Callback', {@(src,evt) obj.loadQuery},...
        'Separator','on'));
    obj.uigroup.addChilds(uimenu(menus.File,...
        'Label','Save Query...',...
        'Callback', {@(src,evt) obj.saveQuery}));
    obj.uigroup.addChilds(uimenu(menus.File,...
        'Label','Load Annotation...',...
        'Callback', {@(src,evt) obj.loadAnnotation},...
        'Separator','on'));
    obj.uigroup.addChilds(uimenu(menus.File,...
        'Label','Save Annotation...',...
        'Callback', {@(src,evt) obj.saveAnnotation}));
    obj.uigroup.addChilds(uimenu(menus.File,...
        'Label','Save Query Resultimages...',...
        'Callback', {@(src,evt) obj.saveQueryResultimages},...
        'Separator','on'));
    
    obj.gmenu_image = uimenu(menus.Vis,...
        'Label','Image',...
        'Callback', {@(src,evt) obj.toggleImageVisible},...
        'Checked',iif(obj.imageVisible,'on','off'),...
        'Separator','on');
    obj.uigroup.addChilds(obj.gmenu_image);
    obj.gmenu_annopolys = uimenu(menus.Vis,...
        'Label','Annotation Polygons',...
        'Callback', {@(src,evt) obj.toggleAnnopolysVisible},...
        'Checked',iif(obj.annopolysVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_annopolys);
    obj.gmenu_annorels = uimenu(menus.Vis,...
        'Label','Annotation Relationships',...
        'Callback', {@(src,evt) obj.toggleAnnorelsVisible},...
        'Checked',iif(obj.annorelsVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_annorels);
    obj.gmenu_descriptors = uimenu(menus.Vis,...
        'Label','Descriptors',...
        'Callback', {@(src,evt) obj.toggleDescriptorsVisible},...
        'Checked',iif(obj.querydescriptorsVisible,'on','off'),...
        'Separator','on');
    obj.uigroup.addChilds(obj.gmenu_descriptors);
    obj.gmenu_innerdescriptor = uimenu(menus.Vis,...
        'Label','Inner Descriptor',...
        'Callback', {@(src,evt) obj.toggleInnerdescriptorVisible},...
        'Checked',iif(obj.innerdescriptorVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_innerdescriptor);
    obj.gmenu_outerdescriptor = uimenu(menus.Vis,...
        'Label','Outer Descriptor',...
        'Callback', {@(src,evt) obj.toggleOuterdescriptorVisible},...
        'Checked',iif(obj.outerdescriptorVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_outerdescriptor);
    obj.gmenu_querytemplate = uimenu(menus.Vis,...
        'Label','Query Template',...
        'Callback', {@(src,evt) obj.toggleQuerytemplateVisible},...
        'Checked',iif(obj.querytemplateVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_querytemplate);
    obj.gmenu_descbinsim = uimenu(menus.Vis,...
        'Label','Bin Similarity',...
        'Callback', {@(src,evt) obj.toggleDescbinsimVisible},...
        'Checked',iif(obj.querydescbinsimVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_descbinsim);
    obj.gmenu_likelihoods = uimenu(menus.Vis,...
        'Label','Region Likelihood',...
        'Callback', {@(src,evt) obj.toggleLikelihoodsVisible},...
        'Checked',iif(obj.querylikelihoodsVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_likelihoods);
    obj.gmenu_examples = uimenu(menus.Vis,...
        'Label','Query Examples',...
        'Callback', {@(src,evt) obj.toggleExamplesVisible},...
        'Checked',iif(obj.examplesVisible,'on','off'),...
        'Separator','on');
    obj.uigroup.addChilds(obj.gmenu_examples);
    obj.gmenu_querydrawing = uimenu(menus.Vis,...
        'Label','Query Drawing',...
        'Callback', {@(src,evt) obj.toggleQuerydrawingVisible},...
        'Checked',iif(obj.querydrawingVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_querydrawing);
    obj.gmenu_results = uimenu(menus.Vis,...
        'Label','Results',...
        'Callback', {@(src,evt) obj.toggleResultsVisible},...
        'Checked',iif(obj.resultsVisible,'on','off'),...
        'Separator','on');
    obj.uigroup.addChilds(obj.gmenu_results);
    
    obj.uigroup.addChilds(uimenu(menus.Actions,...
        'Label','Scroll To ...',...
        'Callback', {@(src,evt) obj.scrollToPressed},...
        'Separator','on'));
    obj.uigroup.addChilds(uimenu(menus.Actions,...
        'Label','Compute & Save Descriptors',...
        'Callback', {@(src,evt) obj.computeandsavedescriptorsbuttonPressed}));
%     obj.uigroup.addChilds(uimenu(menus.Actions,...
%         'Label','Query Saved Descriptors: Subject -> Object',...
%         'Callback', {@(src,evt) obj.querysaveddescsbuttonPressed('single')},...
%         'Separator','on'));
%     obj.uigroup.addChilds(uimenu(menus.Actions,...
%         'Label','Query Saved Descriptors: Subject -> X',...
%         'Callback', {@(src,evt) obj.querysaveddescsbuttonPressed('alltgt')}));
%     obj.uigroup.addChilds(uimenu(menus.Actions,...
%         'Label','Query Saved Descriptors: X -> Object',...
%         'Callback', {@(src,evt) obj.querysaveddescsbuttonPressed('allsrc')}));
%     obj.uigroup.addChilds(uimenu(menus.Actions,...
%         'Label','Query Saved Descriptors: X -> X',...
%         'Callback', {@(src,evt) obj.querysaveddescsbuttonPressed('all')}));
%     obj.uigroup.addChilds(uimenu(menus.Actions,...
%         'Label','Query Workingset: Subject -> Object',...
%         'Callback', {@(src,evt) obj.querybuttonPressed('single')},...
%         'Separator','on'));
%     obj.uigroup.addChilds(uimenu(menus.Actions,...
%         'Label','Query Workingset: Subject -> X',...
%         'Callback', {@(src,evt) obj.querybuttonPressed('alltgt')}));
%     obj.uigroup.addChilds(uimenu(menus.Actions,...
%         'Label','Query Workingset: X -> Object',...
%         'Callback', {@(src,evt) obj.querybuttonPressed('allsrc')}));
%     obj.uigroup.addChilds(uimenu(menus.Actions,...
%         'Label','Query Workingset: X -> X',...
%         'Callback', {@(src,evt) obj.querybuttonPressed('all')}));
%     obj.uigroup.addChilds(uimenu(menus.Actions,...
%         'Label','Query Image: Subject -> Object',...
%         'Callback', {@(src,evt) obj.queryimgbuttonPressed('single')},...
%         'Separator','on'));
%     obj.uigroup.addChilds(uimenu(menus.Actions,...
%         'Label','Query Image: Subject -> X',...
%         'Callback', {@(src,evt) obj.queryimgbuttonPressed('alltgt')}));
%     obj.uigroup.addChilds(uimenu(menus.Actions,...
%         'Label','Query Image: X -> Object',...
%         'Callback', {@(src,evt) obj.queryimgbuttonPressed('allsrc')}));
%     obj.uigroup.addChilds(uimenu(menus.Actions,...
%         'Label','Query Image: X -> X',...
%         'Callback', {@(src,evt) obj.queryimgbuttonPressed('all')}));
    
    obj.gmenu_drawquerysrc = uimenu(menus.Tools,...
        'Label','Sketch Query Source',...
        'Checked',iif(strcmp(obj.tool,'drawquerysrc'),'on','off'),...
        'Callback', {@(src,evt) obj.drawquerysrcPressed});
    obj.uigroup.addChilds(obj.gmenu_drawquerysrc);
    obj.gmenu_drawquerytgt = uimenu(menus.Tools,...
        'Label','Sketch Query Target',...
        'Checked',iif(strcmp(obj.tool,'drawquerytgt'),'on','off'),...
        'Callback', {@(src,evt) obj.drawquerytgtPressed});
    obj.uigroup.addChilds(obj.gmenu_drawquerytgt);
    
    obj.gmenu_marksrc = uimenu(menus.Tools,...
        'Label','Select Example Query Source',...
        'Checked',iif(strcmp(obj.tool,'selexamplesource'),'on','off'),...
        'Callback', {@(src,evt) obj.marksrcPressed},...
        'Separator','on');
    obj.uigroup.addChilds(obj.gmenu_marksrc);
    obj.gmenu_marktgt = uimenu(menus.Tools,...
        'Label','Select Example Query Target',...
        'Checked',iif(strcmp(obj.tool,'selexampletargets'),'on','off'),...
        'Callback', {@(src,evt) obj.marktgtPressed});
    obj.uigroup.addChilds(obj.gmenu_marktgt);
    
    obj.gmenu_selannno = uimenu(menus.Tools,...
        'Label','Select Annotation',...
        'Checked',iif(strcmp(obj.tool,'annoselect'),'on','off'),...
        'Callback', {@(src,evt) obj.selannoPressed},...
        'Separator','on');
    obj.uigroup.addChilds(obj.gmenu_selannno);
    obj.gmenu_seldesc = uimenu(menus.Tools,...
        'Label','Select Descriptor',...
        'Checked',iif(strcmp(obj.tool,'queryrelselect'),'on','off'),...
        'Callback', {@(src,evt) obj.seldescPressed});
    obj.uigroup.addChilds(obj.gmenu_seldesc);
    
    obj.gmenu_drawannno = uimenu(menus.Tools,...
        'Label','Draw Annotation',...
        'Checked',iif(strcmp(obj.tool,'drawanno'),'on','off'),...
        'Callback', {@(src,evt) obj.drawannoPressed},...
        'Separator','on');
    obj.uigroup.addChilds(obj.gmenu_drawannno);
    
    obj.gtoolbuttons = uibuttongroup(toolpanel,...
        'Position',[0,0,0.3,1],...
        'SelectedObject',[],...
        'SelectionChangedFcn',{@(src,evt) obj.toolbuttonPressed});
    obj.uigroup.addChilds(obj.gtoolbuttons);
    obj.uigroup.addChilds(uicontrol(obj.gtoolbuttons,...
        'Style','togglebutton',...
        'String','<html><strong>Example</strong> Query Source</html>',...
        'FontSize',12,...
        'Min',0,'Max',1,...
        'Value',iif(strcmp(obj.tool,'selexamplesource'),1,0),...
        'Tag','selexamplesource',...
        'Units','normalized',...    
        'Position',[0,0.5,0.5,0.5]));
    % apparently the first button added to the button group gets selected,
    % regardless of its value
    obj.gtoolbuttons.Children(1).Value = iif(strcmp(obj.tool,'selexamplesource'),1,0);
    obj.uigroup.addChilds(uicontrol(obj.gtoolbuttons,...
        'Style','togglebutton',...
        'String','<html><strong>Example</strong> Query Target</html>',...
        'FontSize',12,...
        'Min',0,'Max',1,...
        'Value',iif(strcmp(obj.tool,'selexampletargets'),1,0),...
        'Tag','selexampletargets',...
        'Units','normalized',...    
        'Position',[0,0,0.5,0.5]));
    obj.uigroup.addChilds(uicontrol(obj.gtoolbuttons,...
        'Style','togglebutton',...
        'String','<html><strong>Sketch</strong> Query Source</html>',...
        'FontSize',12,...
        'Min',0,'Max',1,...
        'Value',iif(strcmp(obj.tool,'drawquerysrc'),1,0),...
        'Tag','drawquerysrc',...
        'Units','normalized',...    
        'Position',[0.5,0.5,0.5,0.5]));
    obj.uigroup.addChilds(uicontrol(obj.gtoolbuttons,...
        'Style','togglebutton',...
        'String','<html><strong>Sketch</strong> Query Target</html>',...
        'FontSize',12,...
        'Min',0,'Max',1,...
        'Value',iif(strcmp(obj.tool,'drawquerytgt'),1,0),...
        'Tag','drawquerytgt',...
        'Units','normalized',...    
        'Position',[0.5,0,0.5,0.5]));
    
    obj.uigroup.addChilds(uicontrol(toolpanel,...
        'Style','pushbutton',...
        'String','<html><center><strong>Query</strong><br>Source &#10145; Target</center></html>',...
        'Callback',{@(src,evt) obj.querybuttonPressed('single')},...
        'FontSize',12,...
        'Units','normalized',...    
        'Position',[0.3,0,0.1,1]));
    obj.uigroup.addChilds(uicontrol(toolpanel,...
        'Style','pushbutton',...
        'String','<html><center><strong>Query</strong><br>Source &#10145; X</center></html>',...
        'Callback',{@(src,evt) obj.querybuttonPressed('alltgt')},...
        'FontSize',12,...
        'Units','normalized',...    
        'Position',[0.4,0,0.1,1]));
    obj.uigroup.addChilds(uicontrol(toolpanel,...
        'Style','pushbutton',...
        'String','<html><center><strong>Query</strong><br>X &#10145; Target</center></html>',...
        'Callback',{@(src,evt) obj.querybuttonPressed('allsrc')},...
        'FontSize',12,...
        'Units','normalized',...    
        'Position',[0.5,0,0.1,1]));
    obj.uigroup.addChilds(uicontrol(toolpanel,...
        'Style','pushbutton',...
        'String','<html><center><strong>Query</strong><br>X &#10145; X</center></html>',...
        'Callback',{@(src,evt) obj.querybuttonPressed('all')},...
        'FontSize',12,...
        'Units','normalized',...    
        'Position',[0.6,0,0.1,1]));
    obj.uigroup.addChilds(uicontrol(toolpanel,...
        'Style','pushbutton',...
        'String','<html><strong>Clear Query</strong></html>',...
        'FontSize',12,...
        'Callback',{@(src,evt) obj.clearquerybuttonPressed},...
        'Units','normalized',...    
        'Position',[0.7,0,0.1,1]));

    obj.selobjectindsListener = obj.addlistener('selobjectinds','PostSet',@(src,evt) obj.selobjectindsChanged);
    obj.selcomponentindsListener = obj.addlistener('selcomponentinds','PostSet',@(src,evt) obj.selcomponentindsChanged);
    obj.selqueryrelListener = obj.addlistener('selqueryrel','PostSet',@(src,evt) obj.selqueryrelChanged);
    obj.selibinindListener = obj.addlistener('selibinind','PostSet',@(src,evt) obj.selibinindChanged);
    obj.selobinindListener = obj.addlistener('selobinind','PostSet',@(src,evt) obj.selobinindChanged);
    obj.selrelListener = obj.addlistener('selrel','PostSet',@(src,evt) obj.selrelChanged);
    obj.selsourceindsListener = obj.addlistener('selsourceinds','PostSet',@(src,evt) obj.selsourceindsChanged);
    obj.seltargetindsListener = obj.addlistener('seltargetinds','PostSet',@(src,evt) obj.seltargetindsChanged);
    obj.openedimgindListener = obj.imgbrowser.addlistener('openedimgind','PostSet',@(src,evt) obj.openedimgindChanged);
    obj.toolListener = obj.addlistener('tool','PostSet',@(src,evt) obj.toolChanged);
    
    obj.loadDatabase(obj.dbfilename);
    
    obj.hide;
end

function delete(obj)
    obj.hide();
    
    delete(obj.imgbrowser(isvalid(obj.imgbrowser)));
    
    delete(obj.selobjectindsListener(isvalid(obj.selobjectindsListener)));
    delete(obj.selcomponentindsListener(isvalid(obj.selcomponentindsListener)));
    delete(obj.selqueryrelListener(isvalid(obj.selqueryrelListener)));
    delete(obj.selibinindListener(isvalid(obj.selibinindListener)));
    delete(obj.selobinindListener(isvalid(obj.selobinindListener)));
    delete(obj.selrelListener(isvalid(obj.selrelListener)));
    delete(obj.selsourceindsListener(isvalid(obj.selsourceindsListener)));
    delete(obj.seltargetindsListener(isvalid(obj.seltargetindsListener)));
    delete(obj.openedimgindListener(isvalid(obj.openedimgindListener)));
    delete(obj.toolListener(isvalid(obj.toolListener)));
    
    delete(obj.uigroup(isvalid(obj.uigroup)));
    
    delete(obj.queryrels(isvalid(obj.queryrels)));
    delete(obj.query(isvalid(obj.query)));
    delete(obj.classification(isvalid(obj.classification)));
%     delete(obj.description(isvalid(obj.description)));
    
    delete(obj.drawcanvas(isvalid(obj.drawcanvas)));
    
    delete(obj.dbrels(isvalid(obj.dbrels)));
end

function clearQueryinfo(obj)
    obj.selqueryrel = zeros(1,0);
    
    delete(obj.queryrels(isvalid(obj.queryrels)));
    obj.queryrels = DatabaseRelationshipSet.empty;
    obj.querylikelihoods = zeros(1,0);
    obj.querydescriptorsim = [];
end

function clearAnnotation(obj)
    obj.clearQueryinfo;
    
    obj.selobjectinds = zeros(1,0);
    obj.selcomponentinds = zeros(1,0);
    obj.selsourceinds = zeros(1,0);
    obj.seltargetinds = zeros(1,0);
    obj.selrel = zeros(2,0);
    
    delete(obj.annotation(isvalid(obj.annotation)));
    obj.annotation = ImageAnnotation.empty;
    obj.wsentrydirty = false;
end

% setWorkingset(obj,wsimginds)
% setWorkingset(obj,wsimgfilenames)
function setWorkingset(obj,wsimgs)
    
    obj.clear;
    delete(obj.wsrels(isvalid(obj.wsrels)));
    
    if iscell(wsimgs)
        [isindb,imginds] = ismember(wsimgs,obj.dbinfo.imgfilenames);
    elseif isnumeric(wsimgs)
        imginds = wsimgs;
        isindb = imginds >= 1 & imginds <= numel(obj.dbinfo.imgfilenames);
    end
    
    if not(all(isindb))
        error('Some images are not in the database');
    end
    
    obj.wsimginds = imginds;
    
    if not(isempty(obj.wsimginds))
        wsimghasrel = obj.dbinfo.imghasrels(obj.wsimginds);
        wsimgfilenames = obj.dbinfo.imgfilenames(obj.wsimginds);
        imgbrdclrs = nan(3,numel(obj.wsimginds));
        imgbrdclrs(:,wsimghasrel) = repmat(obj.imgbordercolor,1,sum(wsimghasrel));
        
        obj.imgbrowser.removeImages(1:numel(obj.imgbrowser.imgfilenames));
        obj.imgbrowser.addImages(...
            strcat([obj.imgpath,'/'],wsimgfilenames),...
            array2strings(1:numel(wsimgfilenames),'%d')',...
            imgbrdclrs);
    end
    
    if not(isempty(obj.dbrels))
        wsimgfilenames = obj.dbinfo.imgfilenames(obj.wsimginds);
        [hasrels,dbrelimginds] = ismember(wsimgfilenames,obj.dbrels.imgfilenames);
        if not(all(hasrels))
            obj.wsrels = DatabaseRelationshipSet.empty;
        else
            obj.wsrels = obj.dbrels.cloneImgsubset(dbrelimginds);
        end
    end
    
    if not(isempty(obj.imgbrowser.imgfilenames))
        obj.imgbrowser.scrollto(1);
        obj.imgbrowser.openedimgind = 1;
    end
    
    disp(['Database has ',num2str(numel(obj.dbinfo.imgfilenames)),' images, ',...
        'working subset has ',num2str(numel(obj.wsimginds)),' images.']);
%     
%     obj.workingsetfilename = '';
end

function clearImage(obj)
    obj.imgcanvas.clear;
end

function clearSketch(obj)
    obj.drawcanvas.clear;
    obj.drawsrclbl = [];
    obj.drawtgtlbl = [];
end

function clearExample(obj)
    obj.selsourceinds = zeros(1,0);
    obj.seltargetinds = zeros(1,0);
end

function clear(obj)
    obj.editpoly = zeros(2,0);
    
    obj.clearAnnotation;
    obj.clearImage;
end

function loadDatabase(obj,filename)
    if nargin < 2 || isempty(filename)
        if not(isempty(obj.lastdatabaseloadname))
            loadname = obj.lastdatabaseloadname;
        else
            loadname = 'db.txt';
        end

        [fname,pathname,~] = ...
            uigetfile({'*.txt','Database (*.txt)'},'Load Database',loadname);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    disp('loading database ...');
    
    [dbinf,dbinffilenames,impath,anpath,wsfilename,tsfilename,descfname] = ImageDatabaseImporter.importImageDatabase(filename);
    
    obj.dbinfo = dbinf;
    obj.dbinfofilenames = dbinffilenames;
    obj.imgpath = impath;
    obj.annopath = anpath;
    obj.workingsetfilename = wsfilename;
    obj.trainingsetfilename = tsfilename;
    obj.descriptorsfilename = descfname;
    
    if not(isempty(obj.workingsetfilename))
        obj.loadWorkingset(obj.workingsetfilename);
    else
        obj.setWorkingset(obj.dbinfo.imgfilenames);
    end
    obj.origwsimginds = obj.wsimginds;
    
    if not(isempty(obj.imgbrowser.imgfilenames))
        obj.imgbrowser.openedimgind = 1;
    end
    
    if not(isempty(obj.descriptorsfilename))
        disp('loading descriptors (this might take some time) ...');
        obj.loadDescriptors(obj.descriptorsfilename);
    end
    
    obj.lastdatabaseloadname = filename;
    
    disp('done');
end

function saveDatabase(obj,filename)
    if nargin < 2 || isempty(filename)
        if not(isempty(obj.lastdatabasesavename))
            savename = obj.lastdatabasesavename;
        else
            savename = 'db.txt';
        end
        
        [fname,pathname,~] = ...
            uiputfile({'*.txt','Database (*.txt)'},'Save Database',savename);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    disp('saving database ...');
    
    ImageDatabaseExporter.exportImageDatabase(...
        obj.dbinfo,...
        obj.dbinfofilenames,...
        obj.imgpath,...
        obj.annopath,...
        obj.workingsetfilename,...
        obj.trainingsetfilename,...
        obj.descriptorsfilename,...
        filename);
    
    obj.lastdatabasesavename = filename;
    
    disp('done');
end

function addWorkingsetEntry(obj,imgfilenames)
    
    if nargin < 2 || isempty(imgfilenames)
        if not(isempty(obj.lastdbaddloadname))
            loadname = obj.lastwsentryloadname;
        else
            loadname = [obj.imgpath,'/','tempimage.png'];
        end

        [fname,pathname,~] = ...
            uigetfile(...
            {'*.png;*.jpg','Image Files (*.png,*.jpg)'},...
            'Add Workingset Entries',...
            loadname,...
            'MultiSelect','on');
        
        if not(isnumeric(fname) && fname == 0)
            imgfilenames = fullfile(pathname,fname);
        else
            return;
        end
    end
    
    if not(iscell(imgfilenames))
        imgfilenames = {imgfilenames};
    end
    
    [imgfilenames,inpath] = ImageDatabaseInfo.dbfilenames(imgfilenames,obj.imgpath);
    
    if not(all(inpath))
        warndlg('Some images are not in the database path.');
        return;        
    end
    
    [isindb,imginds] = ismember(imgfilenames,obj.dbinfo.imgfilenames);
    
    if not(all(isindb))
        warndlg('Some images are not in the database.');
        return;
    end
    
    % remove images that are already in the workingset
    imginds(ismember(imginds,obj.wsimginds)) = [];
    
    obj.setWorkingset([obj.wsimginds,imginds]);
    
    if not(isempty(obj.wsimginds)) && not(isempty(imginds))
        firstnewind = (numel(obj.wsimginds)-numel(imginds))+1;
        obj.imgbrowser.scrollto(firstnewind);
        obj.imgbrowser.openedimgind = firstnewind;
    end
    
    obj.lastwsentryloadname = imgfilenames{1};
end

function loadWorkingsetEntry(obj,imgfilename)
    if obj.wsentrydirty
        btn = questdlg('Current database entry has not been saved, save it now?','Save Database Entry?','Yes','No','Yes');
        if strcmp(btn,'Yes')
            obj.saveWorkingsetEntry;
        end
    end
    
    obj.clear;
    obj.hideAxescontents;
    
    obj.imgfilename = imgfilename;
    obj.annofilename = ImageAnnotation.img2annofilename(obj.imgfilename);
    
    % load image
    imginfo = imfinfo([obj.imgpath,'/',obj.imgfilename]);
    imgres = [imginfo.Width;imginfo.Height];
    [imgmin,imgmax] = ImageAnnotation.imres2imsize(imgres);
    obj.imgcanvas.loadFromFile([obj.imgpath,'/',obj.imgfilename],imgmin,imgmax);
    
%     if not(obj.imageVisible)
%         obj.toggleImageVisible;
%     end
    
    obj.annotation = AnnotationImporter.importAnnotation([obj.annopath,'/',obj.annofilename]);
    
    obj.wsentrydirty = false;
    
    obj.updateMainaxes; % image aspect might be different
    obj.annotationChanged;
    obj.queryEntryChanged;
    obj.classificationEntryChanged;
end

function saveWorkingsetEntry(obj)
    
    % get workingset index of current image
    wsimgfilenames = obj.dbinfo.imgfilenames(obj.wsimginds);
    wsind = find(strcmp(obj.imgfilename,wsimgfilenames),1,'first');
    if isempty(wsind)
        error('Current image was not found in the working set.');
    end
    
    dbind = find(strcmp(obj.imgfilename,obj.dbinfo.imgfilenames),1,'first');
    
    % update dbinfo
    if not(isempty(obj.annotation.relationships.id)) ~= obj.dbinfo.imghasrels(dbind)
        
        obj.dbinfo.imghasrels(dbind) = not(isempty(obj.annotation.relationships.id));
        
        ImageDatabaseInfoExporter.updateDbinfo(...
            obj.imgfilename,obj.dbinfo.imghasrels(dbind),...
            obj.dbinfofilenames.dbinfo);
        
        if not(isempty(obj.imgbrowser)) && not(isempty(obj.imgbrowser.imgfilenames))
            ibind = find(strcmp([obj.imgpath,'/',obj.imgfilename],obj.imgbrowser.imgfilenames),1,'first');
            if not(isempty(ibind))
                if obj.dbinfo.imghasrels(dbind)
                    obj.imgbrowser.setImgbordercolors(ibind,obj.imgbordercolor);
                else
                    obj.imgbrowser.setImgbordercolors(ibind,obj.nan(3,1));
                end
            end
        end
    end
    
    % update annotation
    AnnotationExporter.exportAnnotation(obj.annotation,[obj.annopath,'/',obj.annofilename]);
    
    % image changes are currently not saved
    
    obj.wsentrydirty = false;
end

function addDatabaseEntry(obj,imgfilenames)
    
    if nargin < 2 || isempty(imgfilenames)
        if not(isempty(obj.lastdbaddloadname))
            loadname = obj.lastdbaddloadname;
        else
            loadname = [obj.imgpath,'/','tempimage.png'];
        end

        [fname,pathname,~] = ...
            uigetfile(...
            {'*.png;*.jpg','Image Files (*.png,*.jpg)'},...
            'New Database Entry',...
            loadname,...
            'MultiSelect','on');
        
        if not(isnumeric(fname) && fname == 0)
            imgfilenames = fullfile(pathname,fname);
        else
            return;
        end
    end
    
    if not(iscell(imgfilenames))
        imgfilenames = {imgfilenames};
    end
    
    [imgfilenames,inimgpath] = ImageDatabaseInfo.dbfilenames(imgfilenames,obj.imgpath);
    
    if not(all(inimgpath))
        warndlg('Some images are not in the database path.');
        return;
    end
    
    hasrels = false(1,numel(imgfilenames));
    for i=1:numel(imgfilenames)

        anfilename = ImageAnnotation.img2annofilename(imgfilenames{i});
        
        if not(exist([obj.annopath,'/',anfilename],'file'))
            warning('No annotation found for the selected file, creating empty annotation.');
            anno = ImageAnnotation;
            AnnotationExporter.exportAnnotation(anno,[obj.annopath,'/',anfilename]);
        else
            anno = AnnotationImporter.importAnnotation([obj.annopath,'/',anfilename]);
        end

        hasrels(i) = not(isempty(anno.relationships.id));

        delete(anno(isvalid(anno)));
    end
    
    newdbinds = ImageDatabaseInfoExporter.updateDbinfo(...
        imgfilenames,hasrels,...
        obj.dbinfofilenames.dbinfo,...
        true);
    obj.dbinfo.imgfilenames(newdbinds) = imgfilenames;
    obj.dbinfo.imghasrels(newdbinds) = hasrels;
    
    newwsimginds = [obj.wsimginds,newdbinds];
    obj.setWorkingset(newwsimginds);
    
    if not(isempty(obj.wsimginds))
        obj.imgbrowser.scrollto(numel(obj.wsimginds));
        obj.imgbrowser.openedimgind = numel(obj.wsimginds);
    end
    
    obj.lastdbaddloadname = [obj.imgpath,'/',imgfilenames{1}];
end

function loadWorkingset(obj,filename)
    
    if nargin < 2 || isempty(filename)
        if not(isempty(obj.lastworkingsetloadname))
            loadname = obj.lastworkingsetloadname;
        else
            [path,~,~] = fileparts(obj.workingsetfilename);
            loadname = fullfile(path,'tempworkingset.txt');
        end

        [fname,pathname,~] = ...
            uigetfile({'*.txt','Text File (*.txt)'},'Load Workingset',loadname);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    fid = fopen(filename,'r');
    imgfilenames = textscan(fid,'%s','Delimiter',{'\n','\r'});
    fclose(fid);
    imgfilenames = imgfilenames{1}';
    
    obj.setWorkingset(imgfilenames);
    obj.origwsimginds = obj.wsimginds;
end

function saveWorkingset(obj,filename)

    if nargin < 2 || isempty(filename)
        if not(isempty(obj.lastworkingsetsavename))
            savename = obj.lastworkingsetsavename;
        else
            [path,~,~] = fileparts(obj.workingsetfilename);
            savename = fullfile(path,'tempworkingset.txt');
        end
        
        [fname,pathname,~] = ...
            uiputfile({'*.txt','TXT (*.txt)'},'Save Workingset',savename);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    wsimgfilenames = obj.dbinfo.imgfilenames(obj.wsimginds);
    nl = repmat({sprintf('\n')},1,numel(wsimgfilenames));
    str = [wsimgfilenames;nl];
    str = [str{:}];

    fid = fopen(filename,'W');
    fwrite(fid,str);
    fclose(fid);
    
    obj.origwsimginds = obj.wsimginds;
    
    obj.lastworkingsetsavename = filename;
    
    disp('done');
end

function loadAnnotation(obj,filename)
    
    if isempty(obj.imgcanvas)
        return;
    end
    
    if isempty(obj.imgcanvas.img)
        errordlg('Please load an image first.');
        return;
    end
    
    if nargin < 2 || isempty(filename)
        if not(isempty(obj.imgfilename))
            loadname = cleanfilename(fullfile(obj.annopath,ImageAnnotation.img2annofilename(obj.imgfilename)));
        elseif not(isempty(obj.lastannotationloadname))
            loadname = obj.lastannotationloadname;
        else
            loadname = cleanfilename(fullfile(obj.annopath,'annotation.xml'));
        end

        [fname,pathname,~] = ...
            uigetfile({'*.xml','Image Annotation (*.xml)'},'Load Image Annotation',loadname);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    obj.clearAnnotation;

    obj.annotation = AnnotationImporter.importAnnotation(filename);
   
    obj.wsentrydirty = true;
    
    obj.lastannotationloadname = filename;
    
    obj.annotationChanged;
    
    disp('done');
end

function saveAnnotation(obj,filename)
    if isempty(obj.annotation)
        return;
    end
    
    if nargin < 2 || isempty(filename)
        if not(isempty(obj.imgfilename))
            savename = cleanfilename(fullfile(obj.annopath,ImageAnnotation.img2annofilename(obj.imgfilename)));
        elseif not(isempty(obj.lastannotationsavename))
            savename = obj.lastannotationsavename;
        else
            savename = cleanfilename(fullfile(obj.annopath,'annotation.xml'));
        end

        [fname,pathname,~] = ...
            uiputfile({'*.xml','Image Annotation (*.xml)'},'Save Image Annotation',savename);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    AnnotationExporter.exportAnnotation(obj.annotation,filename);
    
    obj.lastannotationsavename = filename;
    
    disp('done');
end

function saveImage(obj,filename)
    if isempty(obj.imgcanvas) || isempty(obj.imgcanvas.img)
        return;
    end
    
    if nargin < 2 || isempty(filename)
        if not(isempty(obj.lastimagesavename))
            savename = obj.lastimagesavename;
        else
            savename = 'image.png';
        end
        
        [fname,pathname,~] = ...
            uiputfile({'*.png','PNG (*.png)';'*.jpg','JPEG (*.jpg)'},'Save Image',savename);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    obj.imgcanvas.saveToFile(filename);
    
    obj.lastimagesavename = filename;
    
    disp('done');
end

function saveQueryDescriptors(obj,filename)
    if isempty(obj.query)
        return;
    end
    
    if nargin < 2 || isempty(filename)
        if not(isempty(obj.lastquerydescsavename))
            savename = obj.lastquerydescsavename;
        else
            savename = '../results/descriptors/tempquerydesc.mat';
        end
        
        [fname,pathname,~] = ...
            uiputfile({'*.mat','MAT (*.mat)'; '*.zip','ZIP (*.zip)'},'Save Query Descriptors',savename);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    r = obj.query.rels; %#ok<NASGU>
    
    [~,fname,fext] = fileparts(filename);
    if strcmp(fext,'.zip')
        temppath = tempname;
        mkdir(temppath);
        unzippedfilename = [fname,'.mat'];
        save(fullfile(temppath,unzippedfilename),'r','-v7.3');
        zip(filename,unzippedfilename,temppath);
        rmdir(temppath,'s');
    else
        save(filename,'r','-v7.3');
    end
    
    obj.lastquerydescsavename = filename;
    
    disp('done');
end

function saveWorkingsetImages(obj,filename)
    if nargin < 2 || isempty(filename)
        if not(isempty(obj.lastwsimgsavename))
            savename = obj.lastwsimgsavename;
        else
            savename = '../results/queryresults/resultimages/temp.svg';
        end
        
        [fname,pathname,~] = ...
            uiputfile({'*.svg','SVG (*.svg)'},'Save Workingset Images',savename);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    xoffset = 0;
    nimgs = 164;
    scale = 100;
    shift = [0;0];
    spacing = 0.1;
    
    canvasmin = obj.imgcanvas.imgmin;
    canvasmax = obj.imgcanvas.imgmax;

    canvasmin_t = (canvasmin+shift).*scale;
    canvasmax_t = (canvasmax+shift).*scale;

    doc = com.mathworks.xml.XMLUtils.createDocument('svg');
    doc.setDocumentURI(['file:/',filename]);

    parentnode = doc.getDocumentElement;
    parentnode.setAttribute('version','1.1');
    parentnode.setAttribute('xmlns','http://www.w3.org/2000/svg');
    parentnode.setAttribute('xmlns:xlink','http://www.w3.org/1999/xlink');
    parentnode.setAttribute('x',[num2str(canvasmin_t(1),10),'px']);
    parentnode.setAttribute('y',[num2str(canvasmin_t(2),10),'px']);
    parentnode.setAttribute('width',[num2str(canvasmax_t(1) - canvasmin_t(1),10),'px']);
    parentnode.setAttribute('height',[num2str(canvasmax_t(2) - canvasmin_t(2),10),'px']);
    parentnode.setAttribute('viewBox',...
        [num2str(canvasmin_t(1),10),' ',num2str(canvasmin_t(2),10),' ',...
        num2str(canvasmax_t(1) - canvasmin_t(1),10),' ',...
        num2str(canvasmax_t(2) - canvasmin_t(2),10)]);
    
    
    for i=1:nimgs
        disp(['saving image ',num2str(i),' / ',num2str(nimgs)]);
        
        resetCamera(obj.axes);
        
        if obj.annopolysVisible
            obj.toggleAnnopolysVisible;
        end
        if obj.annorelsVisible
            obj.toggleAnnorelsVisible;
        end
        if obj.querydescriptorsVisible
            obj.toggleDescriptorsVisible;
        end
        if obj.querylikelihoodsVisible
            obj.toggleLikelihoodsVisible;
        end
        if obj.examplesVisible
            obj.toggleExamplesVisible;
        end
%         if not(obj.imageVisible)
%             obj.toggleImageVisible;
%         end
        if obj.imageVisible
            obj.toggleImageVisible;
        end

        
        imgmin = obj.imgcanvas.imgmin;
        imgmax = obj.imgcanvas.imgmax;
        
        % just for artificial dataset
%         imgres = [613;793];
        crop = [0;150];
        croppedimg = obj.imgcanvas.img(crop(2)+1:end-crop(2),crop(1)+1:end-crop(1),:);
        croppedx = (size(croppedimg,2) * (2/size(croppedimg,1))) / 2;
        imgmin = [-croppedx;-1];
        imgmax = [croppedx;1];
        delete(obj.gresultimg(isvalid(obj.gresultimg)));
        obj.gresultimg = showImages(croppedimg,imgmin,imgmax,obj.gresultimg,obj.axes);
        
        shift(1) = xoffset;
        sc = scale .* (2 / (imgmax(2) - imgmin(2)));
        shift = (shift ./ sc) .* scale;
        
        graphics2svg(obj.axes,parentnode,...
            [],[],[],canvasmin,canvasmax,sc,shift); % scaled up 100
        
        if not(isempty(obj.imgbrowser)) && not(isempty(obj.imgbrowser.openedimgind))
            obj.imgbrowser.openedimgind = min(numel(obj.imgbrowser.imgfilenames),obj.imgbrowser.openedimgind+1);
            obj.imgbrowser.scrollto(obj.imgbrowser.openedimgind);
        else
            error('Image browser not found.');
        end
        
        xoffset = xoffset + (imgmax(1)-imgmin(1)) + spacing;
    end
    
    xmlwrite(filename,doc);
    
    obj.lastwsimgsavename = filename;
    
    disp('done');
end

function saveQueryResultimages(obj,filename)
    
    if isempty(obj.query)
        warning('Query is empty.');
        return;
    end
    
    if nargin < 2 || isempty(filename)
        if not(isempty(obj.lastqueryresultimgsavename))
            savename = obj.lastqueryresultimgsavename;
        else
            savename = '../results/queryresults/resultimages/temp.svg';
        end
        
        [fname,pathname,~] = ...
            uiputfile({'*.svg','SVG (*.svg)'},'Save Query Result Images',savename);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    xoffset = 0;
    nimgs = 40;
    scores = zeros(1,nimgs);
    scale = 100;
    shift = [0;0];
    spacing = 0.1;
    
    canvasmin = [obj.gresultimg.XData(1);obj.gresultimg.YData(1)];
    canvasmax = [obj.gresultimg.XData(2);obj.gresultimg.YData(2)];

    canvasmin_t = (canvasmin+shift).*scale;
    canvasmax_t = (canvasmax+shift).*scale;

    doc = com.mathworks.xml.XMLUtils.createDocument('svg');
    doc.setDocumentURI(['file:/',filename]);

    parentnode = doc.getDocumentElement;
    parentnode.setAttribute('version','1.1');
    parentnode.setAttribute('xmlns','http://www.w3.org/2000/svg');
    parentnode.setAttribute('xmlns:xlink','http://www.w3.org/1999/xlink');
    parentnode.setAttribute('x',[num2str(canvasmin_t(1),10),'px']);
    parentnode.setAttribute('y',[num2str(canvasmin_t(2),10),'px']);
    parentnode.setAttribute('width',[num2str(canvasmax_t(1) - canvasmin_t(1),10),'px']);
    parentnode.setAttribute('height',[num2str(canvasmax_t(2) - canvasmin_t(2),10),'px']);
    parentnode.setAttribute('viewBox',...
        [num2str(canvasmin_t(1),10),' ',num2str(canvasmin_t(2),10),' ',...
        num2str(canvasmax_t(1) - canvasmin_t(1),10),' ',...
        num2str(canvasmax_t(2) - canvasmin_t(2),10)]);
    
    
    for i=1:nimgs
        disp(['saving image ',num2str(i),' / ',num2str(nimgs)]);
        
        resetCamera(obj.axes);
        
        if not(obj.resultsVisible)
            obj.toggleResultsVisible;
        end
        
        % select the best relationship pair
        if not(isempty(obj.querylikelihoods))
            [scores(i),obj.selqueryrel] = max(obj.querylikelihoods);
        end
        
        imgmin = [obj.gresultimg.XData(1);obj.gresultimg.YData(1)];
        imgmax = [obj.gresultimg.XData(2);obj.gresultimg.YData(2)];
        
        
        
        
        shift(1) = xoffset;
        
        graphics2svg(obj.axes,parentnode,...
            [],[],[],canvasmin,canvasmax,scale,shift); % scaled up 100
        
%         [fpath,fname,fext] = fileparts(filename);
%         graphics2svg(obj.axes,...
%             fullfile(fpath,[fname,'_',sprintf('%03d',i),fext]),...
%             [],[],[],canvasmin,canvasmax,scale,shift); % scaled up 100
        
        if not(isempty(obj.imgbrowser)) && not(isempty(obj.imgbrowser.openedimgind))
            obj.imgbrowser.openedimgind = min(numel(obj.imgbrowser.imgfilenames),obj.imgbrowser.openedimgind+1);
            obj.imgbrowser.scrollto(obj.imgbrowser.openedimgind);
        else
            error('Image browser not found.');
        end
        
        xoffset = xoffset + (imgmax(1)-imgmin(1)) + spacing;
    end
    
    xmlwrite(filename,doc);
    
    [fpath,fname,~] = fileparts(filename);
    scoresfilename = fullfile(fpath,[fname,'_scores.txt']);
    fid = fopen(scoresfilename,'w');
    for i=1:nimgs
        fprintf(fid,'%f\n',scores(i));
    end
    fclose(fid);
    
    obj.lastqueryresultimgsavename = filename;
    
    disp('done');
    
end

function loadDescriptors(obj,filename)
    
    if nargin < 2 || isempty(filename)
        if not(isempty(obj.lastdescloadname))
            loadname = obj.lastdescloadname;
        else
            loadname = '../data/descriptors/tempdescriptors.mat';
        end
        
        [fname,pathname,~] = ...
            uigetfile({'*.zip','ZIP (*.zip)'; '*.mat','MAT (*.mat)'},'Load Descriptors',loadname);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
%     dlg = dialog('Name','Loading Descriptors ...','Position',[0,0,250,100]);
%     uicontrol('Parent',dlg,...
%         'Style','text',...
%         'Position',[0,0,1,1],...
%         'String','Loading precomputed descriptors, this may take some time ...');
%     movegui(dlg,'center')
    dlg = waitdlg('Loading precomputed descriptors, this may take some time ...');
    drawnow;
    
    disp('Loading precomputed descriptors ...');
    
    delete(obj.dbrels(isvalid(obj.dbrels)));
    
    [~,~,fext] = fileparts(filename);
    
    if strcmp(fext,'.zip')
        temppath = tempname;
        mkdir(temppath);
        unzippedfilename = unzip(filename,temppath);
        r = load(unzippedfilename{1});
        rmdir(temppath,'s');
    else
        r = load(filename);
    end
    
    r = r.r;
    
    obj.dbrels = r;
    
    % re-set working set to refresh wsrels
    obj.setWorkingset(obj.wsimginds);
    
    obj.descriptorsfilename = filename;
    
    obj.lastdescloadname = filename;
    
    disp('done');
    
    delete(dlg(isvalid(dlg)));
end


function saveDescriptors(obj,filename,desc)
    if nargin < 3
        desc = obj.dbrels;
    end
    
    if isempty(desc)
        warning('Descriptors are empty.');
        return;
    end
    
    if nargin < 2 || isempty(filename)
        if not(isempty(obj.lastdescsavename))
            savename = obj.lastdescsavename;
        else
            savename = '../data/descriptors/tempdescriptors.mat';
        end
        
        [fname,pathname,~] = ...
            uiputfile({'*.zip','ZIP (*.zip)'; '*.mat','MAT (*.mat)'},'Save Descriptors',savename);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    disp('saving descriptors ...');
    
    r = desc; %#ok<NASGU>
    
    [~,fname,fext] = fileparts(filename);
    if strcmp(fext,'.zip')
        temppath = tempname;
        mkdir(temppath);
        unzippedfilename = [fname,'.mat'];
        save(fullfile(temppath,unzippedfilename),'r','-v7.3');
        zip(filename,unzippedfilename,temppath);
        rmdir(temppath,'s');
    else
        save(filename,'r','-v7.3');
    end
    
    obj.descriptorsfilename = filename;
    
    obj.lastdescsavename = filename;
    
    disp('done');
end

function saveQuery(obj,filename)
    if isempty(obj.query)
        return;
    end
    
    if nargin < 2 || isempty(filename)
        if not(isempty(obj.lastquerysavename))
            savename = obj.lastquerysavename;
        else
            savename = '../results/queryresults/tempquery.mat';
        end
        
        [fname,pathname,~] = ...
            uiputfile({'*.mat','MAT (*.mat)'; '*.zip','ZIP (*.zip)'},'Save Query',savename);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    q = obj.query; %#ok<NASGU>
    
    r = obj.query.rels; %#ok<NASGU>
    
    [~,fname,fext] = fileparts(filename);
    if strcmp(fext,'.zip')
        temppath = tempname;
        mkdir(temppath);
        unzippedfilename = [fname,'.mat'];
        save(fullfile(temppath,unzippedfilename),'q','-v7.3');
        zip(filename,unzippedfilename,temppath);
        rmdir(temppath,'s');
    else
        save(filename,'q','-v7.3');
    end
    
    obj.lastquerysavename = filename;
    
    disp('done');
end

function loadQuery(obj,filename)
    
    if nargin < 2 || isempty(filename)
        if not(isempty(obj.lastqueryloadname))
            loadname = obj.lastqueryloadname;
        else
            loadname = '../results/queries/tempquery.mat';
        end
        
        [fname,pathname,~] = ...
            uigetfile({'*.mat','MAT (*.mat)'; '*.zip','ZIP (*.zip)'},'Load Query',loadname);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    [~,~,fext] = fileparts(filename);
    
    if strcmp(fext,'.zip')
        temppath = tempname;
        mkdir(temppath);
        unzippedfilename = unzip(filename,temppath);
        q = load(unzippedfilename{1});
        rmdir(temppath,'s');
    else
        q = load(filename);
    end
    
    q = q.q;
    
    if isempty(q.rels)
        return; % user did not enter the database path
    end
    rankimages = not(isempty(q.rels.imgfilenames));
    obj.setQuery(q,rankimages);
    
    obj.lastqueryloadname = filename;
end

function saveClassification(obj,filename)
    if isempty(obj.classification)
        return;
    end
    
    if nargin < 2 || isempty(filename)
        if not(isempty(obj.lastclassificationsavename))
            savename = obj.lastclassificationsavename;
        else
            savename = '../results/classification/tempclassification.mat';
        end
        
        [fname,pathname,~] = ...
            uiputfile({'*.mat','MAT (*.mat)'; '*.zip','ZIP (*.zip)'},'Save Classification',savename);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    c = obj.classification; %#ok<NASGU>
    
    [~,fname,fext] = fileparts(filename);
    if strcmp(fext,'.zip')
        temppath = tempname;
        mkdir(temppath);
        unzippedfilename = [fname,'.mat'];
        save(fullfile(temppath,unzippedfilename),'c','-v7.3');
        zip(filename,unzippedfilename,temppath);
        rmdir(temppath,'s');
    else
        save(filename,'c','-v7.3');
    end
    
    obj.lastclassificationsavename = filename;
    
    disp('done');
end

function loadClassification(obj,filename)
    
    if nargin < 2 || isempty(filename)
        if not(isempty(obj.lastclassificationloadname))
            loadname = obj.lastclassificationloadname;
        else
            loadname = '../results/classification/tempclassification.mat';
        end
        
        [fname,pathname,~] = ...
            uigetfile({'*.mat','MAT (*.mat)'; '*.zip','ZIP (*.zip)'},'Load Classification',loadname);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    [~,~,fext] = fileparts(filename);
    
    if strcmp(fext,'.zip')
        temppath = tempname;
        mkdir(temppath);
        unzippedfilename = unzip(filename,temppath);
        c = load(unzippedfilename{1});
        rmdir(temppath,'s');
    else
        c = load(filename);
    end
    
    c = c.c;
    
    obj.setClassification(c);
    
    obj.lastclassificationloadname = filename;
    
    disp('done');
end

function loadRelationtemplates(obj,filenames)
    for i=1:size(filenames,1)
        im = imread(filenames{i,2});
        im = im2double(flipud(im));
        obj.reltemplates(i,:) = {filenames{i,1},im};
    end
end

function setDesctype(obj,type)
    obj.desctype = type;
end

function setSimmethod(obj,method)
    obj.descsimmethod = method;
end

function params = predictorparams(obj)
%     obj.predtype = 'brsvm';
    obj.predtype = 'brknn';
    
    if strcmp(obj.predtype,'brsvm')
%         params = {...
%             'KernelFunction','linear'};
%         params = {...
%             'KernelFunction','gaussian'};
        params = {};
    elseif strcmp(obj.predtype,'brknn')
        params = {...
            'NumNeighbors',5};
    else
        error('Unknown predictor type.');
    end
end

function params = descriptorparams(obj)

    obj.desctype = 'reldisthist'; % default
%     obj.desctype = 'avg'; % compare 1
%     obj.desctype = 'ibs'; % compare 2
    

    obj.descsimmethod = 'negativeL1dist'; % default and compare 1
%     obj.descsimmethod = 'negativeIBSweightedL1dist'; % compare 2

    if strcmp(obj.desctype,'avg')
        
        obj.niangularbins = 8;
        obj.niradialbins = 2;%4
        
        params = {...
            'pointdensity',1,...
            'normalizehist','binarea',...
            'normalizeregion','descsum',...
            'radlin',0,...
            'radf',1};
        
    elseif strcmp(obj.desctype,'ibs')
        
        params = {...
            'ndist',10,...
            'nnormalangle',10,...
            'nnormaldiff',5,...
            'defaultdistvar',0.0318,...
            'defaultnanglevar',0.0333,...
            'defaultndiffvar',0.0293,...
            'separationinset',0.003};

        % default variance values gotten empirically from the COCO 10k
        % dataset and the riding relationship

    elseif strcmp(obj.desctype,'reldisthist')
        
        obj.niangularbins = 8;
        obj.niradialbins = 2;
        obj.noangularbins = 8;
        obj.noradialbins = 2;

        params = {...
            'ipointdensity',10000,...
            'opointdensity',1,...
            'normalizeinner','binarea',...
            'normalizeouter','none',...
            'normalizeregion','binregionoverlapsumdescsum',...
            'iradf',1,... % in multiples of the outer histogram radius
            'oradf',0.5,... % in multiples of the region span
            'iradlin',0,... % in [0,1], 0: logarithmic radii, 1: linear radii
            'oradlin',1,... % in [0,1], 0: logarithmic radii, 1: linear radii
            'batchsize',1e6};

    else
        error('Unknown descriptor type.');
    end
end

function template = descriptorTemplate(obj)
    
    descparams = obj.descriptorparams;
    
    if strcmp(obj.desctype,'avg')
        
        template = RegionavgDescriptorSet(...
            obj.niangularbins,obj.niradialbins,...
            descparams{:});
        
    elseif strcmp(obj.desctype,'ibs')
        
        descparams = nvpairs2struct(descparams);
        
        ndist = descparams.ndist;
        nnormalangle = descparams.nnormalangle;
        nnormaldiff = descparams.nnormaldiff;
        
        descparams = rmfield(descparams,{'ndist','nnormalangle','nnormaldiff'});
        
        descparams = struct2nvpairs(descparams);
        
        template = IBSDescriptorSet(...
            ndist,nnormalangle,nnormaldiff,...
            descparams{:});
        
    elseif strcmp(obj.desctype,'reldisthist')

        template = ReldisthistDescriptorSet(...
            obj.niangularbins,obj.niradialbins,obj.noangularbins,obj.noradialbins,...
            descparams{:});
        
    else
        error('Unknown descriptor type.');
    end
end

function [template,subjectlbl,objectlbl] = example2template(obj,examplesubjects,exampleobjects)
    
    template = [];
    subjectlbl = zeros(1,0);
    objectlbl = zeros(1,0);

    if numel(examplesubjects) > 1
        % When choosing relationships in the other images, only a single
        % source region is always chosen as well.
        warndlg('Can currently only query with a single source region.');
        uiwait;
        return;
    end
    
    if not(all([examplesubjects.label] == examplesubjects(1).label)) || ...
       not(all([exampleobjects.label] == exampleobjects(1).label))
        warndlg('All query subjects must have the same label and all query objects must have the same label.');
        uiwait;
        return;
    end
    
    imgmin = obj.imgcanvas.imgmin;
    imgmax = obj.imgcanvas.imgmax;
    imgres = size(obj.imgcanvas.img);
    imgres = imgres([2,1])';
    
    descparams = obj.descriptorparams;
    if strcmp(obj.desctype,'avg')
        template = RegionavgDescriptorSet(...
            obj.niangularbins,obj.niradialbins,...
            {examplesubjects},{exampleobjects},...
            imgmin,imgmax,imgres,...
            descparams{:});
        
        if size(template.val,2) ~= 1
            error('Multiple templates not supported.');
        end
        
    elseif strcmp(obj.desctype,'ibs')    
        
        descparams = nvpairs2struct(descparams);
        
        ndist = descparams.ndist;
        nnormalangle = descparams.nnormalangle;
        nnormaldiff = descparams.nnormaldiff;
        
        descparams = rmfield(descparams,{'ndist','nnormalangle','nnormaldiff'});
        
        descparams = struct2nvpairs(descparams);
        
        template = IBSDescriptorSet(...
            ndist,nnormalangle,nnormaldiff,...
            {examplesubjects},{exampleobjects},...
            imgmin,imgmax,imgres,...
            descparams{:});
        
        if size(template.val,2) ~= 1
            error('Multiple templates not supported.');
        end
        
    elseif strcmp(obj.desctype,'reldisthist')
        template = ReldisthistDescriptorSet(...
            obj.niangularbins,obj.niradialbins,obj.noangularbins,obj.noradialbins,...
            {examplesubjects},{exampleobjects},...
            imgmin,imgmax,imgres,...
            descparams{:});
        
        if size(template.val,2) ~= 1
            error('Multiple templates not supported.');
        end
    else
        error('Unknown descriptor type.');
    end
    
    subjectlbl = examplesubjects(1).label;
    objectlbl = exampleobjects(1).label;
end

function startQueryWithDrawing(obj,querymode)
    
    if nargin < 2 || isempty(querymode)
        querymode = 'all';
    end
    
    if isempty(obj.wsrels)
        warndlg('No precomputed descriptors loaded.');
        return;
    end
    
    if numel(obj.wsimginds) ~= numel(obj.wsrels.imgfilenames) || not(all(strcmp(obj.dbinfo.imgfilenames(obj.wsimginds),obj.wsrels.imgfilenames)))
        warning('Relationship workingset out of sync with image workingset.');
        return;
    end
    
    srcclr = rgbhex2rgbdec('ffba5f');
    srcmask = ...
        obj.drawcanvas.img(:,:,1) == srcclr(1) & ...
        obj.drawcanvas.img(:,:,2) == srcclr(2) & ...
        obj.drawcanvas.img(:,:,3) == srcclr(3) & ...
        obj.drawcanvas.img(:,:,4) > 0.5;
    
    tgtclr = rgbhex2rgbdec('92b1ff');
    tgtmask = ...
        obj.drawcanvas.img(:,:,1) == tgtclr(1) & ...
        obj.drawcanvas.img(:,:,2) == tgtclr(2) & ...
        obj.drawcanvas.img(:,:,3) == tgtclr(3) & ...
        obj.drawcanvas.img(:,:,4) > 0.5;

    srcpolys = bwmask2polygons(srcmask,...
        'removenoise',true,...
        'cutholes',true,...
        'normalize',true,...
        'flipy',false);
    tgtpolys = bwmask2polygons(tgtmask,...
        'removenoise',true,...
        'cutholes',true,...
        'normalize',true,...
        'flipy',false);
    
    if isempty(srcpolys) || isempty(tgtpolys)
        warning('Source or target regions vanish (may be too small).');
        return;
    end
    
    % from normalized coordinates in draw canvas to image coordinates
    for i=1:numel(srcpolys);
        srcpolys{i} = bsxfun(@plus,bsxfun(@times,srcpolys{i},obj.drawcanvas.imgmax-obj.drawcanvas.imgmin),obj.drawcanvas.imgmin);
    end
    for i=1:numel(tgtpolys);
        tgtpolys{i} = bsxfun(@plus,bsxfun(@times,tgtpolys{i},obj.drawcanvas.imgmax-obj.drawcanvas.imgmin),obj.drawcanvas.imgmin);
    end
    
    % from image coordinates to annotation coordinates (normalized in image canvas)
    srcpolys = ImageAnnotation.im2ancoords(srcpolys,obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);
    tgtpolys = ImageAnnotation.im2ancoords(tgtpolys,obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);
   
    srcobj = ImageObject(0,0,srcpolys,false);
    tgtobj = ImageObject(0,0,tgtpolys,false);
    
    wsimgrels = obj.wsrels.clone;
    
    srclabel = obj.drawsrclbl;
    tgtlabel = obj.drawtgtlbl;
    
    if strcmp(querymode,'all')
        srclabel = '';
        tgtlabel = '';
    elseif strcmp(querymode,'single')
    elseif strcmp(querymode,'alltgt')
        tgtlabel = '';
    elseif strcmp(querymode,'allsrc')
        srclabel = '';
    end
    
    q = obj.createQuery(srcobj,tgtobj);
    
    ticid = tic;
    obj.runQuery(q,wsimgrels,srclabel,tgtlabel,'rankimages',true);
    toc(ticid);
    
%     delete(wsimgrels(isvalid(wsimgrels)));
end

function startQueryWithExample(obj,querymode)
    
    if nargin < 2 || isempty(querymode)
        querymode = 'all';
    end
    
    if isempty(obj.selsourceinds) || isempty(obj.seltargetinds)
        warndlg('Please select source- and target regions first.','Select Regions','modal');
        return;
    end
    
    if isempty(obj.wsrels)
        warndlg('No precomputed descriptors loaded.');
        return;
    end
    
    if numel(obj.wsimginds) ~= numel(obj.wsrels.imgfilenames) || not(all(strcmp(obj.dbinfo.imgfilenames(obj.wsimginds),obj.wsrels.imgfilenames)))
        warning('Relationship workingset out of sync with image workingset.');
        return;
    end
    
    wsimgrels = obj.wsrels.clone;
    
    srclabel = obj.annotation.imgobjects(obj.selsourceinds(1)).label;
    tgtlabel = obj.annotation.imgobjects(obj.seltargetinds(1)).label;
    
    if strcmp(querymode,'all')
        srclabel = '';
        tgtlabel = '';
    elseif strcmp(querymode,'single')
    elseif strcmp(querymode,'alltgt')
        tgtlabel = '';
    elseif strcmp(querymode,'allsrc')
        srclabel = '';
    end
    
    q = obj.createQuery(...
        obj.annotation.imgobjects(obj.selsourceinds),...
        obj.annotation.imgobjects(obj.seltargetinds));
            
    ticid = tic;
    obj.runQuery(q,wsimgrels,srclabel,tgtlabel,'rankimages',true);
    toc(ticid);
    
%     delete(wsimgrels(isvalid(wsimgrels)));
end

% createQuery(obj,querysentence)
% createQuery(obj,querysrcobj,querytgtobj)
% createQuery(...,'mode',querymode)
function [q,subjectlbl,objectlbl] = createQuery(obj,varargin)

    % parse inputs
    querysentence = '';
    querysrcobj = ImageObject.empty;
    querytgtobj = ImageObject.empty;
    if ischar(varargin{1})
        querysentence = varargin{1};
        varargin(1) = [];
    elseif isa(varargin{1},'ImageObject')
        querysrcobj = varargin{1};
        querytgtobj = varargin{2};
        varargin(1:2) = [];
    else
        error('Invalid arguments')
    end
    
    obj.descriptorparams; % to set correct sim method, etc.
    
    options = struct(...
        'querymode','single',...
        'rankimages',true,...
        'nbest',100000,...
        'simmethod',obj.descsimmethod,...
        'ignoreownlabel',true,... % false
        'savedescriptors',true);
%             'labeled','yes',... % temp!
    options = nvpairs2struct(varargin,options);
    
    % get new template if requested
    newtemplate = [];
    if not(isempty(querysentence))
        [newtemplate,subjectlbl,objectlbl] = obj.sentence2template(querysentence);
    elseif not(isempty(querysrcobj)) && not(isempty(querytgtobj))
        [newtemplate,subjectlbl,objectlbl] = obj.example2template(querysrcobj,querytgtobj);
    end

    if strcmp(options.querymode,'single')
        % do nothing
    elseif strcmp(options.querymode,'alltgt')
        objectlbl = zeros(1,0);
    elseif strcmp(options.querymode,'allsrc')
        subjectlbl = zeros(1,0);
    elseif strcmp(options.querymode,'all')
        objectlbl = zeros(1,0);
        subjectlbl = zeros(1,0);
    else
        error('Unknown query mode.');
    end
    
    params = options;
    params = rmfield(params,'querymode');
    params = rmfield(params,'rankimages');

%     if strcmp(obj.desctype,'point')
%         params.pointdensity = 10000;
%     end
%     params.nbest = 100000; % save decriptors and region info for the best 100000 images
%     params.simmethod = obj.descsimmethod;
% %     params.ignoreownlabel = false;
%     params.ignoreownlabel = true; % temp
    params = struct2nvpairs(params);

    q = RelationshipQuery(newtemplate,params{:});
end

% runQuery(obj,queryset)
% runQuery(obj,queryset,srclbl,targetlbl)
% runQuery(...,nvpairs)
% queryset may either be a DatabaseRelationshipSet or a set of imgfilenames
function runQuery(obj,q,queryset,srclbl,tgtlbl,varargin)

    if isempty(q)
        error('Empty query given.');
    end
    
    options = struct(...
        'rankimages',true);
    options = nvpairs2struct(varargin,options);
    
    q.runQuick(queryset,srclbl,tgtlbl);
    
    obj.setQuery(q,options.rankimages);
end

function setQuery(obj,query,rankimages)
    
    if nargin < 3 || isempty(rankimages)
        rankimages = false;
    end
    
    if isempty(query)
        return;
    end
    
    if isempty(obj.query) ~= isempty(query) || obj.query ~= query
        delete(obj.query(isvalid(obj.query)));
        obj.query = query;
    end
    
    if rankimages
        obj.rankWorkingset(obj.query);
    else
        [~,browserinds] = ismember(...
            strcat([obj.imgpath,'/'],obj.query.rels.imgfilenames),...
            obj.imgbrowser.imgfilenames);
        mask = browserinds >= 1;
        browserinds = browserinds(mask);

        if not(isempty(browserinds))
            if strcmp(obj.desctype,'point')
                imgtext = array2strings(obj.query.imglikelihoodrange(2,mask),'%.8f')';
            else
                imgtext = array2strings(obj.query.imglikelihoodrange(2,mask),'%.8f')';
            end
            obj.imgbrowser.setImgtext(browserinds,imgtext);
        end
    end
    
    obj.queryEntryChanged;
    
    if not(obj.resultsVisible)
        obj.toggleResultsVisible;
    end
    
    if obj.querydrawingVisible
        obj.toggleQuerydrawingVisible;
    end
end

function rankWorkingset(obj,query)
    [ranking,maxlhs] = query.getRanking;
    obj.setWorkingset(query.rels.imgfilenames(ranking));
    obj.imgbrowser.setImgtext(1:numel(ranking),array2strings(maxlhs,'%.8f'));
end

function clearQuery(obj)
    delete(obj.query(isvalid(obj.query)));
    obj.query = RelationshipQuery.empty;
    
    obj.setWorkingset(obj.origwsimginds);
    
    obj.queryEntryChanged;
end

function c = trainClassification(obj,trainimgfilenames)
    descparams = obj.descriptorparams;
    predparams = obj.predictorparams;
    
    disp('training ...');
    c = RelationshipClassification(...
        [],[],trainimgfilenames,obj.imgpath,obj.annopath,...
        'desctype',obj.desctype,...
        'descparams',nvpairs2struct(descparams),...
        'descsimmethod',obj.descsimmethod,...
        'predtype',obj.predtype,...
        'predparams',nvpairs2struct(predparams),...
        'niangular',obj.niangularbins,...
        'niradial',obj.niradialbins,...
        'noangular',obj.noangularbins,...
        'noradial',obj.noradialbins);
end

function setClassification(obj,classification)
    delete(obj.classification(isvalid(obj.classification)));
    obj.classification = classification;
    
%     obj.gvalidatebutton.Enable = 'on';
%     obj.gclassifybutton.Enable = 'on';
%     obj.gclassifyimgbutton.Enable = 'on';
    
    obj.classificationEntryChanged;
end

function clearClassification(obj)
    delete(obj.classification(isvalid(obj.classification)));
    obj.classification = RelationshipClassification.empty;
    
%     obj.gvalidatebutton.Enable = 'off';
%     obj.gclassifybutton.Enable = 'off';
%     obj.gclassifyimgbutton.Enable = 'off';
    
    obj.classificationEntryChanged;
end

function txt = objectinfotext(obj,objind)
    txt = [...
        '---------------','\n',...
        'Id: ',num2str(obj.annotation.imgobjects(objind).id),'\n',...
        'Label: ',obj.dbinfo.categories{obj.annotation.imgobjects(objind).label},'\n'];
end

function txt = queryrelinfotext(obj,queryrelind)
    if isempty(obj.queryrels) 
        return;
    end
    
    imgobjids = [obj.annotation.imgobjects.id];
    srcobjs = obj.annotation.imgobjects(ismember(imgobjids,obj.queryrels.sourceids{queryrelind}));
    tgtobjs = obj.annotation.imgobjects(ismember(imgobjids,obj.queryrels.targetids{queryrelind}));
    
    txt = [...
        '---------------','\n',...
        'Id: ',num2str(obj.queryrels.sourceids{queryrelind}),'\n',...
        'Label: ',sprintf('%s ',obj.dbinfo.categories{[srcobjs.label]}),'\n',...
        'Area: ',sprintf('%s ',srcobjs.area),'\n',...
        'Target Id: ',num2str(obj.queryrels.targetids{queryrelind}),'\n',...
        'Target Label: ',sprintf('%s ',obj.dbinfo.categories{[tgtobjs.label]}),'\n',...    
        'Score: ',num2str(obj.querylikelihoods(queryrelind)),'\n'];
end

function unblock(obj)
    obj.blockcallbacks = false;
    obj.navwidget.unblock;
    obj.imgcanvas.unblock;
    obj.imgbrowser.unblock;
    obj.drawcanvas.unblock;
end

function mousePressed(obj,src,evt)

    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

%     disp(src.SelectionType);
    
    obj.selpanel = gparent(obj.fig.CurrentObject,'uipanel',true);
    
    obj.mousedown = true;

    obj.mousepos = mouseposOnPlane(obj.axes);
    obj.clickedpos = obj.mousepos;
    
    if obj.selpanel == obj.mainpanel
        if obj.navwidget.zoommode || ...
           obj.navwidget.panmode || ...
           obj.navwidget.orbitmode

            obj.navwidget.mousePressed(src,evt);
        elseif strcmp(obj.tool,'annoselect')
            % find closest, then smallest annotation
            if not(isempty(obj.annotation))
                
                [objind,compind] = ImageEditor.closestImageObject(...
                    obj.clickedpos(1:2),obj.annotation.imgobjects,...
                    obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);
                
                if not(isempty(objind))
                    fprintf(obj.objectinfotext(objind));
                end
                
                obj.selobjectinds = selectMultiple(obj.selobjectinds,objind,src.SelectionType);
                obj.selcomponentinds = selectMultiple(obj.selcomponentinds,[objind;compind],src.SelectionType);
            end
        elseif strcmp(obj.tool,'queryrelselect')
            % select closest descriptor
            if not(isempty(obj.queryrels))
                    
                if strcmp(src.SelectionType,'alt') && ...
                   not(isempty(obj.selqueryrel)) &&...
                   isa(obj.queryrels.descriptors,'ReldisthistDescriptorSet')

                    descind = obj.queryrels.descind(obj.selqueryrel(1));     
                    [~,~,~,~,ibincenters,obincenters,~] = ...
                        obj.getReldisthistDescriptorGeometry(...
                        descind);

                    if obj.innerdescriptorVisible
                        dists = sqrt(sum(bsxfun(@minus,obincenters,obj.clickedpos(1:2,1)).^2,1));
                        [~,obj.selobinind] = min(dists);
                    elseif obj.outerdescriptorVisible
                        dists = sqrt(sum(bsxfun(@minus,ibincenters,obj.clickedpos(1:2,1)).^2,1));
                        [~,obj.selibinind] = min(dists);
                    else
                        dists = sqrt(sum(bsxfun(@minus,obincenters,obj.clickedpos(1:2,1)).^2,1));
                        [~,obj.selobinind] = min(dists);
                    end
                else
                    querysrcids = obj.queryrels.sourceids;
                    queryrelinds = cellind_mex(querysrcids);
                    
                    querysrcids = [querysrcids{:}];
                    queryrelinds = [queryrelinds{:}];
                    [~,querysrcinds] = ismember(querysrcids,[obj.annotation.imgobjects.id]);
                    
                    querysrcindstemp = querysrcinds;
                    querysrcindstemp(querysrcindstemp==0) = numel(obj.annotation.imgobjects)+1;
                    queryrelinds = array2cell_mex(queryrelinds,querysrcindstemp);
                    
                    querysrcinds(querysrcinds==0) = [];
                    [minobjind,~,mindist] = ImageEditor.closestImageObject(...
                        obj.clickedpos(1:2),obj.annotation.imgobjects(querysrcinds),...
                        obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);
                    
%                     disp(obj.annotation.imgobjects(querysrcinds(minobjind)).area);
                    
                    if isinf(mindist)
                        obj.selqueryrel = zeros(1,0);
                    else
                        queryrelinds = queryrelinds{querysrcinds(minobjind)};
                        [~,maxind] = max(obj.querylikelihoods(queryrelinds));
                        if not(isempty(maxind))
                            obj.selqueryrel = queryrelinds(maxind);
                        else
                            obj.selqueryrel = zeros(1,0);
                        end
                    end
                    
                    if not(isempty(obj.selqueryrel))
                        fprintf(obj.queryrelinfotext(obj.selqueryrel));
                        imgobjids = [obj.annotation.imgobjects.id];
                        obj.selsourceinds = find(ismember(imgobjids,obj.queryrels.sourceids{obj.selqueryrel}));
                        obj.seltargetinds = find(ismember(imgobjids,obj.queryrels.targetids{obj.selqueryrel}));
                    else
                        obj.selsourceinds = zeros(1,0);
                        obj.seltargetinds = zeros(1,0);
                    end
                end
            end
        elseif strcmp(obj.tool,'selexamplesource')
            [objind,~] = ImageEditor.closestImageObject(...
                obj.clickedpos(1:2),obj.annotation.imgobjects,...
                obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);
            
            obj.selsourceinds = selectMultiple(obj.selsourceinds,objind,src.SelectionType);
        elseif strcmp(obj.tool,'selexampletargets')
            [objind,~] = ImageEditor.closestImageObject(...
                obj.clickedpos(1:2),obj.annotation.imgobjects,...
                obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);
            
            obj.seltargetinds = selectMultiple(obj.seltargetinds,objind,src.SelectionType);
        elseif strcmp(obj.tool,'drawanno')
            if strcmp(src.SelectionType,'extend')
                if not(isempty(obj.annotation)) && not(isempty(obj.editpoly))
                    newid = missingint([obj.annotation.imgobjects.id]);
                    lbl = categorydlg('Pick a Region Label:','','',obj.dbinfo.categories);
                    
                    if not(isempty(lbl))
                        lblind = find(strcmp(obj.dbinfo.categories,lbl),1,'first');
                        if isempty(lblind)
                            btn = questdlg(['Category ''',lbl,''' does not exist, do you want to create it?'],...
                                'Create New Category?','Yes','No','No');
                            if strcmp(btn,'Yes')
                                obj.dbinfo.categories{end+1,:} = lbl;
                                ImageDatabaseInfoExporter.exportCategories(...
                                    obj.dbinfo.categories,obj.dbinfofilenames.categories)
                                lblind = numel(obj.dbinfo.categories);
                            end
                        end
                        if not(isempty(lblind))
                            epoly = ImageAnnotation.im2ancoords(obj.editpoly,...
                                obj.imgcanvas.imgmin,...
                                obj.imgcanvas.imgmax);
                            imgobj = ImageObject(newid,lblind,{epoly});
                            obj.annotation.addImageobjects(imgobj);
                            obj.wsentrydirty = true;
                        end
                    end
                    
                    obj.editpoly = zeros(2,0);
                    obj.showEditpoly;
                    obj.showAnnopolys;
                end
            elseif strcmp(src.SelectionType,'alt')
                if not(isempty(obj.editpoly))
                    obj.editpoly(:,end) = [];
                end
                obj.showEditpoly;
            elseif strcmp(src.SelectionType,'normal')
                obj.editpoly(:,end+1) = obj.clickedpos(1:2);
                obj.showEditpoly;
            end
        elseif strcmp(obj.tool,'drawquerysrc')
            if strcmp(src.SelectionType,'alt')
                obj.drawcanvas.setTool('eraser');
            else
                obj.drawcanvas.setTool('brush');
            end

            obj.drawcanvas.mousePressed(src,evt);

        elseif strcmp(obj.tool,'drawquerytgt')

            if strcmp(src.SelectionType,'alt')
                obj.drawcanvas.setTool('eraser');
            else
                obj.drawcanvas.setTool('brush');
            end

            obj.drawcanvas.mousePressed(src,evt);

        elseif strcmp(obj.tool,'')
            % do nothing
        else
            error('Unknown tool.')
%             obj.imgcanvas.mousePressed(src,evt);
        end
    elseif obj.selpanel == obj.imgbrowserpanel
        if isvalid(obj.imgbrowser)
            obj.imgbrowser.mousePressed(src,evt);
        end
    else
        % do nothing
    end

    obj.blockcallbacks = false;
end

function mouseMoved(obj,src,evt)

    if obj.mousedown

        if obj.blockcallbacks
            return;
        end
        obj.blockcallbacks = true;

        obj.mousepos = mouseposOnPlane(obj.axes);

        if obj.selpanel == obj.mainpanel
            if obj.navwidget.zoommode || ...
               obj.navwidget.panmode || ...
               obj.navwidget.orbitmode
                % camera navigation

                obj.navwidget.mouseMoved(src,evt);
            elseif strcmp(obj.tool,'annoselect')
                % do nothing
            elseif strcmp(obj.tool,'queryrelselect')
                % do nothing
            elseif strcmp(obj.tool,'selexamplesource')
                % do nothing
            elseif strcmp(obj.tool,'selexampletargets')
                % do nothing
            elseif strcmp(obj.tool,'drawanno')
                % do nothing
            elseif strcmp(obj.tool,'drawquerysrc')
                obj.drawcanvas.mouseMoved(src,evt);
            elseif strcmp(obj.tool,'drawquerytgt')
                obj.drawcanvas.mouseMoved(src,evt);
            elseif strcmp(obj.tool,'')
                % do nothing
            else
                error('Unknown tool.')
            end
        elseif obj.selpanel == obj.imgbrowserpanel
            if isvalid(obj.imgbrowser)
                obj.imgbrowser.mouseMoved(src,evt);
            end
        end

        obj.blockcallbacks = false;
    end
end

function mouseReleased(obj,src,evt)

    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = false;

    if obj.selpanel == obj.mainpanel
        if obj.navwidget.zoommode || ...
           obj.navwidget.panmode || ...
           obj.navwidget.orbitmode

            obj.navwidget.mouseReleased(src,evt);
        elseif strcmp(obj.tool,'annoselect')
            % do nothing
        elseif strcmp(obj.tool,'queryrelselect')
            % do nothing
        elseif strcmp(obj.tool,'selexamplesource')
            % do nothing
        elseif strcmp(obj.tool,'selexampletargets')
            % do nothing
        elseif strcmp(obj.tool,'drawanno')
            % do nothing
        elseif strcmp(obj.tool,'drawquerysrc')
            obj.drawcanvas.mouseReleased(src,evt);
            % do nothing
        elseif strcmp(obj.tool,'drawquerytgt')
            obj.drawcanvas.mouseReleased(src,evt);
            % do nothing
        elseif strcmp(obj.tool,'')
            % do nothing
        else
            error('Unknown tool.')
        end
    elseif obj.selpanel == obj.imgbrowserpanel
        if isvalid(obj.imgbrowser)
            obj.imgbrowser.mouseReleased(src,evt);
        end
    end

    obj.blockcallbacks = false;
end

function keyPressed(obj,src,evt)
    if strcmp(evt.Key,'1')
        if obj.navwidget.panmode || obj.navwidget.zoommode || obj.navwidget.orbitmode
            obj.navwidget.setPanmode(false);
        else
            obj.navwidget.setPanmode(true);
        end
    elseif strcmp(evt.Key,'c')
        resetCamera(obj.axes);
    elseif obj.selpanel == obj.imgbrowserpanel
        if strcmp(evt.Key,'downarrow')
            obj.selpanel = obj.mainpanel;
        else
            obj.imgbrowser.keyPressed(src,evt);
        end
    elseif obj.selpanel == obj.mainpanel
        if strcmp(evt.Key,'2')
            obj.tool = 'annoselect';
        elseif strcmp(evt.Key,'3')
            if not(obj.resultsVisible)
                obj.toggleResultsVisible;
            end
            if obj.querydrawingVisible
                obj.toggleQuerydrawingVisible;
            end
        elseif strcmp(evt.Key,'rightarrow')
            obj.imgbrowser.openedimgind = min(numel(obj.imgbrowser.imgfilenames),obj.imgbrowser.openedimgind+1);
            obj.imgbrowser.scrollto(obj.imgbrowser.openedimgind);
        elseif strcmp(evt.Key,'leftarrow')
            obj.imgbrowser.openedimgind = max(1,obj.imgbrowser.openedimgind-1);
            obj.imgbrowser.scrollto(obj.imgbrowser.openedimgind);
        elseif strcmp(evt.Key,'9')
            obj.tool = 'drawquerysrc';
            if not(obj.querydrawingVisible)
                obj.toggleQuerydrawingVisible;
            end
            if obj.resultsVisible
                obj.toggleResultsVisible;
            end
        elseif strcmp(evt.Key,'0')
            obj.tool = 'drawquerytgt';
            if not(obj.querydrawingVisible)
                obj.toggleQuerydrawingVisible;
            end
            if obj.resultsVisible
                obj.toggleResultsVisible;
            end
        elseif strcmp(evt.Key,'backspace')
            obj.clearQuery;
        elseif strcmp(obj.tool,'annoselect')
            if strcmp(evt.Key,'delete')
                obj.annotation.removeImageobjects(obj.selobjectinds);
                obj.selobjectinds = zeros(1,0);

                obj.wsentrydirty = true;
            end
        elseif strcmp(obj.tool,'queryrelselect')    
            if strcmp(evt.Key,'delete')
                obj.selibinind = zeros(1,0);
                obj.selobinind = zeros(1,0);
            end
        elseif strcmp(obj.tool,'drawquerysrc')
            if strcmp(evt.Key,'q')
                obj.startQueryWithDrawing('single');
            elseif strcmp(evt.Key,'l')
                srclbl = categorydlg('Pick a Source Region Label:','','',obj.dbinfo.categories);
                if not(isempty(srclbl))
                    srclbl = find(strcmp(srclbl,obj.dbinfo.categories),1,'first');
                    if isempty(srclbl)
                        warndlg('Given label was not found in the categories.');
                    else
                        obj.drawsrclbl = srclbl;
                    end
                end
            elseif strcmp(evt.Key,'delete')
                obj.drawsrclbl = [];
                obj.drawtgtlbl = [];
                obj.drawcanvas.keyPressed(src,evt);
            else
                obj.drawcanvas.keyPressed(src,evt);
            end
        elseif strcmp(obj.tool,'drawquerytgt')
            if strcmp(evt.Key,'q')
                obj.startQueryWithDrawing('single');
            elseif strcmp(evt.Key,'l')
                tgtlbl = categorydlg('Pick a Target Region Label:','','',obj.dbinfo.categories);
                if not(isempty(tgtlbl))
                    tgtlbl = find(strcmp(tgtlbl,obj.dbinfo.categories),1,'first');
                    if isempty(tgtlbl)
                        warndlg('Given label was not found in the categories.');
                    else
                        obj.drawtgtlbl = tgtlbl;
                    end
                end
            elseif strcmp(evt.Key,'delete')
                obj.drawsrclbl = [];
                obj.drawtgtlbl = [];
                obj.drawcanvas.keyPressed(src,evt);
            else
                obj.drawcanvas.keyPressed(src,evt);
            end
        end
    end
end

function keyReleased(obj,src,evt)
    if obj.selpanel == obj.mainpanel
        if strcmp(evt.Key,'9')
            % do nothing
        elseif strcmp(evt.Key,'0')
            % do nothing
        elseif strcmp(obj.tool,'drawquerysrc')
            if strcmp(evt.Key,'q')
                % do nothing
            elseif strcmp(evt.Key,'l')
                % do nothing
            else
                obj.drawcanvas.keyReleased(src,evt);
            end
        elseif strcmp(obj.tool,'drawquerytgt')
            if strcmp(evt.Key,'q')
                % do nothing
            elseif strcmp(evt.Key,'l')
                % do nothing
            else
                obj.drawcanvas.keyReleased(src,evt);
            end
        end 
    end
end

function selobjectindsChanged(obj)
    if obj.annopolysVisible
        obj.showAnnopolys;
    end
end

function selcomponentindsChanged(obj) %#ok<MANU>
    % do nothing
end

function selqueryrelChanged(obj)
    if obj.querydescriptorsVisible
        obj.showQuerydescriptors;
    end
end

function selibinindChanged(obj)
    if obj.querydescriptorsVisible
        obj.showQuerydescriptors;
    end
end

function selobinindChanged(obj)
    if obj.querydescriptorsVisible
        obj.showQuerydescriptors;
    end
end

function selrelChanged(obj)
    if not(isempty(obj.annotation))
        [mask,srcinds] = ismember(...
            [obj.annotation.relationships.sourceobjs{obj.selrel(1,:)}],...
            obj.annotation.imgobjects);
        obj.selsourceinds = srcinds(mask);
        
        [mask,tgtinds] = ismember(...
            [obj.annotation.relationships.targetobjs{obj.selrel(1,:)}],...
            obj.annotation.imgobjects);
        obj.seltargetinds = tgtinds(mask);
    end

    if obj.annorelsVisible
        obj.showAnnorels;
    end
end

function selsourceindsChanged(obj)
    if obj.examplesVisible
        obj.showExamples;
    end
end

function seltargetindsChanged(obj)
    if obj.examplesVisible
        obj.showExamples;
    end
end

function openedimgindChanged(obj)
    
    if not(isempty(obj.imgbrowser)) && not(isempty(obj.imgbrowser.openedimgind))
        wsimgfilename = cleanfilename(obj.imgbrowser.imgfilenames{obj.imgbrowser.openedimgind});
        [pos] = strfind(wsimgfilename,obj.imgpath);
        if numel(pos) ~= 1
            error('Invalid file name, does not seem to be in the database path.');
        end
        wsimgfilename(pos:pos+numel(obj.imgpath)) = []; % including last separator
        obj.loadWorkingsetEntry(wsimgfilename);
    end

end

function annotationChanged(obj)
    if obj.annopolysVisible
        obj.showAnnopolys;
    end
    if obj.annorelsVisible
        obj.showRelpanel;
        obj.showAnnorels;
    end
end

function classificationEntryChanged(obj)
    obj.clsfrelid = zeros(1,0);
    obj.clsfrellbls = cell(1,0);
    obj.clsfrellblweights = cell(1,0);

    if not(isempty(obj.classification))
        if obj.classificationvalidationVisible
            % show validation results
            if not(isempty(obj.classification.validatelabels))
                imgind = find(strcmp(obj.classification.trainimgfilenames,obj.imgfilename),1,'first');
                if not(isempty(imgind))
                    relmask = obj.classification.trainimginds == imgind;

                    obj.clsfrelid = obj.classification.trainrelids(relmask);
                    [obj.clsfrellbls,obj.clsfrellblweights] = RelationshipClassification.lblmat2lblset(...
                        obj.classification.validatelabels(:,relmask),obj.classification.labelids);
                end
            end
        elseif obj.classificationVisible
            % show test results
            if not(isempty(obj.classification.testlabels))
                imgind = find(strcmp(obj.classification.testimgfilenames,obj.imgfilename),1,'first');
                relmask = obj.classification.testimginds == imgind;

                obj.clsfrelid = obj.classification.testrelids(relmask);
                [obj.clsfrellbls,obj.clsfrellblweights] = RelationshipClassification.lblmat2lblset(...
                    obj.classification.testlabels,obj.classification.labelids);
            end
        end
    end
    
    if obj.annorelsVisible
        obj.showRelpanel;
    end
end

function queryEntryChanged(obj)
    obj.clearQueryinfo;
        
    if not(isempty(obj.query))
        ind = find(strcmp(obj.query.rels.imgfilenames,obj.imgfilename),1,'first');
        
        relinds = find(obj.query.rels.imageind == ind);

        if not(isempty(relinds))
            obj.querydescriptorsim = zeros(size(obj.query.rels.descriptors.val,1),numel(relinds));
            hasdesc = not(isnan(obj.query.rels.descind(relinds)));
            
            obj.queryrels = obj.query.rels.cloneSubset(relinds);
            obj.querylikelihoods = obj.query.likelihoods(relinds);
            [~,obj.querydescriptorsim(:,hasdesc)] = obj.query.template.similarity(...
                obj.query.template.val,...
                obj.query.rels.descriptors.val(:,obj.query.rels.descind(relinds(hasdesc))),...
                obj.query.settings.simmethod);
        end
    end
    if obj.querylikelihoodsVisible
        obj.showQuerylikelihoods;
    end
    if obj.resultsVisible
        obj.showResults;
    end
end

function drawquerysrcPressed(obj)
    if strcmp(obj.tool,'drawquerysrc')
        obj.tool = '';
    else
        obj.tool = 'drawquerysrc';
    end
end

function drawquerytgtPressed(obj)
    if strcmp(obj.tool,'drawquerytgt')
        obj.tool = '';
    else
        obj.tool = 'drawquerytgt';
    end
end

function marksrcPressed(obj)
    if strcmp(obj.tool,'selexamplesource')
        obj.tool = '';
    else
        obj.tool = 'selexamplesource';
    end
end

function marktgtPressed(obj)
    if strcmp(obj.tool,'selexampletargets')
        obj.tool = '';
    else
        obj.tool = 'selexampletargets';
    end
end

function selannoPressed(obj)
    if strcmp(obj.tool,'annoselect')
        obj.tool = '';
    else
        obj.tool = 'annoselect';
    end
end

function seldescPressed(obj)
    if strcmp(obj.tool,'queryrelselect')
        obj.tool = '';
    else
        obj.tool = 'queryrelselect';
    end
end

function drawannoPressed(obj)
    if strcmp(obj.tool,'drawanno')
        obj.tool = '';
    else
        obj.tool = 'drawanno';
    end
end

function toolbuttonPressed(obj)
    if isempty(obj.gtoolbuttons.SelectedObject)
        obj.tool = '';
    else
        obj.tool = obj.gtoolbuttons.SelectedObject.Tag;
    end
end

function toolChanged(obj)
    if strcmp(obj.tool,'queryrelselect')
        if not(obj.imageVisible)
            obj.toggleImageVisible;
        end
        if not(obj.querydescriptorsVisible)
            obj.toggleDescriptorsVisible;
        end
        if not(obj.querylikelihoodsVisible)
            obj.toggleLikelihoodsVisible;
        end
        if obj.annorelsVisible
            obj.toggleAnnorelsVisible;
        end
        if obj.annopolysVisible
            obj.toggleAnnopolysVisible;
        end
        if obj.querydrawingVisible
            obj.toggleQuerydrawingVisible;
        end
        if obj.examplesVisible
            obj.toggleExamplesVisible;
        end
        if obj.resultsVisible
            obj.toggleResultsVisible;
        end
        if obj.editorfig.unitsVisible
            obj.editorfig.toggleDisplayUnits;
        end
        obj.gmenu_seldesc.Checked = 'on';

    else
        obj.gmenu_seldesc.Checked = 'off';
    end

    if strcmp(obj.tool,'annoselect')
        if not(obj.imageVisible)
            obj.toggleImageVisible;
        end
        if not(obj.annopolysVisible)
            obj.toggleAnnopolysVisible;
        end
        if obj.querylikelihoodsVisible
            obj.toggleLikelihoodsVisible;
        end
        if obj.querydescriptorsVisible
            obj.toggleDescriptorsVisible;
        end
        if obj.annorelsVisible
            obj.toggleAnnorelsVisible;
        end
        if obj.querydrawingVisible
            obj.toggleQuerydrawingVisible;
        end
        if obj.examplesVisible
            obj.toggleExamplesVisible;
        end
        if obj.resultsVisible
            obj.toggleResultsVisible;
        end
        if obj.editorfig.unitsVisible
            obj.editorfig.toggleDisplayUnits;
        end
        obj.gmenu_selannno.Checked = 'on';
    else
        obj.gmenu_selannno.Checked = 'off';
    end
    
    if strcmp(obj.tool,'drawanno')
        if not(obj.imageVisible)
            obj.toggleImageVisible;
        end
        if not(obj.annopolysVisible)
            obj.toggleAnnopolysVisible;
        end
        if obj.querylikelihoodsVisible
            obj.toggleLikelihoodsVisible;
        end
        if obj.querydescriptorsVisible
            obj.toggleDescriptorsVisible;
        end
        if obj.annorelsVisible
            obj.toggleAnnorelsVisible;
        end
        if obj.querydrawingVisible
            obj.toggleQuerydrawingVisible;
        end
        if obj.examplesVisible
            obj.toggleExamplesVisible;
        end
        if obj.resultsVisible
            obj.toggleResultsVisible;
        end
        if obj.editorfig.unitsVisible
            obj.editorfig.toggleDisplayUnits;
        end
        obj.gmenu_drawannno.Checked = 'on';
    else
        obj.gmenu_drawannno.Checked = 'off';
    end
    
    if strcmp(obj.tool,'selexamplesource')
        if not(obj.imageVisible)
            obj.toggleImageVisible;
        end
        if not(obj.annopolysVisible)
            obj.toggleAnnopolysVisible;
        end
        if obj.querylikelihoodsVisible
            obj.toggleLikelihoodsVisible;
        end
        if obj.querydescriptorsVisible
            obj.toggleDescriptorsVisible;
        end
        if obj.annorelsVisible
            obj.toggleAnnorelsVisible;
        end
        if obj.querydrawingVisible
            obj.toggleQuerydrawingVisible;
        end
        if not(obj.examplesVisible)
            obj.toggleExamplesVisible;
        end
        if obj.resultsVisible
            obj.toggleResultsVisible;
        end
        if obj.editorfig.unitsVisible
            obj.editorfig.toggleDisplayUnits;
        end
        obj.gmenu_marksrc.Checked = 'on';
        obj.gtoolbuttons.Children(strcmp({obj.gtoolbuttons.Children.Tag},'selexamplesource')).Value = 1;
    else
        obj.gmenu_marksrc.Checked = 'off';
        obj.gtoolbuttons.Children(strcmp({obj.gtoolbuttons.Children.Tag},'selexamplesource')).Value = 0;
    end
    
    if strcmp(obj.tool,'selexampletargets')
        if not(obj.imageVisible)
            obj.toggleImageVisible;
        end
        if not(obj.annopolysVisible)
            obj.toggleAnnopolysVisible;
        end
        if obj.querylikelihoodsVisible
            obj.toggleLikelihoodsVisible;
        end
        if obj.querydescriptorsVisible
            obj.toggleDescriptorsVisible;
        end
        if obj.annorelsVisible
            obj.toggleAnnorelsVisible;
        end
        if obj.querydrawingVisible
            obj.toggleQuerydrawingVisible;
        end
        if not(obj.examplesVisible)
            obj.toggleExamplesVisible;
        end
        if obj.resultsVisible
            obj.toggleResultsVisible;
        end
        if obj.editorfig.unitsVisible
            obj.editorfig.toggleDisplayUnits;
        end
        
        obj.gmenu_marktgt.Checked = 'on';
        obj.gtoolbuttons.Children(strcmp({obj.gtoolbuttons.Children.Tag},'selexampletargets')).Value = 1;
    else
        obj.gmenu_marktgt.Checked = 'off';
        obj.gtoolbuttons.Children(strcmp({obj.gtoolbuttons.Children.Tag},'selexampletargets')).Value = 0;
    end
    
    if strcmp(obj.tool,'drawquerysrc')
        if obj.annorelsVisible
            obj.toggleAnnorelsVisible;
        end
        if obj.imageVisible
            obj.toggleImageVisible;
        end
        if obj.querydescriptorsVisible
            obj.toggleDescriptorsVisible;
        end
        if obj.annorelsVisible
            obj.toggleAnnorelsVisible;
        end
        if obj.annopolysVisible
            obj.toggleAnnopolysVisible;
        end
        if obj.querylikelihoodsVisible
            obj.toggleLikelihoodsVisible;
        end
        if not(obj.querydrawingVisible)
            obj.toggleQuerydrawingVisible;
        end
        if obj.examplesVisible
            obj.toggleExamplesVisible;
        end
        if obj.resultsVisible
            obj.toggleResultsVisible;
        end
        
        if not(isempty(obj.drawcanvas)) && isvalid(obj.drawcanvas)
            obj.drawcanvas.setBrushval([rgbhex2rgbdec('FFBA5F')';1]); % orange
        end
        
        if not(obj.editorfig.unitsVisible)
            obj.editorfig.toggleDisplayUnits;
        end
        
        obj.gmenu_drawquerysrc.Checked = 'on';
        obj.gtoolbuttons.Children(strcmp({obj.gtoolbuttons.Children.Tag},'drawquerysrc')).Value = 1;
    else
        obj.gmenu_drawquerysrc.Checked = 'off';
        obj.gtoolbuttons.Children(strcmp({obj.gtoolbuttons.Children.Tag},'drawquerysrc')).Value = 0;
    end
    
    if strcmp(obj.tool,'drawquerytgt')
        if obj.annorelsVisible
            obj.toggleAnnorelsVisible;
        end
        if obj.imageVisible
            obj.toggleImageVisible;
        end
        if obj.querydescriptorsVisible
            obj.toggleDescriptorsVisible;
        end
        if obj.annorelsVisible
            obj.toggleAnnorelsVisible;
        end
        if obj.annopolysVisible
            obj.toggleAnnopolysVisible;
        end
        if obj.querylikelihoodsVisible
            obj.toggleLikelihoodsVisible;
        end
        if not(obj.querydrawingVisible)
            obj.toggleQuerydrawingVisible;
        end
        if obj.examplesVisible
            obj.toggleExamplesVisible;
        end
        if obj.resultsVisible
            obj.toggleResultsVisible;
        end
        
        if not(obj.editorfig.unitsVisible)
            obj.editorfig.toggleDisplayUnits;
        end
        
        if not(isempty(obj.drawcanvas)) && isvalid(obj.drawcanvas)
            obj.drawcanvas.setBrushval([rgbhex2rgbdec('92B1FF')';1]); % blue
        end
        obj.gmenu_drawquerytgt.Checked = 'on';
        obj.gtoolbuttons.Children(strcmp({obj.gtoolbuttons.Children.Tag},'drawquerytgt')).Value = 1;
    else
        obj.gmenu_drawquerytgt.Checked = 'off';
        obj.gtoolbuttons.Children(strcmp({obj.gtoolbuttons.Children.Tag},'drawquerytgt')).Value = 0;
    end
    
    if not(isempty(obj.tool))
        obj.navwidget.navmode = false;
    end
end

function rellistChanged(obj)
    obj.selrel = obj.grellist.UserData(obj.grellist.Value,:)';
end

function createquerybuttonPressed(obj)
    
    querysentence = obj.gqueryfield.String;
    if not(isempty(querysentence))
        q = obj.createQuery(querysentence,'querymode','single');
    elseif not(isempty(obj.selsourceinds) || isempty(obj.seltargetinds))
        q = obj.createQuery(...
            obj.annotation.imgobjects(obj.selsourceinds),...
            obj.annotation.imgobjects(obj.seltargetinds),...
            'querymode','single');
    else
        warndlg('Write a query sentence or select a query source and target region first.','Define Query First','modal');
        return;
    end
    
    obj.tool = '';

    obj.setQuery(q);
end

function computedescriptorsbuttonPressed(obj)
    
    if not(isempty(obj.wsrels))
        answer = questdlg('Re-compute and replace descriptors in images that already have descriptors, or only compute descriptors in images that still don''t have descriptors?','Replace Descriptors?','Replace','Expand','Cancel','Replace');
        if strcmp(answer,'Replace')
            overwrite = true;
        elseif strcmp(answer,'Expand')
            overwrite = false;
            % do nothing
        else
            return;
        end
    else
        overwrite = true;
    end
    
    desctemplate = obj.descriptorTemplate;
    
    wsimgfilenames = obj.dbinfo.imgfilenames(obj.wsimginds);
    [obj.dbrels,~,errorimages] = computeDescriptors(obj.dbrels,wsimgfilenames,desctemplate,obj.imgpath,obj.annopath,'overwrite',overwrite);
    obj.descriptorsfilename = '';
    
    if not(isempty(errorimages))
        disp('******************');
        disp('There were errors in images:');
        disp(errorimages);
        disp('******************');
    end
end

function computeandsavedescriptorsbuttonPressed(obj)
    
    if not(isempty(obj.lastcomputedescsavename))
        savename = obj.lastcomputedescsavename;
    else
        savename = '../results/descriptors/tempdescriptors.zip';
    end

    [fname,pathname,~] = ...
        uiputfile({'*.zip','ZIP (*.zip)';'*.mat','MAT (*.mat)'},'Save Descriptors',savename);

    if not(isnumeric(fname) && fname == 0)
        filename = [pathname,fname];
    else
        return;
    end
    
    obj.lastcomputedescsavename = filename;
        
    desctemplate = obj.descriptorTemplate;
    
    wsimgfilenames = obj.dbinfo.imgfilenames(obj.wsimginds);
    [desc,~,errorimages] = computeDescriptors(DatabaseRelationshipSet,wsimgfilenames,desctemplate,obj.imgpath,obj.annopath);
    
    obj.saveDescriptors(filename,desc);
    
    if not(isempty(errorimages))
        disp('******************');
        disp('There were errors in images:');
        disp(errorimages);
        disp('******************');
    end
end

function querybuttonPressed(obj,mode)
    if nargin < 2 || isempty(mode)
        mode = 'single';
    end
    
    srcclr = rgbhex2rgbdec('ffba5f');
    srcmask = ...
        obj.drawcanvas.img(:,:,1) == srcclr(1) & ...
        obj.drawcanvas.img(:,:,2) == srcclr(2) & ...
        obj.drawcanvas.img(:,:,3) == srcclr(3) & ...
        obj.drawcanvas.img(:,:,4) > 0.5;
    
    tgtclr = rgbhex2rgbdec('92b1ff');
    tgtmask = ...
        obj.drawcanvas.img(:,:,1) == tgtclr(1) & ...
        obj.drawcanvas.img(:,:,2) == tgtclr(2) & ...
        obj.drawcanvas.img(:,:,3) == tgtclr(3) & ...
        obj.drawcanvas.img(:,:,4) > 0.5;
    
    hassketch = false;
    hasexample = false;
    
    if any(srcmask(:)) && any(tgtmask(:))
        hassketch = true;
    end
    
    if not(isempty(obj.selsourceinds)) && not(isempty(obj.seltargetinds))
        hasexample = true;
    end
    
    if hassketch
        if hasexample
            warndlg('Both sketch and example are available, querying with the sketch.','Sketch and Example Available','modal');
            drawnow;
        end
        obj.startQueryWithDrawing(mode);
    elseif hasexample
        obj.startQueryWithExample(mode);
    else
        warndlg('Please sketch two regions or select two existing regions.','Neither Sketch nor Example Available','modal');
    end

end

function clearquerybuttonPressed(obj)
    obj.clearQuery;
end

function trainbuttonPressed(obj)
    
    if isempty(obj.trainingsetfilename)
        trainimgfilenames = obj.dbinfo.imgfilenames(obj.wsimginds);
    else
        f = fopen(obj.trainingsetfilename);             
        trainimgfilenames = textscan(f,'%s','delimiter','\n');
        fclose(f);
        trainimgfilenames = trainimgfilenames{1};
%         trainimgfilenames = strcat([obj.imgpath,'/'],trainimgfilenames);
    end
    
    c = obj.trainClassification(trainimgfilenames);
    obj.setClassification(c);
    
    disp('done');
end

function validatebuttonPressed(obj)
    if not(isempty(obj.classification))
        disp('validating ...');
        obj.classification.validate;
        disp('done');
        obj.classificationEntryChanged;
    end
end

function classifybuttonPressed(obj)
    if not(isempty(obj.classification))
        disp('predicting ...');
        obj.classification.predict(...
            [],[],obj.imgbrowser.imgfilenames,obj.imgpath,obj.annopath);
        disp('done');
        obj.classificationEntryChanged;
    end
end

function classifyimgbuttonPressed(obj)
    if not(isempty(obj.classification))
        disp('predicting ...');
        obj.classification.predict(...
            [],[],obj.imgbrowser.imgfilenames,obj.imgpath,obj.annopath);
        disp('done');
        obj.classificationEntryChanged;
    end
end

function clearclassbuttonPressed(obj)
    obj.clearClassification;
end

function addrelbuttonPressed(obj)
    if not(isempty(obj.annotation)) && not(isempty(obj.selsourceinds)) && ...
            not(isempty(obj.seltargetinds))
        % show relationship type dialog
        [lbl,weight] = relcategorydlg('Pick a Relationship Category:','',obj.dbinfo.relcategories);
        if isempty(lbl)
            return;
        end
        
        lblind = find(strcmp(obj.dbinfo.relcategories,lbl),1,'first');
        
        if isempty(lblind)
            btn = questdlg(['Category ''',lbl,''' does not exist, do you want to create it?'],...
                'Create New Category?','Yes','No','No');
            if strcmp(btn,'Yes')
                obj.dbinfo.relcategories{end+1,:} = lbl;
                ImageDatabaseInfoExporter.exportRelcategories(...
                    obj.dbinfo.relcategories,obj.dbinfofilenames.relcategories)
                lblind = numel(obj.dbinfo.relcategories);
            end
        end
        
        if not(isempty(lblind))
            % add relation between marked subjects and objects
            srcobjs = obj.annotation.imgobjects(obj.selsourceinds);
            tgtobjs = obj.annotation.imgobjects(obj.seltargetinds);

            ind = obj.annotation.relationships.findRelationships(...
                {srcobjs},{tgtobjs},'exact');
            if ind==0
                id = missingint(obj.annotation.relationships.id);
                obj.annotation.relationships.addRelationships(id,srcobjs,tgtobjs,lblind,weight);
                newselrel = [size(obj.annotation.relationships.id,2);-1];
            else
                obj.annotation.relationships.addLabels(ind,lblind,weight);
                newselrel = [ind;numel(obj.annotation.relationships.labels{ind})];
            end
            obj.wsentrydirty = true;
            
            obj.selrel = newselrel;
            
            obj.selsourceinds = zeros(1,0);
            obj.seltargetinds = zeros(1,0);

            obj.annotationChanged;
        end
    end
end

function removerelbuttonPressed(obj)
    % remove selected relationships
    if not(isempty(obj.annotation))
        if not(isempty(obj.selrel))
            relind = unique(obj.selrel(1,:));
            removerelinds = zeros(1,0);
            for i=1:numel(relind)
                lblinds = obj.selrel(2,obj.selrel(1,:)==relind(i));
                if any(lblinds < 0)
                    % whole relationship selected
                    removerelinds(end+1) = relind(i); %#ok<AGROW>
                else
                    obj.annotation.relationships.removeLabels(relind(i),lblinds);
%                     if isempty(obj.annotation.relationships.labels{relind(i)})
%                         removerelinds(end+1) = relind(i); %#ok<AGROW>
%                     end
                end
            end
            obj.annotation.relationships.removeRelationships(removerelinds);

%             obj.annotation.removeRelationships(obj.selrel);
            obj.wsentrydirty = true;
            
            obj.selrel = zeros(2,0);

            obj.annotationChanged;
        end
    end
end

function scrollToPressed(obj)
    if not(isempty(obj.imgbrowser)) && not(isempty(obj.imgbrowser.imgfilenames))
        scrollpos = inputdlg(...
            ['Scroll to image number (1 - ',num2str(numel(obj.imgbrowser.imgfilenames)),'):'],...
            'Scroll To');
        if not(isempty(scrollpos))
            scrollpos = str2double(scrollpos{1});
            if scrollpos < 1
                errordlg('Invalid image number.','Invalid Image Number','modal');
                return;
            end
            obj.imgbrowser.scrollto(scrollpos);
        end
    end
end

function imgbrowserpanelResized(obj)
    if obj.isvisible && obj.imgbrowserpanelVisible
        obj.showImgbrowserpanel;
    end
end

function toggleImageVisible(obj)
    obj.imageVisible = not(obj.imageVisible);
    if obj.imageVisible
        set(obj.gmenu_image,'Checked','on');
        obj.imgcanvas.show;
    else
        set(obj.gmenu_image,'Checked','off');
        obj.imgcanvas.hide;
    end    
end

function toggleAnnopolysVisible(obj)
    obj.annopolysVisible = not(obj.annopolysVisible);
    if obj.annopolysVisible
        set(obj.gmenu_annopolys,'Checked','on');
        obj.showAnnopolys;
    else
        set(obj.gmenu_annopolys,'Checked','off');
        obj.hideAnnopolys;
    end
end

function toggleAnnorelsVisible(obj)
    obj.annorelsVisible = not(obj.annorelsVisible);
    if obj.annorelsVisible
        set(obj.gmenu_annorels,'Checked','on');
        obj.showRelpanel;
        obj.showAnnorels;
    else
        set(obj.gmenu_annorels,'Checked','off');
        obj.hideRelpanel;
        obj.hideAnnorels;
    end
end

function toggleDescriptorsVisible(obj)
    obj.querydescriptorsVisible = not(obj.querydescriptorsVisible);
    if obj.querydescriptorsVisible
        set(obj.gmenu_descriptors,'Checked','on');
        obj.showQuerydescriptors;
    else
        set(obj.gmenu_descriptors,'Checked','off');
        obj.hideQuerydescriptors;
    end
end

function toggleInnerdescriptorVisible(obj)
    obj.innerdescriptorVisible = not(obj.innerdescriptorVisible);
    if obj.innerdescriptorVisible
        if obj.outerdescriptorVisible
            obj.toggleOuterdescriptorVisible;
        end
        set(obj.gmenu_innerdescriptor,'Checked','on');
    else
        set(obj.gmenu_innerdescriptor,'Checked','off');
    end
    if obj.querydescriptorsVisible
        obj.showQuerydescriptors;
    end
end

function toggleOuterdescriptorVisible(obj)
    obj.outerdescriptorVisible = not(obj.outerdescriptorVisible);
    if obj.outerdescriptorVisible
        if obj.innerdescriptorVisible
            obj.toggleInnerdescriptorVisible;
        end
        set(obj.gmenu_outerdescriptor,'Checked','on');
    else
        set(obj.gmenu_outerdescriptor,'Checked','off');
    end
    if obj.querydescriptorsVisible
        obj.showQuerydescriptors;
    end
end

function toggleQuerytemplateVisible(obj)
    obj.querytemplateVisible = not(obj.querytemplateVisible);
    if obj.querytemplateVisible
        set(obj.gmenu_querytemplate,'Checked','on');
    else
        set(obj.gmenu_querytemplate,'Checked','off');
    end
    if obj.querydescriptorsVisible
        obj.showQuerydescriptors;
    end    
end

function toggleDescbinsimVisible(obj)
    obj.querydescbinsimVisible = not(obj.querydescbinsimVisible);
    if obj.querydescbinsimVisible
        set(obj.gmenu_descbinsim,'Checked','on');
    else
        set(obj.gmenu_descbinsim,'Checked','off');
    end
    if obj.querydescriptorsVisible
        obj.showQuerydescriptors;
    end    
end

function toggleLikelihoodsVisible(obj)
    obj.querylikelihoodsVisible = not(obj.querylikelihoodsVisible);
    if obj.querylikelihoodsVisible
        set(obj.gmenu_likelihoods,'Checked','on');
        obj.showQuerylikelihoods;
    else
        set(obj.gmenu_likelihoods,'Checked','off');
        obj.hideQuerylikelihoods;
    end
end

function toggleExamplesVisible(obj)
    obj.examplesVisible = not(obj.examplesVisible);
    if obj.examplesVisible
        set(obj.gmenu_examples,'Checked','on');
        obj.showExamples;
    else
        set(obj.gmenu_examples,'Checked','off');
        obj.hideExamples;
    end
end

function toggleResultsVisible(obj)
    obj.resultsVisible = not(obj.resultsVisible);
    if obj.resultsVisible
        obj.tool = '';
        
        if obj.annopolysVisible
            obj.toggleAnnopolysVisible;
        end
        if obj.annorelsVisible
            obj.toggleAnnorelsVisible;
        end
        if obj.querydescriptorsVisible
            obj.toggleDescriptorsVisible;
        end
        if obj.querylikelihoodsVisible
            obj.toggleLikelihoodsVisible;
        end
        if obj.imageVisible
            obj.toggleImageVisible;
        end
        if obj.examplesVisible
            obj.toggleExamplesVisible;
        end
        
        set(obj.gmenu_results,'Checked','on');
        obj.showResults;
    else
        set(obj.gmenu_results,'Checked','off');
        obj.hideResults;
    end
end

function toggleQuerydrawingVisible(obj)
    obj.querydrawingVisible = not(obj.querydrawingVisible);
    if obj.querydrawingVisible
        set(obj.gmenu_querydrawing,'Checked','on');
        obj.showQuerydrawing;
    else
        set(obj.gmenu_querydrawing,'Checked','off');
        obj.hideQuerydrawing;
    end
end

function layoutPanels(obj)
    if isgraphics(obj.toolpanel) && isgraphics(obj.mainpanel) && ...
            isgraphics(obj.imgbrowserpanel) && isgraphics(obj.relpanel)
        imgbrowserpanelpos = obj.imgbrowserpanel.Position;
        toolpanelpos = obj.toolpanel.Position;
        mainpanelpos = obj.mainpanel.Position;

        if obj.imgbrowserpanelVisible
            imgbrowserpanelpos(2) = (1-toolpanelpos(4)-0.3);
            imgbrowserpanelpos(4) = 0.3;
            
            mainpanelpos(4) = max(1-(toolpanelpos(4)+imgbrowserpanelpos(4)),0.0001);
            relpanelpos(4) = max(1-(toolpanelpos(4)+imgbrowserpanelpos(4)),0.0001);
        else
            imgbrowserpanelpos(2) = (1-toolpanelpos(4));
            imgbrowserpanelpos(4) = 0;
            
            mainpanelpos(4) = max(1-toolpanelpos(4),0.0001);
            relpanelpos(4) = max(1-toolpanelpos(4),0.0001);
        end
        
        if obj.annorelsVisible
            relpanelpos(1) = 0.8;
            relpanelpos(3) = 0.2;
            
            mainpanelpos(3) = max(1-relpanelpos(3),0.0001);
        else
            relpanelpos(1) = 1;
            relpanelpos(3) = 0;
            
            mainpanelpos(3) = 1;
        end
        
        if any(obj.imgbrowserpanel.Position ~= imgbrowserpanelpos)
            obj.imgbrowserpanel.Position = imgbrowserpanelpos;
        end
        if any(obj.mainpanel.Position ~= mainpanelpos)
            obj.mainpanel.Position = mainpanelpos;
        end
        if any(obj.relpanel.Position ~= relpanelpos)
            obj.relpanel.Position = relpanelpos;
        end
    end
end

function showImgbrowserpanel(obj)
    if isgraphics(obj.imgbrowserpanel)
        if not(strcmp(obj.imgbrowserpanel.Visible,'on'))
            obj.imgbrowserpanel.Visible = 'on';
            obj.layoutPanels;
        end
    end
    
    if isvalid(obj.imgbrowser)
        obj.imgbrowser.show;
    end
end

function hideImgbrowserpanel(obj)
    if isvalid(obj.imgbrowser)
        obj.imgbrowser.hide;
    end

    if isgraphics(obj.imgbrowserpanel)
        if not(strcmp(obj.imgbrowserpanel.Visible,'off'))
            obj.imgbrowserpanel.Visible = 'off';
            obj.layoutPanels;
        end
    end
end

function showRelpanel(obj)
    if isgraphics(obj.relpanel)
        if isvalid(obj.annotation)
            relstr = cell(0,1);
            relinds = zeros(0,2);
            for i=1:size(obj.annotation.relationships.id,2)
                
                crellbls = zeros(1,0);
                crellblweights = zeros(1,0);
                if not(isempty(obj.clsfrelid))
                    crelinds = find(obj.clsfrelid == obj.annotation.relationships.id(i));
                    if not(isempty(crelinds))
                        crellbls = obj.clsfrellbls{crelinds};
                        crellblweights = obj.clsfrellblweights{crelinds};
                    end
                end
                
                srcobjs = obj.annotation.relationships.sourceobjs{i};
                tgtobjs = obj.annotation.relationships.targetobjs{i};
                
                srclbls = [srcobjs.label];
                tgtlbls = [tgtobjs.label];
                
                if not(all(srclbls == srclbls(1))) || not(all(tgtlbls == tgtlbls(1)))
                    relstr{end+1} = 'INVALID RELATIONSHIP!'; %#ok<AGROW>
                    relinds(end+1,:) = [-1,-1]; %#ok<AGROW>
                else
                    srcidstr = sprintf('%d,',[srcobjs.id]);
                    srcidstr(end) = [];
                    tgtidstr = sprintf('%d,',[tgtobjs.id]);
                    tgtidstr(end) = [];
                    
                    relstr{end+1,:} = [...
                        obj.dbinfo.categories{srclbls(1)},'(',srcidstr,')',...
                        ' -> ',...
                        obj.dbinfo.categories{tgtlbls(1)},'(',tgtidstr,')']; %#ok<AGROW>
                    relinds(end+1,:) = [i,-1]; %#ok<AGROW>
                    
                    for j=1:numel(obj.annotation.relationships.labels{i})
                        
                        reltypestr = obj.dbinfo.relcategories{obj.annotation.relationships.labels{i}(j)};

                        relstr{end+1,:} = [...
                            '   ',reltypestr,' ',...
                            num2str(obj.annotation.relationships.labelweights{i}(j))]; %#ok<AGROW>
                        relinds(end+1,:) = [i,j]; %#ok<AGROW>
                        
                        if not(isempty(obj.clsfrelid))
                            crellblind = find(obj.annotation.relationships.labels{i}(j) == crellbls);
                            if not(isempty(crellblind))
                                relstr{end} = [relstr{end},...
                                    ' (',num2str(crellblweights(crellblind)),')'];
                                crellbls(crellblind) = [];
                                crellblweights(crellblind) = [];
                            else
                                relstr{end} = [relstr{end},' (0)'];
                            end
                        end
                    end
                    
                    if not(isempty(obj.clsfrelid))
                        for j=1:numel(crellbls)
                            reltypestr = obj.dbinfo.relcategories{crellbls(j)};
                            relstr{end+1,:} = [...
                                '   ',reltypestr,'    ',...
                                ' (',num2str(crellblweights(j)),')']; %#ok<AGROW>
                            relinds(end+1,:) = [i,-1]; %#ok<AGROW> 
                        end
                    end
                end
            end
            
            if numel(obj.grellist.String) ~= numel(relstr) || ...
                    (not(isempty(obj.grellist.String)) && not(all(strcmp(obj.grellist.String,relstr))))
                obj.grellist.String = relstr;
                obj.grellist.UserData = relinds;
            end
            
            if isempty(obj.selrel) && isempty(obj.grellist.Value)
                % do nothing
            elseif isempty(obj.selrel) && not(isempty(obj.grellist.Value))
                obj.grellist.Value = [];
            elseif isempty(obj.grellist.Value) ~= isempty(obj.selrel) || ...
                    any(any(obj.grellist.UserData(obj.grellist.Value,:) ~= obj.selrel',2))
                
                [~,obj.grellist.Value] = ismember(obj.selrel',obj.grellist.UserData,'rows');
                obj.grellist.Value(obj.grellist.Value == 0) = [];
            end
        else
            if not(isempty(obj.grellist.String))
                obj.grellist.String = {};
                obj.grellist.UserData = zeros(0,2);
            end
            if not(isempty(obj.grellist.Value))
                obj.grellist.Value = [];
            end
        end
        if not(strcmp(obj.relpanel.Visible,'on'))
            obj.relpanel.Visible = 'on';
            obj.layoutPanels;
        end
    end
end

function hideRelpanel(obj)
    if isgraphics(obj.relpanel)
        if not(strcmp(obj.relpanel.Visible,'off'))
            obj.relpanel.Visible = 'off';
            obj.layoutPanels;
        end
    end
end

function updateMainaxes(obj)
    
    if not(isempty(obj.imgcanvas)) && not(isempty(obj.imgcanvas.img))

        fpadding = [0;0;0.05];
        
        bbmin = [obj.imgcanvas.imgmin;0];
        bbmax = [obj.imgcanvas.imgmax;0];
        bbdiag = sqrt(sum((bbmax-bbmin).^2,1));
        bbmax = bbmax + bbdiag .* fpadding;
        bbmin = bbmin - bbdiag .* fpadding;
        
        set(obj.axes,'XLim',[bbmin(1),bbmax(1)]);
        set(obj.axes,'YLim',[bbmin(2),bbmax(2)]);
        set(obj.axes,'ZLim',[bbmin(3),bbmax(3)]);
        
    end
end

function showAxescontents(obj)
    if obj.annopolysVisible
        obj.showAnnopolys
    end
    
    if obj.annorelsVisible
        obj.showAnnorels;
    end
    
    if obj.querydescriptorsVisible
        obj.showQuerydescriptors;
    end
    
    if obj.examplesVisible
        obj.showExamples;
    end
    
    if obj.querydrawingVisible
        obj.showQuerydrawing;
    end
    
    obj.showEditpoly;
end

function hideAxescontents(obj)
    obj.hideAnnopolys;
    obj.hideQuerydescriptors;
    obj.hideExamples;
    obj.hideEditpoly;
    obj.hideQuerydrawing;
end

function showAnnopolys(obj)
    if not(isempty(obj.annotation)) && not(isempty(obj.imgcanvas)) && not(isempty(obj.annotation.imgobjects))
        % get annotation polygons and convert from [0,1] to [imgmin,imgmax]        
        polys = [obj.annotation.imgobjects.polygon];
        polys = ImageAnnotation.an2imcoords(polys,obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);
        for i=1:numel(polys)
            polys{i}(3,:) = 0.01 + i.*0.01;
        end
        
        % get colors for each label in the annotation
        polylbls = cell(1,numel(obj.annotation.imgobjects));
        objpolyinds = cell(1,numel(obj.annotation.imgobjects));
        c = 0;
        for i=1:numel(obj.annotation.imgobjects)
            nobjpolys = numel(obj.annotation.imgobjects(i).polygon);
            polylbls{i} = ones(1,nobjpolys).*obj.annotation.imgobjects(i).label;
            objpolyinds{i} = c+1:c+nobjpolys;
            c = c + nobjpolys;
        end
        polylbls = [polylbls{:}];
        polyedgewidth = ones(1,numel(polylbls));
        polyedgewidth([objpolyinds{obj.selobjectinds}]) = 3;
        catcolors = [[0,0,0];obj.dbinfo.categorycolors]; % black for no label
        polycolors = catcolors(polylbls+1,:)';
        
        obj.gannopolys = showFaces(...
            polys,obj.gannopolys,obj.axes,...
            'facecolor',polycolors,...
            'facealpha',0.5,...
            'edgecolor',polycolors,...
            'edgewidth',polyedgewidth,...
            'stackz',(1:numel(polys)));
    else
        obj.hideAnnopolys;
    end
end

function hideAnnopolys(obj)
    delete(obj.gannopolys(isgraphics(obj.gannopolys)));
    obj.gannopolys = gobjects(1,0);
end

function showResults(obj)
    if not(isempty(obj.annotation)) && not(isempty(obj.imgcanvas)) && ...
        not(isempty(obj.querylikelihoods)) && not(isempty(obj.queryrels))
        
        % select the best relationship pair
        if not(isempty(obj.querylikelihoods))
            [~,relind] = max(obj.querylikelihoods);
            
            imgobjids = [obj.annotation.imgobjects.id];
            srcinds = find(ismember(imgobjids,obj.queryrels.sourceids{relind}));
            tgtinds = find(ismember(imgobjids,obj.queryrels.targetids{relind}));
        end
        
        % get annotation polygons and convert from [0,1] to [imgmin,imgmax]        
        if isempty(srcinds)
            srcpolys = cell(1,0);
        else
            srcpolys = [obj.annotation.imgobjects(srcinds).polygon];
            srcpolys = ImageAnnotation.an2imcoords(srcpolys,obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);
        end

        % get annotation polygons and convert from [0,1] to [imgmin,imgmax]        
        if isempty(tgtinds)
            tgtpolys = cell(1,0);
        else
            tgtpolys = [obj.annotation.imgobjects(tgtinds).polygon];
            tgtpolys = ImageAnnotation.an2imcoords(tgtpolys,obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);            
        end

        
%         % do not clip and crop
%         img = obj.imgcanvas.img;
%         imgmin = obj.imgcanvas.imgmin;
%         imgmax = obj.imgcanvas.imgmax;
        
        % clip anything beyond extent of descriptor
        suma = 0;
        c = zeros(2,1);
        for j=1:numel(srcpolys)
            [cx,cy,a] = polygonCentroid(srcpolys{j}(1,:),srcpolys{j}(2,:));
            c = c + [cx;cy] .* abs(a);
            suma = suma + a;
        end
        c = c ./ suma;
        
        allpoints = [srcpolys{:}];
        sqdists = ...
            bsxfun(@minus,allpoints(1,:)',allpoints(1,:)).^2 + ...
            bsxfun(@minus,allpoints(2,:)',allpoints(2,:)).^2;
        r = sqrt(max(sqdists(:))); % = * 0.5 (oradf) * 2 (+ inner rad at fathest point)
        descimgmin = c-r;
        descimgmax = c+r;
        
        imgmin = max(descimgmin,obj.imgcanvas.imgmin);
        imgmax = min(descimgmax,obj.imgcanvas.imgmax);
        
        % clip polygons on rectangle
        if not(isempty(tgtpolys))
            rect = [...
                imgmin(1),imgmin(1),imgmax(1),imgmax(1);...
                imgmin(2),imgmax(2),imgmax(2),imgmin(2)];
            tgtpolysx = cell(1,numel(tgtpolys));
            tgtpolysy = cell(1,numel(tgtpolys));
            for i=1:numel(tgtpolys)
                tgtpolysx{i} = tgtpolys{i}(1,:);
                tgtpolysy{i} = tgtpolys{i}(2,:);
            end
            [tgtpolysx,tgtpolysy] = polybool('intersection',tgtpolysx,tgtpolysy,rect(1,:),rect(2,:));
            tgtpolys = cell(1,numel(tgtpolysx));
            for i=1:numel(tgtpolysx)
                tgtpolys{i} = [tgtpolysx{i};tgtpolysy{i}];
            end
        end
        
        % crop image
        imgres = [size(obj.imgcanvas.img,2);size(obj.imgcanvas.img,1)];
        imgsize = obj.imgcanvas.imgmax-obj.imgcanvas.imgmin;
        imgmin_canvaspix = round(((imgmin-obj.imgcanvas.imgmin)./imgsize) .* (imgres-1) + 1);
        imgmax_canvaspix = round(((imgmax-obj.imgcanvas.imgmin)./imgsize) .* (imgres-1) + 1);
        img = obj.imgcanvas.img(...
            imgmin_canvaspix(2):imgmax_canvaspix(2),...
            imgmin_canvaspix(1):imgmax_canvaspix(1),:);
        
        % move so center is at 0
        translate = -(imgmin+imgmax)./2;
        imgmax = imgmax + translate;
        imgmin = imgmin + translate;
        srcpolys = cellfun(@(x) bsxfun(@plus,x,translate),srcpolys,'UniformOutput',false);
        tgtpolys = cellfun(@(x) bsxfun(@plus,x,translate),tgtpolys,'UniformOutput',false);
        
        % scale so height = 1
        scale = 1/(imgmax(2)-imgmin(2));
        imgmax = imgmax .* scale;
        imgmin = imgmin .* scale;
        srcpolys = cellfun(@(x) x .* scale,srcpolys,'UniformOutput',false);
        tgtpolys = cellfun(@(x) x .* scale,tgtpolys,'UniformOutput',false);
        
        
        for i=1:numel(srcpolys)
            srcpolys{i}(3,:) = 0.02;
        end
        for i=1:numel(tgtpolys)
            tgtpolys{i}(3,:) = 0.01;
        end
        
        obj.gresultsrcregions = showFaces(...
            srcpolys,obj.gresultsrcregions,obj.axes,...
            'facecolor',[1.0000    0.7294    0.3725]',...
            'edgecolor','none',...
            'facealpha',0.8);%1
        
        obj.gresulttgtregions = showFaces(...
            tgtpolys,obj.gresulttgtregions,obj.axes,...
            'facecolor',[0.5725    0.6941    1.0000]',...
            'edgecolor','none',...
            'facealpha',0.8);%1
        
        obj.gresultimg = showImages(img,imgmin,imgmax,obj.gresultimg,obj.axes);
        
        rect = [...
            imgmin(1),imgmin(1),imgmax(1),imgmax(1);...
            imgmin(2),imgmax(2),imgmax(2),imgmin(2)];
        rect(3,:) = 0.03;
        obj.gresultrect = showPolylinesOld(...
            rect(:,[1:end,1]),obj.gresultrect,obj.axes,0,0,[0.5;0.5;0.5]);
    else
        obj.hideResults;
    end
end

function hideResults(obj)
    delete(obj.gresultsrcregions(isvalid(obj.gresultsrcregions)));
    obj.gresultsrcregions = gobjects(1,0);
    delete(obj.gresulttgtregions(isvalid(obj.gresulttgtregions)));
    obj.gresulttgtregions = gobjects(1,0);
    delete(obj.gresultimg(isvalid(obj.gresultimg)));
    obj.gresultimg = gobjects(1,0);
    delete(obj.gresultrect(isvalid(obj.gresultrect)));
    obj.gresultrect = gobjects(1,0);
end

function showExamples(obj)
    if not(isempty(obj.annotation)) && not(isempty(obj.imgcanvas))
        
        nannopolys = 0;
        for i=1:numel(obj.annotation.imgobjects)
            nannopolys = nannopolys + numel(obj.annotation.imgobjects(i).polygon);
        end
        
        % get annotation polygons and convert from [0,1] to [imgmin,imgmax]        
        subjpolys = [obj.annotation.imgobjects(obj.selsourceinds).polygon];
        subjpolys = ImageAnnotation.an2imcoords(subjpolys,obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);
        for i=1:numel(subjpolys)
            subjpolys{i}(3,:) = (nannopolys+1)*0.01 + 0.1;
        end
        
        obj.gexamplesubjects = showFaces(...
            subjpolys,obj.gexamplesubjects,obj.axes,...
            'facecolor','none',...
            'edgecolor',rgbhex2rgbdec('ffba5f')',... % orange
            'edgestyle',':',...
            'edgewidth',5);
        
        % get annotation polygons and convert from [0,1] to [imgmin,imgmax]        
        objpolys = [obj.annotation.imgobjects(obj.seltargetinds).polygon];
        objpolys = ImageAnnotation.an2imcoords(objpolys,obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);
        for i=1:numel(objpolys)
            objpolys{i}(3,:) = (nannopolys+1)*0.01 + 0.1;
        end
        
        obj.gexampleobjects = showFaces(...
            objpolys,obj.gexampleobjects,obj.axes,...
            'facecolor','none',...
            'edgecolor',rgbhex2rgbdec('92b1ff')',... % blue
            'edgestyle',':',...
            'edgewidth',5);
    else
        obj.hideExamples;
    end
end

function hideExamples(obj)
    delete(obj.gexamplesubjects(isvalid(obj.gexamplesubjects)));
    obj.gexamplesubjects = gobjects(1,0);
    delete(obj.gexampleobjects(isvalid(obj.gexampleobjects)));
    obj.gexampleobjects = gobjects(1,0);
end

function showAnnorels(obj)
    if not(isempty(obj.annotation)) && not(isempty(obj.annotation.relationships.id)) && not(isempty(obj.selrel))
        
%         sourceids = [obj.annotation.relationships.sourceids{obj.selrel}];
%         targetids = [obj.annotation.relationships.targetids{obj.selrel}];
%         
%         imgobjids = [obj.annotation.imgobjects.id]; 
%         
%         [~,sourceinds] = ismember(sourceids,imgobjids);
%         sourceinds(sourceinds==0) = []; % remove indices that were not found
%         [~,targetinds] = ismember(targetids,imgobjids);
%         targetinds(targetinds==0) = []; % remove indices that were not found
%         
%         sourcepolys = [obj.annotation.imgobjects(sourceinds).polygon];
%         sourcepolys = ImageAnnotation.an2imcoords(sourcepolys,obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);
%         for i=1:numel(sourcepolys)
%             sourcepolys{i}(3,:) = 0.01 + 10;
%         end
%         
%         obj.gannorelsource = showFaces(...
%             sourcepolys,obj.gannorelsource,obj.axes,...
%             'facecolor','none',...
%             'edgecolor','red',...
%             'edgestyle',':',...
%             'edgewidth',2);
%         
%         % get annotation polygons and convert from [0,1] to [imgmin,imgmax]        
%         targetpolys = [obj.annotation.imgobjects(targetinds).polygon];
%         targetpolys = ImageAnnotation.an2imcoords(targetpolys,obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);
%         for i=1:numel(targetpolys)
%             targetpolys{i}(3,:) = 0.01 + 10;
%         end
%         
%         obj.gannoreltarget = showFaces(...
%             targetpolys,obj.gannoreltarget,obj.axes,...
%             'facecolor','none',...
%             'edgecolor','green',...
%             'edgestyle',':',...
%             'edgewidth',2);
    else
        obj.hideAnnorels;
    end
end

function hideAnnorels(obj)
    delete(obj.gannorelsource(isvalid(obj.gannorelsource)));
    obj.gannorelsource = gobjects(1,0);
    delete(obj.gannoreltarget(isvalid(obj.gannoreltarget)));
    obj.gannoreltarget = gobjects(1,0);
end

function [iangularpos,iradialpos,oangularpos,oradialpos,...
          ibincenters,obincenters,descirad,descorad,desccenter] = ...
        getReldisthistDescriptorGeometry(obj,descind)
    
    if isa(obj.queryrels.descriptors,'ReldisthistDescriptorSet')
                
%         % find center of region
%         regionind = find(...
%             [obj.annotation.imgobjects.id] == ...
%             obj.querylikelihoods(1,descind),...
%             1,'first');
%         region = obj.annotation.imgobjects(regionind);
%         regionpolys = ImageAnnotation.an2imcoords(region.polygon,...
%             obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);
%         sumarea = 0;
%         desccenter = [0;0];
%         for j=1:numel(regionpolys)
%             [regioncx,regioncy,regiona] = ...
%                 polygonCentroid(regionpolys{j}(1,:),regionpolys{j}(2,:));
%             desccenter = desccenter + [regioncx;regioncy].*abs(regiona);
%             sumarea = sumarea + abs(regiona);
%         end
%         desccenter = desccenter ./ sumarea;

        desccenter = ImageAnnotation.an2imcoords(obj.queryrels.descriptors.opos{descind},...
            obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);
        desccenter = mean(desccenter,2);        

        % create bin polygons and bin colors
        descirad = ImageAnnotation.an2imradius(...
            obj.queryrels.descriptors.irad(descind),...
            obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);
        descorad = ImageAnnotation.an2imradius(...
            obj.queryrels.descriptors.orad(descind),...
            obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);

        [iangularpos,iradialpos,oangularpos,oradialpos] = obj.queryrels.descriptors.binaxes(...
                obj.queryrels.descriptors.niangular,obj.queryrels.descriptors.niradial,...
                obj.queryrels.descriptors.noangular,obj.queryrels.descriptors.noradial,...
                descirad,descorad,...
                obj.queryrels.descriptors.settings.iradlin,obj.queryrels.descriptors.settings.oradlin);

        obincenters = polarhistbins2D(oangularpos,oradialpos,desccenter);
        ibincenters = polarhistbins2D(iangularpos,iradialpos,desccenter);
    end
end

function showQuerydescriptors(obj)
    if not(isempty(obj.queryrels)) && not(isempty(obj.queryrels.descriptors)) && ...
       not(isempty(obj.selqueryrel)) && not(isnan(obj.queryrels.descind(obj.selqueryrel(1)))) && ...
       not(isempty(obj.imgcanvas))
            
        % bin values
        if obj.querytemplateVisible && not(isempty(obj.query))
            binvals = obj.query.template.val;

            % normalize bin values by minimum and maxmimum bin value in
            % current image or the template
            descind = obj.queryrels.descind(obj.selqueryrel(1));
            maxbinval = max(max(obj.query.template.val(:)),max(obj.queryrels.descriptors.val(:,descind)));
            minbinval = min(min(obj.query.template.val(:)),min(obj.queryrels.descriptors.val(:,descind)));
%             for i=1:numel(obj.query.descriptors)
%                 maxbinval = max(maxbinval,max(obj.query.descriptors{i}.val(:)));
%                 minbinval = min(minbinval,min(obj.query.descriptors{i}.val(:)));
%             end
            binvals = (binvals - minbinval) ./ (maxbinval - minbinval);
        elseif obj.querydescbinsimVisible && not(isempty(obj.querydescriptorsim))
            % normal: show bin similarity
            [~,maxrelind] = max(obj.querylikelihoods);
%             descsimnorm = sum(obj.querydescriptorsim(:,obj.selqueryrel(1)));
%             descsimnorm = max(obj.querydescriptorsim(:,maxrelind));
            descsimnorm = sum(obj.querydescriptorsim(:,maxrelind));
            if descsimnorm == 0
                binvals = zeros(size(obj.descroptorsim,1),1);
            else
                descind = obj.queryrels.descind(obj.selqueryrel(1));
                binvals = abs(obj.querydescriptorsim(:,descind)) ./ ...
                    (descsimnorm*0.05);
            end


            % --------------

%             % temp - show bin distances from a given bin
%             iangularnum = 1;
%             iradialnum = 2;
%             oangularnum = 1;
%             oradialnum = 1;
% 
%             binnum = sub2ind([...
%                 obj.query.template.niangular,...
%                 obj.query.template.niradial,...
%                 obj.query.template.noangular,...
%                 obj.query.template.noradial],...
%                 iangularnum,iradialnum,oangularnum,oradialnum);
% 
%             bindists = obj.queryrels.descriptors.bindistance(...
%                 obj.query.template.niangular,obj.query.template.niradial,...
%                 obj.query.template.noangular,obj.query.template.noradial,...
%                 obj.query.template.settings.iradlin,...
%                 obj.query.template.settings.oradlin);
% 
%             binvals = bindists(:,binnum);
%             binvals = binvals ./ max(bindists(:)); % normalize
        else
            descind = obj.queryrels.descind(obj.selqueryrel(1));
            binvals = obj.queryrels.descriptors.val(:,descind);

            % normalize bin values by minimum and maxmimum bin value in
            % current image or the template
            maxbinval = max(max(obj.query.template.val(:)),max(obj.queryrels.descriptors.val(:,descind)));
            minbinval = min(min(obj.query.template.val(:)),min(obj.queryrels.descriptors.val(:,descind)));
%             for i=1:numel(obj.query.descriptors)
%                 maxbinval = max(maxbinval,max(obj.query.descriptors{i}.val(:)));
%                 minbinval = min(minbinval,min(obj.query.descriptors{i}.val(:)));
%             end
            binvals = (binvals - minbinval) ./ (maxbinval - minbinval);
        end

        if isa(obj.queryrels.descriptors,'PointrelDescriptorSet')
            descind = obj.queryrels.descind(obj.selqueryrel(1));
            seldescpos = ImageAnnotation.an2imcoords(obj.queryrels.descriptors.pos(:,descind),...
                obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);
            descrad = ImageAnnotation.an2imradius(obj.queryrels.descriptors.rad,...
                obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);

            [angularpos,radialpos] = obj.queryrels.descriptors.binaxes(...
                obj.queryrels.descriptors.nangular,obj.queryrels.descriptors.nradial,descrad,...
                obj.queryrels.descriptors.settings.radlin);

            [verts,faces] = meshPolarhist2D(...
                angularpos,radialpos,[],seldescpos,64);
            verts = [verts;ones(1,size(verts,2)).*1.1]; % add depth


%             binvals = max(0,min(1,binvals.*10));
            binvals = max(0,min(1,binvals));
            bincolors = binvals(:)';
            binalpha = 0.7;
%             bincolors = ...
%                 bsxfun(@times,[0;0;0],(1-binvals(:)')) + ...
%                 bsxfun(@times,[0;1;0],binvals(:)');
%             binalpha = max(0.5,min(1,0.5+binvals(:)'.*0.5));

%             bincolors = obj.distinctcolors(:,binvals+1);

            obj.gdescriptorbins = showPatches(...
                verts,faces,obj.gdescriptorbins,obj.axes,...
                'fvcolor',bincolors,...
                'fvalpha',binalpha,...
                'facecolor','flat',...
                'facealpha','flat',...
                'edgecolor',[0.5;0.5;0.5]);
        elseif isa(obj.queryrels.descriptors,'RegionavgDescriptorSet')

            descind = obj.queryrels.descind(obj.selqueryrel(1));
            desccenter = ImageAnnotation.an2imcoords(obj.queryrels.descriptors.pos{descind},...
                obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);
            desccenter = mean(desccenter,2);

            descrad = ImageAnnotation.an2imradius(obj.queryrels.descriptors.rad(descind),...
                obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);

            [angularpos,radialpos] = obj.queryrels.descriptors.binaxes(...
                obj.queryrels.descriptors.nangular,obj.queryrels.descriptors.nradial,descrad,...
                obj.queryrels.descriptors.settings.radlin);

            [verts,faces] = meshPolarhist2D(...
                angularpos,radialpos,[],desccenter,64);
            verts = [verts;ones(1,size(verts,2)).*1.1]; % add depth

            binvals = max(0,min(1,binvals));
            bincolors = binvals(:)';
            binalpha = 0.5;

            % show descriptor bins
            obj.gdescriptorbins = showPatches(...
                verts,faces,obj.gdescriptorbins,obj.axes,...
                'fvcolor',bincolors,...
                'fvalpha',binalpha,...
                'facecolor','flat',...
                'facealpha','flat',...
                'edgecolor',[0.5;0.5;0.5]);

        elseif isa(obj.queryrels.descriptors,'ReldisthistDescriptorSet')

            % get bin geometry
            descind = obj.queryrels.descind(obj.selqueryrel(1));
            [iangularpos,iradialpos,oangularpos,oradialpos,...
                ibincenters,obincenters,descirad,descorad,desccenter] = ...
                obj.getReldisthistDescriptorGeometry(...
                descind);

            % get bin values
            binvals = reshape(binvals,...
                obj.queryrels.descriptors.niangular,obj.queryrels.descriptors.niradial,...
                obj.queryrels.descriptors.noangular,obj.queryrels.descriptors.noradial);


            if obj.innerdescriptorVisible
                % show descriptor bins
                pointcolors = reshape(binvals,size(binvals,1)*size(binvals,2),[]);
                pointcolors = mean(pointcolors,1);
                pointcolors = max(0,min(1,pointcolors));
                pointpos = [obincenters;ones(1,size(obincenters,2)).*1.3]; % add depth
                obj.godescriptorbins = showPoints(...
                    pointpos,obj.godescriptorbins,obj.axes,...
                    'fvcolor',pointcolors,...    
                    'markersymbol','o',...
                    'markerfacecolor','flat',...
                    'markeredgecolor',[1;0;0]);

                if not(isempty(obj.selobinind))
                    [verts,faces] = meshPolarhist2D(...
                        iangularpos,iradialpos,[],obincenters(:,obj.selobinind),64);
                    verts = [verts;ones(1,size(verts,2)).*1.2]; % add depth

                    selobina = mod(obj.selobinind(1)-1,obj.queryrels.descriptors.noangular)+1;
                    selobinr = floor((obj.selobinind(1)-1) / obj.queryrels.descriptors.noangular)+1;

                    binvals = binvals(:,:,selobina,selobinr);
                    binvals = max(0,min(1,binvals));
                    bincolors = binvals(:)';
                    binalpha = 0.5;

                    obj.gdescriptorbins = showPatches(...
                        verts,faces,obj.gdescriptorbins,obj.axes,...
                        'fvcolor',bincolors,...
                        'fvalpha',binalpha,...
                        'facecolor','flat',...
                        'facealpha','flat',...
                        'edgecolor',[0.5;0.5;0.5],...
                        'edgealpha',1);
                else
                    delete(obj.gdescriptorbins(isvalid(obj.gdescriptorbins)));
                    obj.gdescriptorbins = gobjects(1,0);
                end

            elseif obj.outerdescriptorVisible
%                     ibcenters = bsxfun(@plus,ibincenters,desccenter-obincenters(:,obj.selobinind));
                pointcolors = reshape(binvals,size(binvals,1)*size(binvals,2),[]);
                pointcolors = mean(pointcolors,2)';
                pointcolors = max(0,min(1,pointcolors));
                pointpos = [ibincenters;ones(1,size(ibincenters,2)).*1.3]; % add depth
                obj.gdescriptorbins = showPoints(...
                    pointpos,obj.gdescriptorbins,obj.axes,...
                    'fvcolor',pointcolors,...        
                    'markersymbol','o',...
                    'markerfacecolor','flat',...
                    'markeredgecolor',[0;1;0]);

                if not(isempty(obj.selibinind))
                    [verts,faces] = meshPolarhist2D(...
                        oangularpos,oradialpos,[],desccenter,64);
                    verts = [verts;ones(1,size(verts,2)).*1.1]; % add depth

                    selibina = mod(obj.selibinind(1)-1,obj.queryrels.descriptors.niangular)+1;
                    selibinr =floor((obj.selibinind(1)-1) / obj.queryrels.descriptors.niangular)+1;

                    binvals = binvals(selibina,selibinr,:,:);
                    binvals = max(0,min(1,binvals));
                    bincolors = binvals(:)';
                    binalpha = 0.5;

                    % show descriptor bins
                    obj.godescriptorbins = showPatches(...
                        verts,faces,obj.godescriptorbins,obj.axes,...
                        'fvcolor',bincolors,...
                        'fvalpha',binalpha,...
                        'facecolor','flat',...
                        'facealpha','flat',...
                        'edgecolor',[0.5;0.5;0.5],...
                        'edgealpha',1);
                else
                    delete(obj.godescriptorbins(isvalid(obj.godescriptorbins)));
                    obj.godescriptorbins = gobjects(1,0);
                end
            else
                [overts,ofaces] = meshPolarhist2D(...
                    oangularpos,oradialpos,[],desccenter,64);
                overts = [overts;ones(1,size(overts,2)).*1.1]; % add depth
                [iverts,ifaces] = meshPolarhist2D(...
                    iangularpos,iradialpos,[],obincenters,64);

                [~,redges] = polarhistedges2D(oangularpos,oradialpos,0);
                
                % scale inner bin patches to fit inside the outer bins
                obinangle = oangularpos(2)-oangularpos(1);
                idisplayrada = sin(obinangle/2).*oradialpos;
                idisplayradr = (redges(2:end) - redges(1:end-1)) / 2;
                idisplayrad = min(idisplayrada,idisplayradr);
                iverts = reshape(iverts,2,[],numel(oangularpos),numel(oradialpos));
                c = 1;
                for i=1:numel(oradialpos)
                    for j=1:numel(oangularpos)
                        if isempty(obj.selobinind) || c~=obj.selobinind
                            iverts(:,:,j,i) = bsxfun(@plus,...
                                bsxfun(@minus,iverts(:,:,j,i),obincenters(:,c)).*(idisplayrad(i)/descirad),...
                                obincenters(:,c));
                        end
                        c = c + 1;
                    end
                end
                iverts = reshape(iverts,2,[]);

                % add depth
                iverts = [iverts;ones(1,size(iverts,2)).*1.2]; 
                if not(isempty(obj.selobinind))
                    iverts = reshape(iverts,3,[],numel(oangularpos)*numel(oradialpos));
                    iverts(3,:,obj.selobinind) = 1.3;
                    iverts = reshape(iverts,3,[]);
                end

%                     binvals = binvals(:,:,selobina,selobinr);
                binvals = max(0,min(1,binvals));
                bincolors = binvals(:)';
                binalpha = 0.8;
%                 binalpha = binvals(:)';

                obj.gdescriptorbins = showPatches(...
                    overts,ofaces,obj.gdescriptorbins,obj.axes,...
                    'facecolor','none',...
                    'edgecolor',[0.5;0.5;0.5],...
                    'edgealpha',1);

                obj.godescriptorbins = showPatches(...
                    iverts,ifaces,obj.godescriptorbins,obj.axes,...
                    'fvcolor',bincolors,...
                    'fvalpha',binalpha,...
                    'facecolor','flat',...
                    'facealpha','flat',...
                    'edgecolor',[0.5;0.5;0.5]);
%                         'edgealpha',1
            end

        elseif isa(obj.queryrels.descriptors,'SepreldisthistDescriptorSet')
            warning('Sepreldisthist visualization not implemented yet.');
            delete(obj.gdescriptorbins(isvalid(obj.gdescriptorbins)));
            obj.gdescriptorbins = gobjects(1,0);   
            delete(obj.godescriptorbins(isvalid(obj.godescriptorbins)));
            obj.godescriptorbins = gobjects(1,0);                
        else
            delete(obj.gdescriptorbins(isvalid(obj.gdescriptorbins)));
            obj.gdescriptorbins = gobjects(1,0);   
            delete(obj.godescriptorbins(isvalid(obj.godescriptorbins)));
            obj.godescriptorbins = gobjects(1,0);
        end
    else
        obj.hideQuerydescriptors;
    end
end

function hideQuerydescriptors(obj)
%     delete(obj.gdescriptorpos(isvalid(obj.gdescriptorpos)));
%     obj.gdescriptorpos = gobjects(1,0);
    
    delete(obj.gdescriptorbins(isvalid(obj.gdescriptorbins)));
    obj.gdescriptorbins = gobjects(1,0);
    delete(obj.godescriptorbins(isvalid(obj.godescriptorbins)));
    obj.godescriptorbins = gobjects(1,0);
end

% function showQuerypointlikelihood(obj)
%     if not(isempty(obj.querypointlikelihood))
%         
%         % normalize likelihoods
%         normlhs = obj.querypointlikelihood(3,:);
%         minlikelihood = min(normlhs);
%         maxlikelihood = max(normlhs);
%         
%         if not(isempty(obj.query))
%             minlikelihood = min(minlikelihood,min(obj.query.pointlikelihoodrange(1,:)));
%             maxlikelihood = max(maxlikelihood,max(obj.query.pointlikelihoodrange(2,:)));
%         end
%         if not(isnan(maxlikelihood))
%             if (maxlikelihood == minlikelihood)
%                 normlhs = 0.5;
%             else
%                 normlhs = (normlhs-minlikelihood) ./ (maxlikelihood-minlikelihood);
%             end
%         end
%         
%         % get positions
%         pos = ImageAnnotation.an2imcoords(obj.querypointlikelihood(1:2,:),...
%             obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);
%         pos = [pos;ones(1,size(obj.querypointlikelihood,2))];
%         adata = ones(1,numel(normlhs)).*0.7;
%         
%         % update graphics
%         obj.gpointlikelihood = showPoints(...
%             pos,obj.gpointlikelihood,obj.axes,...
%             'fvcolor',normlhs,...
%             'fvalpha',adata,...
%             'markerfacecolor','flat');
%     else
%         obj.hideQuerypointlikelihood;
%     end
% end

% find the best relationship for each object in the image
function bestrelinds = bestSourceobjs(obj)
    querysrcids = obj.queryrels.sourceids;
    queryrelinds = cellind_mex(querysrcids);

    querysrcids = [querysrcids{:}];
    queryrelinds = [queryrelinds{:}];
    [~,querysrcinds] = ismember(querysrcids,[obj.annotation.imgobjects.id]);

    querysrcindstemp = querysrcinds;
    querysrcindstemp(querysrcindstemp==0) = numel(obj.annotation.imgobjects)+1;
    queryrelinds = array2cell_mex(queryrelinds,querysrcindstemp);
% 
%     if isinf(mindist)
%         obj.selqueryrel = zeros(1,0);
%     else
%         queryrelinds = queryrelinds{querysrcinds(minobjind)};
%         [~,maxind] = max(obj.querylikelihoods(queryrelinds));
%         if not(isempty(maxind))
%             obj.selqueryrel = queryrelinds(maxind);
%         else
%             obj.selqueryrel = zeros(1,0);
%         end
%     end
%     
    
    bestrelinds = zeros(1,0);
    for i=1:numel(queryrelinds)
        [~,maxind] = max(obj.querylikelihoods(queryrelinds{i}));
        if not(isempty(maxind))
            bestrelinds(:,end+1) = queryrelinds{i}(maxind); %#ok<AGROW>
        end
    end
end

function showQuerylikelihoods(obj)
    if not(isempty(obj.querylikelihoods)) && not(isempty(obj.imgcanvas))
        
        % normalize likelihoods
        normlhs = obj.querylikelihoods;
        minlikelihood = min(normlhs);
        maxlikelihood = max(normlhs);
        if not(isempty(obj.query))
            minlikelihood = min(minlikelihood,min(obj.query.imglikelihoodrange(1,:)));
            maxlikelihood = max(maxlikelihood,max(obj.query.imglikelihoodrange(2,:)));
        end
        if not(isnan(maxlikelihood))
            if (maxlikelihood == minlikelihood)
                normlhs = 0.5;
            else
                normlhs = (normlhs-minlikelihood) ./ (maxlikelihood-minlikelihood);
            end
        end
        
        bestrelinds = obj.bestSourceobjs;
        
        % get colors and edge widths
        objids = [obj.annotation.imgobjects.id];
        polys = cell(1,0);
        polylhs = zeros(1,0);
        for i=1:numel(bestrelinds)
            objpolys = [obj.annotation.imgobjects(...
                ismember(objids,obj.queryrels.sourceids{bestrelinds(i)})).polygon];
            polys = [polys,objpolys]; %#ok<AGROW>
            polylhs = [polylhs,ones(1,numel(objpolys)).*normlhs(bestrelinds(i))]; %#ok<AGROW>
        end
        cmap = colormap;
        polycolors = cmap(round(polylhs.*(size(cmap,1)-1)+1),:)';
        polyedgewidth = ones(1,numel(polylhs)).*3;
        
        % transform polygons from [0,1] to [imgmin,imgmax]
        polys = ImageAnnotation.an2imcoords(polys,obj.imgcanvas.imgmin,obj.imgcanvas.imgmax);
        for i=1:numel(polys)
            polys{i}(3,:) = 0.01 + i.*0.01;
        end
        
        % update graphics
        obj.glikelihoods = showFaces(...
            polys,obj.glikelihoods,obj.axes,...
            'facecolor',polycolors,...
            'facealpha',0.5,...
            'edgecolor',polycolors,...
            'edgewidth',polyedgewidth,...
            'stackz',(1:numel(polys)));
    else
        obj.hideQuerylikelihoods;
    end
end

function hideQuerylikelihoods(obj)
    delete(obj.glikelihoods(isvalid(obj.glikelihoods)));
    obj.glikelihoods = gobjects(1,0);
end

function showEditpoly(obj)
    if not(isempty(obj.editpoly))
        % add depth
        verts = [obj.editpoly;ones(1,size(obj.editpoly,2)).*1.1];
        
        obj.geditpoly = showFaces(...
            verts,obj.geditpoly,obj.axes,...
            'facecolor','blue',...
            'facealpha',0.5,...
            'edgecolor','blue',...
            'edgewidth',2);
        
        obj.geditpolystart = showFaces(...
            verts(:,1),obj.geditpolystart,obj.axes,...
            'facecolor','none',...
            'markersymbol','o',...
            'markerfacecolor','blue',...
            'markeredgecolor','none');
    else
        obj.hideEditpoly;
    end
end

function hideEditpoly(obj)
    delete(obj.geditpoly(isvalid(obj.geditpoly)));
    obj.geditpoly = gobjects(1,0);
    delete(obj.geditpolystart(isvalid(obj.geditpolystart)));
    obj.geditpolystart = gobjects(1,0);
end

function showQuerydrawing(obj)
    if not(isempty(obj.drawcanvas)) && isvalid(obj.drawcanvas)
        obj.drawcanvas.show;
    end
end

function hideQuerydrawing(obj)
    if not(isempty(obj.drawcanvas)) && isvalid(obj.drawcanvas)
        obj.drawcanvas.hide;
    end
end


function show(obj)
    obj.show@AxesWidget;

    if isvalid(obj.uigroup)
        obj.uigroup.show;
    end
    
    if not(isempty(obj.navwidget)) &&  isvalid(obj.navwidget)
        obj.navwidget.show;
    end
    if obj.imageVisible && not(isempty(obj.imgcanvas)) &&  isvalid(obj.imgcanvas)
        obj.imgcanvas.show;
    end
    
    obj.layoutPanels;
    
    if isvalid(obj.selobjectindsListener)
        obj.selobjectindsListener.Enabled = true;
    end
    if isvalid(obj.selcomponentindsListener)
        obj.selcomponentindsListener.Enabled = true;
    end
    if isvalid(obj.selqueryrelListener)
        obj.selqueryrelListener.Enabled = true;
    end
    if isvalid(obj.selibinindListener)
        obj.selibinindListener.Enabled = true;
    end
    if isvalid(obj.selobinindListener)
        obj.selobinindListener.Enabled = true;
    end
    if isvalid(obj.selrelListener)
        obj.selrelListener.Enabled = true;
    end
    if isvalid(obj.selsourceindsListener)
        obj.selsourceindsListener.Enabled = true;
    end
    if isvalid(obj.seltargetindsListener)
        obj.seltargetindsListener.Enabled = true;
    end
    if isvalid(obj.openedimgindListener)
        obj.openedimgindListener.Enabled = true;
    end
    if isvalid(obj.toolListener)
        obj.toolListener.Enabled = true;
    end
    
    obj.updateMainaxes;
    obj.showAxescontents;
    if obj.imgbrowserpanelVisible
        obj.showImgbrowserpanel;
    end
    if obj.annorelsVisible
        obj.showRelpanel;
    end
end

function hide(obj)
    obj.hide@AxesWidget;
    
    if isvalid(obj.uigroup)
        obj.uigroup.hide;
    end

    if not(isempty(obj.navwidget)) &&  isvalid(obj.navwidget)
        obj.navwidget.hide;
    end
    if not(isempty(obj.imgcanvas)) &&  isvalid(obj.imgcanvas)
        obj.imgcanvas.hide;
    end
    if not(isempty(obj.imgbrowser)) &&  isvalid(obj.imgbrowser)
        obj.imgbrowser.hide;
    end
    
    if isvalid(obj.selobjectindsListener)
        obj.selobjectindsListener.Enabled = false;
    end
    if isvalid(obj.selcomponentindsListener)
        obj.selcomponentindsListener.Enabled = false;
    end
    if isvalid(obj.selqueryrelListener)
        obj.selqueryrelListener.Enabled = false;
    end
    if isvalid(obj.selibinindListener)
        obj.selibinindListener.Enabled = false;
    end
    if isvalid(obj.selobinindListener)
        obj.selobinindListener.Enabled = false;
    end
    if isvalid(obj.selrelListener)
        obj.selrelListener.Enabled = false;
    end
    if isvalid(obj.selsourceindsListener)
        obj.selsourceindsListener.Enabled = false;
    end
    if isvalid(obj.seltargetindsListener)
        obj.seltargetindsListener.Enabled = false;
    end
    if isvalid(obj.openedimgindListener)
        obj.openedimgindListener.Enabled = false;
    end
    if isvalid(obj.toolListener)
        obj.toolListener.Enabled = false;
    end
    
    obj.hideAxescontents;
    obj.hideImgbrowserpanel;
    obj.hideRelpanel;
end

end

methods(Static)
    
function [objind,compind,minedgedist] = closestImageObject(pos,imgobjects,imgmin,imgmax)
    objind = [];
    compind = [];
%     minarea = inf;
    minedgedist = inf;
    for i=1:numel(imgobjects)
        if imgobjects(i).label <= 0
            continue;
        end
        
        for j=1:numel(imgobjects(i).polygon)

            annopoly = ImageAnnotation.an2imcoords(imgobjects(i).polygon{j},imgmin,imgmax);
            in = pointInPolygon_mex(...
                pos(1),pos(2),...
                annopoly(1,:),...
                annopoly(2,:));
            if in
                [edgedist,~] = pointPolylineDistance_mex(...
                    pos(1),pos(2),annopoly(1,[1:end,1]),annopoly(2,[1:end,1]));
                if edgedist < minedgedist
                    minedgedist = edgedist;
                    objind = i;
                    compind = j;
                end
%                 area = polyarea(...
%                     annopoly(1,:),...
%                     annopoly(2,:));
%                 if area < minarea
%                     minarea = area;
%                     objind = i;
%                     compind = j;
%                 end
            end
        end
    end
end

end

end