function catname = categorydlg(dlgtext,dbcatname,bestguess,categories,showlblfield)
    dwidth = 400;
    dheight = 400;
    
    if nargin < 5 || isempty(showlblfield)
        showlblfield = true;
    end
    
    categories_sorted = sort(categories);
    
    screenpos = get(0,'screensize');
    
    dminx = screenpos(1)+screenpos(3)/2-dwidth/2;
    dminy = screenpos(2)+screenpos(4)/2-dheight/2;
    
    d = dialog('Position',[dminx dminy dwidth dheight],'Name','Pick Category','WindowStyle','normal');

    if isempty(categories)
        listselind = 1;
    else
        listselind = strnearest(dbcatname,categories);
        listselind = listselind(1); % in case multiple strings have the same distance
    end
    
    for i=numel(dbcatname):-1:1
        listselind = find(strncmpi(dbcatname,categories_sorted,i),1,'first');
        if not(isempty(listselind))
            break;
        end
    end
    if isempty(listselind)
        listselind = 1;
    end
    
    if isempty(bestguess) && not(isempty(categories_sorted))
        bestguess = categories_sorted{listselind};
    end
    
    margin = 20;
    
    uicontrol('Parent',d,...
        'Style','text',...
        'HorizontalAlignment','left',...
        'Position',[margin,dheight-(25+margin),dwidth-margin*2,25],...
        'String',dlgtext);
    uicontrol('Parent',d,...
        'Style','text',...
        'HorizontalAlignment','center',...
        'FontSize',20,...
        'Position',[margin,dheight-(25+40+margin),dwidth-margin*2,40],...
        'String',dbcatname);
    editfield = uicontrol('Parent',d,...
        'Style','edit',...
        'Min',0,...
        'Max',1,...
        'HorizontalAlignment','left',...
        'String',bestguess,...
        'Position',[margin,margin+25,dwidth-margin*2,25],...
        'Visible',iif(showlblfield,'on','off'));
    uicontrol('Parent',d,...
        'Position',[dwidth/2-70/2 margin 70 25],...
        'HorizontalAlignment','center',...    
        'String','Done',...
        'Callback',{@(src,evt) donePressed(d,editfield)});
    uicontrol('Parent',d,...
        'Style','listbox',...
        'Min',0,...
        'Max',1,...
        'HorizontalAlignment','left',...
        'Value',listselind,...
        'String',categories_sorted,...
        'Position',[margin,margin+25+25,dwidth-margin*2,dheight-(margin*2+25+25+40+25)],...
        'Callback',{@(src,evt) catlistChanged(src,editfield)});
    
    uiwait(d);
    
    if isvalid(d)
        catname = get(d,'UserData');
        delete(d);
    else
        catname = '';
    end
end

function donePressed(dlg,editfield)
    set(dlg,'UserData',editfield.String);
    uiresume(dlg);
end

function catlistChanged(catlist,editfield)
    if isempty(catlist.Value)
        editfield.String = '';
    else
        editfield.String = catlist.String{catlist.Value};
    end
end
