classdef RegionrelDescriptorSet < handle
    
properties
    settings = struct;
end

methods(Static)
function s = defaultSettings
    s = struct;
end
end

methods

% obj = RegionrelDescriptorSet()
% obj = RegionrelDescriptorSet(obj2)
function obj = RegionrelDescriptorSet(varargin)
    
    obj.settings = obj.defaultSettings;
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'RegionrelDescriptorSet')
        obj.copyFrom(varargin{1});
    else
        error('Invalid input arguments.');
    end
end

function delete(obj)
    obj.clear;
end

function clear(obj) %#ok<MANU>
    % do nothing
end

function copyFrom(obj,obj2)
    obj.settings = obj2.settings;
end

function obj2 = clone(obj)
    obj2 = RegionrelDescriptorSet(obj);
end

function compute(obj)
    obj.clear;
end

end % methods

methods(Abstract)
    b = iscompatible(obj,obj2);
    append(obj,obj2,ind2);
    copySubset(obj,ind,obj2,ind2);
    keepSubset(obj,ind);
    deleteSubset(obj,ind);
end

end % classdef
