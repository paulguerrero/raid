function c = cellselectrow(c,r)
    for i=1:numel(c);
        c{i} = c{i}(r{i},:);
    end
end
