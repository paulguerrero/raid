#include "mex.h"

// #include<vector>
// #include<algorithm>

/* the gateway function */
// b = cellisempty_mex(cellarray,checkcontent)
// if checkcontent is false (default), only returns true of the cell is empty,
// if checkcontent is true, also returns true of the cell contains an empty array

// if second input is true, empty is also true if content of cell is empty
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    if (nrhs != 1 && nrhs != 2) {
        mexErrMsgTxt("One input required.");
    }
    
    if(nlhs != 1) {
        mexErrMsgTxt("One output required.");
    }
    
//     mexPrintf("1\n");
//     mexEvalString("drawnow;");

    // get pointers to values and indices
    if (!mxIsClass(prhs[0],"cell")) {
        mexErrMsgTxt("Must pass a cell array.");
    }
    
    bool checkcontent = false;
    
    if (nrhs >= 2) {
        if (!mxIsLogicalScalar(prhs[1])) {
            mexErrMsgTxt("Second input must be a logical scalar.");
        }
        
        checkcontent = mxIsLogicalScalarTrue(prhs[1]);
    }
    
    plhs[0] = mxCreateLogicalArray(mxGetNumberOfDimensions(prhs[0]),mxGetDimensions(prhs[0]));
    mwSize ncells = mxGetNumberOfElements(plhs[0]);
    
    mxLogical* elm_p = mxGetLogicals(plhs[0]);
    
    for (mwIndex i=0; i<ncells; i++) {
        
        mxArray* cell = mxGetCell(prhs[0],i);
        if (cell == NULL) {
            elm_p[i] = (mxLogical) 1;
        } else {
            if (checkcontent && mxIsEmpty(cell)) {
                elm_p[i] = (mxLogical) 1;
            } else {
                elm_p[i] = (mxLogical) 0;
            }
        }
    }
}
