% normalized indexed color to rgb
function I = nind2rgb(I,cmap)
    I = ind2rgb(round(I.*(size(cmap,1)-1)+1),cmap);
end
