function [maxmask,centroidsubs,centroidinds] = nonMaxSuppr(I, ks, thresh)
    
    if nargin < 3
        thresh = 0;
    end
    
    Idim = ndims(I);
    if Idim ~= 2 && Idim ~= 3
        error('Input image must be either 2D or 3D intensities.')
    end

    se = ones(ks .* ones(1,Idim));
    windowmax = imdilate(I,se);
    maxmask = I >= windowmax & ...
              I > 0 & ...
              I >= max(I(:))*thresh;

    if nargout > 1
        % get connected components of the maxima, each conn. comp. is one
        % position (may be multiple pixels because of rasterization and
        % discrete distance transform)
        if Idim == 2
            cc = bwconncomp(maxmask,8);
        elseif Idim == 3
            cc = bwconncomp(maxmask,26);
        end

        if Idim == 2
            rprops = regionprops(cc,'Centroid');
            if isempty(rprops)
                centroidsubs = zeros(2,0);
            else
                centroidsubs = round(cat(1,rprops.Centroid)');
            end
            centroidsubs(1,:) = max(1,min(size(I,2),centroidsubs(1,:)));
            centroidsubs(2,:) = max(1,min(size(I,1),centroidsubs(2,:)));
            if nargout > 2
                centroidinds = sub2ind(size(maxmask),centroidsubs(2,:),centroidsubs(1,:));
%                 ov = I(centroidpixinds);
            end
            
        elseif Idim == 3
            rprops = regionprops(cc,'Centroid');
            if isempty(rprops)
                centroidsubs = zeros(3,0);
            else
                centroidsubs = round(cat(1,rprops.Centroid)');
            end
            centroidsubs(1,:) = max(1,min(size(I,2),centroidsubs(1,:)));
            centroidsubs(2,:) = max(1,min(size(I,1),centroidsubs(2,:)));
            centroidsubs(3,:) = max(1,min(size(I,3),centroidsubs(3,:)));
            if nargout > 2
                centroidinds = sub2ind(size(maxmask),centroidsubs(2,:),centroidsubs(1,:),centroidsubs(3,:));
%                 ov = I(centroidpixinds);
            end
        end
        
    end
end
