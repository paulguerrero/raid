% rs must be nx2, ind and w nxm
function irs = rosyinterp(rs,inds,w,nrosy,prefdirs)
%     rsrep = rosyrep(rs,nrosy);
    irs = rs(:,inds);
    irs = rosyrep(irs,nrosy);
    irsx = reshape(irs(1,:),size(inds,1),size(inds,2));
    irsy = reshape(irs(2,:),size(inds,1),size(inds,2));
    
    irs = [sum(irsx.*w,2);...
           sum(irsy.*w,2)];
	if nargin >= 5
        irs = rosyunrep(irs,nrosy,prefdirs);
    else
        irs = rosyunrep(irs,nrosy);
	end
    
    
    
%     irs = rosyunrep(...
%             rosyrep(z(:,inds(1,:)),nrosy).*[fs(1,:);fs(1,:)] + ...
%             rosyrep(z(:,inds(2,:)),nrosy).*[fs(2,:);fs(2,:)] + ...
%             rosyrep(z(:,inds(3,:)),nrosy).*[fs(3,:);fs(3,:)] + ...
%             rosyrep(z(:,inds(4,:)),nrosy).*[fs(4,:);fs(4,:)],...
%          nrosy);
end
