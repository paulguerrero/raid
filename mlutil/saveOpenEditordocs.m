% restore with 'openEditordocs'
function saveOpenEditordocs(filename)
    if nargin < 1 || isempty(filename)
        [fname,pathname,~] = ...
            uiputfile({'*.txt','Text (*.txt)'},'Save Open M-Files');

        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,'/',fname];
        else
            return;
        end
    end
    
    f = matlab.desktop.editor.getAll;
    docfilenames = {f.Filename};
    
    fid = fopen(filename,'w');
    for i=1:numel(docfilenames)
        fprintf(fid,'%s\n',docfilenames{i});
    end
    fclose(fid);
end
