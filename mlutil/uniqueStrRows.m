function [ustrmat,istrmat,iustrmat] = uniqueStrRows(strmat)
    
    if isempty(strmat)
        ustrmat = strmat;
        istrmat = zeros(0,1);
        iustrmat = zeros(0,1);
        return;
    end

    [ustrmat,perm] = sortrows(strmat);
    
    j = not(all(strcmp(ustrmat(1:end-1,:), ustrmat(2:end,:)),2));

    j(end+1) = true;
    ustrmat = ustrmat(j,:);
    
    if nargout >= 2
        istrmat = perm(j);
    end
    
    if nargout >= 3
        iustrmat = cumsum([1;j(1:end-1)]);
        iustrmat(perm) = iustrmat;
    end
    
end
