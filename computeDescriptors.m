% rels = computeDescriptors(currentrels,newrels,desctemplate,imgpath,annopath,sourcelbl,targetlbl)
% rels = computeDescriptors(currentrels,newimgfilenames,desctemplate,imgpath,annopath,sourcelbl,targetlbl)
function [rels,imginds,errorimages] = computeDescriptors(varargin)

    argoffset = 0;
    if isa(varargin{2},'DatabaseRelationshipSet')
        rels = varargin{argoffset+1};
        inputrels = varargin{argoffset+2};
        desctemplate = varargin{argoffset+3};
        imgpath = varargin{argoffset+4};
        annopath = varargin{argoffset+5};
        
        if numel(varargin) >= 6
            sourcelbl = varargin{argoffset+6};
            targetlbl = varargin{argoffset+7};
            argoffset = argoffset+7;
        else
            sourcelbl = cell(1,0);
            targetlbl = cell(1,0);
            argoffset = argoffset+5;
        end
        
        imgfnames = inputrels.imgfilenames;
    else
        rels = varargin{argoffset+1};
        imgfnames = varargin{argoffset+2};
        desctemplate = varargin{argoffset+3};
        imgpath = varargin{argoffset+4};
        annopath = varargin{argoffset+5};
        
        if numel(varargin) >= 6
            sourcelbl = varargin{argoffset+6};
            targetlbl = varargin{argoffset+7};
            argoffset = argoffset+7;
        else
            sourcelbl = cell(1,0);
            targetlbl = cell(1,0);
            argoffset = argoffset+5;
        end
        
        inputrels = DatabaseRelationshipSet.empty;
    end
    
    if not(isempty(inputrels)) && not(isempty(inputrels.descriptors)) && ...
       not(isempty(rels)) && not(isempty(rels.descriptors)) && ...
       not(rels.descriptors.iscompatible(inputrels.descriptors))
   
        error('New descriptors are not compatible with the current descriptors.');
    end
    
    options = struct(...
        'labeled','any',... % should the query only contain labeled, only unlabeled or both kinds of relationships        
        'ignoreownlabel',false,...
        'overwrite',true);
    options = nvpairs2struct(varargin(argoffset+1:end),options);
    
    if not(iscell(imgfnames))
        imgfnames = {imgfnames};
    end
    
    if isempty(imgfnames)
        return;
    end
    
    % delete image infos that are overwritten
    [imgexists,imginds] = ismember(imgfnames,rels.imgfilenames);
    
    if options.overwrite && any(imgexists)
        relinds = unique(cat(1,rels.imgrelinds{imginds(imgexists)}));
        if not(isempty(relinds))
            % one per relationship (may not be stored for all queried relationships)
            rels.deleteSubset(relinds);
        end
        imgexists(:) = false;
    end
    
    currentimgcount = numel(rels.imgfilenames);
    rels.addImgfilenames(imgfnames(not(imgexists)));
    imginds(not(imgexists)) = currentimgcount+1:numel(rels.imgfilenames);
    
    % todo: split rels into batches of size maybe 200, marge back into
    % rels afterwards => append and subset operations take too long
    % when applied to large matrices
    
    totaldesctime = 0;
    desccount = 0;
    
    imgrels = cell(1,numel(imginds));
    errorimages = cell(0,1);
    
    for i=1:numel(imginds)
        imgind = imginds(i);
        disp(['Image ',num2str(i),' / ',num2str(numel(imginds))]);
        
        imgfilename = rels.imgfilenames{imgind};
        
        % get all relevant relationships in the current image
        if not(isempty(inputrels))
            % query given relationships
            imgrels{i} = DatabaseRelationshipSet;
            imgrels{i}.append(inputrels,inputrels.imgrelinds{i});
            imgrels{i}.lockimgfilenames = true;
            
            % filter by source and target labels
            validmask = true(1,numel(imgrels{i}.id));
            
            if not(isempty(sourcelbl)) || not(isempty(targetlbl))
                if not(isempty(sourcelbl))
                    relind = cellind_mex(imgrels{i}.sourcelabels);
                    relind = [relind{:}];
                    imgsrclbls = [imgrels{i}.sourcelabels{:}];
                    validsrclbl = ismember(imgsrclbls,sourcelbl);
                    validsrclbl = array2cell_mex(double(validsrclbl),relind,numel(imgrels{i}.sourcelabels));
                    validsrclbl = cellfun(@(x) not(isempty(x)) && all(x),validsrclbl);
                else
                    validsrclbl = true(1,numel(imgrels{i}.sourcelabels));
                end

                if not(isempty(targetlbl))
                    relind = cellind_mex(imgrels{i}.targetlabels);
                    relind = [relind{:}];
                    imgtgtlbls = [imgrels{i}.targetlabels{:}];
                    validtgtlbl = ismember(imgtgtlbls,targetlbl);
                    validtgtlbl = array2cell_mex(double(validtgtlbl),relind,numel(imgrels{i}.targetlabels));
                    validtgtlbl = cellfun(@(x) not(isempty(x)) && all(x),validtgtlbl);
                else
                    validtgtlbl = true(1,numel(imgrels{i}.targetlabels));
                end
                
                validmask = validmask & validsrclbl & validtgtlbl;
            end
            
            % filter relationships where any sourcelabel == targetlabel
            if options.ignoreownlabel
                validinds = find(validmask);
                for j=validinds
                    validmask(j) = validmask(j) && not(any(ismember(...
                        imgrels{i}.targetlabels{j},...
                        imgrels{i}.sourcelabels{j})));
                end
            end
            
            % filter out relationships that are not labeled (if requested)
            if strcmp(options.labeled,'yes')
                validmask = validmask & not(isnan(imgrels{i}.id));
            elseif strcmp(options.labeled,'no')
                validmask = validmask & isnan(imgrels{i}.id);
            elseif strcmp(options.labeled,'any')
                % do nothing
            else
                error('Unknown labeled filter option.');
            end
            
            imgrels{i}.keepSubset(find(validmask)); %#ok<FNDSB>
            
            % get source and target objects for relationships that do not
            % have descriptors
            needobjinds = find(isnan(imgrels{i}.descind));
            imganno = ImageAnnotation.empty;
            if not(isempty(needobjinds))
                
                annofilename = ImageAnnotation.img2annofilename(imgfilename);
                imganno = AnnotationImporter.importAnnotation([annopath,'/',annofilename]);                
                imgobjectids = [imganno.imgobjects.id];
                
                srcobjs = cell(1,numel(imgrels{i}.id));
                tgtobjs = cell(1,numel(imgrels{i}.id));
                for j=needobjinds
                    srcobjs{j} = imganno.imgobjects(ismember(imgobjectids,imgrels{i}.sourceids{j}));
                    tgtobjs{j} = imganno.imgobjects(ismember(imgobjectids,imgrels{i}.targetids{j}));
                end
            end
        else
            % query all relationships to/from objects with the given labels
            [imgrels{i},imganno,srcobjs,tgtobjs] = findDatabaseRelationships(...
                imgfilename,annopath,...
                'labeled',options.labeled,...
                'sourcelbl',sourcelbl,...
                'targetlbl',targetlbl,...
                'ignoreownlabel',options.ignoreownlabel);
            imgrels{i}.lockimgfilenames = true;
        end
        
        if isempty(imgrels{i}.id)
            continue;
        end
        
        % copy existing relationships to keep them (if requested)
        if not(options.overwrite)
            qrelinds = rels.findRelationships(...
                imgrels{i}.sourceids,imgrels{i}.targetids,imgind,'exact');
            hasimgrelinds = find(qrelinds>=1);
            qrelinds = qrelinds(hasimgrelinds);
            
            if not(isempty(qrelinds))
                imgrels{i}.copySubset(hasimgrelinds,rels,qrelinds);
                
                rels.deleteSubset(qrelinds);
            end
        end
        
        % compute descriptors if necessary
        nothasdescinds = find(isnan(imgrels{i}.descind));
        if not(isempty(nothasdescinds))
            imginfo = imfinfo([imgpath,'/',imgfilename]);
            imgres = [imginfo.Width;imginfo.Height];
            [imgmin,imgmax] = ImageAnnotation.imres2imsize(imgres);
            
            newdescriptors = desctemplate.clone; % this also clones all settings, etc.
            try
                tic;
                newdescriptors.compute(...
                    srcobjs(nothasdescinds),tgtobjs(nothasdescinds),...
                    imgmin,imgmax,imgres);
                newdescsrcarea = zeros(1,numel(nothasdescinds));
                newdescsrcsaliency = zeros(1,numel(nothasdescinds));
                for j=1:numel(nothasdescinds)
                    newdescsrcarea(j) = sum([srcobjs{nothasdescinds(j)}.area]);
                    newdescsrcsaliency(j) = max([srcobjs{nothasdescinds(j)}.saliency]);
                end
                totaldesctime = totaldesctime + toc;
                desccount = desccount + numel(nothasdescinds);
                disp(['Average time: ',num2str(totaldesctime/desccount)]);
            catch err
                warning(['Error caught in image ''',imgfilename,''': ',...
                    err.message]);
                delete(newdescriptors(isvalid(newdescriptors)));
                errorimages{end+1,:} = imgfilename; %#ok<AGROW>
                continue;
            end
            
            nnew = size(newdescriptors.val,2);
            if isempty(imgrels{i}.descriptors)
                imgrels{i}.descriptors = newdescriptors;
                imgrels{i}.descsrcarea = newdescsrcarea;
                imgrels{i}.descsrcsaliency = newdescsrcsaliency;
            else
                imgrels{i}.descriptors.append(newdescriptors);
                imgrels{i}.descsrcarea = [imgrels{i}.descsrcarea,newdescsrcarea];
                imgrels{i}.descsrcsaliency = [imgrels{i}.descsrcsaliency,newdescsrcsaliency];
                delete(newdescriptors(isvalid(newdescriptors)));
            end
            imgrels{i}.descind(nothasdescinds) = ...
                size(imgrels{i}.descriptors.val,2)-nnew+1:size(imgrels{i}.descriptors.val,2);
        end
        
        delete(imganno(isvalid(imganno)));
    end
    
    batchsize = 100;
    nbatches = ceil(numel(imgrels)/batchsize);
    
    c = 1;
    for i=1:nbatches
        batchset = DatabaseRelationshipSet;
        
        for j=1:min(batchsize,numel(imgrels)-(c-1))
            batchset.append(imgrels{c});
            c = c + 1;
        end
        rels.append(batchset);
    end

end
