function restartEditor

    initImgstructure;
    
    closeEditor;
    
    % create editor
    evalin('base','editor = StructuralImageEditor;');
end
