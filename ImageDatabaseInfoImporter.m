classdef ImageDatabaseInfoImporter < handle

properties
end

methods

function obj = ImageDatabaseInfoImporter
    if nargin == 0
        % do nothing
    else
        error('Invalid arguments.');
    end
end

function clear(obj) %#ok<MANU>
    % do nothing
end
    
function imgdbinfo = import(obj,filenames)
    
    if isempty(filenames)
        error('No filenames given.')
    end
    
    if ischar(filenames)
        dbpath = filenames;
        filenames = struct;
        filenames.dbinfo = fullfile(dbpath,'dbinfo.txt');
        filenames.categories = fullfile(dbpath,'categories.xml');
        filenames.relcategories = fullfile(dbpath,'relcategories.xml');
        filenames.categorycolors = fullfile(dbpath,'categorycolors.txt');
    end
    
    if not(all(isfield(filenames,{'dbinfo','categories','relcategories','categorycolors'})))
        error('Some filenames are missing.');
    end
    
    obj.clear;
    
    imgdbinfo = ImageDatabaseInfo;
    
    [imgdbinfo.imgfilenames,imgdbinfo.imghasrels] = obj.importDbinfo(filenames.dbinfo);    
    imgdbinfo.categories = obj.importCategories(filenames.categories);
    imgdbinfo.relcategories = obj.importRelcategories(filenames.relcategories);
    imgdbinfo.categorycolors = obj.importCategorycolors(filenames.categorycolors);
end

end

methods(Static)

function imgdbfino = importImageDatabaseInfo(filenames,varargin)
    importer = ImageDatabaseInfoImporter;
    imgdbfino = importer.import(filenames,varargin{:});
    delete(importer(isvalid(importer)));
end

function [imgfilenames,hasrels] = importDbinfo(filename)
    fid = fopen(filename,'r');
    dbinfo = textscan(fid,'%s %d','Delimiter',',');
    fclose(fid);
    imgfilenames = dbinfo{1}';
    hasrels = logical(dbinfo{2}');
end

function imgfilenames = importWorkingset(filename)
    fid = fopen(filename,'r');
    imgfilenames = textscan(fid,'%s','Delimiter',{'\n','\r'});
    fclose(fid);
    imgfilenames = imgfilenames{1}';
end

function categories = importCategories(filename)
    
    % open xml file
%     fp = fopen(filename,'rt');
%     filestr = fread(fp,inf,'*char')';
%     fclose(fp);
%     filestr = regexprep(filestr, '<!DOCTYPE [^>]*>','','once','ignorecase');
%     
%     xmldoc = xmlreadstring(filestr);

    xmldoc = xmlread(filename);
    xmlnode = xmldoc.getDocumentElement;
    
    % read categories
    categorynodelist = getDOMChildElements(xmlnode,'category');
    categories = cell(numel(categorynodelist),1);
    categoryids = zeros(numel(categorynodelist),1);
    for i=1:numel(categorynodelist)
        categories{i} = char(categorynodelist(i).getAttribute('name'));
        categoryids(i) = sscanf(char(categorynodelist(i).getAttribute('id')),'%d');
    end
    
    % sort categories by id and make sure all ids are accounted for
    [ids,perm] = sort(categoryids,'ascend');
    if any(ids ~= (1:numel(ids))')
        error('Some categories are missing.');
    end
    categories = categories(perm,:);
end

function relcategories = importRelcategories(filename)
    
    % open xml file
%     fp = fopen(filename,'rt');
%     filestr = fread(fp,inf,'*char')';
%     fclose(fp);
%     filestr = regexprep(filestr, '<!DOCTYPE [^>]*>','','once','ignorecase');
%     
%     xmldoc = xmlreadstring(filestr);

    xmldoc = xmlread(filename);
    xmlnode = xmldoc.getDocumentElement;
    
    % read categories
    categorynodelist = getDOMChildElements(xmlnode,'relcategory');
    relcategories = cell(numel(categorynodelist),1);
    categoryids = zeros(numel(categorynodelist),1);
    for i=1:numel(categorynodelist)
        relcategories{i} = char(categorynodelist(i).getAttribute('name'));
        categoryids(i) = sscanf(char(categorynodelist(i).getAttribute('id')),'%d');
    end
    
    % sort categories by id and make sure all ids are accounted for
    [ids,perm] = sort(categoryids,'ascend');
    if any(ids ~= (1:numel(ids))')
        error('Some categories are missing.');
    end
    relcategories = relcategories(perm,:);
end

% catcolors is a nx3 array of [r,g,b]
function categorycolors = importCategorycolors(filename)
    
    fid = fopen(filename,'r');
    categorycolors = textscan(fid,'%s','Delimiter','\n');
    fclose(fid);
    categorycolors = cat(1,categorycolors{1}{:});
    categorycolors = rgbhex2rgbdec(categorycolors,true);
end

% catconversions are nx2 cell arrays
function catconversions = importCategoryconversions(filename)
    
    % open xml file
%     fp = fopen(filename,'rt');
%     filestr = fread(fp,inf,'*char')';
%     fclose(fp);
%     filestr = regexprep(filestr, '<!DOCTYPE [^>]*>','','once','ignorecase');
%     
%     xmldoc = xmlreadstring(filestr);

    xmldoc = xmlread(filename);
    xmlnode = xmldoc.getDocumentElement;
    
    % read categories
    conversionnodelist = getDOMChildElements(xmlnode,'conversion');
    catconversions = cell(numel(conversionnodelist),2);
    conversionids = zeros(numel(conversionnodelist),1);
    for i=1:numel(conversionnodelist)
        catconversions{i,1} = char(conversionnodelist(i).getAttribute('from'));
        catconversions{i,2} = char(conversionnodelist(i).getAttribute('to'));
        conversionids(i) = sscanf(char(conversionnodelist(i).getAttribute('id')),'%d');
    end
    
    % sort categories by id and make sure all ids are accounted for
    [ids,perm] = sort(conversionids,'ascend');
    if any(ids ~= (1:numel(ids))')
        error('Some category conversions are missing.');
    end
    catconversions = catconversions(perm,:);
end

end

end
