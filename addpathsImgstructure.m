function addpathsImgstructure  

    paths = fullfile(pwd,{...
        'mlutil',... % utility functions
        'mlgeometry',... % geometry functions
        'mlboost',... % for polygonPolygonBoolean_mex
        'mluiutil',... % for user interface utilities
        'mluiwidget',... % for user interface utilities
        'mlsvg',... % to read svg annotations
        'uiwidgets',... % for user interface widgets, such as the editors
        'skeleton',... % for tracing the image skeleton and converting it to a polyline
        }); 
    
    addpath(paths{:});
end