classdef StructuralImageEditor < EditorFigure
    
properties
    % widgets
    navwidget = NavigationWidget.empty;
    imgeditorcanvas = ImageCanvas.empty;
    
    imgeditor = ImageEditor.empty;
end

methods
    
function obj = StructuralImageEditor
    
    obj@EditorFigure;
    
    obj.fig.Position = [20,20,1920,1080];
    
    % create menus struct
    menus = struct;
    for i=1:numel(obj.fig.Children)
        if strcmp(obj.fig.Children(i).Type,'uimenu')
            menus.(obj.fig.Children(i).Label) = obj.fig.Children(i);
        end
    end
    
    % create widgets and editors
    obj.navwidget = NavigationWidget(obj.mainaxes,...
        obj.navpanel);
    obj.imgeditorcanvas = ImageCanvas(obj.mainaxes,...
        [-1;-1],[1;1],100000);
        
    obj.imgeditor = ImageEditor(obj.mainaxes,...
        obj,...
        obj.navwidget,obj.imgeditorcanvas,...
        obj.fig,obj.toolpanel,obj.mainpanel,...
        menus);
    
    obj.addEditor(obj.imgeditor);
    
    obj.seleditor = obj.imgeditor;
    resetCamera(obj.mainaxes);
end
    
function delete(obj)
    
    % delete editors and widgets
    if not(isempty(obj.imgeditor))
        delete(obj.imgeditor(isvalid(obj.imgeditor)));
    end
    
    if not(isempty(obj.imgeditorcanvas))
        delete(obj.imgeditorcanvas(isvalid(obj.imgeditorcanvas)));
    end
    if not(isempty(obj.navwidget))
        delete(obj.navwidget(isvalid(obj.navwidget)));
    end
    
end

end

end
