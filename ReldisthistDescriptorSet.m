% Relationship Distribution Histogram
classdef ReldisthistDescriptorSet < RegionrelDescriptorSet
    
properties
    % the first dimension is a flattened array of size
    % niangular x niradial x noangular x noradial
    % the second dimension is the number descriptors in the set
    val = [];
    
    irad = zeros(1,0);
    orad = zeros(1,0);
    
    % positions for inner and outer histograms
    ipos = cell(1,0);
    opos = cell(1,0);
    
    niangular = 0;
    niradial = 0;
    noangular = 0;
    noradial = 0;
end

% Describes valid paths for source region A and target region B:
% paths (or triangles) of 3 points a,b,c, where a and b are in A and c in B.

methods(Static)
function s = defaultSettings
    s = RegionrelDescriptorSet.defaultSettings;
    
    s.ipointdensity = 10000; % points per image area
    s.opointdensity = 10000; % points per image area
    
    s.batchsize = 1e6; % size of one batch in number of bins when summing up outer histograms over the region
    
    s.oradf = 1; % in multiples of the region span
    s.iradf = 0.5; % in multiples of the outer histogram radius
    
    s.oradlin = 1; % in [0,1], 0: logarithmic radii, 1: linear radii
    s.iradlin = 0; % in [0,1], 0: logarithmic radii, 1: linear radii
    
    s.fixedrad = zeros(1,0);
    
%     s.normalizehist
%     % - none: no normalization (absolute count of paths in the bins,
%     % depends on actual size of bins, which depends on the size of the image)
%     % - binvolume: normalize by 4d bin volume
%     % - descvolume: normalize by 4d descriptor volume
%     % - descsum: normalize by sum of descriptor values (if used with 'none'
%     % histogram normalization: distribution of relative position of points
%     % in the target region relative to points in the source region)    


    s.normalizeinner = 'binarea'; % normalization of inner histogram, see PointrelDecsriptorSet
    s.normalizeouter = 'binarea'; % normalization of outer histogram
    % - none: no normalization (if inner is also 'none': absolute number of
    % valid paths starting at a given point falling into each bin, depends
    % on area of bins, which depend on image size)
    % - binarea: bin area (if inner is also normalized by bin
    % area, the 4d histogram is normalized by bin volume: density of valid
    % paths starting at a given point, size of bins factored out)
    % - binregionoverlap: overlap of the outer bin with the
    % region (integral of region over outer bin) = number of points b in
    % the bin: each outer bin holds an average over the inner histograms
    % inside the bin (= average of valid paths over all point b in the bin)
    % - descarea: descriptor area (if inner is also normalized
    % by descriptor area, the 4d histogram is normalized by descriptor volume)
    % - descsum: sum of descriptor values (if used with 'none'
    % inner normalization: distribution of valid paths starting from a
    % given point in A)
    % - descnormsum: sum of descriptor values normalized by bin area =>
    % normalized histogram (if used with 'binarea' inner normalization:
    % distribution over 4d histogram of density of valid paths starting at
    % a given point, size of bins factored out)
    
    s.normalizeregion = 'regionarea'; % normalization of sum over region
    % - none: no normalization (just sum up descriptors, resulting values
    % depend on the size of the region)
    % - binregionoverlapsum: sum of the region overlaps of a bin
    % accumulated over the region: when used with 'none' outer
    % normalization, gives an average of the inner histograms in each bin,
    % with everything else (region area and bin size, etc.) factored out
    % - regionarea: region area (in practice, points are
    % assumed to be distributed equally over the area, so just divide by
    % number of points)
    % - descsum: sum of descriptor values (if used with 'none'
    % for both inner and outer normalization: distribution of all valid
    % paths in the region (i.e. first two points in A, third point in B)
    % - binregionoverlapsumdescsum: same as binregionoverlapsum, but
    % normalized to be a distribution over the accumulated 4d histogram
    
    % some combinations:
    % - distribution4D: distribution of all valid paths in the region and
    % 4D histogram
    % (larger bins have larger values)
    % 'none','none','descsum'
    % - normbindist4D: distribution over region of path density over histogram
    % (size of bins factored out)
    % 'binarea','binarea','descsum'
    % - normbindist4D: average over region of path density over histogram
    % (size of bins factored out)
    % 'binarea','binarea','regionarea'
    % - normbinavgdist4D: average over region of path density distribution over histogram
    % (size of bins factored out)
    % 'binarea','descnormsum','regionarea'
    % - avginnernormbinhist2D: average of normalized (by bin area) inner
    % histograms
    % 'binarea','none','binregionoverlapsum'
    
    % other option:
    % like 'binarea','none','binregionoverlapsum', but also removes the
    % bias of the average in each outer bin towards bins with larger
    % overlap binregionoverlap is clamped to 0 (no overlap at all) or 1 (at
    % least some overlap)
    % 'binarea','binregionoverlap','countofnonzerobinregionoverlap'
    
end
end

methods

% obj = ReldisthistDescriptorSet()
% obj = ReldisthistDescriptorSet(obj2)
% obj = ReldisthistDescriptorSet(niangular,niradial,noangular,noradial)
% obj = ReldisthistDescriptorSet(niangular,niradial,noangular,noradial,irad,orad,ipos,opos,val)
% obj = ReldisthistDescriptorSet(niangular,niradial,noangular,noradial,sourceregions,targetregions,imgmin,imgmax,imgres)
% obj = ReldisthistDescriptorSet(...,nvpairs)
function obj = ReldisthistDescriptorSet(varargin)
    
    obj = obj@RegionrelDescriptorSet;
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'ReldisthistDescriptorSet')
        obj.copyFrom(varargin{1});
    elseif numel(varargin) == 4 || numel(varargin) >= 5 && ischar(varargin{5}) || ...
           numel(varargin) == 9 || numel(varargin) >= 10 && ischar(varargin{10})

        if numel(varargin) >= 9 && not(ischar(varargin{5}))
            obj.settings = nvpairs2struct(varargin(10:end),obj.settings);
        else
            obj.settings = nvpairs2struct(varargin(5:end),obj.settings);
        end
       
        obj.niangular = varargin{1};
        obj.niradial = varargin{2};
        obj.noangular = varargin{3};
        obj.noradial = varargin{4};

        obj.val = zeros(obj.niangular*obj.niradial*obj.noangular*obj.noradial,0);

        if numel(varargin) >= 9 && not(ischar(varargin{5})) && not(ischar(varargin{8})) && isnumeric(varargin{5})
            % set manually
            if size(varargin{9},2) ~= size(varargin{5},2) || ...
               size(varargin{9},1) ~= obj.niangular * obj.niradial * obj.noangular * obj.noradial
                error('Invalid size for descriptor values.');
            end
            if size(varargin{6},2) ~= size(varargin{5},2)
                error('Number of inner and outer radii do not match.');
            end
            if size(varargin{7},2) ~= size(varargin{5},2) || ...
               size(varargin{8},2) ~= size(varargin{5},2)
                error('Number of positions and radii do not match.');
            end
            
            obj.irad = varargin{5};
            obj.orad = varargin{6};
            obj.ipos = varargin{7};
            obj.opos = varargin{8};
            obj.val = varargin{9};
        elseif numel(varargin) >= 9 && not(ischar(varargin{5})) && not(ischar(varargin{8}))
            % compute from image regions
            obj.compute(varargin{5:9});
        end
    else
        error('Invalid input arguments.');
    end
end

function delete(obj)
    % do nothing
    obj.clear;
end

function clear(obj)
    obj.clear@RegionrelDescriptorSet;
    
    obj.val = zeros(obj.niangular*obj.niradial*obj.noangular*obj.noradial,0);
    
    obj.irad = zeros(1,0);
    obj.orad = zeros(1,0);
    
    obj.ipos = cell(1,0);
    obj.opos = cell(1,0);
end

function copyFrom(obj,obj2)
    obj.copyFrom@RegionrelDescriptorSet(obj2);
    
    obj.val = obj2.val;
    
    obj.irad = obj2.irad;
    obj.orad = obj2.orad;
    
    obj.ipos = obj2.ipos;
    obj.opos = obj2.opos;
    
    obj.niangular = obj2.niangular;
    obj.niradial = obj2.niradial;
    obj.noangular = obj2.noangular;
    obj.noradial = obj2.noradial;
end

function obj2 = clone(obj)
    obj2 = ReldisthistDescriptorSet(obj);
end

function obj2 = cloneSubset(obj,ind)
    obj2 = ReldisthistDescriptorSet;
    
    obj2.niangular = obj.niangular;
    obj2.niradial = obj.niradial;
    obj2.noangular = obj.noangular;
    obj2.noradial = obj.noradial;
    
    obj2.settings = obj.settings;
    
    obj2.append(obj,ind);
end


% this currently computes:
% for a source region A and a target region B, in each bin (i,j,k,l) the
% following percentage of paths (or triangles) with three points a,b,c:
% a = any point
% b = any point + (direction,distance) in (k,l)
% c = any point + (direction,distance) in (k,l) + (direction,distance) in (i,j)
% paths where (a and b are in A and c is in B) / (a is in A)
% - multiply by bin volume to get an average over a in A of the absolute
% number of paths where b is in A and c in B
%
% - to get an average (instead of a histogram) of the inner histogram (=
% the distribution of the vectors bc) over the space of vectors ab,
% normalize the average of each bin not by the number of postions, but by
% the total area accumulated in that bin when summing up the 4d histograms
% over A
% this would give:
% for a source region A and a target region B, in each bin (i,j,k,l) the
% following percentage of paths (or triangles) with three points a,b,c:
% a = any point
% b = any point + (direction,distance) in (k,l)
% c = any point + (direction,distance) in (k,l) + (direction,distance) in (i,j)
% paths where (a and b are in A and c is in B) / (a and b are in A)
% - multiply by bin volume to get an average over a and b in A of the
% absolute number of paths where c is in B

% todo:
% - when averaging the 4D histogram over A, use average weighted by (voronoi
% area if histogram position inside A) / (area of region A) to approximate
% the integral over A normalized by the area of A
% => for now we assume the points are approx. equally spaced in A = they
% have approx. the same voronoi area

function compute(obj,sourceregions,targetregions,imgmin,imgmax,imgres)
    obj.clear;
    
    if isempty(sourceregions)
        return;
    end
    
    % pre-filter the shape contexts at the positions, one filter for each
    % bin size (one per radial position), centered at the positions (like
    % creating an image pyramid, but same number of 'pixels' in each layer)
    % create delaunay triangulation or a scatteredInterpolant
    % interpolate the value at the center of each outer bin using the
    % pre-filtered inner shape contexts (based on radial position of outer bin)
    % also rasterize own region and multiply interpolated shape contexts
    % with bin overlap
    % => should approximate integrating the shape contexts computed at
    % every point on the region over the area of the outer bin
    % but: bins are normalized to be in [0,1] bin area is multiplied in
    % later when computing the similarity to the template
    
    ninnerbins = obj.niangular*obj.niradial;
    nouterbins = obj.noangular*obj.noradial;
    
    obj.val = zeros(obj.niangular*obj.niradial*obj.noangular*obj.noradial,numel(sourceregions));
    obj.irad = zeros(1,numel(sourceregions));
    obj.orad = zeros(1,numel(sourceregions));
    obj.ipos = cell(1,numel(sourceregions));
    obj.opos = cell(1,numel(sourceregions));
    for i=1:numel(sourceregions)
        
        if isempty(sourceregions{i})
            error('Empty source region.');
        end
        
        sourcepolys = ImageAnnotation.an2imcoords([sourceregions{i}.polygon],imgmin,imgmax);
        
        % get position and radius of outer histograms in the region
        if obj.settings.opointdensity == 1
            % get source region polygons without self-intersections
            mergedsourcepolyx = {sourcepolys{1}(1,:)};
            mergedsourcepolyy = {sourcepolys{1}(2,:)};
            if numel(sourcepolys) == 1
                [mergedsourcepolyx,mergedsourcepolyy] = polybool('union',...
                    mergedsourcepolyx,mergedsourcepolyy,...
                    mergedsourcepolyx,mergedsourcepolyy);
            elseif numel(sourcepolys) > 1
                for j=2:numel(sourcepolys) % also merge first polygon with itself to get rid of self-intersections
                    [mergedsourcepolyx,mergedsourcepolyy] = polybool('union',...
                        mergedsourcepolyx,mergedsourcepolyy,...
                        sourcepolys{j}(1,:),sourcepolys{j}(2,:));
                end
            end
            
            % one outer histogram at the region centroid
            suma = 0;
            oposl = zeros(2,1);
            for j=1:numel(mergedsourcepolyx)
                [cx,cy,a] = polygonCentroid(mergedsourcepolyx{j},mergedsourcepolyy{j});
                oposl = oposl + [cx;cy] .* a; % inner contours (holes) are counter-clockwise and weighted by negative area
                suma = suma + a;
            end
            opossize = suma;
            oposl = oposl ./ suma;

%             allsrcpolypts = [sourcepolys{i}{:}];
%             oposl = minboundcircle(allsrcpolypts(1,:)',allsrcpolypts(2,:)');
%             oposl = oposl';
            
%             % radius is set to the maximum distance from centroid of the region
%             allpoints = [sourcepolys{:}];
%             sqdists = ...
%                 (oposl(1)-allpoints(1,:)).^2 + ...
%                 (oposl(2)-allpoints(2,:)).^2;
%             oradl = sqrt(max(sqdists));
            
            if not(isempty(obj.settings.fixedrad))
                oradl = ImageAnnotation.an2imradius(obj.settings.fixedrad);
            else
                % radius is set to the span of the region (maximally distant points)
                allpoints = [sourcepolys{:}];
                sqdists = ...
                    bsxfun(@minus,allpoints(1,:)',allpoints(1,:)).^2 + ...
                    bsxfun(@minus,allpoints(2,:)',allpoints(2,:)).^2;
                oradl = sqrt(max(sqdists(:))) * obj.settings.oradf;
%                 oradl = sqrt(max(sqdists(:)));
%                 oradl = sqrt(max(sqdists(:))) * 0.5; % temp1
%                 oradl = sqrt(max(sqdists(:))) * 1.5; % temp1
%                 oradl = sqrt(max(sqdists(:))) * 2; % temp1
            end
        else
            [oposl,~,~,~,opossize] = regiongridsamples(sourceregions{i},imgmin,imgmax,obj.settings.opointdensity);
            
            if not(isempty(obj.settings.fixedrad))
                oradl = ImageAnnotation.an2imradius(obj.settings.fixedrad);
            else
                % radius is set to the span of the region (maximally distant points)
                allpoints = [sourcepolys{:}];
                sqdists = ...
                    bsxfun(@minus,allpoints(1,:)',allpoints(1,:)).^2 + ...
                    bsxfun(@minus,allpoints(2,:)',allpoints(2,:)).^2;
                oradl = sqrt(max(sqdists(:)));                
            end
        end
        obj.opos{i} = ImageAnnotation.im2ancoords(oposl,imgmin,imgmax);
        obj.orad(i) = ImageAnnotation.im2anradius(oradl,imgmin,imgmax);        
        
        % get position and radius of inner histograms
        [iposl,iposnx,iposny,iposmask] = regiongridsamples(sourceregions{i},imgmin,imgmax,obj.settings.ipointdensity);
        obj.ipos{i} = ImageAnnotation.im2ancoords(iposl,imgmin,imgmax);
        obj.irad(i) = obj.orad(i) * obj.settings.iradf;
%         obj.irad(i) = obj.orad(i).*0.5;
%         obj.irad(i) = obj.orad(i).*0.25; % temp1
%         obj.irad(i) = obj.orad(i).*0.75; % temp1
%         obj.irad(i) = obj.orad(i).*1; % temp1
        iradl = ImageAnnotation.an2imradius(obj.irad(i),imgmin,imgmax);
        
        if isempty(targetregions{i}) || isempty(iposl) || isempty(oposl)
            continue;
        end
        
        % compute geometric properties of histogram
        [iangularpos,iradialpos,oangularpos,oradialpos] = ReldisthistDescriptorSet.binaxes(...
            obj.niangular,obj.niradial,obj.noangular,obj.noradial,iradl,oradl,...
            obj.settings.iradlin,obj.settings.oradlin);

        [~,oredges] = polarhistedges2D(oangularpos,oradialpos,0);

        % get approximate bin width and height and area of all outer bins
        binrwidths = diff(oredges);
        binareas = (pi.*oredges.^2) ./ obj.noangular;
        binareas = diff(binareas);
        binawidths = binareas ./ binrwidths;
        if max(max(binawidths ./ binrwidths, binrwidths ./ binawidths)) > 3
            warning([...
                'Bin is very skewed (factor ',...
                num2str(max(max(binawidths ./ binrwidths, binrwidths ./ binawidths))),...
                ', where 1 is unskewed), gaussian scale space approximation ',...
                'of the integral over the bin might be inaccurate.']);
        end

        % get volume of each 4d bin
        [binvol,ibinarea,obinarea] = obj.binvolumes(iangularpos,iradialpos,oangularpos,oradialpos);
        idescarea = sum(ibinarea); %#ok<NASGU>
        odescarea = sum(obinarea);
        descvol = sum(binvol); %#ok<NASGU>

        % approximate source region area as sum of area of rectangular
        % vornoi-like cells around outer positions
        sourceregionarea = prod(opossize).*size(oposl,2);

        % compute point descriptors
        % todo: compute just once and save which region each descriptor
        % belongs to (more efficient)
        idescs = PointrelDescriptorSet(...
            obj.niangular,obj.niradial,...
            obj.irad(i),obj.ipos{i},...
            targetregions{i},...
            imgmin,imgmax,imgres,...
            'normalize',obj.settings.normalizeinner,...
            'radlin',obj.settings.iradlin);
        
        obincenters = polarhistbins2D(oangularpos,oradialpos,oposl);
        
        % if doing a c++ implementation, just average the inner histograms
        % directly using a gaussian of appropriate size at each bin center
        % (complexity is increased by a constant factor equal to the number
        % of outer bins)
        
        % -----------------------
        
        % todo: find non-zero area of largest filter of ipixmask i.e. add
        % filtersize to imgmin_pix and imgmax_pix to get
        % imgmin_pixfilt,imgmax_pixfilt and also compute
        % imgmin_filt,imgmax_filt (in non-pixel coordinates), then any bins
        % outside the rectangle will certainly be zero.
        % to compute obinnnsubs use imgmin_filt, imgmax_filt instead of
        % imgmin,imgmax
        % create scale space
        imgsize = imgmax-imgmin;
        gsdevs = sqrt(binareas./(2*pi));
        if imgsize(1) > imgsize(2)
            pixsize = imgsize(1) / (iposnx-1);
        else
            pixsize = imgsize(2) / (iposny-1);
        end
        gsdevs_pix = gsdevs ./ pixsize;
        filtersizes = 2.*ceil(2.*gsdevs_pix)+1; % filter radius = 2 * std dev.
        
        regionmask = reshape(iposmask,iposny,iposnx);
        % new region mask min and max in old region mask pixel coordinates
        regionmask_newmin_pix = [...
            find(any(regionmask,1),1,'first')-(filtersizes(end)-1)/2;...
            find(any(regionmask,2),1,'first')-(filtersizes(end)-1)/2];
        regionmask_newmax_pix = [...
            find(any(regionmask,1),1,'last')+(filtersizes(end)-1)/2;...
            find(any(regionmask,2),1,'last')+(filtersizes(end)-1)/2];
        ssnx = regionmask_newmax_pix(1) - regionmask_newmin_pix(1) + 1;
        ssny = regionmask_newmax_pix(2) - regionmask_newmin_pix(2) + 1;
        
        ssmin = ((regionmask_newmin_pix-1) ./ ([iposnx;iposny]-1)) .* ...
            (imgmax-imgmin) + imgmin;
        ssmax = ((regionmask_newmax_pix-1) ./ ([iposnx;iposny]-1)) .* ...
            (imgmax-imgmin) + imgmin;

        regionmask_oldmin_pix = [1;1] + ([1;1] - regionmask_newmin_pix);
        regionmask_oldmax_pix = regionmask_oldmin_pix + [iposnx;iposny] -1;
        
%         regionmask_new_pixind = regionmask_oldmin_pix(2):regionmask_oldmax_pix(2);
        regionmask_old = regionmask;
        regionmask = false(ssny,ssnx);
        regionmask(...
            max(1,regionmask_oldmin_pix(2)):min(ssny,regionmask_oldmax_pix(2)),...
            max(1,regionmask_oldmin_pix(1)):min(ssnx,regionmask_oldmax_pix(1))) = ...
            regionmask_old(...
            max(1,regionmask_newmin_pix(2)):min(iposny,regionmask_newmax_pix(2)),...
            max(1,regionmask_newmin_pix(1)):min(iposnx,regionmask_newmax_pix(1)));
        clear regionmask_old;
        
%         scalespacemin_pix = imgmin-filtersizes(end)
        singlebinlayer = zeros(ssny,ssnx);
        scalespace = zeros(ninnerbins,ssny*ssnx,numel(gsdevs)+1);
        scalespace_regionmask = zeros(1,ssny*ssnx,numel(gsdevs)+1);
        scalespace(:,regionmask,1) = idescs.val;
        scalespace_regionmask(1,:,1) = regionmask(:);
        for j=1:numel(gsdevs)
            filterkernel = fspecial('gaussian',[filtersizes(j),filtersizes(j)],gsdevs_pix(j));
            for k=1:ninnerbins
                singlebinlayer(regionmask) = idescs.val(k,:);
                
%                 scalespace(k,:,j+1) = reshape(imgaussfilt(singlebinlayer,gsdev_pix),1,[]);
                scalespace(k,:,j+1) = reshape(imfilter(...
                    singlebinlayer,filterkernel),1,[]);
            end
%             scalespace_regionmask(1,:,j+1) = reshape(imgaussfilt(binimage_regionmask,gsdev_pix),1,[]);
            scalespace_regionmask(1,:,j+1) = reshape(imfilter(...
                double(regionmask),filterkernel),1,[]);
        end
        
        delete(idescs(isvalid(idescs)));
        clear regionmask;
        
        obinnnsubs = round([...
            ((obincenters(2,:) - ssmin(2)) ./ (ssmax(2)-ssmin(2))) .* (ssny-1) + 1;...    
            ((obincenters(1,:) - ssmin(1)) ./ (ssmax(1)-ssmin(1))) .* (ssnx-1) + 1]);
        % all bin centers outside the rectangular region are bins where the
        % largest gaussian has no overlap with the region
        obininregion = all(obinnnsubs >= 1,1) & all(bsxfun(@le, obinnnsubs, [ssny;ssnx]),1);
        
%         % for each bin outside: find nearest neighbor in region
%         % (gaussian weighted average approaches nearest-neighbor if all
%         % data points are very far away relative to the gaussian std. dev.)
%         % => not true, for points that are colinear in a line orthogonal
%         % to direction to center keep their relative weights as the
%         % descriptor moves away
%         not(obininregion)
%         obinnnregioninds(obininregion) = knnsearch(iposl',obincenters(:,not(obininregion))');
%         obinpixinds = find(scalespace_regionmask(1,:,:));

        % use nearest neighbor in the top layer of the scale space for bin
        % centers outside the scale space (at least there are no discontinuities)
        % => problem: some pixles inside the top layer may also be zero
        % => find closest non-zero pixel inside the top layer?
        
        
        obinnninds = nan(nouterbins,size(oposl,2));
        obinnninds(obininregion) = sub2ind([ssny,ssnx],...
            obinnnsubs(1,obininregion),obinnnsubs(2,obininregion));
        
        % get scale index of each outer bin (index of the ring)
        % first scale is unfiltered
        obinscaleinds = reshape(repmat(1:obj.noradial,obj.noangular,size(oposl,2)),[],size(oposl,2))+1;
        
        % get indices of the nearest neighbor in scale space for all outer
        % bin centers
        obinssinds = nan(nouterbins,size(oposl,2));
        obinssinds(obininregion) = sub2ind(...
            [size(scalespace,2),size(scalespace,3)],...
            obinnninds(obininregion),obinscaleinds(obininregion));
        
        % get overlap with the source region of all outer bins
        % (approximated as the integral of the part of the correspoding
        % normal distribution that is inside the region) * bin area
        obinroverlap = zeros(nouterbins,size(oposl,2));
        obinroverlap(obininregion) = scalespace_regionmask(obinssinds(obininregion));
        % in region if more than 0.001% of the gaussian volume (which
        % approximates the normalized bin overlap) is inside the region
        obininregion = obinroverlap > 0.00001;
        obinarea = repmat(obinarea,1,size(oposl,2));
        obinroverlap = obinroverlap.*obinarea;
        
        % approximate voronoi cell of outer position inside the region as
        % source region area divided by number of outer position (positions
        % are assumed to be distributed equally in the region)
        oposarea = repmat(sourceregionarea / size(oposl,2),1,size(oposl,2));
        batchsize = max(1,floor(obj.settings.batchsize/(ninnerbins*nouterbins)));
        for j=1:batchsize:size(oposl,2)
            batchoposind = j:min(j+batchsize-1,size(oposl,2));
            
%             batchnninds = obinnninds(:,batchoposind);
            batchinregion = obininregion(:,batchoposind);
%             batchscaleinds = obinscaleinds(:,batchoposind);
            batchroverlap = obinroverlap(:,batchoposind);
            batchssinds = obinssinds(:,batchoposind);
            batcharea = obinarea(:,batchoposind);
            
            v = zeros(ninnerbins,nouterbins*numel(batchoposind));
            v(:,batchinregion) = scalespace(:,batchssinds(batchinregion));
            
            % multiply in batch area since gaussian factors in only the
            % normalized overlap of gaussian with region
            v(:,batchinregion) = bsxfun(@times,v(:,batchinregion),batcharea(batchinregion)');
            
            % normalize outer histogram
            if strcmp(obj.settings.normalizeouter,'none')
                % do nothing
            elseif strcmp(obj.settings.normalizeouter,'binarea')
                v = bsxfun(@rdivide,v,obinarea(:)');
            elseif strcmp(obj.settings.normalizeouter,'binregionoverlap')
                mask = batchroverlap > 0;
                v(:,mask) = bsxfun(@rdivide,v(:,mask),batchroverlap(mask)');
            elseif strcmp(obj.settings.normalizeouter,'descarea')
                v = v ./ odescarea;
            elseif strcmp(obj.settings.normalizeouter,'descsum')
                v = reshape(v,[],numel(batchoposind));
                descsum = sum(v,1);
                mask = descsum > 0;
                v(:,mask) = bsxfun(@rdivide,v(:,mask),descsum(mask));
                v = reshape(v,ninnerbins,[]);
            elseif strcmp(obj.settings.normalizeouter,'descnormsum')
                v = bsxfun(@rdivide,v,obinarea(:)');
                
                v = reshape(v,[],numel(batchoposind));
                descsum = sum(v,1);
                mask = descsum > 0;
                v(:,mask) = bsxfun(@rdivide,v(:,mask),descsum(mask));
                v = reshape(v,ninnerbins,[]);
            else
                error('Unknown outer histogram normalization method.');
            end
            
            v = reshape(v,[],numel(batchoposind));
            
            obj.val(:,i) = obj.val(:,i) + sum(bsxfun(@times,v,oposarea(batchoposind)),2);
        end
        
        % normalize integral of 4d histograms over region
        if strcmp(obj.settings.normalizeregion,'none')
            % do nothing
        elseif strcmp(obj.settings.normalizeregion,'binregionoverlapsum')
            v = reshape(obj.val(:,i),[],nouterbins);
            binoverlapsum = sum(bsxfun(@times,obinroverlap,oposarea),2);
            mask = binoverlapsum > 0;
            v(:,mask) = bsxfun(@rdivide,v(:,mask),binoverlapsum(mask)');
            obj.val(:,i) = reshape(v,[],1);
            
            % for each bin outside: use gaussian with std.deviation set just
            % high enought so filtersize reaches some pixels in the region mask
            farobininds = find(not(mask));
            obj.val(:,i) = obj.setFarObins(...
                obj.val(:,i),scalespace,scalespace_regionmask,...
                farobininds,obinnnsubs,ninnerbins,ssnx,ssny); %#ok<FNDSB>

        elseif strcmp(obj.settings.normalizeregion,'regionarea')
            obj.val(:,i) = obj.val(:,i) ./ sum(oposarea);
        elseif strcmp(obj.settings.normalizeregion,'descsum')
            descsum = sum(obj.val(:,i));
            if descsum > 0
                obj.val(:,i) = obj.val(:,i) ./ descsum;
            end
        elseif strcmp(obj.settings.normalizeregion,'binregionoverlapsumdescsum')
            v = reshape(obj.val(:,i),[],nouterbins);
            binoverlapsum = sum(bsxfun(@times,obinroverlap,oposarea),2);
            mask = binoverlapsum > 0;
            v(:,mask) = bsxfun(@rdivide,v(:,mask),binoverlapsum(mask)');
            obj.val(:,i) = reshape(v,[],1);
            
            % for each bin outside: use gaussian with std.deviation set just
            % high enought so filtersize reaches some pixels in the region mask
            farobininds = find(not(mask));
            obj.val(:,i) = obj.setFarObins(...
                obj.val(:,i),scalespace,scalespace_regionmask,...
                farobininds,obinnnsubs,ninnerbins,ssnx,ssny); %#ok<FNDSB>
            
            descsum = sum(obj.val(:,i));
            if descsum > 0
                obj.val(:,i) = obj.val(:,i) ./ descsum;
            end
%         elseif strcmp(obj.settings.normalizeregion,'regionareadescsum')
%             obj.val(:,i) = obj.val(:,i) ./ sum(oposarea);
%             
%             descsum = sum(obj.val(:,i));
%             if descsum > 0
%                 obj.val(:,i) = obj.val(:,i) ./ descsum;
%             end
        else
            error('Unknown outer histogram normalization method.');
        end
        
        % -----------------------
        
%         % create scale space of increasingly gaussian-filtered samples of
%         % inner histograms (e.g. if samples form a grid, it is the same as
%         % creating a gaussian scale-space of an image with 'ninnerbins'
%         % color components)
%         % set std. deviation at each scale so gaussian has same integral as
%         % a bin at the scale (with height = height of gaussian = 1)
%         % (integral of isotropic bivariate gaussian is 2*pi*variance)
%         gsdevs = sqrt(binareas./(2*pi));
%         scalespace = gaussScalespace2D(iposl,idescs.val,gsdevs,3);
%         
%         delete(idescs(isvalid(idescs)));
%         
%         % linear and natural do not work, since they would give bad
%         % extrapolation close to the boundary of the region if it is not
%         % sampled with points => need reasonably well-distributed points
%         % in the region
%         
%         % find normalized overlap of outer bin areas with region
%         % (normalized by bin area)
%         odescregionoverlap = PointrelDescriptorSet(...
%             obj.noangular,obj.noradial,...
%             obj.opos{i},obj.orad(i),...
%             sourceregions(i),...
%             imgmin,imgmax,imgres,...
%             'normalize','none');        
%         obinroverlap = odescregionoverlap.val;
%         obininregion = obinroverlap > 0;
%         delete(odescregionoverlap(isvalid(odescregionoverlap)));
%         
%         % find nearest inner descriptor position for each outer bin center
%         obinnninds = nan(1,size(obincenters,2));
%         obinnninds(obininregion) = knnsearch(iposl',obincenters(:,obininregion)');
%         obinnninds = reshape(obinnninds,nouterbins,size(oposl,2));
% 
%         % get scale index of each outer bin (index of the ring)
%         obinscaleinds = reshape(repmat(1:obj.noradial,obj.noangular,size(oposl,2)),[],size(oposl,2));
%         
%         batchsize = max(1,floor(obj.settings.batchsize/(ninnerbins*nouterbins)));
%         
%         % approximate voronoi cell of outer position inside the region as
%         % source region area divided by number of outer position (positions
%         % are assumed to be distributed equally in the region)
%         oposarea = repmat(sourceregionarea / size(oposl,2),1,size(oposl,2));
%         
%         for j=1:batchsize:size(oposl,2)
%             batchoposind = j:min(j+batchsize-1,size(oposl,2));
%             
%             batchnninds = obinnninds(:,batchoposind);
%             batchinregion = obininregion(:,batchoposind);
%             batchscaleinds = obinscaleinds(:,batchoposind);
%             batchroverlap = obinroverlap(:,batchoposind);
%             batchssind = sub2ind([size(scalespace,2),size(scalespace,3)],...
%                 batchnninds(batchinregion),...
%                 batchscaleinds(batchinregion));
%             
%             v = zeros(ninnerbins,nouterbins*numel(batchoposind));
%             v(:,batchinregion) = scalespace(:,batchssind);
%             v(:,batchinregion) = bsxfun(@times,v(:,batchinregion),batchroverlap(batchinregion)');
%             
%             % normalize outer histogram
%             if strcmp(obj.settings.normalizeouter,'none')
%                 % do nothing
%             elseif strcmp(obj.settings.normalizeouter,'binarea')
%                 normfac = bsxfun(@rdivide,ones(size(batchroverlap)),obinarea);
%                 v = bsxfun(@times,v,normfac(:)');
%             elseif strcmp(obj.settings.normalizeouter,'binregionoverlap')
%                 mask = batchroverlap > 0;
%                 v(:,mask) = bsxfun(@rdivide,v(:,mask),batchroverlap(mask)');
%             elseif strcmp(obj.settings.normalizeouter,'descarea')
%                 v = v ./ odescarea;
%             elseif strcmp(obj.settings.normalizeouter,'descsum')
%                 v = reshape(v,[],numel(batchoposind));
%                 descsum = sum(v,1);
%                 mask = descsum > 0;
%                 v(:,mask) = bsxfun(@rdivide,v(:,mask),descsum(mask));
%                 v = reshape(v,ninnerbins,[]);
%             elseif strcmp(obj.settings.normalizeouter,'descnormsum')
%                 normfac = bsxfun(@rdivide,ones(size(batchroverlap)),obinarea);
%                 v = bsxfun(@times,v,normfac(:)');
%                 
%                 v = reshape(v,[],numel(batchoposind));
%                 descsum = sum(v,1);
%                 mask = descsum > 0;
%                 v(:,mask) = bsxfun(@rdivide,v(:,mask),descsum(mask));
%                 v = reshape(v,ninnerbins,[]);
%             else
%                 error('Unknown outer histogram normalization method.');
%             end
%             
%             v = reshape(v,[],numel(batchoposind));
%             
%             obj.val(:,i) = obj.val(:,i) + sum(bsxfun(@times,v,oposarea(batchoposind)),2);
%         end
%         
%         % normalize integral of 4d histograms over region
%         if strcmp(obj.settings.normalizeregion,'none')
%             % do nothing
%         elseif strcmp(obj.settings.normalizeregion,'binregionoverlapsum')
%             v = reshape(obj.val(:,i),[],nouterbins);
%             binoverlapsum = sum(bsxfun(@times,obinroverlap,oposarea),2);
%             mask = binoverlapsum > 0;
%             v(:,mask) = bsxfun(@rdivide,v(:,mask),binoverlapsum(mask)');
%             obj.val(:,i) = reshape(v,[],1);
%         elseif strcmp(obj.settings.normalizeregion,'regionarea')
%             obj.val(:,i) = obj.val(:,i) ./ sum(oposarea);
%         elseif strcmp(obj.settings.normalizeregion,'descsum')
%             descsum = sum(obj.val(:,i));
%             if descsum > 0
%                 obj.val(:,i) = obj.val(:,i) ./ descsum;
%             end
%         elseif strcmp(obj.settings.normalizeregion,'binregionoverlapsumdescsum')
%             v = reshape(obj.val(:,i),[],nouterbins);
%             binoverlapsum = sum(bsxfun(@times,obinroverlap,oposarea),2);
%             mask = binoverlapsum > 0;
%             v(:,mask) = bsxfun(@rdivide,v(:,mask),binoverlapsum(mask)');
%             obj.val(:,i) = reshape(v,[],1);
%             
%             descsum = sum(obj.val(:,i));
%             if descsum > 0
%                 obj.val(:,i) = obj.val(:,i) ./ descsum;
%             end
% %         elseif strcmp(obj.settings.normalizeregion,'regionareadescsum')
% %             obj.val(:,i) = obj.val(:,i) ./ sum(oposarea);
% %             
% %             descsum = sum(obj.val(:,i));
% %             if descsum > 0
% %                 obj.val(:,i) = obj.val(:,i) ./ descsum;
% %             end
%         else
%             error('Unknown outer histogram normalization method.');
%         end
        
        
        % -----------------------
        
%         % todo: could also replace this with test if the bincenter is
%         % inside a pixel mask
%         sourcepolys = sourceregions(i).polygon;
%         for j=1:numel(sourcepolys)
%             sourcepolys{j} = bsxfun(@plus,...
%                 bsxfun(@times,sourcepolys{j},imgmax-imgmin),...
%                 imgmin);    
%         end
%         inregionmask = pointInPolygon_mex(...
%             bincenters(1,:),bincenters(2,:),...
%             sourcepolys,'winding'); % crossing
%         inregionmask = any(inregionmask,2)';
%         
%         bincentersx = bincenters(1,:);
%         bincentersx = reshape(bincentersx,nouterbins,size(opos,2));
%         bincentersy = bincenters(2,:);
%         bincentersy = reshape(bincentersy,nouterbins,size(opos,2));
%         inregionmask = reshape(inregionmask,nouterbins,size(opos,2));
%         
%         % todo: handle cases where size(pos,2) < 3
%         
%         interpolant = scatteredInterpolant(ipos(1,:)',ipos(2,:)',scalespace(1,:,1)','nearest','nearest');
%         binoffset = 0;
%         obinoffset = 0;
%         v = zeros(obj.noangular,size(opos,2));
%         vx = zeros(obj.noangular,size(opos,2));
%         vy = zeros(obj.noangular,size(opos,2));
%         for j=1:size(scalespace,3)
%             bininds = binoffset+1 : ninnerbins : binoffset+ninnerbins*obj.noangular;
%             obininds = obinoffset+1 : obinoffset+obj.noangular; 
%             
%             for k=1:ninnerbins
%                 % for all outer bins in ring j, sum over all points the
%                 % interpolated value for inner bin k
%                 
%                 interpolant.Values = scalespace(k,:,j)';
%                 
%                 vx(:) = bincentersx(obininds,:);
%                 vy(:) = bincentersy(obininds,:);
%                 
%                 v(:) = 0;
%                 v(inregionmask(obininds,:)) = interpolant(...
%                     vx(inregionmask(obininds,:)),...
%                     vy(inregionmask(obininds,:)));
%                 
%                 obj.val(bininds,i) = sum(v,2);
%                 
%                 bininds = bininds + 1;
%             end
%             
%             binoffset = binoffset+ninnerbins*obj.noangular;
%             obinoffset = obinoffset+obj.noangular;
%         end
%         obj.val(:,i) = obj.val(:,i)./size(opos,2); % normalize with number of points (scale invariance)
        
%         % normalize with number positions inside the region
%         % => not good, also need to know how many points are added to an
%         % outer bin, which would be factored out here
%         binoffset = 0;
%         for j=1:nouterbins
%             bininds = binoffset+1:binoffset+ninnerbins;
%             obj.val(bininds,i) = obj.val(bininds,i)./sum(inregionmask(j,:));
%             binoffet = binoffet+ninnerbins;
%         end

        % -----------------------
        
    end
    
end

function val = setFarObins(...
        obj,val,scalespace,scalespace_regionmask,...
        farobininds,obinnnsubs,ninnerbins,ssnx,ssny)
    
    % for each bin outside: use gaussian with std.deviation set just
    % high enought so filtersize reaches some pixels in the region mask
    regionmask = reshape(scalespace_regionmask(1,:,1),ssny,ssnx);
    [regionmaski,regionmaskj] = find(regionmask);
    for j=1:numel(farobininds)
        obini = obinnnsubs(1,farobininds(j));
        obinj = obinnnsubs(2,farobininds(j));
        bininds = (farobininds(j)-1)*ninnerbins+1:farobininds(j)*ninnerbins;

        frad = min(max(...
            abs(obini - regionmaski),...
            abs(obinj - regionmaskj)));
        fsize = frad*2+1;
        sdev = frad/2;

        fkernel = fspecial('gaussian',[fsize,fsize],sdev);

        % filter position in image coordinates
        fmin = [obini;obinj]-frad;
        fmax = [obini;obinj]+frad;
        [imgfi,imgfj] = ndgrid(...
            max(fmin(1),1):min(fmax(1),ssny),...
            max(fmin(2),1):min(fmax(2),ssnx));
        imgfind = sub2ind([ssny,ssnx],imgfi(:),imgfj(:));

        % image position in filter coordinates
        imin = ([1;1]-([obini;obinj]-frad)) + [1;1];
        imax = imin + [ssny;ssnx] - 1;
        [filterii,filterij] = ndgrid(...
            max(imin(1),1):min(imax(1),fsize),...
            max(imin(2),1):min(imax(2),fsize));
        filteriind = sub2ind([fsize,fsize],filterii(:),filterij(:));

        normregionoverlap = scalespace_regionmask(1,imgfind,1) * fkernel(filteriind);

        if normregionoverlap <= 0
            error('This should not happen.');
        end

        val(bininds) = ...
            (scalespace(:,imgfind,1) * fkernel(filteriind)) ./ ...
            normregionoverlap;
    end
end

function [d,dbin] = distance(obj,template,val,method)
    
    binweights = ones(size(val,1),1);
    
    params = cell(1,0);
    if strcmp(method,'EMD')
        params{end+1} = ReldisthistDescriptorSet.bindistance(...
            obj.niangular,obj.niradial,obj.noangular,obj.noradial,...
            obj.settings.iradlin,obj.settings.oradlin);
    end
    
    d = zeros(size(template,2),size(val,2));
    for i=1:size(template,2)
        if nargout >= 2
            [d(i,:),dbin] = weighteddist(template(:,i),val,binweights,method,params{:});
        else
            d(i,:) = weighteddist(template(:,i),val,binweights,method,params{:});
        end
    end
end

function [s,sbin] = similarity(obj,template,val,method)
    
    binweights = ones(size(val,1),1);
    
%     [iangularpos,iradialpos,oangularpos,oradialpos] = ReldisthistDescriptorSet.binaxes(...
%             obj.niangular,obj.niradial,obj.noangular,obj.noradial,1,1);
    
%     % factor in bin volume weight
%     binvol = ReldisthistDescriptorSet.binvolumes(iangularpos,iradialpos,oangularpos,oradialpos);
%     binvol = binvol ./ sum(binvol); % divide by total descriptor area
%     binweights = binweights .* binvol;
    
%     % factor in bin distance weight => probably not good
%     ibindist = repmat(iradialpos,numel(iangularpos),1);
%     ibindist = ibindist(:)./iradialpos(end);
%     obindist = repmat(oradialpos,numel(oangularpos),1);
%     obindist = obindist(:)./oradialpos(end);
%     bindist = bsxfun(@times,ibindist',obindist);
%     binweights = binweights .* (1./bindist(:));

    params = cell(1,0);
    if strcmp(method,'negativeEMD')
        params{end+1} = ReldisthistDescriptorSet.bindistance(...
            obj.niangular,obj.niradial,obj.noangular,obj.noradial,...
            obj.settings.iradlin,obj.settings.oradlin);
    end
    
    s = zeros(size(template,2),size(val,2));
    for i=1:size(template,2)
        if nargout >= 2
            [s(i,:),sbin] = weightedsim(template(:,i),val,binweights,method,params{:});
        else
            s(i,:) = weightedsim(template(:,i),val,binweights,method,params{:});
        end
    end
end

function b = iscompatible(obj,obj2)
    b = obj.niangular == obj2.niangular && ...
        obj.niradial == obj2.niradial && ...
        obj.noangular == obj2.noangular && ...
        obj.noradial == obj2.noradial && ...
        isequaln(obj.settings,obj2.settings);
end

function append(obj,obj2,ind2)
    if not(iscompatible(obj,obj2))
        error('Descriptors are not compatible.');
    end
    
    if nargin < 3
        ind2 = 1:size(obj2.val,2);
    end

    obj.val = [obj.val,obj2.val(:,ind2)];
    obj.irad = [obj.irad,obj2.irad(:,ind2)];
    obj.orad = [obj.orad,obj2.orad(:,ind2)];
    obj.ipos = [obj.ipos,obj2.ipos(:,ind2)];
    obj.opos = [obj.opos,obj2.opos(:,ind2)];
end

function keepSubset(obj,ind)
    obj.val = obj.val(:,ind);
    obj.irad = obj.irad(:,ind);
    obj.orad = obj.orad(:,ind);
    obj.ipos = obj.ipos(:,ind);
    obj.opos = obj.opos(:,ind);
end

function deleteSubset(obj,ind)
    obj.val(:,ind) = [];
    obj.irad(:,ind) = [];
    obj.orad(:,ind) = [];
    obj.ipos(:,ind) = [];
    obj.opos(:,ind) = [];
end

function copySubset(obj,ind,obj2,ind2)
    if not(iscompatible(obj,obj2))
        error('Descriptors are not compatible.');
    end
    
    if nargin < 4 || isempty(ind2)
        ind2 = 1:size(obj2.val,2);
    end
    
    obj.val(:,ind) = obj2.val(:,ind2);
    obj.irad(:,ind) = obj2.irad(:,ind2);
    obj.orad(:,ind) = obj2.orad(:,ind2);
    obj.ipos(:,ind) = obj2.ipos(:,ind2);
    obj.opos(:,ind) = obj2.opos(:,ind2);
end

end % methods

% methods(Access=protected)
% 
% function innerpointPyramid(obj,val,pos,gsdevs)
%     
% 
% end
%     
% % function 
% % end
% 
% end

methods(Static)
    
function [iangularpos,iradialpos,oangularpos,oradialpos] = binaxes(...
        niangular,niradial,noangular,noradial,irad,orad,iradlin,oradlin)
    [iangularpos,iradialpos] = PointrelDescriptorSet.binaxes(niangular,niradial,irad,iradlin);
    [oangularpos,oradialpos] = logpolarhistaxes2D(noangular,noradial,0,orad,oradlin);

%     [iangularpos,iradialpos] = PointrelDescriptorSet.binaxes(niangular,niradial,irad);
%     [oangularpos,oradialpos] = logpolarhistaxes2D(noangular,noradial,0,orad);
%     [oangularpos,oradialpos] = logpolarhistaxes2D(noangular,noradial,0,orad,0.5); % temp1
end

function [binvol,ibinarea,obinarea] = binvolumes(iangularpos,iradialpos,oangularpos,oradialpos)
    % 4d bin volume = product of inner and outer bin areas
    [ibinverts,ibinfaces] = meshPolarhist2D(...
        iangularpos,iradialpos,...
        [],[],64);
	ibinarea = polyarea(...
        reshape(ibinverts(1,ibinfaces),size(ibinfaces,1),[]),...
        reshape(ibinverts(2,ibinfaces),size(ibinfaces,1),[]));
    [obinverts,obinfaces] = meshPolarhist2D(...
        oangularpos,oradialpos,...
        [],[],64);
	obinarea = polyarea(...
        reshape(obinverts(1,obinfaces),size(obinfaces,1),[]),...
        reshape(obinverts(2,obinfaces),size(obinfaces,1),[]));
    binvol = bsxfun(@times,ibinarea',obinarea);
    binvol = binvol(:);
    ibinarea = ibinarea(:);
    obinarea = obinarea(:);
end

% should be independent of radius
function d = bindistance(niangular,niradial,noangular,noradial,iradlin,oradlin)
    
    ntotalbins = niangular*niradial*noangular*noradial;
    
    [iangularpos,iradialpos,oangularpos,oradialpos] = ReldisthistDescriptorSet.binaxes(...
            niangular,niradial,noangular,noradial,1,1,iradlin,oradlin);

    % todo: this could also be computed once and stored in a file (if that
    % is actually faster)
    
    % matrix can get very large for larger descriptors
    if ntotalbins^2 > 20000000
        % more than 20 million bin distance entries (you get something
        % close to this e.g. when using 8x8x8x8 histograms)
        error('Bin distance matrix is too large.');
    end
        
%     [aedges,redges] = polarhistedges2D(iangularpos,iradialpos);
    
    % thresholds for the metric in each histogram dimension
    iangulardistvar = pi/2; % in radians
    iradialdistvar = 2; % in log_2 space (2 x radial distance = 1/value difference)
    oangulardistvar = pi/2; % in radians
    oradialdistvar = 2; % in log_2 space (2 x radial distance = 1/value difference)
    
    % thresholds for the metric in each histogram dimension
%     iangulardistvar = pi/2; % in radians
%     iradialdistvar = 2; % in log_2 space (2 x radial distance = 1/value difference)
%     oangulardistvar = pi/1.5; % in radians
%     oradialdistvar = 3; % in log_2 space (2 x radial distance = 1/value difference)
    
%     iangulardistvar = pi/4; % in radians
%     iradialdistvar = 1; % in log_2 space (2 x radial distance = 1/value difference)
%     oangulardistvar = pi*2; % in radians
%     oradialdistvar = 8; % in log_2 space (2 x radial distance = 1/value difference)
    
%     iangulardistvar = pi; % in radians
%     iradialdistvar = 4; % in log_2 space (2 x radial distance = 1/value difference)
%     oangulardistvar = pi/2; % in radians
%     oradialdistvar = 2; % in log_2 space (2 x radial distance = 1/value difference)
    
    % -----------

%     % threshold in each histogram dimension separately, then sum up (sum
%     % may be up to sum(dimthresholds))
% 
%     iangulardist = min(1,abs(smod(bsxfun(@minus,iangularpos,iangularpos'),-pi,pi))./iangulardistvar);
% %     iradialdist = min(1,abs(bsxfun(@minus,1:numel(iradialpos),(1:numel(iradialpos))'))./2);
%     iradialdist = min(1,abs(bsxfun(@minus,...
%         log(iradialpos)./log(2),...
%         (log(iradialpos)./log(2))'))./iradialdistvar);
%     
%     oangulardist = min(1,abs(smod(bsxfun(@minus,oangularpos,oangularpos'),-pi,pi))./oangulardistvar);
% %     oradialdist = min(1,abs(bsxfun(@minus,1:numel(oradialpos),(1:numel(oradialpos))'))./2);
%     oradialdist = min(1,abs(bsxfun(@minus,...
%         log(oradialpos)./log(2),...
%         (log(oradialpos)./log(2))'))./oradialdistvar);
% 
%     idist = bsxfun(@plus,iangulardist(:).^2,iradialdist(:)'.^2);
%     odist = bsxfun(@plus,oangulardist(:).^2,oradialdist(:)'.^2);
%     d = sqrt(bsxfun(@plus,idist(:).^2,odist(:)'.^2));

    % -----------
    
    % sum up all values, then threshold the sum
    iangulardist = abs(smod(bsxfun(@minus,iangularpos,iangularpos'),-pi,pi))./iangulardistvar;
%     iradialdist = min(1,abs(bsxfun(@minus,1:numel(iradialpos),(1:numel(iradialpos))'))./2);
    iradialdist = abs(bsxfun(@minus,...
        log(iradialpos)./log(2),...
        (log(iradialpos)./log(2))'))./iradialdistvar;
    
    oangulardist = abs(smod(bsxfun(@minus,oangularpos,oangularpos'),-pi,pi))./oangulardistvar;
%     oradialdist = min(1,abs(bsxfun(@minus,1:numel(oradialpos),(1:numel(oradialpos))'))./2);
    oradialdist = abs(bsxfun(@minus,...
        log(oradialpos)./log(2),...
        (log(oradialpos)./log(2))'))./oradialdistvar;
    
    idist = bsxfun(@plus,iangulardist(:).^2,iradialdist(:)'.^2);
    odist = bsxfun(@plus,oangulardist(:).^2,oradialdist(:)'.^2);
    d = min(2,sqrt(bsxfun(@plus,idist(:).^2,odist(:)'.^2)));
    
    % -----------
    
    % todo: try with euclidean distance between outer bin positions

    d = reshape(d,...
        numel(iangularpos),numel(iangularpos),...
        numel(iradialpos),numel(iradialpos),...
        numel(oangularpos),numel(oangularpos),...
        numel(oradialpos),numel(oradialpos));
    d = permute(d,[1,3,5,7,2,4,6,8]);
    d = reshape(d,ntotalbins,ntotalbins);
end

function distmethod = simmethod2distmethod(simmethod)
    if strcmp(simmethod,'negativeEMD')
        distmethod = 'EMD';
    elseif strcmp(simmethod,'negativeL1dist')
        distmethod = 'L1dist';
    elseif strcmp(simmethod,'negativeL2dist')
        distmethod = 'L2dist';
    elseif strcmp(simmethod,'product')
        distmethod = 'none';
    else
        error('No distance method for given similarity method found.');
    end
end

function obj = saveobj(obj)
    % do nothing for now
end

function obj = loadobj(obj)
    if isstruct(obj)
        error('Could not load the object, need to implement this conversion.');
    else
        if not(isfield(obj.settings,'oradf')) % legacy
            obj.settings.oradf = 1; % default value
        end
        if not(isfield(obj.settings,'iradf')) % legacy
            obj.settings.iradf = 0.5; % default value
        end
        if not(isfield(obj.settings,'oradlin')) % legacy
            obj.settings.oradlin = 1; % default value
        end
        if not(isfield(obj.settings,'iradlin')) % legacy
            obj.settings.iradlin = 0; % default value
        end
    end
end

end % methods(Static)

end % classdef
