classdef ImageObject < handle

properties(SetAccess=protected)
    id = 0; % id of the image object, needs to be unique within the image (0 means no id)
    label = 0; % label index of the image object (0 means no label)
    polygon = cell(1,0); % polygons that form the image region for this object;
                         % 2xn array, first row is x-, second row y-coordinate
                         % (polygons are always clockwise, since every polygon is supposed
                         % to represent an outer contour, holes are not allowed for now)
    iscrowd = false; % true if the polygon represents multiple objects,
                     %too many to annotate all individually (for example a pile of apples)
    saliency = nan; % saliency of the image object (nan means unknown)
end

properties(Dependent,SetAccess=protected)
    boundingbox;
    area; % normalized to image area (image area = 1)
end

properties(Access=protected)
    p_boundingbox = [];
    p_area = [];
end

methods
    
% obj = ImageObject()
% obj = ImageObject(obj2)
% obj = ImageObject(id,label)
% obj = ImageObject(id,label,polygon)
% obj = ImageObject(id,label,polygon,iscrowd)
function obj = ImageObject(varargin)
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'ImageObject')
        obj.copyFrom(varargin{1});
    elseif numel(varargin) == 2
        obj.setId(varargin{1});
        obj.setLabel(varargin{2});
    elseif numel(varargin) == 3
        obj.setId(varargin{1});
        obj.setLabel(varargin{2});
        obj.setPolygon(varargin{3});
    elseif numel(varargin) == 4
        obj.setId(varargin{1});
        obj.setLabel(varargin{2});
        obj.setPolygon(varargin{3});
        obj.setIscrowd(varargin{4});
    else
        error('Invalid arguments.');
    end
end

function copyFrom(obj,obj2)
    obj.id = obj2.id;
    obj.label = obj2.label;
    obj.polygon = obj2.polygon;
    obj.iscrowd = obj2.iscrowd;
    obj.area = obj2.area;
    obj.saliency = obj2.saliency;
end

function obj2 = clone(obj)
    obj2 = ImageObject(obj);
end

function setId(obj,id)
    obj.id = id;
end

function setLabel(obj,lbl)
    if not(isnumeric(lbl)) || not(isscalar(lbl))
        error('Invalid label.');
    end
    obj.label = lbl;
end

function setPolygon(obj,poly)
    if not(iscell(poly))
        error('Polygon components must be given as cell array');
    end
    
    obj.polygon = cell(1,0);
    obj.addPolygonComponents(poly);
end

function addPolygonComponents(obj,poly)
    if not(iscell(poly))
        poly = {poly};
    end
    
    % no holes allowed, all components must be cw outer contours
    cind = numel(obj.polygon)+1;
    for i=1:numel(poly)
        if size(poly{i},2) < 3
            warning('polygon with less than 3 vertices found, skipping.');
            continue;
        end
        [obj.polygon{:,cind}(1,:),obj.polygon{:,cind}(2,:)] = poly2cw(poly{i}(1,:),poly{i}(2,:));
        cind = cind+1;
    end
    
    obj.p_boundingbox = [];
end

function setIscrowd(obj,iscrowd)
    obj.iscrowd = iscrowd;
end

function update(obj)
    if isempty(obj.p_boundingbox)
        obj.updateBoundingbox;
    end
    if isempty(obj.p_area)
        obj.updateArea;
    end
end

function val = get.boundingbox(obj)
    if isempty(obj.p_boundingbox)
        obj.updateBoundingbox;
    end
    
    val = obj.p_boundingbox;
end

function val = get.area(obj)
    if isempty(obj.p_area)
        obj.updateArea;
    end
    
    val = obj.p_area;
end

function updateBoundingbox(obj)
    obj.p_boundingbox = pointsetBoundingbox(obj.polygon);
end

function updateArea(obj)
    areas = polygonArea_mex(num2cell(obj.polygon));
    obj.p_area = sum(areas);
end

end

end
