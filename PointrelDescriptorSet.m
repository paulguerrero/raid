% set of decsriptors with same size
classdef PointrelDescriptorSet < handle
    
properties
    % values are ordered: innermost ring, 2nd innermost ring, etc.
    % and counterclockwise inside each ring, starting at direction [1,0]
    val = [];
    
    % pos and rad are in normalized image coordinates (i.e. (0,0) is the
    % imgmin, (1,1) is the imgmax)
    % rad is in multiples of the largest image dimension
    pos = zeros(2,0);
    rad = 0; % radius of the last bin edge - usually proportional to span (maximum point distance) of the region
    
    nangular = 0;
    nradial = 0;
    
    settings = struct;
end

methods(Static)
function s = defaultSettings
    s = struct;
    
%     s.normalizebinarea = true; % normalize the histogram bins by the area of the bins
%     s.normalizebin = 'area';
%     s.normalizedesc = 'none';

    s.radlin = 0;

    s.normalize = 'binearea';
    % - none: no normalization (not good by itself, values depend on the
    % actual size of the image, but useful if normalization is done
    % somewhere else, e.g. if this descriptor is part of a larger
    % descriptor)
    % - binarea: area of the bins => scale invariant, each bin in [0,1]
    % (size of bins factored out)
    % - descarea: area of the descriptor => scale invariant, sum of bins in
    % [0,1]
    % - descsum: sum of descriptor values => normalized histogram
    % (distribution of target region area over the histogram, usually more
    % in larger bins)
    % - descnormsum: sum of descriptor values normalized by bin area =>
    % normalized histogram (distribution of area density over histogram,
    % size of bins factored out)
end
end

methods

% obj = PointrelDescriptorSet()
% obj = PointrelDescriptorSet(obj2)
% obj = PointrelDescriptorSet(nangular,nradial) % empty descriptor of given size
% obj = PointrelDescriptorSet(nangular,nradial,rad,pos,val) % manually set descriptor
% obj = PointrelDescriptorSet(nangular,nradial,rad,pos,targetregions,imgmin,imgmax,imgres) % compute descriptor from image regions
% obj = PointrelDescriptorSet(...,nvpairs)
function obj = PointrelDescriptorSet(varargin)
    
    obj.settings = obj.defaultSettings;
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'PointrelDescriptorSet')
        obj.copyFrom(varargin{1});
    elseif numel(varargin) == 2 || numel(varargin) >= 3 && ischar(varargin{3}) || ...
            numel(varargin) == 5 || numel(varargin) >= 6 && ischar(varargin{6}) || ...
            numel(varargin) == 8 || numel(varargin) >= 9 && ischar(varargin{9})
        
        if numel(varargin) >= 8 && not(ischar(varargin{6})) && not(ischar(varargin{3}))
            obj.settings = nvpairs2struct(varargin(9:end),obj.settings);
        elseif numel(varargin) >= 5 && not(ischar(varargin{3}))
            obj.settings = nvpairs2struct(varargin(6:end),obj.settings);
        else
            obj.settings = nvpairs2struct(varargin(3:end),obj.settings);
        end
        
        obj.nangular = varargin{1};
        obj.nradial = varargin{2};
        
        obj.val = zeros(obj.nangular*obj.nradial,0);
        
        if numel(varargin) >= 8 && not(ischar(varargin{6})) && not(ischar(varargin{3}))
            % compute from image regions
            obj.compute(varargin{3:8});
        elseif numel(varargin) >= 5 && not(ischar(varargin{3}))
            % set the descriptor manually
            if size(varargin{5},2) ~= size(varargin{3},2) || ...
               size(varargin{5},1) ~= obj.nangular*obj.nradial
                error('Invalid size for descriptor values.');
            end
            
            if not(isscalar(obj.rad))
                error('Radius must be a scalar.');
            end
            
            obj.rad = varargin{3};
            obj.pos = varargin{4};
            obj.val = varargin{5};
        end
    else
        error('Invalid input arguments.');
    end
end

function delete(obj)
    % do nothing
    obj.clear;
end

function clear(obj)
    obj.pos = zeros(2,0);
    obj.rad = 0;
    obj.val = zeros(obj.nangular*obj.nradial,0);
end

function copyFrom(obj,obj2)
    obj.val = obj2.val;
    
    obj.pos = obj2.pos;
    obj.rad = obj2.rad;
    
    obj.nangular = obj2.nangular;
    obj.nradial = obj2.nradial;
    
    obj.settings = obj2.settings;
end

function obj2 = clone(obj)
    obj2 = PointrelDescriptorSet(obj);
end

function obj2 = cloneSubset(obj,ind)
    obj2 = PointrelDescriptorSet;
    
    obj2.nangular = obj.nangular;
    obj2.nradial = obj.nradial;
    obj2.rad = obj.rad;
    
    obj2.settings = obj.settings;
    
    obj2.append(obj,ind);
end

function compute(obj,rad,pos,targetregions,imgmin,imgmax,imgres)
    obj.clear;
    
    obj.pos = pos;
    obj.rad = rad;
    posl = ImageAnnotation.an2imcoords(obj.pos,imgmin,imgmax);
    radl = ImageAnnotation.an2imradius(obj.rad,imgmin,imgmax);
    
    obj.val = zeros(obj.nangular*obj.nradial,size(posl,2));
    
    if isempty(targetregions)
        return;
    end
    
    % generate 1D bin positions along angular and radial dimension
    [angularpos,radialpos] = PointrelDescriptorSet.binaxes(...
        obj.nangular,obj.nradial,radl,obj.settings.radlin);
    
    [binverts,binfaces] = meshPolarhist2D(angularpos,radialpos,[],posl,64);
    
    % just to be safe
    if size(binfaces,2) ~= numel(obj.val)
        error('Number of bin positions does not match number of values, this should not happen.');
    end
    
    if not(isempty([targetregions.polygon]))
        obj.anpolys2binmask(binverts,binfaces,imgmin,imgmax,imgres,{targetregions.polygon});
%     elseif not(isempty([targetregions.pixinds]))
%         obj.anpix2binmask(binverts,binfaces,imgmin,imgmax,imgres,{targetregions.pixinds});
    else
        error('Region has neither polygons nor pixel indices.');
    end
end

function b = iscompatible(obj,obj2)
    b = obj.nangular == obj2.nangular && ...
        obj.nradial == obj2.nradial && ...
        obj.rad == obj2.rad && ...
        isequaln(obj.settings,obj2.settings);
end

function append(obj,obj2,ind2)
    if not(iscompatible(obj,obj2))
        error('Descriptors are not compatible.');
    end
    
    if nargin < 3
        ind2 = 1:size(obj2.val,2);
    end
    
    obj.val = [obj.val,obj2.val(:,ind2)];
    obj.pos = [obj.pos,obj2.pos(:,ind2)];
end

function keepSubset(obj,ind)
    obj.val = obj.val(:,ind);
    obj.pos = obj.pos(:,ind);
end

function deleteSubset(obj,ind)
    obj.val(:,ind) = [];
    obj.pos(:,ind) = [];
end

function copySubset(obj,ind,obj2,ind2)
    if not(iscompatible(obj,obj2))
        error('Descriptors are not compatible.');
    end
    
    if nargin < 4 || isempty(ind2)
        ind2 = 1:size(obj2.val,2);
    end
    
    obj.val(:,ind) = obj2.val(:,ind2);
    obj.pos(:,ind) = obj2.pos(:,ind2);
end

end % methods

methods(Access=protected)
    
% soft mask, each bin value in [0,1] = binarea covered by annotation polygon / binarea
function anpolys2binmask(obj,binverts,binfaces,imgmin,imgmax,imgres,anpolys)
    
    obj.val(:) = 0;
    
    % set values (polygons in 'an' are assumed to be zordered - lowest first)
    anpolys = [anpolys{end:-1:1}];
    
    if isempty(anpolys)
        return;
    end
    
%     % convert from normalized polygon coordinates in [0,1] to
%     % coordinates in [imgmin,imgmax]
%     for i=1:numel(anpolys)
%         anpolys{i} = bsxfun(@plus,...
%             bsxfun(@times,anpolys{i},imgmax-imgmin),...
%             imgmin);
%     end
    anpolys = ImageAnnotation.an2imcoords(anpolys,imgmin,imgmax);
    
    % have to be merged with matlab, may have intersecting edges
    mergedanpolyx = {anpolys{1}(1,:)};
    mergedanpolyy = {anpolys{1}(2,:)};
    if numel(anpolys) == 1
        [mergedanpolyx,mergedanpolyy] = polybool('union',...
            mergedanpolyx,mergedanpolyy,...
            mergedanpolyx,mergedanpolyy);
    elseif numel(anpolys) > 1
        for i=2:numel(anpolys) % also merge first polygon with itself to get rid of self-intersections
            [mergedanpolyx,mergedanpolyy] = polybool('union',...
                mergedanpolyx,mergedanpolyy,...
                anpolys{i}(1,:),anpolys{i}(2,:));
        end
    end
    
    binpolys = cell(1,size(binfaces,2));
    for i=1:size(binfaces,2)
        binpolys{i} = {binverts(:,binfaces(:,i))}; % no holes
    end
    
    % bin intersection area = sum intersection with outer contour polygons
    % - sum innersection with inner contour polygons (holes) of the merged
    % annotation polygons
    isouter = ispolycw(mergedanpolyx,mergedanpolyy);
    mergedanpoly = cell(1,numel(mergedanpolyx));
    for i=1:numel(mergedanpolyx)
        mergedanpoly{i} = {[mergedanpolyx{i};mergedanpolyy{i}]};
    end
    mp = polygonPolygonBoolean_mex('intersection',mergedanpoly,binpolys);
    mpa = polygonsetArea_mex(mp);
    obj.val(:) = sum(mpa(isouter,:),1) - sum(mpa(not(isouter),:),1);
    
    if strcmp(obj.settings.normalize,'none')
        % do nothing
    elseif strcmp(obj.settings.normalize,'binarea')
        binareas = polyarea(...
            reshape(binverts(1,binfaces),size(binfaces,1),[]),...
            reshape(binverts(2,binfaces),size(binfaces,1),[]));
        obj.val = obj.val ./ reshape(binareas,size(obj.val,1),size(obj.val,2));
    elseif strcmp(obj.settings.normalize,'descarea')
        binareas = polyarea(...
            reshape(binverts(1,binfaces),size(binfaces,1),[]),...
            reshape(binverts(2,binfaces),size(binfaces,1),[]));
        descareas = sum(reshape(binareas,size(obj.val,1),size(obj.val,2)),1);
        obj.val = bsxfun(@rdivide,obj.val,descareas);
    elseif strcmp(obj.settings.normalize,'descsum')
        descsum = sum(obj.val,1);
        mask = descsum > 0;
        obj.val(:,mask) = bsxfun(@rdivide,obj.val(:,mask),descsum(mask));
    elseif strcmp(obj.settings.normalize,'binareadescsum')
        binareas = polyarea(...
            reshape(binverts(1,binfaces),size(binfaces,1),[]),...
            reshape(binverts(2,binfaces),size(binfaces,1),[]));
        obj.val = obj.val ./ reshape(binareas,size(obj.val,1),size(obj.val,2));
        
%         obj.val = bsxfun(@rdivide,obj.val(:,mask),sum(obj.val,1));
        
        descsum = sum(obj.val,1);
        mask = descsum > 0;
        obj.val(:,mask) = bsxfun(@rdivide,obj.val(:,mask),descsum(mask));
    else
        error('Unknown normalization method.');
    end
end

function anpix2binmask(obj,binverts,binfaces,imgmin,imgmax,imgres,anpixinds)
    
    obj.val(:) = 0;

%     posl = bsxfun(@plus,bsxfun(@times,obj.pos,imgmax-imgmin),imgmin);
    posl = ImageAnnotation.an2imcoords(obj.pos,imgmin,imgmax);
    
    anpixinds = unique([anpixinds{:}]);
    anmask = zeros(imgres(1),imgres(2));
    anmask(anpixinds) = 1;
    
    binmask = zeros(size(anmask));
    ndescbins = obj.nangular*obj.nradial;
    binoffset = 0;
    tinybininds = zeros(1,0);
    for i=1:size(posl,2)
        
%         if mod(i,10) == 1
%             disp([num2str(i),' / ',num2str(size(posl,2))]);
%         end
        
        % clear bin mask
        binmask(:) = 0;
        
        % get bounding box of descriptor and intersect with bounding box of
        % annotation polygons to get valid area for bins
        descbbmin = posl(:,i)-obj.radialpos(end);
        descbbmax = posl(:,i)+obj.radialpos(end);
        
        if all(abs((anpolybbmin+anpolybbmax)-(descbbmin+descbbmax)) <= ...
                   (anpolybbmax-anpolybbmin)+(descbbmax-descbbmin))
            
            % find all bins inside the annotation polygon bounding box

%             % todo: this does for example not work if all annotation
%             % polygons are inside a single bin
%             descfaces = binfaces(:,binoffset+1:binoffset+ndescbins);
%             binvertsx = binverts(1,descfaces);
%             binvertsy = binverts(2,descfaces);
%             bininds = find(any(...
%                 binvertsx < anpolybbmin(1) | binvertsy < anpolybbmin(2) | ...
%                 binvertsx > anpolybbmax(1) | binvertsy > anpolybbmax(2),1));
            
            bininds = 1:ndescbins; % use all bins for now

            % rasterize all bins of the descriptor
            for j=bininds
                binmask = polygonRasterization(binmask,...
                    binverts(1,binfaces(:,j)).*scaling(1)+translation(1),...
                    binverts(2,binfaces(:,j)).*scaling(2)+translation(2),...
                    j,'winding');
            end
            
            binmask_nonzero = round(binmask(binmask>0));
            binpixcount = accumarray(binmask_nonzero,ones(size(binmask_nonzero)),[ndescbins,1]);
            
            % accumarray to count number of pixels in given bin

            % for other bins use sum(maskpixels==1) / numpixels in bin
            binmask = binmask .* anmask;
            binmask_nonzero = round(binmask(binmask>0));
            obj.val(:,i) = accumarray(binmask_nonzero,ones(size(binmask_nonzero)),[ndescbins,1]) ./ ...
                binpixcount;
            
            tinybininds = [tinybininds,binoffset + find(binpixcount==0)]; %#ok<AGROW>
            % todo: interpolate mask at bin center to get value
            % (approximation of integral over mask / bin area)
            % and also remove bins outside annotation poly bounding box
        end
        
        binoffset = binoffset + ndescbins;
    end
    
    % set value of tiny bins (bins that don't contain any pixels) to the
    % interpolated value of the bin mask at the center of the bin
    tinybinlocalinds = smod(tinybininds,ndescbins+1);
    tinybinainds = smod(tinybinlocalinds,obj.nangular+1);
    tinybinrinds = (tinybinlocalinds-tinybinainds) / numel(obj.radialpos) + 1;
    tinybindescinds = (tinybininds-tinybinlocalinds) / ndescbins + 1;
    [tinybinpos(1,:),tinybinpos(2,:)] = pol2cart(tinybinainds,tinybinrinds);
    tinybinpos = tinybinpos + posl(:,tinybindescinds);
    
    % remove tiny bins outside the image
    outsideimgmask = tinybinpos(1,:) > imgmin(1) & tinybinpos(1,:) < imgmax(1) & ...
        tinybinpos(2,:) > imgmin(2) & tinybinpos(2,:) < imgmax(2);
    tinybinpos = tinybinpos(:,outsideimgmask);
    tinybininds = tinybininds(outsideimgmask);
    
    % get interpolated bin mask value at center of tiny bins
    binmaskinterpolant = griddedInterpolant(...
        {linspace(imgmin(2),imgmax(2),imgres(2)),...
        linspace(imgmin(1),imgmax(1),imgres(1))},...
        binmask,'linear','nearest');
    obj.val(tinybininds) = binmaskinterpolant(tinybinpos(1,:),tinybinpos(2,:));
    
%     if not(obj.settings.normalizebinarea)
%         binareas = polyarea(...
%             reshape(binverts(1,binfaces),size(binfaces,1),[]),...
%             reshape(binverts(2,binfaces),size(binfaces,1),[]));
%         binareas = reshape(binareas,size(obj.val,1),size(obj.val,2));
%         obj.val = obj.val .* binareas;
%         obj.val = bsxfun(@rdivide,obj.val,sum(binareas,1));
%     end
    
    if strcmp(obj.settings.normalize,'none')
        binareas = polyarea(...
            reshape(binverts(1,binfaces),size(binfaces,1),[]),...
            reshape(binverts(2,binfaces),size(binfaces,1),[]));
        binareas = reshape(binareas,size(obj.val,1),size(obj.val,2));
        obj.val = obj.val .* binareas;
    elseif strcmp(obj.settings.normalize,'binarea')
        % do nothing (already normalized by bin area)
    elseif strcmp(obj.settings.normalize,'descarea')
        binareas = polyarea(...
            reshape(binverts(1,binfaces),size(binfaces,1),[]),...
            reshape(binverts(2,binfaces),size(binfaces,1),[]));
        binareas = reshape(binareas,size(obj.val,1),size(obj.val,2));
        obj.val = obj.val .* binareas;
        descareas = sum(binareas,1);
        obj.val = bsxfun(@rdivide,obj.val,descareas);
    elseif strcmp(obj.settings.normalize,'binareadescsum')
        % distribution of the bin values
        descsum = sum(obj.val,1);
        mask = descsum > 0;
        obj.val(:,mask) = bsxfun(@rdivide,obj.val(:,mask),descsum(mask));
    elseif strcmp(obj.settings.normalize,'descsum')
        % distribution of the bin values weighed by bin area
        
        binareas = polyarea(...
            reshape(binverts(1,binfaces),size(binfaces,1),[]),...
            reshape(binverts(2,binfaces),size(binfaces,1),[]));
        binareas = reshape(binareas,size(obj.val,1),size(obj.val,2));
        obj.val = obj.val .* binareas;

        descsum = sum(obj.val,1);
        mask = descsum > 0;
        obj.val(:,mask) = bsxfun(@rdivide,obj.val(:,mask),descsum(mask));
    else
        error('Unknown normalization method.');
    end
end

function [d,dbin] = distance(obj,template,val,method)
    
    binweights = ones(size(val,1),1);
    
    params = cell(1,0);
    if strcmp(method,'EMD')
        params{end+1} = PointrelDescriptorSet.bindistance(obj.nangular,obj.nradial,obj.settings.radlin);
    end
    
    d = zeros(size(template,2),size(val,2));
    for i=1:size(template,2)
        if nargout >= 2
            [d(i,:),dbin] = weighteddist(template(:,i),val,binweights,method,params{:});
        else
            d(i,:) = weighteddist(template(:,i),val,binweights,method,params{:});
        end
    end
end

function [s,sbin] = similarity(obj,template,val,method)
    
    binweights = ones(size(val,1),1);
    
%     [angularpos,radialpos] = PointrelDescriptorSet.binaxes(...
%             obj.nangular,obj.nradial,1);
    
%     % factor in bin area weight
%     binarea = PointrelDescriptorSet.binareas(angularpos,radialpos);
%     binarea = binarea ./ sum(binarea); % divide by total descriptor area
%     binweights = binweights .* binarea;
%     
%     % factor in bin distance weight
%     bindist = repmat(radialpos,numel(angularpos),1);
%     bindist = bindist(:)./radialpos(end);
%     binweights = binweights .* (1./bindist);
    
    params = cell(1,0);
    if strcmp(method,'negativeEMD')
        % negative earth mover's distance
        
        params{end+1} = PointrelDescriptorSet.bindistance(obj.nangular,obj.nradial);
    end
    
    s = zeros(size(template,2),size(val,2));
    for i=1:size(template,2)
        if nargout >= 2
            [s(i,:),sbin] = weightedsim(template(:,i),val,binweights,method,params{:});
        else
            s(i,:) = weightedsim(template(:,i),val,binweights,method,params{:});
        end
    end
end
    
end % methods(Access=protected)

methods(Static)
    
% radius in image coordinates! result is in image coordinates, too
function [angularpos,radialpos] = binaxes(nangular,nradial,rad,iradlin)
    [angularpos,radialpos] = logpolarhistaxes2D(nangular,nradial,0,rad,iradlin);
%     [angularpos,radialpos] = logpolarhistaxes2D(nangular,nradial,0,rad);
%     [angularpos,radialpos] = logpolarhistaxes2D(nangular,nradial,0,rad,0.5); % temp1
end

function binarea = binareas(angularpos,radialpos)
    [binverts,binfaces] = meshPolarhist2D(...
        angularpos,radialpos,...
        [],[],64);
	binarea = polyarea(...
        reshape(binverts(1,binfaces),size(binfaces,1),[]),...
        reshape(binverts(2,binfaces),size(binfaces,1),[]));
    binarea = binarea(:);
end

function d = bindistance(nangular,nradial,radlin)

    ntotalbins = nangular*nradial;
    
    [angularpos,radialpos] = PointrelDescriptorSet.binaxes(...
            niangular,niradial,1,radlin);
    
    % todo: this could also be computed once and stored in a file (if that
    % is actually faster)
    
    % matrix can get very large for larger descriptors
    if ntotalbins^2 > 20000000
        % more than 20 million bin distance entries (you get something
        % close to this e.g. when using 8x8x8x8 histograms)
        error('Bin distance matrix is too large.');
    end
    
    % thresholds for the metric in each histogram dimension
    angulardistvar = pi/2; % in radians
    radialdistvar = 2; % in log_2 space (2 x radial distance = 1/value difference)
    
    % -----------

%     % threshold in each histogram dimension separately, then sum up (sum
%     % may be up to sum(dimthresholds))
% 
%     angulardist = min(1,abs(smod(bsxfun(@minus,angularpos,angularpos'),-pi,pi))./angulardistvar);
% %     radialdist = min(1,abs(bsxfun(@minus,1:numel(radialpos),(1:numel(radialpos))'))./2);
%     radialdist = min(1,abs(bsxfun(@minus,...
%         log(radialpos)./log(2),...
%         (log(radialpos)./log(2))'))./radialdistvar);
%     
%     d = sqrt(bsxfun(@plus,angulardist(:).^2,radialdist(:)'.^2));

    % -----------
    
    % sum up all values, then threshold the sum
    angulardist = abs(smod(bsxfun(@minus,angularpos,angularpos'),-pi,pi))./angulardistvar;
%     radialdist = min(1,abs(bsxfun(@minus,1:numel(radialpos),(1:numel(radialpos))'))./2);
    radialdist = abs(bsxfun(@minus,...
        log(radialpos)./log(2),...
        (log(radialpos)./log(2))'))./radialdistvar;
    
    d = min(2,sqrt(bsxfun(@plus,angulardist(:).^2,radialdist(:)'.^2)));
    
    % -----------
    
    % todo: try with euclidean distance between outer bin positions

    d = reshape(d,...
        numel(angularpos),numel(angularpos),...
        numel(radialpos),numel(radialpos));
    d = permute(d,[1,3,2,4]);
    d = reshape(d,ntotalbins,ntotalbins);
end

function distmethod = simmethod2distmethod(simmethod)
    if strcmp(simmethod,'negativeEMD')
        distmethod = 'EMD';
    elseif strcmp(simmethod,'negativeL1dist')
        distmethod = 'L1dist';
    elseif strcmp(simmethod,'negativeL2dist')
        distmethod = 'L2dist';
    elseif strcmp(simmethod,'product')
        distmethod = 'none';
    else
        error('No distance method for given similarity method found.');
    end
end

function obj = saveobj(obj)
    % do nothing for now
end

function obj = loadobj(obj)
    if isstruct(obj)
        error('Could not load the object, need to implement this conversion.');
    else
        if not(isfield(obj.settings,'radlin')) % legacy
            obj.settings.radlin = 0; % default value
        end
    end
end

end % methods(Static)

end % classdef
