% pointdensity in number of points per image area
function pos = regionboundarysamples(imgregions,imgmin,imgmax,pointdensity)

    if nargin < 4 || isempty(pointdensity)
        pointdensity = 10000;
    end
    
    ndiag = sqrt(2*pointdensity); % approx. number of points on diagonal of image
    pointspacing = sqrt(sum((imgmax-imgmin).^2,1)) ./ (ndiag-1);
    
    subjpolys = [imgregions.polygon];
    
    % convert from normalized polygon coordinates in [0,1] to
    % coordinates in [imgmin,imgmax]
    for i=1:numel(subjpolys)
        subjpolys{i} = bsxfun(@plus,...
            bsxfun(@times,subjpolys{i},imgmax-imgmin),...
            imgmin);
    end
    
    pos = zeros(2,0);
    for i=1:numel(subjpolys)
        minedgelen = sqrt(sum((imgmax-imgmin).^2,1))*0.000001;
        poly = polylineClean(subjpolys(i),true,minedgelen);
        pos = [pos,polylineResample(poly{1},pointspacing,'spacing',true)]; %#ok<AGROW>
    end
end
