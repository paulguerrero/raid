% pointdensity in number of points per image area
function [pos,nx,ny,posmask,possize] = regiongridsamples(imgregions,imgmin,imgmax,pointdensity)

    if nargin < 4 || isempty(pointdensity)
        pointdensity = 10000;
    end
    
    subjpolys = [imgregions.polygon];

    % get a regular grid inside the image regions that contain the 'subject'
    imgsize = imgmax-imgmin;
    aspectratio = imgsize(1)/imgsize(2); % width/height
    
    ny = max(1,round(sqrt(pointdensity/aspectratio)));
    nx = max(1,round(ny*aspectratio));
    [posgridy,posgridx] = ndgrid(...
        linspace(imgmin(2),imgmax(2),ny),...
        linspace(imgmin(1),imgmax(1),nx));
    posgridx = posgridx(:)';
    posgridy = posgridy(:)';
    
    % approximate size of voronoi cell of position in the grid
    possize = zeros(2,1);
    if nx == 1
        possize(1) = imgmax(1)-imgmin(1);
    else
        possize(1) = (imgmax(1)-imgmin(1)) / (nx-1);
    end
    if ny == 1
        possize(2) = imgmax(2)-imgmin(2);
    else
        possize(2) = (imgmax(2)-imgmin(2)) / (ny-1);
    end
    
    % convert from normalized polygon coordinates in [0,1] to
    % coordinates in [imgmin,imgmax]
    subjpolys = ImageAnnotation.an2imcoords(subjpolys,imgmin,imgmax);
%     for i=1:numel(subjpolys)
%         subjpolys{i} = bsxfun(@plus,...
%             bsxfun(@times,subjpolys{i},imgmax-imgmin),...
%             imgmin);
%     end
    
    % get grid points inside the subject polygons
    posmask = false(size(posgridx));
    for i=1:numel(subjpolys)
        posmask = posmask | pointInPolygon_mex(...
            posgridx,posgridy,...
            subjpolys{i}(1,:),subjpolys{i}(2,:),...
            'winding'); % crossing
    end
    
    % get grid points inside the subject image regions
    pos = [posgridx(posmask);posgridy(posmask)];
end
