classdef RelationshipQuery < handle
    
properties(SetAccess=protected)
    % Query Template
    template = ReldisthistDescriptorSet.empty;
    
    % Query Result
    rels = DatabaseRelationshipSet.empty;
    
    imglikelihoodrange = zeros(2,0);
    
    likelihoods = zeros(1,0);
    
    settings = struct;
end

methods(Static)
function s = defaultSettings
    s = struct;
    
    s.simmethod = 'negativeL1dist';
    s.labeled = 'any'; % should the query only contain labeled, only unlabeled or both kinds of relationships
    s.ignoreownlabel = false;
    s.onerelpersource = false; % only keep the best relationship for each source region
    s.saliency = 'area';
    s.savedescriptors = true;
    
    s.nbest = 100;
end
end

methods

% obj = RelationshipQuery()
% obj = RelationshipQuery(obj2)
% obj = RelationshipQuery(template)
% obj = RelationshipQuery(template,nvpairs)
function obj = RelationshipQuery(varargin)
    
    obj.settings = RelationshipQuery.defaultSettings;
    
    obj.rels = DatabaseRelationshipSet;
    obj.rels.lockimgfilenames = true;
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'RelationshipQuery')
        obj.copyFrom(varargin{1});
    elseif numel(varargin) >= 1
        obj.template = varargin{1}; % this class takes ownership!!
        
        obj.settings = nvpairs2struct(varargin(2:end),obj.settings);
    else
        error('Invalid input arguments.');
    end
end

function delete(obj)
    delete(obj.template(isvalid(obj.template)));
    delete(obj.rels(isvalid(obj.rels)));
end

function copyFrom(obj,obj2)
    delete(obj.template(isvalid(obj.template)));
    obj.template = obj2.template.clone;
    
    delete(obj.rels(isvalid(obj.rels)));
    obj.rels = obj2.rels.clone;
    
    obj.imglikelihoodrange = obj2.imglikelihoodrange;
    
    obj.likelihoods = obj2.likelihoods;
    
    obj.settings = obj2.settings;
end

function setTemplate(obj,template)
    obj.clear; % otherwise results will not correspond to the template
    
    obj.template = template;
end

function setParams(obj,varargin)
    obj.clear;
    
    if numel(varargin) == 1 && isstruct(varargin{1})
        varargin = struct2nvpairs(varargin{1});
    end
    
    obj.settings = nvpairs2struct(varargin,obj.settings);
end

function obj2 = clone(obj)
    obj2 = RelationshipQuery(obj);
end

function obj2 = cloneSubset(obj,imginds)
    obj2 = RelationshipQuery;
    obj2.template = obj.template.clone;
    
    [obj2.rels,subsetrelinds] = obj.rels.cloneImgsubset(imginds);

    obj2.likelihoods = obj.likelihoods(:,subsetrelinds);

    obj2.imglikelihoodrange = obj.imglikelihoodrange(:,imginds);
end

function keepSubset(obj,imginds)
    subsetrelinds = obj.rels.keepImgsubset(imginds);
    
    obj.likelihoods = obj.likelihoods(:,subsetrelinds);
    
    obj.imglikelihoodrange = obj.imglikelihoodrange(:,imginds);
end

% clear the result of the query
function clear(obj)
    delete(obj.rels(isvalid(obj.rels)));
    obj.rels = DatabaseRelationshipSet;
    obj.rels.lockimgfilenames = true;
    
    obj.imglikelihoodrange = zeros(2,0);
    
    obj.likelihoods = zeros(1,0);
end

% runs the query on a precomputed set of relationship descriptors
% also returns ranked source region ids, target region ids, image indices
% and similarities
% ownership of inputrels is passed to the query
function [srcids,tgtids,imginds,sim] = runQuick(obj,inputrels,sourcelbl,targetlbl)
    
    if not(isempty(inputrels)) && not(isempty(inputrels.descriptors)) && ...
            not(obj.template.iscompatible(inputrels.descriptors))
        error('Given descriptors are not compatible with the template.');
    end
    
    if isempty(obj.template)
        error('The query is missing a template.');
    end
    
    if not(all(inputrels.descind >= 1))
        error('Some given relationships are missing their descriptors.');
    end
    
    if numel(inputrels.descsrcarea) ~= size(inputrels.descriptors.val,2) || numel(inputrels.descsrcsaliency) ~= size(inputrels.descriptors.val,2)
        error('Source region area or saliency is missing from the given relationships.');
    end
    
    if obj.settings.onerelpersource
        error('One relationship per source region not yet implemented for the quick query.');
    end
    
    if not(strcmp(obj.settings.labeled,'any'))
        error('Filtering by labeled/not labeled not yet implemented for the quick query.');
    end

    obj.clear;

    obj.rels = inputrels.clone; % copy instead of clone for faster queries, but does not save the query information

    % get subset of relationships with correct source and target label
    relmask = true(1,numel(obj.rels.id));
    if nargin >= 3 && not(isempty(sourcelbl))
        relind = cellind_mex(inputrels.sourcelabels);
        relind = [relind{:}];
        relsrclbls = [obj.rels.sourcelabels{:}];
        if numel(sourcelbl) == 1
            relmask(relind(relsrclbls ~= sourcelbl)) = false;
        else
            relmask(not(ismember(relsrclbls,sourcelbl))) = false;
        end
    end
    if nargin >= 4 && not(isempty(targetlbl))
        relind = cellind_mex(inputrels.targetlabels);
        relind = [relind{:}];
        reltgtlbls = [obj.rels.targetlabels{:}];
        if numel(targetlbl) == 1
            relmask(relind(reltgtlbls ~= targetlbl)) = false;
        else
            relmask(not(ismember(reltgtlbls,targetlbl))) = false;
        end
    end
    if obj.settings.ignoreownlabel
        relinds = find(relmask);
        
        % todo: special treatment for rels that have only one source and
        % target as well as rels that have only one source or only one
        % target
        
        for i=1:numel(relinds)
            if ismember(obj.rels.sourcelabels{relinds(i)},obj.rels.targetlabels{relinds(i)})
                relmask(relinds(i)) = false;
            end
        end
    end
    if not(all(relmask))
        obj.rels.keepSubset(find(relmask)); %#ok<FNDSB>
    end
    
    % compare descriptor values
%     descdists = sum(abs(bsxfun(@minus,obj.template.val,obj.rels.descriptors.val)),1);
%     [~,descranking] = sort(descdists,'ascend');

%     descsim = -sum(abs(bsxfun(@minus,obj.template.val,obj.rels.descriptors.val)),1);
    descsim = obj.template.similarity(obj.template.val,obj.rels.descriptors.val,obj.settings.simmethod);
    
    % factor in image area saliency
    if strcmp(obj.settings.saliency,'none')
        % do nothing
    elseif strcmp(obj.settings.saliency,'area')
        if strcmp(obj.settings.simmethod,'negativeL1dist') % || strcmp(obj.settings.simmethod,'negativeEMD') % nhere
            % similarity from [-2,0] to [0,2]
            descsim = descsim + 2;

            % weigh amount of similarity above 1 by area weight
            mask = descsim > 1;
            areaweight = min(1,obj.rels.descsrcarea(mask)./0.02);
            descsim(mask) = 1 + (descsim(mask)-1) .* areaweight;
        else
            warning('Not implemented for similarity method yet, not using saliency');
        end
    elseif strcmp(obj.settings.saliency,'saliency')
        error('This type of saliency is not impleneted yet for the quick run.');
    else
        error('Unknown saliency measure.');    
    end
    
    % rank relationships by descriptor distance
    relsim = descsim(obj.rels.descind);
    
    [~,relranking] = sort(relsim,'descend');
    srcids = obj.rels.sourceids(relranking);
    tgtids = obj.rels.targetids(relranking);
    imginds = obj.rels.imageind(relranking);
    sim = relsim(relranking);
    
    obj.likelihoods = relsim;
    obj.imglikelihoodrange = nan(2,numel(obj.rels.imgfilenames));
    for i=1:numel(obj.rels.imgfilenames)
        if not(isempty(obj.rels.imgrelinds{i}))
            obj.imglikelihoodrange(:,i) = [...
                min(obj.likelihoods(obj.rels.imgrelinds{i}));...
                max(obj.likelihoods(obj.rels.imgrelinds{i}))];
        end
    end
    
    if not(obj.settings.savedescriptors)
        obj.rels.removeDescriptors;
    end
end

% ranking: indices of images sorted by descending likelihood
function [ranking,maxlhs] = getRanking(obj)
    % rank images by region score if available, otherwise by point score
    if any(not(isnan(obj.imglikelihoodrange(2,:))))
        maxlhs = obj.imglikelihoodrange(2,:);
    else
        maxlhs = obj.imglikelihoodrange(2,:);
    end
    
    [maxlhs,ranking] = obj.sortLikelihoods(maxlhs);
end

end

methods(Static)
    
function [lhs,perm] = sortLikelihoods(lhs)
    nanmask = isnan(lhs);
    notnanind = find(not(nanmask));
    [~,perm] = sort(lhs(notnanind),'descend');
    perm = [notnanind(perm),find(nanmask)];
    lhs = lhs(perm);
end

function obj = saveobj(obj)
    % do nothing for now
end

function obj = loadobj(obj)

    if not(isfield(obj.settings,'nbest')) % legacy
        obj.settings.nbest = 100; % default value
    end
    if not(isfield(obj.settings,'labeled')) % legacy
        obj.settings.labeled = 'any'; % default value
    end
    if not(isfield(obj.settings,'onerelpersource')) % legacy
        obj.settings.onerelpersource = true; % default value
    end
    if not(isfield(obj.settings,'saliency')) % legacy
        obj.settings.saliency = 'none'; % default value
    end
    if not(isfield(obj.settings,'savedescriptors')) % legacy
        obj.settings.savedescriptors = true; % default value
    end
    if isfield(obj.settings,'pointdensity') % legacy
        obj.settings = rmfield(obj.settings,'pointdensity');
    end
    
    if isstruct(obj)
        
        disp('converting query...');
        
        if isfield(obj,'regionlikelihoods') % legacy
            
            annopath = inputdlg('Annotation path:');
            if isempty(annopath)
                obj = RelationshipQuery.empty;
                return;
            end
            annopath = cleanfilename(annopath{1});
            
            params = struct2nvpairs(obj.settings);

            qobj = RelationshipQuery(obj.template.clone,params{:});

            % copy output
            imgind = cellind_mex(obj.regionlikelihoods);
            imgind = [imgind{:}];
            if not(isempty(imgind))
            
                imgind = imgind(1,:);

                rlhs = [obj.regionlikelihoods{:}];

                qobj.imglikelihoodrange = obj.regionlikelihoodrange;
                qobj.likelihoods = rlhs(3,:);

                qobj.rels = DatabaseRelationshipSet;
                qobj.rels.imgfilenames = obj.imgfilenames;
                qobj.rels.imageind = imgind;
                qobj.rels.imgrelinds = array2cell(1:numel(qobj.rels.imageind),qobj.rels.imageind,numel(qobj.rels.imgfilenames));

                % get target ids, source labels, target labels, labels and label weights
                qobj.rels.descriptors = qobj.template.clone;
                qobj.rels.descriptors.clear;
                qobj.rels.descsrcarea = nan(1,0);
                qobj.rels.descsrcsaliency = nan(1,0);
                for i=1:numel(obj.regionlikelihoods)
                    disp(['converting query for image ',num2str(i),' / ',num2str(numel(obj.regionlikelihoods))]);

                    if not(isempty(obj.regionlikelihoods{i}))

                        nrels = size(obj.regionlikelihoods{i},2);
                        tgtlbls = obj.regionlikelihoods{i}(2,:);

                        annofilename = ImageAnnotation.img2annofilename(obj.imgfilenames{i});
                        annotation = AnnotationImporter.importAnnotation([annopath,'/',annofilename]);
                        imgobjectids = [annotation.imgobjects.id];
                        imgobjectlbls = [annotation.imgobjects.label];
                        
                        for j=1:nrels
                            qobj.rels.sourceids{end+1} = obj.regionlikelihoods{i}(1,j);
                            qobj.rels.targetids{end+1} = [annotation.imgobjects(imgobjectlbls == tgtlbls(j)).id];
                            srcobjind = find(imgobjectids == qobj.rels.sourceids{end},1,'first');
                            tgtobjind = find(imgobjectlbls == tgtlbls(j));
                            if isempty(srcobjind)
                                error('Source object could not be found in the annotation.');
                            end
                            qobj.rels.sourcelabels{end+1} = [annotation.imgobjects(srcobjind).label];
                            qobj.rels.targetlabels{end+1} = [annotation.imgobjects(tgtobjind).label];
                            relind = annotation.relationships.findRelationships(...
                                {annotation.imgobjects(srcobjind)},...
                                {annotation.imgobjects(tgtobjind)},...
                                'exact');
                            if relind >= 1
                                qobj.rels.id(end+1) = annotation.relationships.id(relind);
                                qobj.rels.labels{end+1} = annotation.relationships.labels{relind};
                                qobj.rels.labelweights{end+1} = annotation.relationships.labelweights{relind};
                            else
                                qobj.rels.id(end+1) = nan;
                                qobj.rels.labels{end+1} = zeros(1,0);
                                qobj.rels.labelweights{end+1} = zeros(1,0);
                            end
                            
                            qobj.rels.descsrcarea(:,end+1) = sum([annotation.imgobjects(srcobjind).area]);
                            qobj.rels.descsrcsaliency(:,end+1) = max([annotation.imgobjects(srcobjind).saliency]);
                        end

                        delete(annotation(isvalid(annotation)));

                        if size(obj.descriptors{i}.val,2) ~= nrels
                            error('Descriptor count and relationship count do not match for an image.');
                        end
                        
                        qobj.rels.descriptors.append(obj.descriptors{i});
                        qobj.rels.descind(:,end+1:end+nrels) = ...
                            size(qobj.rels.descriptors.val,2)-nrels+1:size(qobj.rels.descriptors.val,2);

                    end
                end
            
            end
            
            obj = qobj;
            
        elseif isfield(obj,'descriptorsim') % legacy
            
            params = struct2nvpairs(obj.settings);
            
            qobj = RelationshipQuery(obj.template.clone,params{:});

            qobj.rels = obj.rels.clone;
            qobj.imglikelihoodrange = obj.imglikelihoodrange;
            qobj.likelihoods = obj.likelihoods;
            
            obj = qobj;
        else
            error('Don''t know how to load the object.');
        end
        
        disp('done');

    else
        % do nothing, obj should be a valid RelationshipQuery
    end
end

end
   
end
