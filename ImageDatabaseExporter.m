classdef ImageDatabaseExporter < handle

properties
    imgdbinfoexporter = ImageDatabaseInfoExporter.empty;
end

methods

function obj = ImageDatabaseExporter()
    if nargin == 0
        % do nothing
    else
        error('Invalid arguments.');
    end
end

function clear(obj)
    delete(obj.imgdbinfoexporter(isvalid(obj.imgdbinfoexporter)));
    obj.imgdbinfoexporter = ImageDatabaseInfoExporter.empty;
end
    
function export(obj,filename,dbinfo,dbinfofilenames,imgpath,annopath,wsfilename,tsfilename,descfname)
    if isempty(filename)
        error('No filename given.')
    end
    
    if not(ischar(filename))
        error('File name must be a string.');
    end
    
    obj.clear;
    
    obj.imgdbinfoexporter = ImageDatabaseInfoExporter(dbinfo);
    obj.imgdbinfoexporter.export(dbinfofilenames);
    
    obj.exportDatabase(filename,dbinfofilenames,imgpath,annopath,wsfilename,tsfilename,descfname);
end
    
end

methods(Static)

function exportImageDatabase(dbinfo,dbinfofilenames,imgpath,annopath,wsfilename,tsfilename,descfname,filename,varargin)
    exporter = ImageDatabaseExporter();
    exporter.export(filename,dbinfo,dbinfofilenames,imgpath,annopath,wsfilename,tsfilename,descfname,varargin{:});
    delete(exporter(isvalid(exporter)));
end

function exportDatabase(filename,dbinfofilenames,imgpath,annopath,wsfilename,tsfilename,descfname)
    
    if not(isstruct(dbinfofilenames)) || not(all(isfield(dbinfofilenames,{'dbinfo','categories','relcategories','categorycolors'})))
        error('Some database info filenames are missing.');
    end
    
    str = [...
        sprintf('dbinfo : %s\n',dbinfofilenames.dbinfo),...
        sprintf('categories : %s\n',dbinfofilenames.categories),...
        sprintf('relcategories : %s\n',dbinfofilenames.relcategories),...
        sprintf('categorycolors : %s\n',dbinfofilenames.categorycolors),...
        sprintf('imagepath : %s\n',imgpath),...
        sprintf('annotationpath : %s\n',annopath),...
    ];
    if not(isempty(wsfilename))
        str = [str,sprintf('workingset : %s\n',wsfilename)];
    end
    if not(isempty(tsfilename))
        str = [str,sprintf('trainingset : %s\n',tsfilename)];
    end
    if not(isempty(descfname))
        str = [str,sprintf('descriptors : %s\n',descfname)];
    end

    fid = fopen(filename,'W');
    fwrite(fid,str);
    fclose(fid);
end

end

end
