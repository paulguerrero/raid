
classdef ImageAnnotation < handle

properties(SetAccess=protected)
    imgobjects = ImageObject.empty(1,0); % the list of image objects
    
    relationships = ImageObjectRelationshipSet.empty; % optional: a list of labeled image object relatinoships
end

methods

% obj = ImageAnnotation()
% obj = ImageAnnotation(obj2)
% obj = ImageAnnotation(imgobjects)
function obj = ImageAnnotation(varargin)
    obj.relationships = ImageObjectRelationshipSet;
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'ImageAnnotation')
        obj.copyFrom(varargin{1});
    elseif numel(varargin) == 1
        obj.setImgobjects(varargin{1});
    else
        error('Invalid number of arguments;');
    end
end

function delete(obj)
    delete(obj.imgobjects(isvalid(obj.imgobjects)));
    delete(obj.relationships(isvalid(obj.relationships)));
end

function copyFrom(obj,obj2)
    delete(obj.imgobjects(isvalid(obj.imgobjects)));
    obj.imgobjects = ImageObject.empty(1,0);
    for i=1:numel(obj2.imgobjects)
        obj.imgobjects(:,end+1) = obj2.imgobjects(i).clone;
    end
    
    delete(obj.relationships(isvalid(obj.relationships)));
    obj.relationships = obj2.relationships.clone;
end

function obj2 = clone(obj)
    obj2 = ImageAnnotation(obj);
end

function setImgobjects(obj,imgobjects)
    obj.removeImageobjects(1:numel(obj.imgobjects));
    obj.addImageobjects(imgobjects);
end

function addImageobjects(obj,imgobjects)
    imgobjids = [imgobjects.id];
    if any(diff(sort([obj.imgobjects.id,imgobjids],'ascend')) == 0)
        error('New set of image objects would have duplicate ids.');
    end
    if any(imgobjids <= 0)
        error('Some image objects do not have IDs or have invalid IDs.');
    end

    obj.imgobjects(:,end+1:end+numel(imgobjects)) = imgobjects;
end

function removeImageobjects(obj,ind)
    
    delobjects = obj.imgobjects(ind);
    
    % remove relationships to or from any of the image objects
    removerelinds = obj.relationships.findRelationships(...
        delobjects,delobjects,'contain_any');
    obj.relationships.removeRelationships(removerelinds);
    
    % remove image objects
    delete(delobjects(isvalid(delobjects)));
    obj.imgobjects(ind) = [];
end

end

methods(Static)
    
% coordinates in [(0,0),(1,1)] to coordinates in [imgmin,imgmax]
function p = an2imcoords(p,imgmin,imgmax)
    if iscell(p)
        for i=1:numel(p)
            p{i} = ImageAnnotation.an2imcoords(p{i},imgmin,imgmax);
        end
    elseif not(isempty(p))
        p = bsxfun(@plus,bsxfun(@times,p,imgmax-imgmin),imgmin);
    end
end

% coordinates in [imgmin,imgmax] to coordinates in [(0,0),(1,1)]
function p = im2ancoords(p,imgmin,imgmax)
    if iscell(p)
        for i=1:numel(p)
            p{i} = ImageAnnotation.im2ancoords(p{i},imgmin,imgmax);
        end
    elseif not(isempty(p))
        p = bsxfun(@rdivide,bsxfun(@minus,p,imgmin),imgmax-imgmin);
    end
end

function r = an2imradius(r,imgmin,imgmax)
    if iscell(r)
        for i=1:numel(r)
            r{i} = ImageAnnotation.an2imradius(r{i},imgmin,imgmax);
        end
    elseif not(isempty(r))
        r = r.*max(imgmax-imgmin);
    end
end

function r = im2anradius(r,imgmin,imgmax)
    if iscell(r)
        for i=1:numel(r)
            r{i} = ImageAnnotation.im2anradius(r{i},imgmin,imgmax);
        end
    elseif not(isempty(r))
        r = r./max(imgmax-imgmin);
    end
end

function a = an2imarea(a,imgmin,imgmax)
    if iscell(a)
        for i=1:numel(a)
            a{i} = ImageAnnotation.an2imarea(a{i},imgmin,imgmax);
        end
    elseif not(isempty(a))
        a = a.*((imgmax(1)-imgmin(1))*(imgmax(2)-imgmin(2)));
    end
end

function a = im2anarea(a,imgmin,imgmax)
    if iscell(a)
        for i=1:numel(a)
            a{i} = ImageAnnotation.im2anarea(a{i},imgmin,imgmax);
        end
    elseif not(isempty(a))
        a = a./((imgmax(1)-imgmin(1))*(imgmax(2)-imgmin(2)));
    end
end

function [imgmin,imgmax] = imres2imsize(imres)
    if imres(1) > imres(2)
        % width is larger
        xsize = 2;
        ysize = 2 .* (imres(2) / imres(1));
    else
        % height is larger
        xsize = 2 .* (imres(1) / imres(2));
        ysize = 2;
    end
    imgmin = -[xsize;ysize]./2;
    imgmax = [xsize;ysize]./2;
end
   
function annofilename = img2annofilename(imgfilename)
    [fpath,fname,~] = fileparts(fullfile(imgfilename)); % fullfile to remove double '/' or '\'
    fpath = strsplit(fpath, {'/','\\'});
    fpath(cellfun(@(x) isempty(x),fpath)) = [];
    
%     % just for compatibility (anno filename should be the same with xml
%     % ending, but stored queries still use old imgfilenames that
%     % contain /images/ somwhere in the middle of the name
%     imgdirind = strcmp(fpath,'images');
%     if isempty(imgdirind)
%         error('No ''images'' directory in image path.');
%     end
%     fpath{imgdirind} = 'annotations';
    
    annofilename = cleanfilename(fpath{:},[fname,'.xml']);
end

% does not work, would need to know file ending of image
% function imgfilename = anno2imgfilename(annofilename)
%     [fpath,fname,~] = fileparts(fullfile(annofilename)); % fullfile to remove double '/' or '\'
%     fpath = strsplit(fpath, {'/','\\'});
%     fpath(cellfun(@(x) isempty(x),fpath)) = [];
%     annodirind = strcmp(fpath,'annotations');
%     if isempty(annodirind)
%         error('No ''annotations'' directory in annotation path.');
%     end
%     fpath{annodirind} = 'images';
%     imgfilename = cleanfilename(fpath{:},[fname,'.xml']);
% end

end

end
