function closeAllAdditonalFigures
    fighandles = findobj('Type','figure');
    fignames = get(fighandles,'Name');
    mask = strcmp(fignames,'Descriptor Editor');
    close(fighandles(not(mask)));
end
